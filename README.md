## Connectomic comparison of mouse and human cortex

Authors: Sahil Loomba†, Jakob Straehle†, Vijayan Ganghadaran†, Natalie Heike†, Abdelrahman Khalifa†, Alessandro Motta†, Niansheng Ju, Meike Sievers, Jens Gempt, Hanno S. Meyer, Moritz Helmstaedter

†These authors contributed equally to this work.

## Cite this article as:

S. Loomba *et al.*, *Science* 377, eabo0924 (2022). DOI: 10.1126/science.abo0924


## Getting Started
The code in this repository was primarily developed for the R2021a release of [MATLAB](https://www.mathworks.com/products/matlab.html).

If you have git installed, you can clone this repository by entering
```
$ git clone https://gitlab.mpcdf.mpg.de/connectomics/human_primate.git
```
into your terminal.

Alternatively, you can download the code as a compressed file via the gitlab frontend.

Here's a list of the packages and folders with a short description of the contents:

* +Figures/+FigXX: Contains scripts to generate figures used in Fig. XX

* auxiliaryMethods: Other utilities used for the analysis.

## Authors

This package was developed by
* **Sahil Loomba** & **Alessandro Motta**

with contribution from
* **Natalie Heike**

under scientific supervision of
* **Moritz Helmstaedter**

Skeleton parser for WebKnossos (.nml) neurite skeleton files makes use of an efficient .nml parser developed by 
* **Alessandro Motta**

The Matlab class used to represent single neurite skeletons was developed by
* **Benedikt Staffler**
* **Alessandro Motta**
* **Manuel Berning**
* **Florian Drawitsch**
* **Ali Karimi**
* **Kevin Boergens**
* **Sahil Loomba**
* **Martin Schmidt**
* **Marcel Beining**

Volumetric data used to generate surfaces uses the [Webknossos-wrapper](https://github.com/scalableminds/webknossos-wrap) file format. You can visit their website for a complete list of authors.

## Acknowledgements
We thank
* **Arseny Kapoulkine** for pugixml, a XML parser library for C++  
  https://pugixml.org/
* **Oliver J. Woodford, Yair M. Altman** for export_fig MATLAB package.

## License
This project is licensed under the [MIT license](LICENSE).  
Copyright (c) 2022 Department of Connectomics, Max Planck Institute for
Brain Research, D-60438 Frankfurt am Main, Germany

![summary_figure](summary_figure.jpeg)

