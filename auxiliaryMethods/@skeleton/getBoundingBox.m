function box = getBoundingBox(skel, whichBox)
    % box = getBoundingBox(skel, whichBox)
    %   Returns the bounding box of the tracing in MATLAB format. Depending
    %   on `whichBox` option, either the user bounding box (set by the user
    %   in the tracing view) or the task bounding box (set by the admin
    %   during task creation) is returned.
    %
    %   If the desired bounding box was not set, the returned box only
    %   contains NaNs as coordinate values.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('whichBox', 'var') || isempty(whichBox)
        whichBox = 'user';
    end
    
    assert(ischar(whichBox));
    whichBox = lower(whichBox);
    assert(ismember(whichBox, {'user', 'task'}));
    whichBox = strcat(whichBox, 'BoundingBox');
    
    try
        box = skel.parameters.(whichBox);
        
        box = { ...
            box.topLeftX, box.topLeftY, box.topLeftZ, ...
            box.width, box.height, box.depth};
        
        box = cellfun(@str2double, box);
        box = Util.convertWebknossosToMatlabBbox(box);
    catch
        % NOTE(amotta): There's all sort of stuff that might go wrong with
        % this crazy text-based and optional representation of the bounding
        % box. So, let's be blunt here and just fall through to a NaN box
        % in case of errors...
        box = nan(3, 2);
    end
end
