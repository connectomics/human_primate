function writeHybrid(skel, outFile)
    % writeHybrid(skel, outFile)
    %   Writes the skeleton and linked volume tracings as hybrid tracing.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % Write NML file
   [~, curSkelName] = fileparts(outFile);
   [curTempDir, curTempDirCleanup] = Util.makeTempDir(); %#ok
    curSkelPath = fullfile(curTempDir, sprintf('%s.nml', curSkelName));
    skel.write(curSkelPath);
    
    % Build ZIP file with skeleton and volume tracings
    filesToZip = fullfile(skel.volumeDir, skel.volumes.location);
    filesToZip{end + 1} = curSkelPath;
    
    zip(outFile, filesToZip);
end
