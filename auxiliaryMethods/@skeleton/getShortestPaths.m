function allPaths = getShortestPaths(skel, treeIdx, sourceNode)
% allPaths = skel.getShortestPaths(treeIdx)
%   Computes the length of the shortest path between each pair
%   of nodes in the tree specified by treeIdx.
%
% treeIdx
%   Scalar. Linear index of tree.
% 
% sourceNode (optional)
%   Only return shortest paths from sourceNode if specified - otherwise all
%   shortest paths. (Dispatches to graphshortestpath matlab function, while
%   otherwise graphallshortestpaths is used!)
%
% allPaths
%   if sourceNode is not specified:
%     Sparse and symmetric NxN matrix. Entry allPaths(i, j) is
%     the length of the shortest path between node i and j in
%     nano-metres.
%   if sourceNode is specified:
%     Nx1 matrix with allPaths(i) containing lengths of shortest path 
%     between sourceNode and node i in nano-metres.
%
% NOTE
%   This method uses the Skeleton object's scale property to
%   convert the node distance into physical units.
%
% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

distMat = skel.createDistanceMatrix(treeIdx);
if exist('sourceNode', 'var') && ~isempty(sourceNode)
    assert(sourceNode < size(distMat, 1));
    assert(sourceNode > 0);
    allPaths = graphshortestpath(distMat, sourceNode, 'Directed', false)';
else
    allPaths = graphallshortestpaths(distMat, 'Directed', false);
end
end