function obj = setComments( obj, treeIdx, nodeIdx, comments, append, appendChar)
%SETCOMMENTS Set comments for the specified nodes.
%
% INPUT treeIdx: int or [Nx1]
%           Index/Indices of the target tree(s).
%       nodeIdx: [Nx1] int or logical
%           Linear or logical indices of nodes.
%       comments: string or [Nx1] cell
%           Comment or list of comments for the corresponding nodes.
%           If there are several nodes indices and comments is a string or
%           a cell of length one then this commen will be used for all
%           nodes.
%       append: bool (optional; Default: false)
%           Whether to append the comment or replace it.
%           Defaults to append = False => replace.
%       appendChar: char (optional; Default: ' ')
%           Add this character if append is set to true.
%           
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%         Martin Schmidt <martin.schmidt@brain.mpg.de> (added append option)

if ischar(comments)
    comments = {comments};
end

if islogical(nodeIdx)
    nodeIdx = find(nodeIdx);
end

if length(nodeIdx) > 1 && length(comments) == 1
    comments = repmat(comments, length(nodeIdx), 1);
elseif length(nodeIdx) ~= length(comments)
    error('Specify a comment for each node.');
end

if length(nodeIdx) > 1 && length(treeIdx) == 1
    treeIdx = repmat(treeIdx, length(nodeIdx), 1);
elseif length(nodeIdx) > 0 && length(nodeIdx) ~= length(treeIdx)
    error('Specify a treeIdx for each node.');
end

if ~exist('append', 'var') || isempty(append)
    append = false;
end

if ~exist('appendChar', 'var')
    appendChar = ' ';
end


for i = 1:length(nodeIdx)
    curComment = comments{i};
    if append
        curPrevComment = obj.nodesAsStruct{treeIdx(i)}(nodeIdx(i)).comment;
        if ~isempty(curPrevComment)
            curComment = strjoin({curPrevComment, curComment}, appendChar);
        end
    end
    obj.nodesAsStruct{treeIdx(i)}(nodeIdx(i)).comment = curComment;
end

end

