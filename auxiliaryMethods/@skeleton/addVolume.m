function skel = addVolume(skel, volDir, volId)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('volId', 'var') || isempty(volId)
        % Let's just add a new volume to the tracing
        volId = height(skel.volumes) + 1;
    end
    
    % Find magnifications
    magNames = findMagDirs(volDir);
    assert(not(isempty(magNames)));
    
    locName = sprintf('data%d.zip', volId);
    otherVolIds = setdiff(1:height(skel.volumes), volId);
    
    % Sanity checks
    assert(1 <= volId && volId <= height(skel.volumes) + 1);
    assert(not(ismember(volId, skel.volumes.id(otherVolIds))));
    assert(not(ismember(locName, skel.volumes.location(otherVolIds))));
    
    for magIdx = 1:numel(magNames)
        magDir = fullfile(volDir, magNames{magIdx});
        
        header = wkwLoadHeader(magDir);
        assert(isequal(header.version, 1));
        assert(isequal(header.voxelsPerBlockDim, 32));
        assert(isequal(header.blocksPerFileDim, 1));
        assert(isequal(header.blockType, 'RAW'));
        assert(isequal(header.voxelType, 'uint32'));
        assert(isequal(header.numChannels, 1));
    end
    
    % Create directories
   [tmpDir, tmpCleanup] = Util.makeTempDir(); %#ok
    
    if isempty(skel.volumeDir)
        skel.volumeDir = Util.makeTempDir();
    end
    
    tmpZipPath = fullfile(tmpDir, locName);
    zipPath = fullfile(skel.volumeDir, locName);
    
    % Create ZIP file and move it to volume directory
    zip(tmpZipPath, fullfile(volDir, magNames), volDir);
    assert(movefile(tmpZipPath, zipPath));
    
    % Update volume table
    skel.volumes(volId, :) = {volId, locName};
end

function magNames = findMagDirs(volDir)
    magNames = dir(fullfile(volDir));
    magNames = {magNames([magNames.isdir]).name};
    
    magMask = regexp(magNames, '^\d+(?:-\d+-\d+)?$');
    magMask = not(cellfun(@isempty, magMask));
    magNames = magNames(magMask);
end
