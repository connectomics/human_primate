function outSkel = splitAtGrid(skel, boxSize, varargin)
    % outSkel = splitAtGrid(skel, boxSize, varargin)
    %   Takes a skeleton `skel` and splits at the regular grid with box
    %   size `boxSize`. Optionally, a grid offset may be specified.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.begin = [1, 1, 1];
    opts.connectOut = true;
    opts = Util.modifyStruct(opts, varargin{:});
    
    % Sanity checks
    assert(isequal(size(opts.begin), [1, 3]));
    assert(islogical(opts.connectOut));
    
    boxSize = reshape(boxSize, 1, []);
    assert(all(boxSize > 0));
    
    if isempty(opts.connectOut) || opts.connectOut
        % By default or if true, take all nodes within box plus the first
        % node outside the current box. That is, connect outside the box.
        edgeMaskFun = @(m) any(m, 2);
    else
        % Take all nodes within box and make sure that no edge leaves box.
        edgeMaskFun = @(m) all(m, 2);
    end
    
    % Prepare output tracing
    treeCount = skel.numTrees();
    outSkel = skel.deleteTrees(1:treeCount);
    
    for curTreeIdx = 1:treeCount
        curNodes = skel.nodes{curTreeIdx};
        curEdges = skel.edges{curTreeIdx};
        curColor = skel.colors{curTreeIdx};
        curComments = skel.nodesAsStruct{curTreeIdx};
        curComments = reshape({curComments.comment}, [], 1);
        
        % Assign nodes to boxes
        curBoxIds = (curNodes(:, 1:3) - opts.begin) ./ boxSize;
        curBoxIds = 1 + floor(curBoxIds);
        
       [curBoxIds, ~, curRelBoxIds] = unique(curBoxIds, 'rows');
        curEdgeBoxes = curRelBoxIds(curEdges);
        
       [outSkel, curGroupId] = outSkel.addGroup(skel.names{curTreeIdx});
        
        for curBoxIdx = 1:size(curBoxIds, 1)
            % Find all nodes within current box
            curEdgeMask = curEdgeBoxes == curBoxIdx;
            curEdgeMask = edgeMaskFun(curEdgeMask);
                
            curAllNodeIds = unique(curEdges(curEdgeMask, :));
            
            % Split into connected components
            curCompIds = curEdges(curEdgeMask, :);
           [~, curCompIds] = ismember(curCompIds, curAllNodeIds);
            curCompIds = graph(curCompIds(:, 1), curCompIds(:, 2));
            curCompIds = curCompIds.conncomp();
            
            % Write out each component as separate tree
            for curCompIdx = 1:max(curCompIds(:))
                curNodeIds = curCompIds == curCompIdx;
                curNodeIds = curAllNodeIds(curNodeIds);
                
                curOutEdges = curEdges(curEdgeMask, :);
               [~, curOutEdges] = ismember(curOutEdges, curNodeIds);
                curOutEdges = curOutEdges(all(curOutEdges, 2), :);
                
                curOutNodes = curNodes(curNodeIds, :);
                curOutComments = curComments(curNodeIds);
                curOutColor = curColor;

                curOutName = sprintf( ...
                    'Box %d, %d, %d. Component %d', ...
                    curBoxIds(curBoxIdx, :), curCompIdx);
                outSkel = outSkel.addTree( ...
                    curOutName, curOutNodes, curOutEdges, ...
                    curOutColor, [], curOutComments, curGroupId);
            end
        end
    end
