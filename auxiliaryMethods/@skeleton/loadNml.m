function nml = loadNml(nmlPath)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>    
    try
        nml = slurpNml(nmlPath);
    catch slurpEx
        ex = MException( ...
            'skeleton:loadNml:readError', ...
            'Could not read "%s"', nmlPath);
        ex = addCause(ex, slurpEx);
        throw(ex);
    end
end
