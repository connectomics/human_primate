function skel = setParams(skel, expName, scale, offset)
    % Sets the dataset-specific parameters.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    assert(ischar(expName));
    assert(isnumeric(scale) && numel(scale) == 3);
    assert(isnumeric(offset));
    
    if numel(unique(offset)) ~= 1
        warning(['Using different offsets in x, y, and z direction' ...
            ' is deprecated. (And is not supported by writeNml!)' ...
            newline 'Using offset(1) for skel.nodeOffset.']);
    end
    
    skel.parameters.experiment.name = expName;
    
    skel.scale = reshape(scale, 1, 3);
    skel.parameters.scale.x = num2str(scale(1));
    skel.parameters.scale.y = num2str(scale(2));
    skel.parameters.scale.z = num2str(scale(3));
    
    % This is the offset that is used for shifting nodes when
    % reading/writing nml files in Matlab.
    % In principle, this can be set to 0, which avoids confusion between
    % wK coordinates and Matlab skeleton class coordinates.
    skel.nodeOffset = offset(1);
    
    % This offset is written to the NML file (things > parameters > offset)
    % but is otherwise not used by any skeleton function.
    % Also not used by wK and set to default value of 0 when downloading 
    % an nml, cf.:
    % https://discuss.webknossos.org/t/nml-format-parameters-offset-x-y-z-used-at-all/1366
    % (Here, we only set it to not break NML read/write functions.)
    default_offset = 0;
    skel.parameters.offset.x = num2str(default_offset);
    skel.parameters.offset.y = num2str(default_offset);
    skel.parameters.offset.z = num2str(default_offset);

end
