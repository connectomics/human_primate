function outFile = writeZipped(skel, outFile)
    % writeHybrid(skel, outFile)
    %   Writes the skeleton as zip. 
    %   If outFile ends with .nml a .zip is appended.
    %
    % Written by
    %   Martin Schmidt <martin.schmidt@brain.mpg.de>
    
    % Write NML file
   [~, curSkelName, ext] = fileparts(outFile);
   [curTempDir, curTempDirCleanup] = Util.makeTempDir(); %#ok
    if ~endsWith(curSkelName, '.nml')
        curSkelName = sprintf('%s.nml', curSkelName);
    end
    curSkelPath = fullfile(curTempDir, curSkelName);
    skel.write(curSkelPath);
    
    % Build ZIP file with skeleton
    if strcmp(ext, '.nml')
        outFile = [outFile '.zip'];
        warning('Appending ".zip" to outFile path');
    end
    [~, ~, ext]  = fileparts(outFile);
    if ~strcmp(ext, '.zip')
        error('Please specify outFile path with .nml or .zip as extension');
    end

    zip(outFile, curSkelPath);

    curTempDirCleanup();
end
