function skel = deleteEmptyTrees( skel )
%DELETEEMPTYTREES Delete all empty trees of a skeleton object.
% Trees are considered empty if skel.nodes is empty for the corresponding
% tree.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
% Modified: Alessandro Motta <alessandro.motta@brain.mpg.de>

toDel = find(cellfun(@isempty, skel.nodes));
skel = skel.deleteTrees(toDel); %#ok
end

