function [skel] = downSample(skel, treeIndices, downsamplefactor, keepNodesWithComment)
% Downsample a Tree.
% INPUT treeIndex: Index (not id!) of tree in skel.(Default=all trees)
%       downsamplefactor: The factor by which degree 2 nodes would be
%       downsampled
%       keepNodesWithComment: (logical) whether to keep nodes that contain
%       a non-empty comment string
% OUTPUT skel: Downsampled skeleton
% Author: Ali Karimi <ali.karimi@brain.mpg.de>

if ~exist('treeIndices','var') || isempty(treeIndices)
    treeIndices = 1:skel.numTrees();
end

if ~exist('keepNodesWithComment','var') || isempty(keepNodesWithComment)
    keepNodesWithComment = false;
end
nodeDegrees = skel.calculateNodeDegree(treeIndices);
nodesSecondDegree = cellfun(@(d) d == 2, nodeDegrees, ...
    'UniformOutput', false);
% Get the indices of nodes that contain a comment
if keepNodesWithComment
    nodesTable = skel.nodes2Table(treeIndices);
    nodesWithoutComment = cellfun(@(T) cellfun(@isempty, T.comment), ...
        nodesTable,'UniformOutput', false);
    % AND operation between second degree nodes and the nodes withou comment
    nodes2downsample = cellfun(@(d, c) d & c, ...
        nodesSecondDegree, nodesWithoutComment, 'UniformOutput', false);
else
    nodes2downsample = nodesSecondDegree;
end
nodes2ds_idx = cellfun(@find, nodes2downsample, 'UniformOutput', false);

for i = 1:length(treeIndices)
   tr = treeIndices(i);
   cur_nodes2ds = nodes2ds_idx{i};
   % Create and index array based on the downsample factor
   idxDel = mod(1:length(cur_nodes2ds), downsamplefactor) > 0;
   % Get the node indices to delete from the array created above
   nodesToDelete = cur_nodes2ds(idxDel);
   % delete nodes
   skel = skel.deleteNodes(tr, nodesToDelete, true);
end

end

