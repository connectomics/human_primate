function swc = loadSwc(path)
    % Reads an SWC file from NeuroMorpho.org and returns it in a structure
    % that is compatible with the output of `slurpNml`.
    % 
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % See http://neuromorpho.org/myfaq.jsp
    types = { ...
        'Undefined', 'Soma', 'Axon', 'BasalDendrite', ...
        'ApicalDendrite', 'Custom', 'Unspecified', 'Glia'};
    
    % NOTE(amotta): x, y, y, and radius are assumed to be in µm
    varNames = {'id', 'type', 'x', 'y', 'z', 'radius', 'parentId'};
    
   [~, fileName] = fileparts(path);
    
    try
        opts = detectImportOptions( ...
            path, 'FileType', 'text', 'Delimiter', ' ', ...
            'CommentStyle', '#', 'NumVariables', numel(varNames));
        opts.VariableNames = varNames;
        T = readtable(path, opts);
    catch readEx
        ex = MException( ...
            'skeleton:loadSwc:readError', ...
            'Could not read "%s"', path);
        ex = addCause(ex, readEx);
        throw(ex);
    end
    
    swc = struct;
    swc.parameters = struct;
    swc.parameters.scale = struct;
    % NOTE(amotta): Parameters are strings
    swc.parameters.scale.x = '1'; % nm
    swc.parameters.scale.y = '1'; % nm
    swc.parameters.scale.z = '1'; % nm
    
    swc.things = struct;
    swc.things.id = 1;
    swc.things.groupId = nan;
    swc.things.name = {fileName};
    
    swc.things.nodes = {struct( ...
        'id', T.id, ...
        'x', round(1000 * T.x), ...
        'y', round(1000 * T.y), ...
        'z', round(1000 * T.z), ...
        'radius', round(1000 * T.radius), ...
        'inVp', nan(height(T), 1), ...
        'inMag', nan(height(T), 1), ...
        'time', nan(height(T), 1))};
    
    swc.things.edges = {struct( ...
        'source', T.parentId(T.parentId > 0), ...
        'target', T.id(T.parentId > 0))};
    
    swc.comments = struct;
    swc.comments.node = T.id;
    swc.comments.content = types(T.type + 1);
    
    swc.branchpoints = struct;
    swc.branchpoints.id = [];
    
    swc.groups = struct;
    swc.groups.name = cell(0, 1);
    swc.groups.id = nan(0, 1);
    swc.groups.children = cell(0, 1);
end
