function obj = mergeTreesSimple( obj, treeIdcs )
%MERGETREESSIMPLE Check specific (or by default all) trees, whether any of 
%                 the nodes are at exactly the same position and merge 
%                 these trees and the respective nodes.
%                 This function reverses the effect of the skel.unBranch
%                 function.
%                 (Note that comments of the nodes being merged might be 
%                  deleted!)
%
% Author: Martin Schmidt <martin.schmidt@brain.mpg.de>

if ~exist('treeIdcs', 'var')
    treeIdcs = 1:obj.numTrees;
end
treeIdcs = reshape(unique(treeIdcs), [], 1);

if numel(treeIdcs) <= 1
    warning('Passed less than 2 unique treeIdcs => nothing to merge.');
    return;
end

% Get all node positions, tree and node idcs
allTreeIdcs = repelem(treeIdcs, obj.numNodes(treeIdcs));
allNodeIdcs = arrayfun( ...
    @(x) (1:x)', ...
    obj.numNodes(treeIdcs), ...
    'uni', false);
allNodeIdcs = vertcat(allNodeIdcs{:});
allNodes = cellfun( ...
    @(x) x(:, 1:3), ...
    obj.nodes(treeIdcs), ...
    'uni', false);
allNodes = vertcat(allNodes{:});

% Get indices of pairwise distance = 0
% (additionally we require different treeIdcs, as we only want to merge 
%  nodes at the same position in different trees)
pwDistZero = ...
    pdist(allNodes, 'cityblock') == 0 & ...
    pdist(allTreeIdcs, 'cityblock') > 0;
if ~any(pwDistZero)
    warning('Nothing to merge');
    return;
end
zeroDistIdcs = zeros(sum(pwDistZero), 2);
[zeroDistIdcs(:, 2), zeroDistIdcs(:, 1)] = ...
    Util.IdxMagic.triind2sub(size(allNodes), find(pwDistZero));

% Gather to be merged tree and node ids
mergeTreeIdcs = reshape(allTreeIdcs(zeroDistIdcs), size(zeroDistIdcs));
mergeNodeIdcs = reshape(allNodeIdcs(zeroDistIdcs), size(zeroDistIdcs));

tree1 = mergeTreeIdcs(:, 1);
tree2 = mergeTreeIdcs(:, 2);

% run skeleton.mergeTrees (this does not yet delete the duplicate nodes!)
[obj, origIdcsDeleted] = obj.mergeTrees( ...
    tree1, mergeNodeIdcs(:, 1), ...
    tree2, mergeNodeIdcs(:, 2), ...
    'saveMerge');

% Calculate which tree(s) are remaining
origIdcsRemaining = unique(...
    mergeTreeIdcs(~ismember(mergeTreeIdcs(:), origIdcsDeleted))...
    );
newIdcsRemaining = ...
    origIdcsRemaining - ... 
    sum(origIdcsRemaining > origIdcsDeleted', 2);
%   > subtract 1 per deleted treeIdx that is greater than the original Idcs
%     of the remaining trees
assert(max(newIdcsRemaining) <= obj.numTrees);

% run deleteDuplicateNodes on the remaining trees
obj = obj.deleteDuplicateNodes(newIdcsRemaining);
end

