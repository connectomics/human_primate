function skel = setBoundingBox(skel, box, whichBox)
    % skel = setBoundingBox(skel, box)
    %   Returns the skel with bounding box added to it.
    %   box: in webknossos format
    %   on `whichBox` option, either the user bounding box (set by the user
    %   in the tracing view) or the task bounding box (set by the admin
    %   during task creation) is returned.
    %
    %
    % Modified from getBoundingBox by:
    %   Sahil Loomba <sahil.loomba@brain.mpg.de>
    
    if ~exist('whichBox', 'var') || isempty(whichBox)
        whichBox = 'user';
    end
    
    assert(ischar(whichBox));
    whichBox = lower(whichBox);
    assert(ismember(whichBox, {'user', 'task'}));
    whichBox = strcat(whichBox, 'BoundingBox');
    
    try
        bbox = struct('topLeftX',num2str(box(1)),...
                     'topLeftY',num2str(box(2)),...
                     'topLeftZ',num2str(box(3)),...
                     'width',num2str(box(4)),...
                     'height',num2str(box(5)),...
                     'depth',num2str(box(6)));
        skel.parameters.(whichBox) = bbox;
    catch ME
        warning('Something went wrong in box assignment')
        rethrow(ME)
    end
    % add node to write skel as nml because empty skeleton can't be written
    if isempty(skel.nodes)
        skel = skel.addTree('bboxStart',box(1:3));
    end
end

