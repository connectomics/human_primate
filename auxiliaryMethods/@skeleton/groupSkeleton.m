function skel = groupSkeleton(skel, groupId)
    % skel = groupSkeleton(skel, groupNameOrId)
    %   Wraps the entire skeleton into a top-level group.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % Make sure that group is at top-level. Otherwise we might introduce
    % cycles. This conditions could be relaxed by explicitly checking that.
    assert(isnan(skel.groups.parent(skel.groups.id == groupId)));
    
    % Subordinate previous top-level trees
    skel.groupId(isnan(skel.groupId)) = groupId;
    
    % Subordinate previous top-level groups
    skel.groups.parent( ...
        isnan(skel.groups.parent) ...
      & skel.groups.id ~= groupId) = groupId;
end
