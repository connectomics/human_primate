function obj = addTreeFromSkel(obj, skel, treeIndices, treeNames, addGroups)
% ADDTREEFROMSKEL Add trees from another skeleton object.
%
% INPUT skel: A Skeleton object different from the one calling
%             this function.
%       treeIndices: (Optional) Linear indices of the trees in
%             skel to add to obj.
%             (Default: all trees in skel).
%       treeNames: (Optional) [Nx1] cell or string
%           Cell array of same length as treeIndices containing the names
%           for the trees in skel which are added to obj.
%           Alternatively, a string can be for all trees, which can contain
%           "%d" which is replaced by the tree id.
%           (Default: Tree names in skel).
%       addGroups: (Optional) bool
%           Whether to add groups from source skel. If set to false, trees
%           from source skel are added to root group (i.e. group id nan).
%           (Default: false)
%
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
% addGroups flag added by:
%         Martin Schmidt <martin.schmidt@brain.mpg.de>

if ~exist('treeIndices','var') || isempty(treeIndices)
    treeIndices = 1:skel.numTrees();
end
treeIndices = treeIndices(:)';

if ~exist('treeNames', 'var') || isempty(treeNames)
    treeNames = skel.names(treeIndices);
elseif ischar(treeNames)
    treeNames = repmat({treeNames}, length(treeIndices), 1);
elseif iscell(treeNames) && (length(treeNames) ~= length(treeIndices))
    error(['A name must be specified for each tree which is added ' ...
            'to the skeleton.']);
end

if ~exist('addGroups', 'var')
    addGroups = false;
end

% Calculate relative offset to shift nodes to match obj.nodeOffset
% (only ~= 0, if skel.nodeOffset ~= obj.nodeOffset)
rel_offset = obj.nodeOffset - skel.nodeOffset;

% add groups from source skel to target skel
% TODO: we need to traverse the tree of groups and subgroups up to the root
% node (id nan) and add all groups in between; here we so far only add
% the groups closest to trees, not those further up in the hierarchy
if addGroups
    groupIdsSrc = skel.groupId(treeIndices);
    groupOffset = max(obj.groups.id);
    groups2Add = skel.groups(ismember(skel.groups.id, groupIdsSrc), :);
    groups2Add.id = groups2Add.id + groupOffset;
    groups2Add.parent(~isnan(groups2Add.parent)) = ...
        groups2Add.parent(~isnan(groups2Add.parent)) + ...
        groupOffset;
    groupIdsTar = groupIdsSrc;
    groupIdsTar(~isnan(groupIdsTar)) = ...
        groupIdsTar(~isnan(groupIdsTar)) ...
        + groupOffset;
    obj.groups(end+1:end+size(groups2Add, 1), :) = groups2Add;
    
    % The following assertion will currently still fail for cases where the
    % added trees are in groups where the parent is not the root group!
    assert(all(ismember(groups2Add.parent(~isnan(groups2Add.parent)), obj.groups.id)));
end

% add trees to obj
curGroup = nan;
for tr = 1:length(treeIndices)
    if addGroups
        curGroup = groupIdsTar(tr);
    end
    
    obj = obj.addTree(...
        treeNames{tr}, ...
        skel.nodes{treeIndices(tr)} + rel_offset, ...
        skel.edges{treeIndices(tr)}, ...
        skel.colors{treeIndices(tr)}, ...
        [], ...
        {}, ...
        curGroup);
    [obj.nodesAsStruct{end}.comment] = ...
        skel.nodesAsStruct{treeIndices(tr)}.comment;
end

end
