classdef skeleton
    %Skeleton Class representing a Skeleton tracing.
    
    properties
        nodes = {};
        nodesAsStruct = {};
        nodesNumDataAll = {};
        nodesTable = {};
        edges = {};
        parameters;
        thingIDs;
        names = {};
        colors = {};
        branchpoints;
        largestID = 0;
        scale = [1, 1, 1];
        nodeOffset = 1;
        verbose = false;
        volumeDir = ''; % path to directory with volume tracings
        filename = ''; % file name (without directory or file extension)
        groupId = [];
        groups = struct2table(struct('name', '', 'id', ...
            [], 'parent', []));
        volumes = struct2table( ...
            struct('id', {nan}, 'location', {''}), 'AsArray', true);
    end
    
    methods
        function obj = skeleton(filename, justCloneParameters, nodeOffset, verbose)
            % Construct Skeleton class from nml file.
            % obj = Skeleton() returns an empty Skeleton object.
            %
            % obj = Skeleton(filename, justcloneparameters, nodeOffset)
            % requires the following inputs.
            % INPUT filename: Full path (including '.nml') to nml file.
            %       justCloneParameters: Only parameter struct is written to
            %                           skel object. (Default: false)
            %       nodeOffset: (Optional) Double specifying an offset to
            %           the x, y and z coordinates of all nodes.
            %           (Default: 1).
            %       verbose: (Optional) Debug messages and warnings
            %           (Default: true).
            % OUTPUT skel: A Skeleton object.
            
            % NOTE nodeOffset should be set to 1 when reading or writing
            %      files made in webknossos due to the webknossos node
            %      offset.
            
            % NOTE(amotta): I had to set dummy values in the `properties`
            % section above because MATLAB won't allow me to specify that
            % `location` is supposed to be a list of strings. These dummy
            % values are removed now (while keeping the type information).
            %
            % This is the only way I've found the make this work:
            %
            %   skel = skeleton();
            %   skel.volumes(end + 1, :) = {1, 'data.zip'};
            %
            % Note that this might go horribly wrong if it's somehow
            % possible to construct a skeleton without passing through
            % here. But it seems safe enough to assume that the constructor
            % gets called...
            obj.volumes = obj.volumes([], :);
            
            if nargin < 1; return; end
            
            if ~exist('justCloneParameters','var') ...
                    || isempty(justCloneParameters)
                justCloneParameters = false;
            end

            if exist('verbose','var') && ~isempty(verbose)
                obj.verbose = verbose;
            end

            if ~exist('nodeOffset', 'var') || isempty(nodeOffset)
                nodeOffset = 1;
            elseif length(nodeOffset) ~= 1
                error('NodeOffset must be a scalar.');
            end

            obj.nodeOffset = nodeOffset;
            if nodeOffset ~= 1 && obj.verbose
                warning('Node offset is set to %d.', nodeOffset);
            end

           [fileDir, obj.filename, fileExt] = fileparts(filename);

            % Hybrid tracing
            if strcmpi(fileExt, '.zip')
                while true
                    tempDir = tempname(Util.getTempDir());
                    if ~exist(tempDir, 'file'); break; end
                end

                tempFiles = unzip(filename, tempDir);
                fileDir = tempDir;

                % Find NML file
               [~, ~, tempFileExts] = cellfun( ...
                    @fileparts, tempFiles, 'UniformOutput', false);

                filename = tempFiles(strcmpi(tempFileExts, '.nml'));
                assert(isscalar(filename));
                filename = filename{1};
                fileExt = '.nml';
            end

            obj.volumeDir = fileDir;

            switch lower(fileExt)
                case '.nml'
                    temp = skeleton.loadNml(filename);
                case '.swc'
                    temp = skeleton.loadSwc(filename);
                otherwise
                    error('Unknown file extension "%s"', fileExt)
            end

            obj.parameters = temp.parameters;
            if isfield(temp,'volumes')
                obj.volumes = struct2table(temp.volumes, 'AsArray', false);
            end
            obj.scale = structfun(@str2double, obj.parameters.scale);
            obj.scale = reshape(obj.scale, 1, []);

            if justCloneParameters
                % NOTE(amotta): Who the hell defines when parameters
                % end and when data starts?
                return;
            end

            obj.thingIDs = zeros(length(temp.things.id),1);
            obj.groupId = nan(length(temp.things.id),1);
            obj.nodes = cell(length(temp.things.nodes),1);
            obj.edges = cell(length(temp.things.edges),1);
            obj.names = cell(length(temp.things.name),1);
            obj.colors = cell(length(temp.things.id),1);
            obj.nodesAsStruct = cell(length(temp.things.id),1);
            obj.nodesNumDataAll = cell(length(temp.things.id),1);

            tmax = zeros(length(temp.things.id),1);
            for i=1:length(temp.things.id)
                % nodes
                obj.nodes{i} = [ ...
                    temp.things.nodes{i}.x + nodeOffset, ...
                    temp.things.nodes{i}.y + nodeOffset, ...
                    temp.things.nodes{i}.z + nodeOffset, ...
                    temp.things.nodes{i}.radius];

                % edges
                [~, obj.edges{i}] = ismember( ...
                    [temp.things.edges{i}.source, ...
                    temp.things.edges{i}.target], ...
                    temp.things.nodes{i}.id);

                obj.thingIDs(i) = temp.things.id(i);
                obj.groupId(i) = temp.things.groupId(i);
                obj.names{i} = temp.things.name{i};
                
                try
                    color = [ ...
                        temp.things.('color.r')(i), ...
                        temp.things.('color.g')(i), ...
                        temp.things.('color.b')(i), ...
                        temp.things.('color.a')(i)];
                catch
                    % NOTE(amotta): The fieldnames 'color.r' et al. are,
                    % strictly speaking, illegal in MATLAB. It is
                    % impossible to create them in pure MATLAB.
                    color = [nan, nan, nan, nan];
                end
                
                obj.colors{i} = color;

                obj.nodesNumDataAll{i} = ...
                    [temp.things.nodes{i}.id, ...
                    temp.things.nodes{i}.radius, ...
                    temp.things.nodes{i}.x + nodeOffset, ...
                    temp.things.nodes{i}.y + nodeOffset, ...
                    temp.things.nodes{i}.z + nodeOffset, ...
                    temp.things.nodes{i}.inVp, ...
                    temp.things.nodes{i}.inMag, ...
                    temp.things.nodes{i}.time];

                nodesNumDataAllToStr = arrayfun(@num2str, ...
                    obj.nodesNumDataAll{i}, 'UniformOutput', false);

                % NOTE(amotta): `parseNml` used to initialize all
                % comments to empty strings. Let's replicate this here.
                comment = arrayfun(@(x)'', ...
                    1:length(temp.things.nodes{i}.id), 'uni', 0)';

                [~, index] = ismember( ...
                    temp.things.nodes{i}.id, ...
                    temp.comments.node);
                comment(index ~= 0) = ...
                    temp.comments.content(index(index ~= 0));
                nodesNumDataAllToStr(:,9) = comment;
                fieldNames = {'id', 'radius', 'x', 'y', 'z', ...
                    'inVp', 'inMag', 'time', 'comment'};
                obj.nodesAsStruct{i} = ...
                    cell2struct(nodesNumDataAllToStr, fieldNames, 2)';

                %handle empty trees (will produce warning below)
                if ~isempty(obj.nodesNumDataAll{i})
                    tmax(i) = max(temp.things.nodes{i}.id);
                end
            end

            if any(tmax == 0)
                warning('NML file "%s" contains empty trees.', obj.filename);
            end

            obj.largestID = max([0; tmax(:)]);
            obj.parameters = temp.parameters;
            obj.branchpoints = temp.branchpoints.id;
            obj.groups = obj.flattenOrNestGroup(temp.groups);
            
            % NOTE(mschmidt): update obj.parameters.offset 
            % (in order to be consistent with
            %  the specified nodeOffset defined in the constructor!)
            obj.parameters.offset.x = num2str(nodeOffset);
            obj.parameters.offset.y = num2str(nodeOffset);
            obj.parameters.offset.z = num2str(nodeOffset);
        end
        
        % Method definitions
        skel = setActiveNodeID(skel, treeIdx,NodeIdx);
    end
    
    methods (Static)
        nml = loadNml(path);
        swc = loadSwc(path);
        
        [l,nl] = physicalPathLength(nodes, edges, voxelSize)
        [skel, treeOrigin] = loadSkelCollection(paths, nodeOffset, ...
            toCellOutput, addFilePrefix)
        skel = fromCellArray(c);
        skel = loadSkelCollectionFromSubfolders(paths, nodeOffset, ...
            toCellOutput)
        [scaledCoords]=setScale(coords,scale)
    end
end
