function h = plot(skel, treeIndices, colors, umScale, lineWidths,...
    realEndingComments, unityScale, somaSize, somaComment, rotationMatrix, ...
    alphaVal, matlab_affineTranform)
% PLOT Simple line plot of Skeleton. Nodes are scaled with
% INPUT treeIndices:(Optional) [Nx1] 
%           vector of linear indices
%           of trees to check for. (Default: all trees)             
%       colors: (Optional) [Nx3] 
%           array of double specifying a colormap.  
%           (Default: colors = colormap(lines))
%       umscale: (Optional, added by AK 20.02.2017) 
%           logical if set true sets the scale to
%           micrometer. (Default: nanometer scale)
%       lineWidths: (Optional) Scalar or [Nx1] 
%           array specifying the line
%           widths for skeleton plots
%       realEndingComments:(Optional):1xN 
%           cell array specifying comments of
%           real tree endings. With this specification the tree would be
%           trimmed to its backbone. added by AK
%       unityScale: (Optional, added by SL) 
%           Logical if set to true keeps the
%           original scale [1 1 1]
%       somaSize: (Optional) 
%           Scalar with size of soma [in nm]. If not
%           given or the node has no 'soma' comment, no soma is plotted
%       somaComment: 
%           str of the comment name, Default: 'soma'
%       rotationMatrix: 3x3 double, Default: identity matrix
%                       Rotation matrix for the plotting
%       matlab_affineTranform: matlab affine transform object (call affine3d to create)
%                       The basic transfrom matrix is a 4x4 matrix
%       alphaVal: 
%           numerical value for face alpha of line plot (default 1)
% OUTPUT h: structure array with each tree plot as an graphics object array as a fieldname treeId"Nr" (e.g.treeId1)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%         Ali Karimi <ali.karimi@brain.mpg.de>
%         Sahil Loomba <sahil.loomba@brain.mpg.de>
%         Marcel Beining <marcel.beining@brain.mpg.de>
%         Alessandro Motta <alessandro.motta@brain.mpg.de>


% If no tree index is specified, plot all
if ~exist('treeIndices','var') || isempty(treeIndices)
    treeIndices = 1:skel.numTrees();
end
if islogical(treeIndices)
    treeIndices = find(treeIndices);
end
if ~exist('somaComment','var') || isempty(somaComment)
    somaComment = 'soma';
end

if ~exist('somaSize','var') || isempty(somaSize)
    somaSize = 0;
else
    [comments, treeIdx, nodeIdx] = skel.getAllComments;
    somaNode = zeros(skel.numTrees,1);
    somaNode(treeIdx(strcmpi(comments,somaComment))) = nodeIdx(strcmpi(comments,somaComment));
end

% If no color is specified, generate appropriate number of colors
if ~exist('colors','var') || isempty(colors)
    colors = lines(numel(treeIndices));
end
% If a color matrix of insufficient size is passed enlarge it
if size(colors,1) < numel(treeIndices)
    colors = padarray(colors,numel(treeIndices)-size(colors,1),'circular',...
        'post');
end

% Check um scale
if ~exist('unityScale','var') || isempty(unityScale) || unityScale == 0
    if ~exist('umScale','var') || isempty(umScale) || umScale == 0
        if isempty(skel.scale)
            scale = [1,1,1];
            warning('No scale information found in skeleton. Using [1 1 1] as scale.')
        else
            scale = skel.scale;
        end
    else
        scale =skel.scale/1000;
    end
else
    scale = [1 1 1];
end

% Check lineWidths
if ~exist('lineWidths','var') || isempty(lineWidths)
    lineWidths = 1;
end
if length(lineWidths) == numel(treeIndices)
    tmp = lineWidths;
    lineWidths = zeros(skel.numTrees(),1);
    lineWidths(treeIndices) = tmp;
elseif length(lineWidths) < skel.numTrees()
   lineWidths = repmat(lineWidths(1),1,skel.numTrees()) ;
end
% Check if Backbone comments exist and Trim the tree if they do
if ~exist('realEndingComments','var') || isempty(realEndingComments)
    trim2BackBone=false;
else
    trim2BackBone=true;
end
% Default rotation matrix: identity
if ~exist('rotationMatrix','var') || isempty(rotationMatrix)
    rotationMatrix = eye(3);
end
% Default affine transform: identity
if ~exist('matlab_affineTranform','var') || isempty(matlab_affineTranform)
    matlab_affineTranform = affine3d(eye(4));
end
% default alphaVal
if ~exist('alphaVal','var') || isempty(alphaVal)
    alphaVal = false; % default alpha 1
end

% Generate plot
treeIndices = reshape(treeIndices, 1, []);
[sX,sY,sZ] = sphere;

%structure to gather the line objects for each tree as h.treeIndex
h = struct;
ax = gca;
hold(ax, 'on');

for tr = treeIndices
    if trim2BackBone
        skel=skel.getBackBone(tr,realEndingComments);
    end
    
    trNodes = transpose(skel.nodes{tr}(:, 1:3));
    trNodes = bsxfun(@times, trNodes, scale(:));
    % Apply rotation (identity matrix default)
    trNodes = rotationMatrix * trNodes;
    % Apply matlab affine 3d transform
    trNodes = (transformPointsForward(matlab_affineTranform, trNodes'))';
    trNodes = trNodes(:, transpose(skel.edges{tr}));
    
    % NOTE(amotta): Calling `plot3` for each edge is extremely slow. MATLAB
    % creates a graphics object for each individual edge, each having a bit
    % of overhead.
    %   The following hack allows us to plot all edges in a single call to
    % `plot3`. We draw all edges as single, continuous line and exploit the
    % fact that MATLAB does not draw edges that involve points with any of
    % the coordinates being `nan`.
    trNodes = reshape(trNodes, 3, 2, []);
    trNodes(:, 3, :) = nan;
    trNodes = reshape(trNodes, 3, []);
    
    hName = sprintf('treeId%d', tr);
    color = colors(treeIndices == tr, :);
    lineWidth = lineWidths(tr);
    
    h.(hName) = plot3(ax, ...
        trNodes(1, :), trNodes(2, :), trNodes(3, :), ...
        'Color', color, 'LineWidth', lineWidth');
    if alphaVal
        if ~isempty(h.(hName))
            h.(hName).Color(4) =  alphaVal;
        end
    end
  
    if somaSize && somaNode(tr)
        coord = skel.nodes{tr}(somaNode(tr),1:3).*scale;
        coord = (rotationMatrix*coord')';
        % Affine transform for soma
        coord = transformPointsForward(matlab_affineTranform, coord);
        surf(sX*somaSize+coord(1),sY*somaSize+coord(2),...
            sZ*somaSize+coord(3),'FaceColor',colors(treeIndices==tr,:),...
            'EdgeColor','None')
    end
end

end
