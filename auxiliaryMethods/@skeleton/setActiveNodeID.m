function skel = setActiveNodeID(skel, treeIdx,NodeIdx)
    % skel = setActiveNodeID(skel, nodeId,TreeId)
    %   Sets the active node ID in WebKnossos
    % treeIdx Double
    %   The linear index of the tree containing the prospective active node
    % NodeIdx Double
    %   The linear index of the node we want to set as active position
    % Written by
    %   Ali Karimi <ali.karimi@brain.mpg.de>
    
    assert(numel(treeIdx) == 1 && numel(NodeIdx) == 1 );
    IDs=skel.nodeIdx2Id(treeIdx);
    NodeID=IDs{1}(NodeIdx);
    % modify skeleton
    skel.parameters.activeNode.id = num2str(NodeID);
end
