function cluster = config(varargin)
    % cluster = Cluster.config(varargin)
    %   Configure a MATLAB `parallel.Cluster` object for job submission to
    %   the GABA or Cobra compute clusters. If this function is called from
    %   a machine that is not connected any of these cluster, it will
    %   return a local cluster (see `parallel.cluster.Local`).
    %
    % Inputs
    %   All of the following key-value pairs are optional.
    %
    %   memory (default: 6)
    %     Integer number of gigabytes of RAM per task. If the requested
    %     amount of memory is zero, Slurm will allocate whole nodes.
    %
    %   time (default: '1:00:00');
    %     Maximum wall-clock time per task in hours:minutes:seconds format.
    %     Tasks which exceed this limit will be killed by the scheduler.
    %     They may or may not be marked as 'Failed' by MATLAB.
    %
    %   priority (default: 50)
    %     Job priority in the range from 0 to 100. Higher values correspond
    %     to higher priorities. This value is automatically translated to
    %     the scheduler-specific ranges.
    %
    %   taskConcurrency (default: 0, i.e., unlimited)
    %     Maximum number of tasks to be executed concurrently. If this
    %     value is zero, the task concurrency is unlimited. On a local
    %     cluster, this value is used to configure the number of workers.
    %
    %   cores (default: 1)
    %     Integer number of cores per task.
    %
    %     Number of cores to reserve for each task. If multiple cores are
    %     requested, MATLAB versions R2017a and newer will execute tasks
    %     using multi-threading.
    %
    %     Requesting zero cores per tasks can be used in combination with
    %     the `tasksPerNode` option to specify the number of tasks per node
    %     independently of the number of cores per node.
    %
    %   tasksPerNode (default: 0)
    %     Integer number of tasks to run per node.
    %
    %     By default, the number of tasks per node is derived for each node
    %     based on the specified memory and core requirements. This option
    %     can be used in combination with requesting zero `cores` to run a
    %     fixed number of tasks per node, irrespective of the number of
    %     available cores.
    %
    %     This option is only supported on Slurm.
    %
    %   gpus (default: 0)
    %     Integer number of GPUs per task.
    %
    %   scheduler (default: 'slurm' if on GABA or Cobra, 'local' otherwise)
    %     Name of scheduler to use for job submissions. We currently
    %     support Slurm ('slurm'), the Sun Grid Engine ('sge'), and local
    %     MATLAB clusters ('local'). For more information on the latter,
    %     see also parallel.cluster.Local.
    %
    %   partition (default: 'p.gaba' for Slurm, 'openmp' for SGE)
    %     Name of cluster partition to use for job submission. On the Sun
    %     Grid Engine, this value corresponds to the parallel environment.
    %     
    %   jobStorageLocation
    %     Path to directory in which MATLAB will store all job- and
    %     task-related files. The directory will be created if it does not
    %     yet exist.
    %
    %     Default:
    %       On GABA: '/tmpscratch/$USER/data/matlab-jobs'
    %       On Cobra: '/ptmp/$USER/data/matlab-jobs'
    %
    %   rawOptions (default: empty)
    %     Additional options to be passed to the scheduler. These options
    %     may be specified as a character vector or as a cell array of
    %     character vectors.
    %
    % Example
    %   cluster = Cluster.config( ...
    %       'scheduler', 'slurm', ...
    %       'priority', 50, ...
    %       'memory', 6, ...
    %       'time', '1:00:00', ...
    %       'taskConcurrency', 10, ...
    %       'gpus', 1);
    %
    %   OR
    %   
    %   cluster = Cluster.config( ...
    %       'priority', 100, ...
    %       'memory', 6, ...
    %       'time', '1:00:00', ...
    %       'scheduler', 'sge');
    %
    %   job = createJob(cluster);
    %   job.Name = 'buildRandMat';
    %
    %   createTask(job, @rand, 1, {10, 10});
    %   submit(job);
    %
    % Written by
    %   Sahil Loomba <sahil.loomba@brain.mpg.de>
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % config
    gabaRegex = '^gaba(g?)\d*\.opt\.rzg\.mpg\.de$';
    cobraRegex = '^co(bra)?\d*$'; % See also +Util/getTempDir.m
    args = parseArgs(varargin{:});
    
    % check if we're on GABA or Cobra
    cluster = parallel.cluster.Generic();
    
    isCluster = ...
        not(isempty(regexp(cluster.Host, gabaRegex, 'once'))) ...
      | not(isempty(regexp(cluster.Host, cobraRegex, 'once')));
    useLocal = ~isCluster || strcmpi(args.scheduler, 'local');

    if useLocal
        % Parallelize over cores of local machine, if we're not on GABA or
        % if the user explicitly asked for this behaviour.
        cluster = parallel.cluster.Local();
    end

    % make sure job storage location exists
    if ~exist(args.jobStorageLocation, 'dir')
        mkdir(args.jobStorageLocation);
    end

    cluster.JobStorageLocation = args.jobStorageLocation;
    cluster.HasSharedFilesystem = true;
    
    if args.cores > 1
        if verLessThan('matlab', '9.2') % R2017a
            warning( ...
                'Cluster:config', ...
               ['Cluster.config does not support multi-threaded ', ...
                'workers in MATLAB versions prior to R2017a. The ', ...
                'tasks will be run on single threads.']);
        else
            cluster.NumThreads = args.cores;
        end
    end

    if useLocal
        cluster = configLocal(cluster, args);
    else
        cluster = configGaba(cluster, args);
    end
end

function cluster = configLocal(cluster, args)
    if args.taskConcurrency > 0
        cluster.NumWorkers = args.taskConcurrency;
    end
end

function submitArgs = buildSubmitFcnArguments(args)
    switch args.scheduler
        case 'sge'
            submitArgs = buildSgeSubmitFcnArguments(args);
        case 'slurm'
            submitArgs = buildSlurmSubmitFcnArguments(args);
    end
    
    % NOTE(amotta): Raw options are always appended to the end of the list.
    % This ensures that they dominate over all other options.
    submitArgs = cat(2, submitArgs, reshape(args.rawOptions, 1, []));
end

function submitArgs = buildSgeSubmitFcnArguments(args)
    % Our user-facing priorities go from 0 (lowest priority) to 100
    % (highest priority). Let's translate this to SGE's -1000 to 0.
    priority = 10 * args.priority - 1000;
    
    submitArgs = { ...
        sprintf('-pe %s %d', args.partition, args.cores), ...
        sprintf('-l h_vmem=%dG', args.memory), ...
        sprintf('-l h_rt=%s', args.time), ...
        sprintf('-p %d', priority)};
    
    if args.taskConcurrency > 0
        submitArgs{end + 1} = sprintf( ...
            '-tc %d', args.taskConcurrency);
    end
end

function submitArgs = buildSlurmSubmitFcnArguments(args)
    if args.cores == 0 && args.tasksPerNode == 0
        error('Cluster:config', [ ...
            'Either the number of cores or the ', ...
            'number of tasks per node must be non-zero.']);
    end
        
    % HACK(amotta); Unfortunately, the task concurrency is passed to Slurm
    % via the same option (`--array`) which also contains the total number
    % of tasks. But at the time of cluster configuration, we do not yet
    % know the task count.
    %
    % As a workaround, we forward to the Slurm integration script not the
    % final --array option for `sbatch`, but a format string that can be
    % used to fill in the task count.
    %
    % The submit function is supposed to replace the `{:taskCount:}`
    % placeholder by the actual value!
    niceness = round(2147483645 * (1 - args.priority / 100));
    taskConcurrency = '--array=1-{:taskCount:}';
    
    if args.taskConcurrency > 0
        taskConcurrency = sprintf('%s%%%d', ...
            taskConcurrency, args.taskConcurrency);
    end
    
    submitArgs = { ...
        sprintf('--partition=%s', args.partition), ...
        sprintf('--mem=%dG', args.memory), ...
        sprintf('--time=%s', args.time), ...
        sprintf('--nice=%d', niceness), ...
        taskConcurrency};
    
    if args.cores > 0
        submitArgs{end + 1} = sprintf( ...
            '--cpus-per-task=%d', args.cores);
    end
    
    if args.tasksPerNode > 0
        submitArgs{end + 1} = sprintf( ...
            '--ntasks-per-node=%d', args.tasksPerNode);
        % NOTE(amotta): I think this is necessary, so that Slurm can
        % correctly compute the number of CPU cores / task and to set the
        % CPU / affinity mask accordingly.
        submitArgs{end + 1} = '--exclusive';
    end

    if args.gpus > 0
        % TODO: Allow user to specify type of GPU
        submitArgs{end + 1} = sprintf( ...
            '--gres=gpu:%d', args.gpus);
    end
end

function cluster = configGaba(cluster, args)
    submitFcnArguments = buildSubmitFcnArguments(args);
    
    cluster = ...
        Cluster.Internal.configMatlabClusterObject( ...
            cluster, args.scheduler, submitFcnArguments);
end

function args = parseArgs(varargin)
    parser = inputParser();
    parser.FunctionName = mfilename();
    
    addParameter(parser, 'jobStorageLocation', ...
        fullfile(Util.getTempDir(), 'data', 'matlab-jobs'), ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
    
    addParameter(parser, 'scheduler', 'slurm', ...
        @(x) any(validatestring(x, {'sge', 'slurm', 'local'})));
    
    addParameter(parser, 'partition', '', ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
    
    addParameter(parser, 'cores', 1, @(x) validateattributes( ...
        x, {'numeric'}, {'scalar', 'integer', 'nonnegative'}));
    
    addParameter(parser, 'tasksPerNode', 0, @(x) validateattributes( ...
        x, {'numeric'}, {'scalar', 'integer', 'nonnegative'}));
    
    % NOTE(amotta): If the user requests zero memory, this is interpreted
    % as "just give me all the damn RAM available on the node". That's also
    % Slurms' interpreation of the memory value.
    addParameter(parser, 'memory', 6, @(x) validateattributes( ...
        x, {'numeric'}, {'scalar', 'integer', 'nonnegative'}));
    
    % TODO: Improve validation of time format
    addParameter(parser, 'time', '1:00:00', ...
        @(x) validateattributes(x, {'char'}, {'nonempty'}));
    
    addParameter(parser, 'priority', 50, @(x) validateattributes( ...
        x, {'numeric'}, {'scalar', 'integer', '>=', 0, '<=', 100}));
    
    addParameter(parser, 'taskConcurrency', 0, @(x) validateattributes( ...
        x, {'numeric'}, {'scalar', 'integer', 'nonnegative'}));
    
    addParameter(parser, 'gpus', 0, @(x) validateattributes( ...
        x, {'numeric'}, {'scalar', 'integer', 'nonnegative'}));
    
    isRawOptions = @(x) assert( ...
        ischar(x) || iscellstr(x), 'MATLAB:invalidType', sprintf( ...
       ['Expected input to be one of these types', newline, newline, ...
        'char or cell array of chars (iscellstr)', newline, newline, ...
        'Instead its type was %s.'], class(x)));
    addParameter(parser, 'rawOptions', {}, isRawOptions);
    
    parse(parser, varargin{:});
    args = parser.Results;
    
    % Set scheduler-specific default values
    switch args.scheduler
        case 'sge'
            defaultPartition = 'openmp';
        case 'slurm'
            defaultPartition = 'p.gaba';
        otherwise
            defaultPartition = '';
    end
    
    if isempty(args.partition)
        args.partition = defaultPartition;
    end
    
    if ~iscell(args.rawOptions)
        assert(ischar(args.rawOptions));
        args.rawOptions = {args.rawOptions};
    end
end

