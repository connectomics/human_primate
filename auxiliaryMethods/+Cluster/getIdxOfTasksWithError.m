function idxError = getIdxOfTasksWithError(job)
% Function will return indices of tasks that thre an error

    errorCell = {job.Tasks(:).Error};
    idxError = find(~cellfun(@isempty, errorCell) | strcmp({job.Tasks(:).State}, 'failed'));

end

