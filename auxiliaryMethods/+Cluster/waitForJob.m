function waitForJob(job, interval, notify)
    % waitForJob(job, interval)
    %   Waits for one or multiple jobs and displays progress data in a
    %   regular interval (60 seconds by default).
    %   notify: Boolean when set true sends an email in case of an error
    %
    % Written by
    %   Manuel Berning <manuel.berning@brain.mpg.de>
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    % Modified by
    %   Sahil Loomba <sahil.loomba@brain.mpg.de>
    if nargin < 2 || isempty(interval)
        interval = 60;
    end
    if nargin < 3 || isempty(notify)
        notify = false;
    end
    
    for i = 1:numel(job)
        try
            jobId = job(i).Id;
            jobName = job(i).Name;
        catch
            % NOTE(amotta): See below for an explanation of why this
            % try-catch is required here.
            continue;
        end
        
        Util.log('Waiting for job %d: %s', jobId, jobName);
    end
    
    finished = false(size(job));
    while ~all(finished)
        pause(interval);
        
        for i = 1:numel(job)
            if finished(i); continue; end
            
            try
                jobId = job(i).Id;
                jobState = job(i).State;
                jobName = job(i).Name;
                errorCell = {job(i).Tasks(:).Error};
                stateCell = {job(i).Tasks(:).State};
            catch
                % NOTE(amotta): The above code might look innocent. But in
                % fact, MATLAB is accessing the job / task files to extract
                % these data. If the file is at the same time being written
                % by the worker, its content might be invalid, which causes
                % an error to be thrown.
                %
                % We don't care about this situation and simply will try
                % again during the next interval. A log statement is shown,
                % so that the user can detect the unlikely case that we get
                % stuck in an infinite loop.
                Util.log( ...
                   ['Encountered invalid job or task file. ', ...
                    'Will check again later..']);
                continue;
            end
            
            errorMask = ...
                not(cellfun(@isempty, errorCell)) ...
              | strcmp(stateCell, 'failed');
          
            nrError = sum(errorMask);
            nrFinished = sum( ...
                strcmp(stateCell, 'finished') ...
              | strcmp(stateCell, 'failed'));
            nrPending = sum(strcmp(stateCell, 'pending'));
            nrRunning = sum(strcmp(stateCell, 'running'));
            
            Util.log( ...
               ['Job %d is %s. Tasks: %d finished, %d running, ', ...
                '%d pending, %d with errors'], jobId, jobState, ...
                nrFinished, nrRunning, nrPending, nrError);
            
            if nrError > 0
                errorTaskIds = find(errorMask);
                errorTaskIdsList = arrayfun( ...
                    @num2str, errorTaskIds, 'UniformOutput', false);
                errorTaskIdsList = strjoin(errorTaskIdsList, ', ');
                
                Util.log( ...
                    '%d tasks encountered errors: %s', ...
                    nrError, errorTaskIdsList);
                
                reportTaskId = not(cellfun(@isempty, errorCell));
                reportTaskId = find(reportTaskId, 1);
                
                if ~isempty(reportTaskId)
                    errorReport = getReport(errorCell{reportTaskId});
                    Util.log('Task %d: %s', reportTaskId, errorReport);
                    if notify; Util.sendEmail('', errorReport); end
                else
                    Util.log([ ...
                        'No MATLAB error log found. ', ...
                        'Probably tasks ran into Out-Of-Memory']);
                end
                
                error('Job %d threw an error', jobId);
            end
            
            if strcmp(jobState, 'finished')
                finished(i) = true;
                Util.log('Finished job %s', jobName);
            end
        end
    end
end
