function klass = typToKlass(typeNameOrId)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    t = struct;
    
    % TODO(amotta): Complete
    t.H5T_STD_I8LE  = 'int8';
    t.H5T_STD_U8LE  = 'uint8';
    t.H5T_STD_I16LE = 'int16';
    t.H5T_STD_U16LE = 'uint16';
    t.H5T_STD_I32LE = 'int32';
    t.H5T_STD_U32LE = 'uint32';
    t.H5T_STD_I64LE = 'int64';
    t.H5T_STD_U64LE = 'uint64';
    t.H5T_IEEE_F32LE = 'single';
    t.H5T_IEEE_F64LE = 'double';
    
    if ischar(typeNameOrId)
        klass = t.(typeNameOrId);
    elseif isa(typeNameOrId, 'H5ML.id')
        typeNames = fieldnames(t);
        for idx = 1:numel(typeNames)
            typeName = typeNames{idx};
            if H5T.equal(typeNameOrId, typeName)
                klass = t.(typeName);
                return;
            end
        end
        
        error('Unknown type id');
    else
        error('Invalid input type');
    end
end
