function categories = readCategories(inFile, dset)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    try
        typeCount = h5readatt(inFile, dset, 'typeCount');
    catch
        % For backward compatibility, fall back to `maxTypeId`
        typeCount = h5readatt(inFile, dset, 'maxTypeId');
        typeCount = typeCount + 1;
    end
    
    categories = cell(1, typeCount);
    for curIdx = 1:typeCount
        curAttrName = sprintf('typeName%d', curIdx - 1);
        curTypeName = h5readatt(inFile, dset, curAttrName);
        categories{curIdx} = curTypeName;
    end
end
