function data = readCategorical(inFile, dset)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    catNames = HDF5.readCategories(inFile, dset);
    cats = uint8(0):uint8(numel(catNames) - 1);
    cats = categorical(cats, cats, catNames);
    
    data = h5read(inFile, dset);
    assert(isempty(data) || max(data(:)) < intmax(class(data)));
    data = reshape(cats(data + 1), size(data));
end
