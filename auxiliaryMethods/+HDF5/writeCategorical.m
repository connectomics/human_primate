function writeCategorical(outFile, dset, data)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    assert(iscategorical(data));
    assert(not(any(isundefined(data(:)))));
    
    typeNames = categories(data);
    assert(numel(typeNames) <= intmax('uint8'));
    typeCount = uint8(numel(typeNames));
    
    % IMPORTANT(amotta): uint8(categorical(uint8(x))) does not return x!
    % Instead, it returns one-based category indices. Change to zero-based!
    data = uint8(data);
    assert(all(data(:)));
    data = data - 1;
    
    numericToHdf5(outFile, dset, data);
    h5writeatt(outFile, dset, 'typeCount', typeCount);
    
    for curIdx = 1:typeCount
        curTypeName = typeNames{curIdx};
        curAttrName = sprintf('typeName%d', curIdx - 1);
        h5writeatt(outFile, dset, curAttrName, curTypeName);
    end
end
