function hasDset = hasDataset(file, dsetName)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if not(isempty(dsetName)) ...
            && startsWith(dsetName, '/')
        dsetName = dsetName(2:end);
    end
    
    hasDset = h5info(file);
    hasDset = {hasDset.Datasets.Name};
    hasDset = ismember(dsetName, hasDset);
end
