function data = readChunked(file, dsetNameOrId, ids, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.parallel = false;
    opts.chunkSize = 1024 * 1024; % in bytes
    opts.asCategorical = false;
    opts = Util.modifyStruct(opts, varargin{:});
    
    if ischar(dsetNameOrId)
        fileId = H5F.open(file);
        dsetId = H5D.open(fileId, dsetNameOrId);
        
        if opts.asCategorical
            catNames = HDF5.readCategories(file, dsetNameOrId);
            assert(numel(catNames) <= intmax('uint8'));
            cats = uint8(0):uint8(numel(catNames) - 1);
            cats = categorical(cats, cats, catNames);
        end
    else
        assert(isempty(file));
        dsetId = dsetNameOrId;
        
        % TODO(amotta): Complain to amotta if you see this. I believe that
        % H5A.open(dsetId, 'attributeName') et al. could be the solution.
        assert(not(opts.asCategorical), 'Not yet implemented');
    end
    
    dsetTypeId = H5D.get_type(dsetId);
    dsetClass = HDF5.typToKlass(dsetTypeId);
    dsetTypeSize = H5T.get_size(dsetTypeId);
    
    dsetSpaceId = H5D.get_space(dsetId);
   [~, dsetSize] = H5S.get_simple_extent_dims(dsetSpaceId);
    dsetSize = flip(dsetSize);
    
    outSize = [dsetSize(1:(end - 1)), numel(ids)];
    if numel(outSize) < 2; outSize = [outSize, 1]; end
    
    chunkSize = prod(dsetSize(1:(end - 1))) * dsetTypeSize;
    chunkSize = ceil(opts.chunkSize / chunkSize);
    
    % NOTE(amotta): MATLAB's HDF5 function (e.g., h5read and h5write)
    % expect arguments of type double. For simplicity, let's fix the type
    % right here, right now.
    ids = double(ids);
   [ids, sortIds] = sort(ids(:));
   
    % Sanity checks
    assert(isempty(ids) || ids(1) >= 1);
    assert(isempty(ids) || ids(end) <= dsetSize(end));
   
    % NOTE(amotta): One-based chunk IDs
    uniChunkIds = ceil(ids / chunkSize);
   [uniChunkIds, ~, chunkCount] = unique(uniChunkIds);
    chunkCount = accumarray(chunkCount(:), 1);
    
    ids = mat2cell(ids, chunkCount);
    sortIds = mat2cell(sortIds, chunkCount);
    
    if opts.parallel
        chunkedData = cell(size(uniChunkIds));
        parfor curIdx = 1:numel(uniChunkIds)
            curData = forChunk( ...
                dsetId, dsetSpaceId, dsetSize, ...
                chunkSize, uniChunkIds(curIdx), ids{curIdx});
            if opts.asCategorical; curData = cats(curData + 1); end %#ok
            chunkedData{curIdx} = curData;
        end
        
        data = zeros(outSize, dsetClass);
        if opts.asCategorical; data = cats(data + 1); end
        
        for curIdx = 1:numel(uniChunkIds)
            curSubsRef = repelem({':'}, numel(dsetSize) - 1);
            curSubsRef = [curSubsRef, sortIds(curIdx)]; %#ok
            
            data(curSubsRef{:}) = chunkedData{curIdx};
            chunkedData{curIdx} = [];
        end
    else
        data = zeros(outSize, dsetClass);
        if opts.asCategorical; data = cats(data + 1); end
        
        for curIdx = 1:numel(uniChunkIds)
            curData = forChunk( ...
                dsetId, dsetSpaceId, dsetSize, ...
                chunkSize, uniChunkIds(curIdx), ids{curIdx});
            if opts.asCategorical; curData = cats(curData + 1); end
            
            curSubsRef = repelem({':'}, numel(dsetSize) - 1);
            curSubsRef = [curSubsRef, sortIds(curIdx)]; %#ok
            data(curSubsRef{:}) = curData;
        end
    end
end
    
function data = forChunk( ...
        dsetId, dsetSpaceId, ...
        dsetSize, chunkSize, chunkId, ids)
    start = (chunkId - 1) * chunkSize + 1;
    count = min((chunkId + 1) * chunkSize, dsetSize(end));
    count = count - start + 1;

    start = [ones(1, numel(dsetSize) - 1), start];
    count = [dsetSize(1:(end - 1)), count];
    
    % To zero-based C order
    start = flip(start) - 1;
    count = flip(count);
    
    memSpaceId = ...
        H5S.create_simple(numel(count), count, []);
    H5S.select_hyperslab(dsetSpaceId, ...
        'H5S_SELECT_SET', start, [], [], count);
    data = H5D.read(dsetId, 'H5ML_DEFAULT', ...
        memSpaceId, dsetSpaceId, 'H5P_DEFAULT');

    subsRef = ids - (chunkId - 1) * chunkSize;
    subsRef = [repelem({':'}, numel(dsetSize) - 1), {subsRef}];
    data = data(subsRef{:});
end
