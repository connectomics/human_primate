function makeWKAggloFileMappingFromAgglos(agglos, maxSegId, name, folder, outType, gitInfo)
% makeWKAggloFileMapping(agglos, maxSegId, name, folder)
%
% Create an agglo file for a webknossos mapping of segments.
% (cf.
% https://discuss.webknossos.org/t/mapped-segmentation-using-on-disk-lookup-table/1565/18)
%
% INPUT agglo: [Nx1] cell-array. Each an equivalence class of global
%           segment IDs that will be mapped to one color.
%       maxSegId: int. Highest segment Id for mapping.
%       name: String specifying the mapping name (without .hdf5 extension).
%       folder: string
%           The folder where the output file is saved.
%       outType: string/char (optional). Default: uint64
%       gitInfo: struct (Output of Util.runInfo, optional). If not
%          provided, Util.runInfo(false) will be called in the context of
%          the caller.
%


if ~exist('outType', 'var') || isempty(outType)
    outType = 'uint64';
end

if ~exist('gitInfo', 'var') || isempty(gitInfo)
    gitInfo = evalin('caller', 'Util.runInfo(false)');
end

% build segLUT only for range of nonzero segIds
minNonzeroSegId = min(cellfun(@min, agglos));
maxNonzeroSegId = max(cellfun(@max, agglos));
segIdOffset = minNonzeroSegId - 1;
assert(segIdOffset >= 0);
agglos = cellfun(@(x) x - segIdOffset, agglos, 'uni', false);
segLUT = Agglo.buildLUT(maxNonzeroSegId - segIdOffset, agglos);

WK.makeWKAggloFileMappingFromLUT( ...
    segLUT, name, folder, false, outType, gitInfo, ...
    minNonzeroSegId, maxSegId);

end