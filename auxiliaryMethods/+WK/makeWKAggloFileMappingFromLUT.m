function makeWKAggloFileMappingFromLUT(segLUT, name, folder, zeroBasedLUT, outType, gitInfo, minNonzeroSegId, maxSegId)
% makeWKAggloFileMappingFromLUT(segLUT, name, folder)
%
% Create an agglo file for a webknossos mapping of segments.
% (cf.
% https://discuss.webknossos.org/t/mapped-segmentation-using-on-disk-lookup-table/1565/18)
%
% INPUT segLUT: [Nx1] array. 
%           segLUT(segId) = aggloId if not zeroBaseLUT, else
%           segLUT(segId + 1) = aggloId
%       name: String specifying the mapping name (without .hdf5 extension).
%       folder: string/char
%          The folder where the output file is saved.
%       zeroBasedLUT: logical (optional).
%          Whether provided segLUT is 0-based (i.e.
%          segLUT(1) contains mapping for segId 0. Note that webknossos 
%          expects this mapping, which is why we have to convert segLUT, if 
%          zeroBasedLUT is set to false (default: false).
%       outType: string/char (optional). Default: uint64
%       gitInfo: struct (Output of Util.runInfo, optional). If not
%          provided, Util.runInfo(false) will be called in the context of
%          the caller.
%       minNonzeroSegId: if segLUT is only constructed starting at the
%                        first nonzero entry => set minNonzeroSegId to this
%                        entry. 
%                        Defaults to 1 if ~zeroBasedLUT and 0 otherwise
%       maxSegId:        if segLUT is only constructed up to the last
%                        nonzero entry, maxSegId makes sure to create a
%                        large enough h5 dataset where segIds beyond the
%                        segLUT up to maxSegId are mapped to 0.
%

if ~exist('zeroBasedLUT', 'var')
    zeroBasedLUT = false;
end


if ~exist('outType', 'var') || isempty(outType)
    outType = 'uint64';
end

if ~exist('gitInfo', 'var') || isempty(gitInfo)
    gitInfo = evalin('caller', 'Util.runInfo(false)');
end

if ~exist('minNonzeroSegId', 'var') || isempty(minNonzeroSegId)
    minNonzeroSegId = 1;
    if zeroBasedLUT
        minNonzeroSegId = 0;
    end
else
    assert(~zeroBasedLUT);
end

segLUT = reshape(segLUT, [], 1);
if ~exist('maxSegId', 'var') || isempty(maxSegId)
    maxSegId = size(segLUT, 1);
end

% Store to aggFile
mapFPath = fullfile(folder, name);
curFile = [mapFPath '.hdf5'];

seg2agg = '/segment_to_agglomerate';
dsetSize = maxSegId + ~zeroBasedLUT;
h5create(curFile, seg2agg, dsetSize, 'Datatype', outType);
h5write(curFile, seg2agg, segLUT, minNonzeroSegId + 1, numel(segLUT));
Util.protect(curFile);

% Store gitinfo
curFile = [name '_gitInfo.mat'];
curFile = fullfile(folder, curFile);
Util.save(curFile, gitInfo);
Util.protect(curFile);

end

