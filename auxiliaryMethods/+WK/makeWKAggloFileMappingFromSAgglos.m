function makeWKAggloFileMappingFromSAgglos(sagglos, name, folder, outType, gitInfo)
% makeWKAggloFileMapping(agglos, maxSegId, name, folder)
%
% Create an agglo file for a webknossos mapping of segments.
% (cf.
% https://discuss.webknossos.org/t/mapped-segmentation-using-on-disk-lookup-table/1565/18)
%
% INPUT sagglo: struct/char/string (super agglomerates)
%           Either filepath or struct with fields axons and indBigAxons
%       name: String specifying the mapping name (without .hdf5 extension).
%       folder: string
%           The folder where the output file is saved.
%       outType: string/char (optional). Default: uint64
%       gitInfo: struct (Output of Util.runInfo, optional). If not
%          provided, Util.runInfo(false) will be called in the context of
%          the caller.
%


if ~exist('outType', 'var') || isempty(outType)
    outType = 'uint64';
end

if ~exist('gitInfo', 'var') || isempty(gitInfo)
    gitInfo = evalin('caller', 'Util.runInfo(false)');
end

if ischar(sagglos) || isstring(sagglos)
    assert(strcmp(sagglos(end-3:end), '.mat'));
    sagglos = load(sagglos);
    assert(isfield(sagglos, 'axons'));
end

% Get maxSegId
allSegIds = Util.cellVertCat(arrayfun( ...
    @(x) x.nodes(:, 4), ...
    sagglos.axons, ...
    'uni', false));
allSegIds = allSegIds(~isnan(allSegIds));
maxSegId = max(allSegIds);

% Apply indBigAxons mask if present
saggloMask = ones(size(sagglos.axons), 'logical');
if isfield(sagglos, 'indBigAxons')
    saggloMask = sagglos.indBigAxons;
end
sagglos = sagglos.axons(saggloMask);

% Pull out segIds and create mapping file
agglos = Agglo.fromSuperAgglo(sagglos);
WK.makeWKAggloFileMappingFromAgglos( ...
    agglos, ...
    maxSegId, ...
    name, folder, ...
    outType, ...
    gitInfo);

end