function s = modifyStruct( s, varargin )
%MODIFYSTRUCT Modify struct fields by name/value pairs.
% INPUT s: A scalar struct.
%       varargin: 1 of 2 options:
%           1) An arbitrary number of name/value pairs which will be
%              added to the struct s (similar to the constructor of struct).
%              If a fieldname already exists than its value is overritten.
%           2) a single input of type struct: will be used to recursively
%              update s (via Util.updateDefaultStruct)
% 
% OUTPUT s: struct
%           Modified input struct s.
% Authors: 
%   Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

nArgs = length(varargin);
if nArgs == 1 && isstruct(varargin{1})
    s = Util.updateDefaultStruct(s, varargin{1});
    return;
elseif floor(nArgs/2) ~= nArgs/2
    error('Optional arguments have to be name/value pairs');
end

for pair = reshape(varargin,2,[])
    name = pair{1};
    s.(name) = pair{2};
end

end

