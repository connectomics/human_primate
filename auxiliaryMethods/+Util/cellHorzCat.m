function [cellCatOut] = cellHorzCat(cellIn)
% [cellCatOut] = cellHorzCat(cellIn)
% 
% cf. docs for Util.cellCat(1, cellIn)
%
% Written by
%   Martin Schmidt <martin.schmidt@brain.mpg.de>
cellCatOut = Util.cellCat(2, cellIn);

end

