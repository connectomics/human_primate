function found_paths = findFilesOrFolders(paths, throw_error)
% corr_path = findFileOrFolder(path)
%   Checks if path exists as file or folder; if not: tries to call
%   Util.getGabaPath and checks whether it can then be found
%
%   INPUT paths     : cell array of char arrays with paths 
%                        or char array with path 
%         throw_error
%                   : bool - whether to throw an error if path is not found
%                        if false, returns bool when path is not found
%                        defaults to true
%
% Author:
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

if ~exist('throw_error', 'var') || isempty(throw_error)
    throw_error = true;
end

% Transform single path to iterable cell array and store original shape
% info
single_path = false;
if ischar(paths)
    single_path = true;
    paths = {paths};
end
origShape = size(paths);
paths = reshape(paths, [], numel(paths));

% check if paths exist as file or folder, if not - correct path to be a
% path to gaba and check if that exists
for k = 1:numel(paths)
    curr_path = paths{k};
    if strcmp(curr_path, '')
        if throw_error
            error(['Provided path was empty.']);
        end
        paths{k} = false;
        continue;
    end
    if ~exist(curr_path, 'dir') && ~exist(curr_path, 'file')
        % check if it can be found on gaba
        curr_path = Util.getGabaPath(curr_path);
        if ~exist(curr_path, 'dir') && ~exist(curr_path, 'file')
            if throw_error
                error([ ...
                    'Could neither find ' newline ...
                    paths{k} newline ...
                    ' nor ' newline ...
                    curr_path])
            else
                paths{k} = false;
            end
        else
            paths{k} = curr_path;
        end
    end
end

% revert process of packing paths into linearly iterable cell array
paths = reshape(paths, origShape);
if single_path
    paths = paths{1};
end

found_paths = paths;

end

