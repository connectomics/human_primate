function progressBar(act_step, tot_step, varargin)
%PROGRESSBAR(act_step,tot_step) prints the remaining time in a
%computation when act_step steps out of tot_steps have been made.
%
% A "tic" must have been called at the beginnig of the computation. This
% code must be called at the end of the step act_step (and not at the
% beginning).
%
% To reduce the computaton overhead, the code will only be active if
% floor(percentage) has changed in the last step (this can easy be
% removed by deleting the first 'if' condition), or if 
% minUpdateInterval seconds have passed since the last update.
%
% Original version by Nicolas Le Roux <lerouxni@iro.umontreal.ca> (2005)
% Adaptions made by: Thomas Kipf <thomas.kipf@brain.mpg.de> (2015)
% Fix for Windows by: Marcel Beining <marcel.beining@brain.mpg.de> (2017)
% Append additional string(s): Martin Schmidt <martin.schmidt@brain.mpg.de> (2021)
% Increase number of decimals of percentage, if ETA > 30 minutes &
% Also update if minUpdateInterval has passed since last update:
%     Martin Schmidt <martin.schmidt@brain.mpg.de> (2021)

persistent nchar timerPbarUpdate nDecimals

minUpdateInterval = 10; % Max interval between updates in seconds
minETAForDecimalPoint = 30*60; % < ETA => add decimal point
if isempty(nDecimals)
    nDecimals = 0;
end


if ~isempty(timerPbarUpdate)
    dtPbarUpdate = toc(timerPbarUpdate);
else
    dtPbarUpdate = 0;
    timerPbarUpdate = tic();
end

% Percentage completed
nDecimalsFactor = 10 .^ nDecimals;
old_perc_complete = floor(100 * nDecimalsFactor * (act_step-1) / tot_step) / nDecimalsFactor;
perc_complete = floor(100 * nDecimalsFactor * act_step / tot_step) / nDecimalsFactor;

if old_perc_complete == perc_complete && dtPbarUpdate < minUpdateInterval
else
    
    % Time spent so far
    time_spent = toc;
    
    % Estimated time per step
    est_time_per_step = time_spent/act_step;
    
    % Estimated remaining time. tot_step - act_step steps are still to make
    est_rem_time = (tot_step - act_step)*est_time_per_step;
    str_steps = [' ' num2str(act_step) '/' num2str(tot_step)];

    if nDecimals == 0 && est_rem_time > minETAForDecimalPoint
        nDecimals = 1;
    end
    
    % Correctly print the remaining time
    if (floor(est_rem_time/60) >= 1)
        str_time = ...
            [' ETA: ' num2str(floor(est_rem_time/60)) 'm ' ...
            num2str(floor(rem(est_rem_time,60))) 's'];
    else
        str_time = ...
            [' ETA: ' num2str(floor(rem(est_rem_time,60))) 's'];
    end
    
    % Create the string [***** x    ] act_step/tot_step (1:10:36)
    str_pb = progress_bar(perc_complete);
    str_tot = strcat(str_pb, str_steps, str_time);

    % Add additional string(s) to be printed
    if nargin > 2
        str_tot = strcat(str_tot, varargin{:});
    end
    
    % Print it
    print_same_line(str_tot,nchar);
    nchar = numel(str_tot)-1; % save string element number for overwriting next time (windows)
    timerPbarUpdate = tic();
end

if act_step == tot_step
    fprintf('\n');
    fprintf('Total running time: %f seconds. \n', toc);
    nchar = []; % make nchar empty again if function is called second time
    dtPbarUpdate = [];
    nDecimals = [];
end

end


function str_pb = progress_bar(percentage)

str_perc = [' ' num2str(percentage) '%% '];

if percentage < 49
    str_o = char(ones(1, floor(percentage/2))*61);
    str_dots_beg = char(ones(1, max(0, 24 - floor(percentage/2)))*46);
    str_dots_end = char([32 ones(1, 24)*46]);
    str_pb = strcat('[', str_o, str_dots_beg, str_perc, str_dots_end, ']');
else
    str_o_beg = char(ones(1, 24)*61);
    str_o_end = char([32 ones(1, max(0, floor((percentage-50)/2)))*61]);
    str_dots = char(ones(1, 50 - floor(percentage/2))*46);
    str_pb = strcat('[', str_o_beg, str_perc, str_o_end, str_dots, ']');
end

end

function print_same_line(string, nchar)
% Modified from code by Les Schaffer (lerouxni@iro.umontreal.ca)

% NOTE(amotta): When using the MATLAB GUI, standard output (stdout) is
% shown in the command window. The command window is supposedly backed by a
% Java `JTextArea`, which ignores "regular" escape sequences.
%
% Thus, escape codes can only be used
% * when running on a UNIX-like system (macOS or Linux) AND
% * when the MATLAB GUI was disabled / Java runs in headless mode.

% NOTE(amotta): When logging the output to a file, this print-on-same-line
% business is pretty bad; it only bloats the file. As a workaroung, let's
% temporarily disable logging.
prevDiaryState = get(0, 'Diary');
diary('off');

if (ismac || isunix) && not(usejava('awt'))
    escape = char(27);
    % clear to end of line - ESC [2K
    er = [ escape '[' '2' 'K' ];
    % goto beginning of line - ESC [1G
    bl = [ escape '[' '1' 'G' ];
    fprintf(strcat(bl, er, string));
else
    delete = '';
    if not(isempty(nchar))
        delete = char(8); % backspace
        delete = repelem(delete, nchar);
    end
    fprintf(strcat(delete, string));
end

diary(prevDiaryState);

end
