function opt = setUserOptions( optDef, optUser, forceExist )
%SETUSEROPTIONS Modify a struct by fields given in a second struct. If a
%field in defOpt does not exist this throws an error.
% INPUT optDef: struct
%       optUser: struct
%           The fields are used to overwrite the fields in optDef. If a
%           field is not found in opt def an error is thrown.
%       forceExist: (Optional) logical
%           Flag to specify that only fields existing in optDef can be
%           changed but not new ones added.
%           (Default: true)
% OUTPUT opt: struct
%           The modified optDef struct.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('forceExist', 'var') || isempty(forceExist)
    forceExist = true;
end

fnamesDef = fieldnames(optDef);
fnamesUser = fieldnames(optUser);

idx = ~ismember(fnamesUser, fnamesDef);
if any(idx) && forceExist
    error('Unrecognized option ''%s''.\n', fnamesUser{find(idx, 1)});
end

for i = 1:length(fnamesUser)
    optDef.(fnamesUser{i}) = optUser.(fnamesUser{i});
end
opt = optDef;

end

