function out = repelem(v, varargin)
    % out = repelem(v, varargin)
    %   This is a "safe" variant of MATLAB's `repelem` function that also
    %   works in case all inputs are empty.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if isempty(v)
        assert(all(cellfun(@isempty, varargin)));
        % HACKHACKHACK(amotta): MATLAB's `repelem` function is degenerate.
        % It throws an error if n is empty. That's why this workaround here
        % is required.
        out = zeros(size(v), 'like', v);
        return;
    end
    
    % Fallback to "regular" function
    out = repelem(v, varargin{:});
end
