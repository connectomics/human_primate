function bboxMat = convertWebknossosToMatlabBbox(bboxWeb)
% bboxMat = convertWebknossosToMatlabBbox(bboxWeb)
% Convert bounding box used in webKnossos to the one used in e.g. 
% readKnossoRoi, pipeline repo etc. Works for a single, as well as a batch
% of bounding boxes.
%  Args:
%   bboxWeb: [6x1] or [Nx6] Webknossos format of 1 or N bounding boxes
%  Out:
%   bboxMat: [3x2] or [Nx3x2] Matlab format of bounding box
% Authors:
%    Manuel Berning <manuel.berning@brain.mpg.de>
%    Marcel Beining <marcel.beining@brain.mpg.de>
%    Ali Karimi <ali.karimi@brain.mpg.de>
%    Martin Schmidt <martin.schmidt@brian.mpg.de>

if any(size(bboxWeb)==6)
    bboxWeb = squeeze(bboxWeb);
    assert(sum(size(bboxWeb) > 1) <= 2);
    bboxAxis = find(size(bboxWeb) == 6, 1, 'last');
    batchAxis = setdiff([1, 2], bboxAxis);
    bboxWeb = permute(bboxWeb, [batchAxis, bboxAxis]);

    batchDim = size(bboxWeb, 1);
    bboxMat = reshape(bboxWeb, batchDim, 3, 2);
    bboxMat(:, :, 1) = bboxMat(:, :, 1) + 1;
    bboxMat(:, :, 2) = bboxMat(:, :, 1) + bboxMat(:, :, 2) - 1;         
    bboxMat = squeeze(bboxMat);
elseif all(ismember([3, 2], size(bboxWeb)))
    disp('Bounding box already in Matlab format. Nothing is done')
    bboxMat = bboxWeb;
else
    error('Unknown bbox format')
end

end

