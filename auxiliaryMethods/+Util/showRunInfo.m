function infoStr = showRunInfo(info)
    % Parse info struct into string. If no return arg is 
    % specified, info string is printed, otherwise returned.
    % 
    % 
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    %   Martin Schmidt <martin.schmidt@brain.mpg.de>
    isDirtyStr = {'', ' (dirty)'};
    
    infoStr = sprintf('%s\n', info.filename);
    
    for curRepoId = 1:numel(info.git_repos)
        curRepo = info.git_repos{curRepoId};
        curDirty = ~isempty(curRepo.diff);
        curDirty = isDirtyStr{1 + curDirty};
        
        infoStr = [ ...
            infoStr, ...
            sprintf('%s %s%s\n', curRepo.remote, curRepo.hash, curDirty)];
    end
    
    infoStr = [ ...
        infoStr, ...
        sprintf('%s@%s. MATLAB %s. %s', ...
            info.user, info.hostname, ...
            info.matlab_version, info.time)];
    if nargout == 0
        fprintf(infoStr);
	fprintf('\n\n');
    end
end
