function snake = camelToSnakeCase(camel, separator)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    if not(exist('separator', 'var'))
        separator = '_';
    end

    assert(ischar(separator));

    camel(1) = lower(camel(1));
    lens = find(camel == upper(camel));
    lens = [lens, numel(camel) + 1];
    lens = [lens(1) - 1, diff(lens)];

    snake = mat2cell(camel, 1, lens);
    snake = cellfun(@lower, snake, 'UniformOutput', false);
    snake = strjoin(snake, separator);
end
