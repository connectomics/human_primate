function [avail_mags, requested_mag, wkw_basepath] = getAvailWKWMags(path)
% varargout = getAvailWKWMags(path)
%   Description:
%         This function takes a path to a wkw dataset (i.e. either the 
%         basepath containing the resolution pyramid or one of the
%         resolution subfolders) and checks which magnifications (i.e.
%         which magnification folders) of the X[-Y-Z] format are available.
%         Note that it does not check whether there is also a wkw header
%         file in each subfolder!
%
%   INPUT path:          path to check for wkw resolutions
%         
%   OUTPUT
%         Lia:           logical : is each element of A a member of B
%         [Lia, Locb]:   Locb: index location of each element of A in B,
%                              nonmembers are set to 0
%           
% Author:
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

% Get rid of last fileseparator if there is one
wkw_path = path(1:end-endsWith(path, {'/', '\', filesep}));

% Extract last_folder_name and wkw_basepath
[wkw_basepath, last_folder_name, ext] = fileparts(wkw_path);
assert(isempty(ext));

% Check if last_folder_name matches mag regexp
re_mag_pattern = '(\d+(\-\d+\-\d+)?)';
last_folder_is_mag = numel(regexp( ...
    last_folder_name, ...
    re_mag_pattern, ...
    'match')) == 1;

requested_mag = [];
if ~last_folder_is_mag
    wkw_basepath = wkw_path;
else
    requested_mag = ...
        uint32(sscanf(regexprep(last_folder_name, '-', ' '), '%d')');
end

mag_folders = Util.regexpdir(wkw_basepath, re_mag_pattern, 'd');
assert(numel(mag_folders) > 0)
avail_mags = cellfun( ...
    @(x) uint32(sscanf(regexprep(x, '-', ' '), '%d')'), ...
    mag_folders, ...
    'uni', false);

% Find out sorting
avail_mags_single = cellfun(@(x) numel(x) == 1, avail_mags);
avail_mags_regular = avail_mags;
avail_mags_regular(avail_mags_single) = cellfun( ...
    @(x) repmat(x, 1, 3), ...
    avail_mags_regular(avail_mags_single), 'uni', false);
avail_mags_regular = vertcat(avail_mags_regular{:});
[~, srtdIdcs] = sortrows(avail_mags_regular);

avail_mags = avail_mags(srtdIdcs);

end

