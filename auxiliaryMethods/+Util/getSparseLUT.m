function sLUT = getSparseLUT(idcs, vals, sz)
% This function creates a sparse 1D matrix 'sLUT' with
% sLUT(idcs(i)) = vals(i), and 0 for every idx not in idcs.
% (Mainly convenience wrapper function around matlab's 'sparse'
%  builtin, in order to create a 1D matrix)
% 
% Input:
%   idcs [Nx1]: idcs of the lookup table
%   vals [Nx1]: values of the lookup table
%               (indexing with indices not in idcs returns 0)
%   sz   [1]  : (optional) size of sparse matrix
%               (if indexing beyond max(idcs) with return of 0 is required)
%
% Author:
%  Martin Schmidt <martin.schmidt@brain.mpg.de>
% 

    numEntries = union([numel(idcs), numel(vals)], [length(idcs), length(vals)]);
    assert(numel(numEntries) == 1);
    if ~(isa(vals, 'logical') | isa(vals, 'double'))
         error('vals need to be either logical or double');
    end

    if ~exist('sz', 'var') || isempty(sz)
        sz = max(idcs);
    else
        assert(sz >= max(idcs));
    end

    sLUT = sparse(double(idcs), ones(size(idcs)), vals, double(sz), 1);

end
