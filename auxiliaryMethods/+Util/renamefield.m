function struct = renamefield(struct, oldNames, newNames)
    % struct = renamefield(struct, oldFieldNames, newFieldNames)
    %   This is a sane alternative to MATLAB's `renameStructField` that
    %   also works when the structure is a non-scalar structure array.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    if not(iscell(oldNames)); oldNames = {oldNames}; end
    if not(iscell(newNames)); newNames = {newNames}; end
    
    assert(not(any(ismember(newNames, oldNames))));
    assert(not(any(ismember(newNames, fieldnames(struct)))));
    assert(isequal(numel(oldNames), numel(newNames)));
    
    for idx = 1:numel(oldNames)
       [struct.(newNames{idx})] = deal(struct.(oldNames{idx}));
        struct = rmfield(struct, oldNames{idx});
    end
end
