function saveStruct(fileName, toSave, varargin)
    % Utility function for saving a structure to MAT file. It automatically
    % changes to V7.3 MAT files, if necessary.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.append = false;
    opts.compression = false;
    opts = Util.modifyStruct(opts, varargin{:});

    args = {};
    sizes = structfun(@byteSizeOf, toSave);
    
    if opts.append
        assert(logical(exist(fileName, 'file')));
        version = getVersion(fileName);
        args{end + 1} = '-append';
    else
        version = '7';
        if any(sizes >= 2 * 1024 * 1024 * 1024)
            % Only v7.3 supports variables larger than 2 GB
            version = '7.3';
        end
        
        args{end + 1} = sprintf('-v%s', version);
    end
    
    canDisableCompression = strcmp(version, '7.3');
    if not(opts.compression) && canDisableCompression
        % NOTE(amotta): Compression is on by default. MATLAB does not (as
        % far as I can tell) provide a flag to explicitly requestion
        % compression. We can only disable it.
        args{end + 1} = '-nocompression';
    end

    save(fileName, '-struct', 'toSave', '-mat', args{:});
end

function v = getVersion(fileName)
    fid = fopen(fileName, 'r');
    header = fread(fid, 10, 'char=>char');
    header = reshape(header, 1, []);
    fclose(fid);
    
    switch header
        % HACKHACKHACK
        case 'MATLAB 5.0'; v = '7';
        case 'MATLAB 7.3'; v = '7.3';
        otherwise; v = '4';
    end
end

function byteSize = byteSizeOf(in) %#ok
    byteSize = whos('in');
    byteSize = byteSize.bytes;
end
