function varargout = ismember(A, B, varargin)
% varargout = ismember(A, B)
%   Description:
%         This function mimmicks Matlab's ismember function but solves the
%         following shortcoming: if A and/or B are cells, Matlab's ismember
%         function will only work, if the cells contain strings;
%         In contrast: this function works for anything that can be run
%         through Matlab's isequal function (so works for numeric, as well
%         and char data, as well as any kind of mixtures)
%
%   INPUT A:             element or list of elements for which the
%                        membership to B should be checked
%         B:             element or list of elements
%         
%   OUTPUT
%         Lia:           logical : is each element of A a member of B
%         [Lia, Locb]:   Locb: index location of each element of A in B,
%                              nonmembers are set to 0
%           
% Author:
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

% Initialize output cell; Note that this function always has at least 1
% output (even if nargout == 0)
varargout = cell(1, max(1, nargout));

% Use Matlab's ismember function, whenever possible
% (Note that in principle the function here should also work for the cell
%  cases, but potentially Matlab's ismember function is faster - so use it
%  whenever possible)
validMatIsmemberInput = ...
    ((isstring(A) | ischar(A) | iscellstr(A)) & ... 
     (isstring(B) | ischar(B) | iscellstr(B))) | ...
     (~iscell(A) & ~iscell(B));
if validMatIsmemberInput
    [varargout{:}] = ismember(A, B, varargin{:});
    return;
else
    if any(strcmp(varargin, 'rows'))
        warning("The 'rows' input is not supported for cell array inputs.");
    end
end

% Otherwise use custom function via isequal to determine membership of
% potentially mixed datatypes and/or mixed sizes

% First make sure that A and B are cells
if ~iscell(A)
    A = {A};
end

if ~iscell(B)
    B = {B};
end

% Determine for each element in A whether it is a member of B
Lia = cellfun(@(a) any(cellfun(@(b) isequal(a, b), B)), A);
varargout{1} = Lia;

% If 2 output arguments are requested:
% > find idx of first occurrence of each element in A
% > if element of A is not in B, Locb is set to 0
if nargout == 2
    Locb = zeros(size(Lia));
    Locb(Lia) = cellfun( ...
        @(a) find(cellfun(@(b) isequal(a, b), B), 1, 'first'), ...
        A(Lia));
    varargout{2} = Locb;
end


end

