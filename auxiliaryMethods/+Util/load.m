function varargout = load(fileName, varargin)
    % Utility for loading a subset of variables from a MAT file and
    % returning them as function output arguments.
    %
    % Example
    %   [edges, borders] = Util.load('data.mat', 'edges', 'borders');
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    assert(nargout == numel(varargin));
    assert(all(cellfun(@ischar, varargin)));
    assert(not(any(cellfun(@isempty, varargin))));
    
    % Prevent user from specifying weird options to `load`.
    assert(not(any(cellfun(@(n) n(1) == '-', varargin))));
    
    warn = warning('off', 'MATLAB:load:variableNotFound');
    data = load(fileName, '-mat', varargin{:});
    warning(warn);
    
    varargout = cellfun( ...
        @(varName) data.(varName), ...
        varargin, 'UniformOutput', false);
end
