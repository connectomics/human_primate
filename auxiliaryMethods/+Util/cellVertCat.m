function [cellCatOut] = cellVertCat(cellIn)
% [cellCatOut] = cellVertCat(cellIn)
% 
% cf. docs for Util.cellCat(2, cellIn)
%
% Written by
%   Martin Schmidt <martin.schmidt@brain.mpg.de>
cellCatOut = Util.cellCat(1, cellIn);

end

