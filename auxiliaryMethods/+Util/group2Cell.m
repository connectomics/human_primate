function [ comps, groupId, groupSize ] = group2Cell( group, ...
    discardSingleGroups, rowIdx )
%GROUP2CELL Combine indices of the same group in a cell array.
% INPUT group: nd int array
%           Integer group id for the respective index.
%       discardSingleGroups: (Optional) logical
%           Flag to discard groups consisting of one element only.
%           (Default: false)
%       rowIdx: (Optional) logical
%           Flag indicating that only the row indices of a [NxM] int group
%           input are grouped.
%           (Default: false)
% OUTPUT comps: [Nx1] cell
%           Cell array of length max(group). Each cell contains all indices
%           of one group. Groups are sorted in increasing order.
%        groupId: [Nx1] int
%           Id of the respective group in comps.
%        groupSize: [Nx1] int
%           The number of elements in the corresponding comps entry.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

if ~exist('rowIdx', 'var') || isempty(rowIdx)
    rowIdx = false;
end

[sGroup, idx] = sort(group(:));

if rowIdx
    [idx, ~] = ind2sub(size(group), idx);
end

groupCount = 0;
if ~isempty(sGroup)
    groupCount = sGroup(end);
end

groupSize = accumarray(sGroup(:), 1, [groupCount, 1]);

if exist('discardSingleGroups', 'var') && discardSingleGroups
    %delete single groups
    toDelGroup = groupSize == 1;
    groupSize(toDelGroup) = 0;
    toDel = toDelGroup(sGroup);
    sGroup = sGroup(~toDel);
    idx = idx(~toDel);
end

groupId = unique(sGroup);
groupSize = groupSize(groupId);
comps = mat2cell(idx, groupSize, 1);

if rowIdx
    comps = cellfun(@unique, comps, 'UniformOutput', false);
end

end

