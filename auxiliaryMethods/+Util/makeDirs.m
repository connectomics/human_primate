function varargout = makeDirs(varargin)
% varargout = makeDirs(varargin)
%   
% Creates all provided directories.
%
% Input arguments
%   varargin: cell(s) of paths, and/or comma separated chars/strings
%       Paths should not contain filenames!
%
% Output arguments
%     vectorized varargout of mkdir function (cf. docs for mkdir)
%
% Written by
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

allDirs = varargin';
tmpMask = cellfun(@iscell, allDirs);
allDirs(tmpMask) = cellfun(@(x) x(:), allDirs(tmpMask), 'uni', false);
allDirs(~tmpMask) = cellfun(@(x) {x}, allDirs(~tmpMask), 'uni', false);
allDirs = Util.cellVertCat(allDirs);

for k = 1:numel(allDirs)
    curDir = allDirs{k};
    curOuts = cell(nargout, 1);
    [curOuts{:}] = mkdir(curDir);
    for l = 1:nargout
        if k == 1
            varargout{l} = cell(numel(allDirs), 1);
        end
        varargout{l}{k} = curOuts{l};
    end
end

end