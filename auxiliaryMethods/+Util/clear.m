function clear(varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>    
    varNames = arrayfun(@inputname, 1:nargin, 'UniformOutput', false);
    emptyMask = cellfun(@isempty, varNames);
    
    if any(emptyMask)
        emptyIdx = find(emptyMask, 1);
        error('Input %d is not a variable', emptyIdx);
    end
    
    expression = [{'clear'}, varNames];
    expression = strjoin(expression);
    
    evalin('caller', expression);
end
