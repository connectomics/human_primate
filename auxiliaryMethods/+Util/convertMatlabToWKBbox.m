function [ bboxWeb ] = convertMatlabToWKBbox( bboxMat, verbose )
% bboxWeb = convertMatlabToWKBbox(bboxMat, verbose)
%  Args:
%   bboxMat: [3x2] or [Nx3x2] Matlab format of bounding box
%   verbose: bool - whether to print the bbox. Defaults to true
%  Out:
%   bboxWeb: [Nx6] Webknossos format of bounding box
% Authors:
%    Ali Karimi <ali.karimi@brain.mpg.de>
%    Martin Schmidt <martin.schmidt@brian.mpg.de>

if ~exist('verbose', 'var') || isempty(verbose)
     verbose = true;
end

% Find (if there is a) batch axis
if sum(size(bboxMat) > 1) == 3
    batchDim = size(bboxMat, 1);
    assert(all(size(bboxMat) == [batchDim, 3, 2]));
else
    bboxMat = squeeze(bboxMat);
    batchDim = 1;
    bboxMat = reshape(bboxMat, horzcat(1, size(bboxMat)));
end

% Convert bounding box used in Matlab to the one used in Webknossos
bboxWeb = zeros(batchDim,6);
bboxWeb(:, 1:3) = bboxMat(:, :, 1) - 1;    
bboxWeb(:, 4:6) = bboxMat(:, :, 2) - bboxMat(:, :, 1) + 1;

if verbose
    for k = 1:batchDim
        fprintf('%u,%u,%u,%u,%u,%u\n', bboxWeb(k, :));
    end
end

end

