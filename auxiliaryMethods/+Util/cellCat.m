function [cellCatOut] = cellCat(dim, cellIn)
% [cCatOut] = cellCat(dim, cellIn)
%   
% Takes in a cell and performs cat(dim, cellIn{:}). Allows to shorten the
% lines:
%    a = cellfun(@func, someCell, 'uni', false);
%    a = cat(1, a{:});
% to:
%    a = Util.cellCat(1, cellfun(@func, someCell));
%
% Input arguments
%    dim
%        dimension along which to concatenate
%     
%    cellIn
%        cell to concatenate
%
% Written by
%   Martin Schmidt <martin.schmidt@brain.mpg.de>
cellCatOut = cat(dim, cellIn{:});

end

