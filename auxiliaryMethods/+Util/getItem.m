function items = getItem(A, idcs, dtype)
% item = getItem(A, idcs, dtype)
% Returns content of A at index idx irrespective of whether we have a cell or
% a normal array. Currently only implemented for 1D indexing.
%
% Input:
%   A: N-D (cell) array   | 
%   idcs: 1-D index array | (optional) Defaults to all indices.
%         (or alternatively: logical array)
%   dtype: char | (optional) cast to specified dtype
%
% Output:
%   items: if A is array: items = A(idcs), if A is cell: items = [A{idcs}].
%
% Written by
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

if ~exist('idcs', 'var') || isempty(idcs)
    idcs = (1:numel(A))';
end

if islogical(idcs)
    idcs = find(idcs);
end

assert(max(idcs) <= numel(A));

if iscell(A)
    tmp = A(idcs);
    if exist('dtype', 'var') && ~isempty(dtype)
        tmp = cellfun(@(curVal) cast(curVal, dtype), tmp, 'uni', false);
    end
    items = [tmp{:}]';
else
    items = A(idcs);
    if exist('dtype', 'var') && ~isempty(dtype)
        items = cast(items, dtype);
    end
end

end
