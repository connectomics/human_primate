function box = getBoundingBox(nml, boxTypes)
    % box = getBoundingBox(nml, boxType)
    %   Returns the NML's bounding box parameter.
    %
    %   `boxTypes` specifies one or multiple bounding box types in
    %   decreasing order of priority. The default is `{'user', 'task'}`.
    %
    %   If none of the specified bounding boxes were set, the returned
    %   values are set to not-a-number (nan).
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if ~exist('boxTypes', 'var') || isempty(boxTypes)
        boxTypes = {'user', 'task'};
    elseif ~iscell(boxTypes)
        boxTypes = {boxTypes};
    end
    
    assert(iscellstr(boxTypes));
    
    box = nan(3, 2);
    nmlParams = nml.parameters;
    for curIdx = 1:numel(boxTypes)
        curType = lower(boxTypes{curIdx});
        curType = sprintf('%sBoundingBox', curType);
        
        if ~isfield(nmlParams, curType); continue; end
        box = nmlParams.(curType);
    
        box = { ...
            box.topLeftX, box.topLeftY, box.topLeftZ, ...
            box.width, box.height, box.depth};
        box = cellfun(@str2double, box);
        
        box = reshape(box, 3, 2);
        box(:, 1) = box(:, 1) + 1;
        box(:, 2) = sum(box, 2) - 1;
        
        break;
    end
end
