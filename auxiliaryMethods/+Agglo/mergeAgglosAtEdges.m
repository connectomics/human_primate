function [labels, agglosOut] = mergeWithEdges(agglos, edges, maxSegId)
    % [labels, agglos] = mergeWithEdges(param, agglos, edges)
    %   The input `agglos` denotes pairwise disjoint agglo-
    %   merates. This function looks at the list of specified
    %   edges and uses them to merge neighbouring agglome-
    %   rates.
    %
    % param
    %   Parameter structure
    %
    % agglos
    %   Pairwise disjoint agglomerates. Cell array where each
    %   entry contains a set of global segment IDs.
    % NOTE: Each agglo must be column vector
    %
    % edges
    %   N x 2 matrix where each row contains the global IDs
    %   of a pair of neighbouring segments.
    %
    % labels
    %   Column vector that has the same number of elements
    %   as `agglos`. Each entry contains the agglomerate ID
    %   after the merge operation.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    % Modified by
    %   Sahil Loomba <sahil.loomba@brain.mpg.de>
    
    % convert segment-to-segment edges into
    % agglomerate-to-agglomerate edges
    edges = buildAggloEdges(maxSegId, agglos, edges);
    
    % build connected components
    labels = Graph.buildConnectedComponents(numel(agglos)+1, edges);% temp hack, see issue in this function
    labels(end) = []; % remove extra agglo entry
    labels(not(labels)) = max(labels) + (1:sum(not(labels)));
    
    % build agglomerates
    agglosOut = accumarray( ...
        labels, 1:numel(agglos), [], ...
        @(ids) {cell2mat(agglos(ids(:)))});
end

function edges = buildAggloEdges(maxSegId, agglos, edges)
    % build segment-to-agglomereate mapping
    segToAggloMap = Agglo.buildLUT(maxSegId, agglos);
    if isrow(edges)
        edges = segToAggloMap(edges);
        assert(iscolumn(edges))
        edges = reshape(edges,1,'');
    else
        edges = segToAggloMap(edges);
        edges = sort(edges, 2);
    end
    % remove edges either of which did not have an agglo found i.e. 0
    idxDel = edges(:,1) == 0 | edges(:,2)==0;
    edges(idxDel,:) = [];
    % remove edges that are within agglomerates
    edges(edges(:,1) == edges(:,2),:) = [];
end

