function agglos = removeSegmentOverlap(agglos)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    maxSegId = agglos(not(cellfun(@isempty, agglos)));
    % NOTE(amotta): All agglos empty? We're done!
    if isempty(maxSegId); return; end
    maxSegId = max(cellfun(@max, maxSegId));

    keepLUT = true(maxSegId, 1);
    keepLUT(Agglo.segmentOverlap(agglos)) = false;
    
    agglos = cellfun( ...
        @(segIds) segIds(keepLUT(segIds)), ...
        agglos, 'UniformOutput', false);
end
