function exportAggloFileToAmira( ...
        param, aggloFilePath, aggloIds, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    agglos = AggloFile(aggloFilePath).agglomerates(aggloIds);
    Visualization.exportAggloToAmira(param, agglos, varargin{:});
    % TODO(amotta): Add agglomerate ID to output file name?
end
