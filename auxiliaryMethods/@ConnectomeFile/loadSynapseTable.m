function synT = loadSynapseTable(self, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.loadVariables = 'available';
    opts = Util.modifyStruct(opts, varargin{:});
    
    assert( ...
        iscell(opts.loadVariables) ...
     || isequal(opts.loadVariables, 'all') ...
     || isequal(opts.loadVariables, 'available'));
    
    aggloIds = [ ...
        flatten(read('/synapse_to_src_agglomerate')), ...
        flatten(read('/synapse_to_dst_agglomerate'))];
    
    synT = table;
    synT.id = flatten(1:size(aggloIds, 1));
    synT.aggloIds = aggloIds;
    
    curVar = 'segIds';
    curDset = '/synapse_segment_ids';
    if shouldLoad(curVar, curDset)
        synT.(curVar) = transpose(read(curDset));
    end
    
    curVar = 'nodeIds';
    curDset = '/synapse_node_ids';
    if shouldLoad(curVar, curDset)
        synT.(curVar) = transpose(read(curDset));
    end
    
    curVar = 'dendType';
    curDset = '/synapse_node_types';
    if shouldLoad(curVar, curDset)
        % TODO(amotta): A strided read could be more efficient.
        synT.(curVar) = transpose(readCategorical(curDset));
        synT.(curVar) = synT.(curVar)(:, 2);
    end
    
    curVar = 'spineHeadId';
    curDset = '/synapse_spine_head_ids';
    if shouldLoad(curVar, curDset)
        synT.(curVar) = flatten(read(curDset));
    end
    
    curVar = 'type';
    curDset = '/synapse_types';
    if shouldLoad(curVar, curDset)
        synT.(curVar) = flatten(readCategorical(curDset));
    end
    
    curVar = 'size';
    curDset = '/synapse_sizes';
    if shouldLoad(curVar, curDset)
        synT.(curVar) = flatten(read(curDset));
    end
    
    curVar = 'pos';
    curDset = '/synapse_positions';
    if shouldLoad(curVar, curDset)
        synT.(curVar) = transpose(read(curDset));
    end
    
    curVar = 'somaDists';
    curDset = '/synapse_to_soma_distance';
    if shouldLoad(curVar, curDset)
        synT.(curVar) = transpose(read(curDset));
    end
    
    % Utilities
    function data = read(dset)
        data = h5read(self.filePath, dset);
    end

    function data = readCategorical(dset)
        data = HDF5.readCategorical(self.filePath, dset);
    end
    
    function load = shouldLoad(var, dset)
        if ischar(opts.loadVariables)
            if strcmpi(opts.loadVariables, 'available')
                load = HDF5.hasDataset(self.filePath, dset);
            elseif stcmpi(opts.loadVariables, 'all')
                load = true;
            else
                error('Invalid input');
            end
        else
            assert(iscell(opts.loadVariables));
            load = ismember(var, opts.loadVariables);
        end
    end
end

function v = flatten(v)
    v = v(:);
end
