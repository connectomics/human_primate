classdef ConnectomeFile < handle
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    properties (SetAccess = protected)
        filePath;
    end
    
    methods
        function self = ConnectomeFile(filePath)
            self.filePath = filePath;
        end
    end
end
