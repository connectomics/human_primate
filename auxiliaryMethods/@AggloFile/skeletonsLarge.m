function skels = skeletonsLarge(self, aggloIds, voxelSize, varargin)
    % Written by
    %   Meike Sievers <meike.sievers@brain.mpg.de>
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    % Including edge affinities, MST and skelton size estimates
    opts = struct;
    opts.toDouble = true;
    opts = Util.modifyStruct(opts, varargin{:});
    
    segIdKlass = self.dsetInfos.agglomerate_to_segments.klass;
    nodeKlass = self.dsetInfos.agglomerate_to_positions.klass;
    edgeKlass = self.dsetInfos.agglomerate_to_edges.klass;
    
    if opts.toDouble
        nodeKlass = 'double';
        edgeKlass = 'double';
    end
    
    skels = struct;
    skels.segIds = zeros(0, 1, segIdKlass);
    skels.nodes = zeros(0, 3, nodeKlass);
    skels.edges = zeros(0, 2, edgeKlass);
    skels = repmat(skels, size(aggloIds));

    for curIdx = 1:numel(aggloIds)
        curAggloId = double(aggloIds(curIdx));

        % Load nodes
        curLims = '/agglomerate_to_segments_offsets';
        curLims = self.read(curLims, 1 + curAggloId, 2);

        curOff = double(curLims(1) + 1);
        curLen = double(curLims(2) - curLims(1));

        curSegIds = '/agglomerate_to_segments';
        curSegIds = self.read(curSegIds, curOff, curLen);
        curSegIds = reshape(curSegIds, [], 1);
        
        curNodes = '/agglomerate_to_positions';
        curNodes = self.read(curNodes, [1, curOff], [3, curLen]);
        if opts.toDouble; curNodes = double(curNodes); end
        curNodes = transpose(1 + curNodes);
        
        % Sanity check
        assert(isequal(size(curSegIds, 1), size(curNodes, 1)));
        skels(curIdx).segIds = curSegIds;
        skels(curIdx).nodes = curNodes;
        
        % Edges
        curLims = '/agglomerate_to_edges_offsets';
        curLims = self.read(curLims, 1 + curAggloId, 2);

        curOff = double(curLims(1) + 1);
        curLen = double(curLims(2) - curLims(1));
        
        if curLen > 0
            % NOTE(amotta): Skip loading of edges if there are none. This
            % is not just an optimization, but also a workaround MATLAB's
            % h5read throwing an error if `count` is zero.
            curEdges = '/agglomerate_to_edges';
            curEdges = self.read(curEdges, [1, curOff], [2, curLen]);
            if opts.toDouble; curEdges = double(curEdges); end
            curEdges = transpose(1 + curEdges);
            skels(curIdx).edges = curEdges;
            
            % Add edge affinities
            curAffinities = '/agglomerate_to_affinities';
            curAffinities = self.read(curAffinities, curOff, curLen);
            skels(curIdx).affinities = curAffinities;
            
            % Add mst and size estimate
            curNodesUm = double(curNodes).*voxelSize/1e3; 
            curEdgeVec = curNodesUm(curEdges(:,1), :)-curNodesUm(curEdges(:,2),:);
            curEdgeWeights = vecnorm(curEdgeVec, 2, 2);
            skelGraph = graph(curEdges(:,1), curEdges(:,2), curEdgeWeights);
            curMST = minspantree(skelGraph); 
            curMSTEdges = curMST.Edges.EndNodes;
            curMSTLength = sum(sum(curMST.Edges.Weight));
            curCuboidSize = max(curNodesUm, [], 1)- min(curNodesUm, [], 1);
            curCuboidSize = sqrt(curCuboidSize(1)^2 ...
                + curCuboidSize(2)^2 + curCuboidSize(3)^2);
            skels(curIdx).edgesMST = curMSTEdges; 
            skels(curIdx).length = curMSTLength;
            skels(curIdx).cuboid = curCuboidSize;
            
        end
    end
end
