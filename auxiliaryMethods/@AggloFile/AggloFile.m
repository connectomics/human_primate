classdef AggloFile < handle
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    properties (SetAccess = protected)
        filePath;
    end
    
    properties (Access = protected)
        fileId;
        groupId;
        dsetDict;
        dsetInfos;
    end
    
    methods
        function self = AggloFile(filePath, filePermission)
            if ~exist('filePermission', 'var') ...
                    || isempty(filePermission)
                filePermission = 'r';
            end
            
            self.filePath = filePath;
            self.dsetDict = struct();
            self.setDsetInfos();
            
            switch lower(filePermission)
                case 'r', openFlags = 'H5F_ACC_RDONLY';
                case 'w', openFlags = 'H5F_ACC_RDWR';
                otherwise, error('Unknown permission');
            end
            
            self.fileId = H5F.open(filePath, openFlags, 'H5P_DEFAULT');
            self.groupId = H5G.open(self.fileId, '/');
        end

        function data = readDataset(self, dsetName, indices)
            assert(dsetName(1) == '/');
            
            if not(exist('indices', 'var')) || isequal(indices, -1)
                % NOTE(amotta): Skip segment / agglomerate zero
                nDims = self.getDsetNDims(dsetName(2:end));
                off = ones(1, nDims); off(end) = 2;
                len = inf(1, nDims);

                data = self.read(dsetName, off, len);
            else
                indices = indices + 1;
                data = self.read(dsetName, indices);
            end
        end
    end
    
    methods (Access = protected)
        function dset = getDsetId(self, name)
            if ~isfield(self.dsetDict, name)
                self.dsetDict.(name) = struct;
            end
            
            if ~isfield(self.dsetDict.(name), 'id')
                self.dsetDict.(name).id = ...
                    H5D.open(self.groupId, name);
            end
            
            dset = self.dsetDict.(name).id;
        end
        
        function dset = getDsetSpaceId(self, name)
            if ~isfield(self.dsetDict, name)
                self.dsetDict.(name) = struct;
            end
            
            if ~isfield(self.dsetDict.(name), 'spaceId')
                self.dsetDict.(name).spaceId = ...
                    H5D.get_space(self.getDsetId(name));
            end
            
            dset = self.dsetDict.(name).spaceId;
        end

        function nDims = getDsetNDims(self, name)
            spaceId = self.getDsetSpaceId(name);

            if ~isfield(self.dsetDict.(name), 'nDims')
                self.dsetDict.(name).nDims = ...
                    H5S.get_simple_extent_ndims(spaceId);
            end

            nDims = self.dsetDict.(name).nDims;
        end
        
        function data = read(self, varargin)
            assert(not(isempty(varargin)));
            dsetName = varargin{1};
            
            assert(dsetName(1) == '/');
            dsetName = dsetName(2:end);
            dsetId = self.getDsetId(dsetName);
            
            switch numel(varargin)
                case 1
                    data = H5D.read(dsetId);

                case 2
                    data = HDF5.readChunked([], dsetId, varargin{2:end});
                    
                case 3
                    assert(all(1 <= varargin{2} & isfinite(varargin{2})));
                    assert(isequal(size(varargin{2}), size(varargin{3})));
                    
                    start = flip(varargin{2} - 1);
                    count = flip(varargin{3});
                    infCount = isinf(count);
                    
                    fileSpaceId = self.getDsetSpaceId(dsetName);
                    
                    if any(infCount)
                       [~, dsetDims] = H5S.get_simple_extent_dims(fileSpaceId);
                        count(infCount) = dsetDims(infCount) - start(infCount);
                    end
                    
                    assert(all(isfinite(count)));

                    memSpaceId = H5S.create_simple(numel(count), count, []);
                    H5S.select_hyperslab(fileSpaceId, 'H5S_SELECT_SET', start, [], [], count);
                    data = H5D.read(dsetId, 'H5ML_DEFAULT', memSpaceId, fileSpaceId, 'H5P_DEFAULT');
                    
                otherwise
                    error('Invalid input arguments');
            end
        end
        
        function setDsetInfos(self)
            h5Infos = h5info(self.filePath);
            h5Infos = h5Infos.Datasets;
            
            self.dsetInfos = struct;
            for curIdx = 1:numel(h5Infos)
                curH5Info = h5Infos(curIdx);
                curName = curH5Info.Name;
                curSize = curH5Info.Dataspace.Size;
                
                % Type / class
                curTyp = curH5Info.Datatype.Type;
                curKlass = self.typToKlass(curTyp);
                
                % Attributes
                curAttrs = struct;
                curH5Attrs = curH5Info.Attributes;
                for curAttrIdx = 1:numel(curH5Attrs)
                    curH5Attr = curH5Attrs(curAttrIdx);
                    curAttrs.(curH5Attr.Name) = curH5Attr.Value;
                end
                
                curDsetInfo = struct;
                curDsetInfo.size = curSize;
                curDsetInfo.klass = curKlass;
                curDsetInfo.attrs = curAttrs;
                self.dsetInfos.(curName) = curDsetInfo;
            end
        end
    end
        
    methods (Static, Access = protected)
        function klass = typToKlass(typ)
            klass = HDF5.typToKlass(typ);
        end
    end
    
    methods (Static)
        writeSkeletons(filePath, maxSegId, skels);
    end
end
