function [typeNames, means] = agglomerateTypes(self, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    dset = '/agglomerate_to_average_types_probabilities';
    means = self.readDataset(dset, varargin{:});

    typeNames = buildTypeNames(self, dset);
    assert(isequal(numel(typeNames), size(means, 1)));
end

function typeNames = buildTypeNames(self, dset)
    dsetInfos = self.dsetInfos.(dset(2:end));
    
    typeCount = dsetInfos.size(1);
    typeNames = cell(1, typeCount);
    
    for curIdx = 1:typeCount
        curTypeName = sprintf('typeName%d', curIdx - 1);
        curTypeName = dsetInfos.attrs.(curTypeName);
        typeNames{curIdx} = curTypeName;
    end
    
    typeNames = categorical(typeNames);
end
