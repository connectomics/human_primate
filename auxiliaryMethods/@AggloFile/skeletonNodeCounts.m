function nodeCounts = skeletonNodeCounts(self, aggloIds)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    dset = '/agglomerate_to_segments_offsets';
    
    if not(exist('aggloIds', 'var')) || isequal(aggloIds, -1)
        % NOTE(amotta): Skip agglomerate zero
        nodeCounts = self.read(dset, 2, inf);
        nodeCounts = diff(nodeCounts(:));
    else
        nodeCounts = ...
            reshape(aggloIds, 1, []) ...
          + cast([1; 2], 'like', aggloIds);
      
        nodeCounts = self.read(dset, nodeCounts);
        nodeCounts = diff(reshape(nodeCounts, 2, []), 1, 1);
        nodeCounts = reshape(nodeCounts, size(aggloIds));
    end
end
