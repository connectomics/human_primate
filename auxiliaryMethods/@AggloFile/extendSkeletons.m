function skels = extendSkeletons(self, skels, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.toDouble = false;
    opts.blockSize = 128 * 1024 * 1024;
    opts.voxelSize = [1, 1, 1];
    opts.radius = inf;
    opts = Util.modifyStruct(opts, varargin{:});
    
    assert(opts.toDouble, [ ...
        'For now, this function only works with `toDouble` set to ', ...
        'true. Sorry about that! - amotta']);
    
    % Sanity checks
    assert(isscalar(opts.radius));
    assert(isequal(size(opts.voxelSize), [1, 3]));
    
    skels = num2cell(skels);
    for curIdx = 1:numel(skels)
        skels{curIdx} = forSkeleton( ...
            self, opts, skels{curIdx});
    end
    
    skels = reshape(cat(1, skels{:}), size(skels));
end

function out = forSkeleton(self, opts, skel)
    outNodeFields = {'segIds', 'nodes', 'dists'};
    outFields = [outNodeFields, {'edges'}];
    
    if ~isfield(skel, 'nodeMask')
        skel.nodeMask = true(size(skel.nodes, 1), 1);
    end
    
    % NOTE(amotta): Only consider segments within mask
    uniAggloIds = setdiff(skel.segIds(skel.nodeMask), 0);
    uniAggloIds = self.segmentToAgglomerateIds(uniAggloIds);
    uniAggloIds = setdiff(uniAggloIds, 0);
    
    kdTree = KDTreeSearcher(double(skel.nodes) .* opts.voxelSize);
    
    out = skel;
    out.dists = zeros(size(out.nodes, 1), 1);
    out = rmfield(out, setdiff(fieldnames(out), outFields));
    out = orderfields(out, outFields);
    
    for curIdx = 1:numel(uniAggloIds)
        curAggloId = uniAggloIds(curIdx);
        
        curExtAgglo = loadSkeletonInRadius( ...
            self, opts, kdTree, curAggloId);
        
        out = mergeAgglomerates( ...
            out, curExtAgglo, 'nodeFields', outNodeFields);
    end
    
    out.seedNodeIds = zeros(size(out.nodes, 1), 1);
    out.seedNodeIds(1:size(skel.nodes, 1)) = 1:size(skel.nodes, 1);
    
    out = restrictToConnectedComponent(out);
    
    % Sanity checks
    mask = out.seedNodeIds > 0;
    assert(isequal(out.segIds(mask), skel.segIds(out.seedNodeIds(mask))));
    assert(isempty(intersect(out.segIds(not(mask)), skel.segIds)));
end

function out = loadSkeletonInRadius(self, opts, kdTree, aggloId)
    voxelSize = opts.voxelSize;
    % NOTE(amotta): bytes to u64
    blockSize = opts.blockSize / 8;
    aggloId = double(aggloId);
    
    %% Process nodes in chunks
    curLims = '/agglomerate_to_segments_offsets';
    curLims = self.read(curLims, 1 + aggloId, 2);

    nodeOff = double(curLims(1) + 1);
    nodeCount = double(curLims(2) - curLims(1));
    nodeMask = false(nodeCount, 1);
    
    curBlockCount = ceil(nodeCount / blockSize);
    blockNodeData = cell(curBlockCount, 3);
    
    for curBlockIdx = 1:curBlockCount
        curOffRel = (curBlockIdx - 1) * blockSize + 1;
        curOffAbs = nodeOff + (curOffRel - 1);
        
        curLimRel = (curOffRel - 1) + blockSize;
        curLimRel = min(nodeCount, curLimRel);
        curLen = (curLimRel - curOffRel) + 1;
        
        % Load nodes
        curNodes = '/agglomerate_to_positions';
        curNodes = self.read(curNodes, [1, curOffAbs], [3, curLen]);
        curNodes = transpose(1 + double(curNodes));
        
        % Mask nodes
        curDists = curNodes .* voxelSize;
       [~, curDists] = knnsearch(kdTree, curDists);
        curDists = reshape(curDists, [], 1);
        
        curMask = curDists < opts.radius;
        curNodes = curNodes(curMask, :);
        curDists = curDists(curMask);
        
        nodeMask(curOffRel:curLimRel) = curMask;
        blockNodeData{curBlockIdx, 1} = curNodes;
        blockNodeData{curBlockIdx, 2} = curDists;
        Util.clear(curNodes);
        
        % Filter segments
        curSegIds = '/agglomerate_to_segments';
        curSegIds = self.read(curSegIds, curOffAbs, curLen);
        
        curSegIds = curSegIds(curMask);
        curSegIds = reshape(curSegIds, [], 1);
        blockNodeData{curBlockIdx, 3} = curSegIds;
        Util.clear(curSegIds);
    end
    
    %% Collect node data
    clear cur*;
    
    curNodeCount = sum(nodeMask);
    nodes = zeros(curNodeCount, 3, 'double');
    dists = zeros(curNodeCount, 1, 'double');
    segIds = zeros(curNodeCount, 1, 'uint64');
    
    curOff = 1;
    curEmpty = [];
    for curIdx = 1:size(blockNodeData, 1)
        curNodes = blockNodeData{curIdx, 1};
        blockNodeData{curIdx, 1} = curEmpty;
        
        curLen = size(curNodes, 1);
        curLim = (curOff - 1) + curLen;
        
        nodes(curOff:curLim, :) = curNodes;
        Util.clear(curNodes);
        
        curDists = blockNodeData{curIdx, 2};
        blockNodeData{curIdx, 2} = curEmpty;
        dists(curOff:curLim) = curDists;
        Util.clear(curDists);
        
        curSegIds = blockNodeData{curIdx, 3};
        blockNodeData{curIdx, 3} = curEmpty;
        segIds(curOff:curLim) = curSegIds;
        Util.clear(curSegIds);
        
        curOff = curOff + curLen;
    end
    
    Util.clear(blockNodeData);
    
    % Sanity checks
    assert(isequal(size(nodes, 1), size(segIds, 1)));
    assert(isequal(size(nodes, 2), 3));
    
    % Translation of node IDs
    nodeIds = cumsum(double(nodeMask));
    nodeIds(not(nodeMask)) = 0;
    Util.clear(nodeMask);

    %% Process edges in chunks
    clear cur*;
    curLims = '/agglomerate_to_edges_offsets';
    curLims = self.read(curLims, 1 + aggloId, 2);

    edgeOff = double(curLims(1) + 1);
    edgeCount = double(curLims(2) - curLims(1));
    
    curBlockCount = ceil(edgeCount / blockSize);
    blockEdgeData = cell(curBlockCount, 1);
    
    for curBlockIdx = 1:curBlockCount
        curOffRel = (curBlockIdx - 1) * blockSize + 1;
        curOffAbs = edgeOff + (curOffRel - 1);
        
        curLimRel = (curOffRel - 1) + blockSize;
        curLimRel = min(edgeCount, curLimRel);
        curLen = curLimRel - curOffRel + 1;
        
        curEdges = '/agglomerate_to_edges';
        curEdges = self.read(curEdges, [1, curOffAbs], [2,  curLen]);
        
        curEdges = reshape(nodeIds(1 + curEdges), 2, []);
        curEdges = curEdges(:, all(curEdges, 1));
        curEdges = transpose(curEdges);
        
        blockEdgeData{curBlockIdx} = curEdges;
        Util.clear(curEdges);
    end
    
    %% Collect edge data
    clear cur*;
    
    curEdgeCount = sum(cellfun( ...
        @(e) size(e, 1), blockEdgeData));
    edges = zeros(curEdgeCount, 2);
    
    curOff = 1;
    curEmpty = [];
    for curIdx = 1:size(blockEdgeData, 1)
        curEdges = blockEdgeData{curIdx};
        blockEdgeData{curIdx} = curEmpty;
        
        curLen = size(curEdges, 1);
        curLim = (curOff - 1) + curLen;
        
        edges(curOff:curLim, :) = curEdges;
        Util.clear(curEdges);
        
        curOff = curOff + curLen;
    end
    
    Util.clear(blockEdgeData);
    
    %% Build output
    out = struct;
    out.segIds = segIds;
    out.nodes = nodes;
    out.edges = edges;
    out.dists = dists;
    
    assert(issorted(out.edges, 2, 'strictascend'));
    assert(issorted(out.edges, 'rows'));
end

function out = restrictToConnectedComponent(agglo)
    g = table;
    g.id = reshape(1:size(agglo.nodes, 1), [], 1);
    g = graph(agglo.edges(:, 1), agglo.edges(:, 2), [], g);

    % Restrict to component around need
    g.Nodes.compId = reshape(conncomp(g), [], 1);
    nodeInSeedMask = agglo.seedNodeIds(g.Nodes.id) > 0;
    compInSeedMask = accumarray(g.Nodes.compId, nodeInSeedMask, [], @any);
    assert(sum(compInSeedMask) == 1);
    
    nodeInCompMask = compInSeedMask(g.Nodes.compId);
    g = subgraph(g, nodeInCompMask);
    
    % Build output
    out = struct;
    out.seedNodeIds = agglo.seedNodeIds(g.Nodes.id, :);
    out.segIds = agglo.segIds(g.Nodes.id, :);
    out.nodes = agglo.nodes(g.Nodes.id, :);
    out.dists = agglo.dists(g.Nodes.id);
    out.edges = g.Edges.EndNodes;
end

function left = mergeAgglomerates(left, right, varargin)
    opts = struct;
    opts.nodeFields = {};
    opts = Util.modifyStruct(opts, varargin{:});
    
    opts.nodeFields = union( ...
        {'segIds', 'nodes'}, opts.nodeFields, 'stable');
    assert(not(ismember({'nodeIds'}, opts.nodeFields)));
    
    % NOTE(amotta): Find node correspondences
   [~, right.nodeIds] = ismember(right.segIds, left.segIds);
    mask = not(right.nodeIds);
    
    % NOTE(amotta): Restrict to nodes that are not already in `left`
    for curFieldIdx = 1:numel(opts.nodeFields)
        curField = opts.nodeFields{curFieldIdx};
       [curData, right.(curField)] = deal(right.(curField), []);
        
        % Apply mask
        curIdx = repelem({':'}, ndims(curData) - 1);
        curData = curData(mask, curIdx{:});
        right.(curField) = curData;
    end
    
    right.nodeIds(mask) = size(left.nodes, 1) + (1:sum(mask));
    
    % NOTE(amotta): Get rid of edges that are fully contained in `left`.
    mask = reshape(mask(right.edges), [], 2);
    mask = any(mask, 2);
    
    right.edges = right.edges(mask, :);
    right.edges = reshape(right.nodeIds(right.edges), [], 2);
    right = rmfield(right, 'nodeIds');
    
    % NOTE(amotta): First, merge the list of
    % * edges within the `left` agglomerate, and
    % * edges between the `left` and `right` agglomerate.
    %
    % These edge lists will interleave. So, we need to sort the resulting
    % list. This is an expensive operation. Let's minimize the work.
    right.edges = sort(right.edges, 2);
    mask = right.edges(:, 1) <= size(left.nodes, 1);
    left.edges = union(left.edges, right.edges(mask, :), 'rows');
    left.edges = [left.edges; right.edges(not(mask), :)];
    
    for curFieldIdx = 1:numel(opts.nodeFields)
        curField = opts.nodeFields{curFieldIdx};
        left.(curField) = [left.(curField); right.(curField)];
    end
    
    % Sanity check
    assert(isscalar(unique(cellfun( ...
        @(f) size(left.(f), 1), opts.nodeFields))));
end
