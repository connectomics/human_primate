function aggloIds = segmentToAgglomerateIds(self, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    dset = '/segment_to_agglomerate';
    aggloIds = self.readDataset(dset, varargin{:});
    
    % NOTE(amotta): If the caller provided an array of segment IDs, then
    % ensure that we return the agglomerate IDs in identical shape.
    segIdsGiven = not(isempty(varargin)) && not(isequal(varargin{1}, -1));
    if segIdsGiven; aggloIds = reshape(aggloIds, size(varargin{1})); end
end
