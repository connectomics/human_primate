function writeSkeletons(filePath, maxSegId, skels)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % Sanity checks
    skels = reshape(skels, [], 1);
    assert(all(isfield(skels, {'segIds', 'nodes', 'edges'})));
    
    for curSkelIdx = 1:numel(skels)
        try
            checkSkeleton(skels(curSkelIdx));
        catch skelExc
            exc = MException( ...
               'AggloFile:writeSkeletons:invalidSkeleton', ...
               'Invalid data in skeleton %d', curSkelIdx);
            exc = addCause(exc, skelExc);
            throw(exc);
        end
    end
    
    % Version
    version = uint8(0);
    h5create(filePath, '/version', 1, 'Datatype', class(version));
    h5write(filePath, '/version', version);
    
    % Edge offsets
    edgeOffsets = arrayfun(@(s) size(s.edges, 1), skels);
    edgeOffsets = uint64([0; 0; edgeOffsets(:)]);
    edgeOffsets = cumsum(edgeOffsets);
    
    h5create( ...
        filePath, '/agglomerate_to_edges_offsets', ...
        numel(edgeOffsets), 'Datatype', class(edgeOffsets));
    h5write(filePath, '/agglomerate_to_edges_offsets', edgeOffsets);
    Util.clear(edgeOffsets);
    
    % Edges
    edges = transpose(vertcat(skels.edges));
    assert(ismatrix(edges) && size(edges, 1) == 2);
    edges = uint64(edges) - 1;
    
    h5create( ...
        filePath, '/agglomerate_to_edges', ...
        size(edges), 'Datatype', class(edges));
    h5write(filePath, '/agglomerate_to_edges', edges);
    Util.clear(edges);
    
    % Node offsets
    nodeCounts = arrayfun(@(s) size(s.nodes, 1), skels);
    nodeOffsets = uint64([0; 0; nodeCounts(:)]);
    nodeOffsets = cumsum(nodeOffsets);
    
    h5create( ...
        filePath, '/agglomerate_to_segments_offsets', ...
        numel(nodeOffsets), 'Datatype', class(nodeOffsets));
    h5write(filePath, '/agglomerate_to_segments_offsets', nodeOffsets);
    Util.clear(nodeOffsets);
    
    % Node positions
    nodes = transpose(vertcat(skels.nodes));
    assert(ismatrix(nodes) && size(nodes, 1) == 3);
    nodes = uint32(nodes) - 1;
    
    h5create( ...
        filePath, '/agglomerate_to_positions', ...
       	size(nodes), 'Datatype', class(nodes));
    h5write(filePath, '/agglomerate_to_positions', nodes);
    Util.clear(nodes);
    
    % Segment IDs
    segIds = uint64(vertcat(skels.segIds));
    
    h5create( ...
        filePath, '/agglomerate_to_segments', ...
        numel(segIds), 'Datatype', class(segIds));
    h5write(filePath, '/agglomerate_to_segments', segIds);
    
    % Segment-to-agglomerate look-up table
    aggloIds = repelem(1:numel(skels), nodeCounts);
    segToAgglo = zeros(maxSegId + 1, 1, 'uint64');
    segToAgglo(segIds + 1) = aggloIds;
    Util.clear(segIds, aggloIds);
    
    h5create( ...
        filePath, '/segment_to_agglomerate', ...
        numel(segToAgglo), 'Datatype', class(segToAgglo));
    h5write(filePath, '/segment_to_agglomerate', segToAgglo);
    Util.clear(segToAgglo);
end

function checkSkeleton(skel)
    assert(iscolumn(skel.segIds));
    assert(size(skel.segIds, 1) == size(skel.nodes, 1));
    assert(ismatrix(skel.nodes) && size(skel.nodes, 2) == 3);
    assert(ismatrix(skel.edges) && size(skel.edges, 2) == 2);
    
    assert(all(skel.segIds(:) >= 1));
    assert(all(skel.nodes(:) >= 1));
    assert(all(skel.edges(:) >= 1));
end
