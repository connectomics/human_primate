function aggloDiags = skeletonBoxDiagonals(self, aggloIds, varargin)
    % aggloDiags = skeletonBoxDiagonals(self, aggloIds, varargin)
    %   Computes the bounding box diagonal lengths for the specified
    %   agglomerates. If `aggloIds` is missing or is set to `-1`, then all
    %   agglomerates are processed. The voxel size can be specified with
    %   the `voxelSize` key-value pair.
    %
    % IMPORTANT
    %   The voxel size must be specified for the same magnification as the
    %   segment positions. This is typically magnification 1(-1-1).
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.voxelSize = [1, 1, 1];
    opts = Util.modifyStruct(opts, varargin{:});
    
    aggloToSegOffs = self.read('/agglomerate_to_segments_offsets');
    aggloToPos = self.read('/agglomerate_to_positions');
    
    if not(exist('aggloIds', 'var')) ...
            || isequal(aggloIds, -1)
        aggloCount = numel(aggloToSegOffs) - 2;
        aggloIds = 1:aggloCount;
    else
        aggloCount = numel(aggloIds);
    end
    
    aggloDiags = zeros(size(aggloIds));
    % NOTE(amotta): Pre-allocate, just to be sure.
    curOffs = class(aggloToSegOffs);
    curOffs = zeros(1, 2, curOffs);

    tic;
    for aggloIdx = 1:aggloCount
        aggloId = aggloIds(aggloIdx);
        curOffs(1) = aggloToSegOffs(aggloId + 1) + 1;
        curOffs(2) = aggloToSegOffs(aggloId + 2);

        % NOTE(amotta): Skip empty or single-segment agglomerates
        if curOffs(2) <= curOffs(1) + 1; continue; end

        curRange = aggloToPos(:, curOffs(1):curOffs(2));
        curRange = max(curRange, [], 2) - min(curRange, [], 2);
        curRange = double(curRange(:)) .* opts.voxelSize(:);
        
        aggloDiags(aggloIdx) = sqrt(sum(curRange .* curRange));
        Util.progressBar(aggloIdx, aggloCount);
    end
end

