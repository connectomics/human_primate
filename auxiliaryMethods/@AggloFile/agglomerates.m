function agglos = agglomerates(self, aggloIds)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    klass = 'agglomerate_to_segments';
    klass = self.dsetInfos.(klass).klass;

    agglos = cell(size(aggloIds));
    agglos(:) = {zeros(0, 1, klass)};

    for curIdx = 1:numel(aggloIds)
        curAggloId = double(aggloIds(curIdx));

        curLims = '/agglomerate_to_segments_offsets';
        curLims = self.read(curLims, 1 + curAggloId, 2);

        curOff = double(curLims(1) + 1);
        curLen = double(curLims(2) - curLims(1));

        curSegIds = '/agglomerate_to_segments';
        curSegIds = self.read(curSegIds, curOff, curLen);

        curSegIds = reshape(curSegIds, [], 1);
        agglos{curIdx} = curSegIds;
    end
end
