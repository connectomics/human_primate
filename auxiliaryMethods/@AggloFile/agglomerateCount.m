function count = agglomerateCount(self)
    % Returns the number of agglomerates,
    % excluding agglomerate zero.
    % 
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    count = self.dsetInfos.agglomerate_to_segments_offsets.size - 2;
    assert(not(count < 0));
end
