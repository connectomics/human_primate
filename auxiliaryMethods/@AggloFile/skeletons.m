function skels = skeletons(self, aggloIds, varargin)
    % Written by
    %   Meike Sievers <meike.sievers@brain.mpg.de>
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.toDouble = false;
    opts.loadNodeTypes = false;
    opts.showProgressBar = false;
    opts.withId = false;
    opts = Util.modifyStruct(opts, varargin{:});
    
    segIdKlass = self.dsetInfos.agglomerate_to_segments.klass;
    nodeKlass = self.dsetInfos.agglomerate_to_positions.klass;
    edgeKlass = self.dsetInfos.agglomerate_to_edges.klass;
    
    if opts.toDouble
        nodeKlass = 'double';
        edgeKlass = 'double';
    end

    skels = struct;
    skels.segIds = zeros(0, 1, segIdKlass);
    skels.nodes = zeros(0, 3, nodeKlass);
    skels.edges = zeros(0, 2, edgeKlass);
    
    if opts.loadNodeTypes
        curAttrs = 'agglomerate_to_node_types';
        curAttrs = self.dsetInfos.(curAttrs).attrs;
        
        % NOTE(amotta): The type IDs are 0-based
        curNodeTypeNames = cell(1, 1 + curAttrs.maxTypeId);
        for curTypeId = 0:curAttrs.maxTypeId
            curTypeName = curAttrs.(sprintf('typeName%d', curTypeId));
            curNodeTypeNames{curTypeId + 1} = curTypeName;
        end
        
        nodeTypes = uint8(0):uint8(numel(curNodeTypeNames) - 1);
        nodeTypes = categorical(nodeTypes, nodeTypes, curNodeTypeNames);
        skels.nodeTypes = nodeTypes(zeros(0, 1));
        Util.clear(curNodeTypeNames);
    end
    
    skels = repmat(skels, size(aggloIds));

    if opts.showProgressBar
        tic;
        Util.log('Loading agglomerates as skeletons');
    end
    
    for curIdx = 1:numel(aggloIds)
        curAggloId = double(aggloIds(curIdx));

        % Load nodes
        curLims = '/agglomerate_to_segments_offsets';
        curLims = self.read(curLims, 1 + curAggloId, 2);

        curOff = double(curLims(1) + 1);
        curLen = double(curLims(2) - curLims(1));

        curSegIds = '/agglomerate_to_segments';
        curSegIds = self.read(curSegIds, curOff, curLen);
        curSegIds = reshape(curSegIds, [], 1);
        
        curNodes = '/agglomerate_to_positions';
        curNodes = self.read(curNodes, [1, curOff], [3, curLen]);
        if opts.toDouble; curNodes = double(curNodes); end
        curNodes = transpose(1 + curNodes);
        
        if opts.loadNodeTypes
            curNodeTypes = '/agglomerate_to_node_types';
            curNodeTypes = self.read(curNodeTypes, curOff, curLen);
            curNodeTypes = nodeTypes(1 + curNodeTypes);
            skels(curIdx).nodeTypes = curNodeTypes(:);
        end
        
        % Sanity check
        assert(isequal(size(curSegIds, 1), size(curNodes, 1)));
        skels(curIdx).segIds = curSegIds;
        skels(curIdx).nodes = curNodes;
        
        % Edges
        curLims = '/agglomerate_to_edges_offsets';
        curLims = self.read(curLims, 1 + curAggloId, 2);

        curOff = double(curLims(1) + 1);
        curLen = double(curLims(2) - curLims(1));
        
        if curLen > 0
            % NOTE(amotta): Skip loading of edges if there are none. This
            % is not just an optimization, but also a workaround MATLAB's
            % h5read throwing an error if `count` is zero.
            curEdges = '/agglomerate_to_edges';
            curEdges = self.read(curEdges, [1, curOff], [2, curLen]);
            if opts.toDouble; curEdges = double(curEdges); end
            curEdges = transpose(1 + curEdges);
            skels(curIdx).edges = curEdges;
        end
        
        if opts.showProgressBar
            Util.progressBar(curIdx, numel(aggloIds));
        end
    end
    if opts.withId
        [skels(1:numel(skels)).id] = Util.deal2varargout(aggloIds);
    end
end
