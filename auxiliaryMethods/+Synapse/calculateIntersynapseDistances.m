function [synToSynDists, synIds] = ...
        calculateIntersynapseDistances( ...
            agglos, aggloSynIds, synAgglos, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opt = struct;
    opt.segWeights = [];
    opt.showProgress = false;
    opt.voxelSize = ones(1, 3);
    opt = Util.modifyStruct(opt, varargin{:});
    
    if isempty(opt.segWeights)
        % By default, pick random segment
        opt.segWeights = max(cellfun(@max, synAgglos));
        opt.segWeights = ones(opt.segWeights, 1);
    end
    
    % Outputs
    synToSynDists = cell(size(agglos));
    synIds = cell(size(agglos));
    
    if opt.showProgress; tic; end
    
    for curIdx = 1:numel(agglos)
        curAgglo = agglos(curIdx);
        curSynIds = aggloSynIds{curIdx};
        curSynAgglos = synAgglos(curSynIds);
        
        curSynNodeIds = mapSynapsesToNodes( ...
            opt.segWeights, curAgglo, curSynAgglos);
        
        % Calculate distances
        curSynToSynDist = SuperAgglo.pairwiseDist( ...
            curAgglo, curSynNodeIds, 'voxelSize', opt.voxelSize);
        
        synToSynDists{curIdx} = curSynToSynDist;
        synIds{curIdx} = curSynIds(:);
        
        if opt.showProgress; Util.progressBar(curIdx, numel(agglos)); end
    end
end

function nodeIds = mapSynapsesToNodes(segWeights, agglo, synapseAgglos)
    aggloSegIds = agglo.nodes(:, 4);
    nodeIds = nan(size(synapseAgglos));
    
    % Map onto segment with highest weight
    for curIdx = 1:numel(synapseAgglos)
        curSegIds = synapseAgglos{curIdx};
       [curMask, curNodeIds] = ismember(curSegIds, aggloSegIds);
       
        curSegIds = curSegIds(curMask);
        curNodeIds = curNodeIds(curMask);
        assert(not(isempty(curNodeIds)));
        
       [~, curMaxIdx] = max(segWeights(curSegIds));
        nodeIds(curIdx) = curNodeIds(curMaxIdx);
    end
end
