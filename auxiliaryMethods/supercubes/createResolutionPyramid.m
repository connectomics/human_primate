function createResolutionPyramid(inParam, bbox, outRoot, isSeg, voxelSize, taskConcurrency, startWithMag, blocksPerFileDim)
%Input:
%   If using just one input argument, and normal wK hierachy with JSONs
%   already present, rest inferred.
%   param = Parameter struct for mag1 KNOSSOS or WKW dataset
%   bbox = Bounding Box of KNOSSOS Hierachy in the magnification specified
%       by `startWithMag` (which defaults to mag 1).
%       (minimal coordinates should align with start of Knossos cube (e.g. [1 1 1]))
%       (Not sure whether it will work if lower corner in bbox is not [1 1 1], NOT tested)
%   outRoot = Where to write higher resolutions (subfolder named as the
%       magnification will be created inside).
%   isSeg = If you try to subsample a segmentation (instead of EM data) set
%       this to true. This will use @mode instead of @median for
%       subsampling and some other tricks.
%   voxelSize = Physical size of the voxels in the `inParam` dataset.
%   startWithMag = Lowest available magnification starting from which the
%       resolution pyramid will be generated.
%   blocksPerFileDim = Double. Side length of the Morton-ordered cube.  Must be a
%       positive power of two. (Default: 32)
%
% Written by
%   Manuel Berning <manuel.berning@brain.mpg.de>
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
% Set according to memory limits, currently optimized for 12 GB RAM,
% segmentation will need 48 GB currently
% Will be paralellized on cubes of this size
cubeSize = [1024, 1024, 1024];

if nargin < 4 || isempty(isSeg)
    isSeg = false;
end

if ~exist('voxelSize', 'var') || isempty(voxelSize)
    voxelSize = [1, 1, 1];
end

if ~exist('taskConcurrency', 'var') || isempty(taskConcurrency)
    taskConcurrency = 100;
end

if ~exist('startWithMag', 'var') || isempty(startWithMag)
    startWithMag = 1;
end

if isscalar(startWithMag)
    startWithMag = repelem(startWithMag, 3);
end

if ~exist('blocksPerFileDim', 'var') || isempty(blocksPerFileDim)
    blocksPerFileDim = 32;
end

assert(isequal(size(startWithMag), [1, 3]));

% NOTE(amotta): Code assumes voxel size to be in mag 1.
voxelSize = voxelSize ./ startWithMag;
assert(voxelSize(1) == voxelSize(2));

if isSeg
    gbRamPerTask = 60; % 48 GB somehow did not suffice last time
    dtype = 'uint32';
else
    gbRamPerTask = 12;
    dtype = 'uint8';
end

if ~isfield(inParam, 'dtype')
    inParam.dtype = dtype;
end

% The root directory specified in inParam might just be a symbolic link. It
% is thus possible, that the mag1 subdirectory is hidden. To avoid this,
% use readlink to resolve the symbolic link.
[~, inParam.root] = system(sprintf( ...
    'readlink -f "%s" < /dev/null', inParam.root));
inParam.root = strcat(strtrim(inParam.root), filesep);
assert(exist(inParam.root, 'dir') ~= 0);

% Make sure input is mag1
if isfield(inParam, 'prefix') && ...
        isempty(regexp(inParam.prefix, '_mag1$', 'once'))
    % TODO(amotta): Think about how and if this needs to be tweaked in case
    % that the user explicitly set the `startWithMag` input.
    error('Input parameters must be for mag1');
end

if nargin < 3 || isempty(outRoot)
    % this goes one dir level up ignoring the last root char (which could be a
    % filesep, but also does not matter if not)
    outRoot = [fileparts(inParam.root(1:end-1)),filesep];
    Util.log('Output is written to %s.', outRoot);
end

% build parameters
outParam = inParam;
outParam.root = outRoot;

if nargin < 2 || isempty(bbox)
    if exist(fullfile(outParam.root, 'section.json'),'file')
        % Get bounding box from section.json
        temp = readJson(fullfile(outParam.root, 'section.json'));
        bbox = double(cell2mat(cat(2, temp.bbox{:}))');
    else
        % Get bounding box from datasource-properties.json
        [rootFolder,folderName] = fileparts(outParam.root(1:end-1));
        temp = readJson(fullfile(rootFolder, 'datasource-properties.json'));
        ind = find(cellfun(@(x) strcmp(x.name,folderName),temp.dataLayers));
        bbox = double(cat(2,cell2mat(temp.dataLayers{ind}.boundingBox.topLeft),[temp.dataLayers{ind}.boundingBox.width,temp.dataLayers{ind}.boundingBox.height,temp.dataLayers{ind}.boundingBox.depth]'));
        voxelSize = cellfun(@(x) double(x),temp.scale)';
    end
    
    clear temp;
    
    % convert to MATLAB style
    assert(all(bbox(:) >= 0));
    bbox(:, 1) = bbox(:, 1) + 1;
    Util.log('Bbox was set to %s.', mat2str(bbox));
else
    bbox = double(bbox);
end

% Determine which magnifications to write
magsToWrite = startWithMag;

while magsToWrite(end, 3) < 512
    curVoxelSize = magsToWrite(end, :) .* voxelSize;
    if 2 * curVoxelSize(1) < curVoxelSize(3)
        % NOTE(amotta): At this point the in-plane pixel size is still
        % very much smaller than the cutting thickness. To reduce the
        % anisotropy let's downsample only within XY plane.
        curPoolVol = [2, 2, 1];
    else
        % NOTE(amotta): The voxel size is now roughly isotropic, so
        % downsample equally in all three dimensions.
        curPoolVol = [2, 2, 2];
    end

    magsToWrite(end + 1, :) = ...
        magsToWrite(end, :) .* curPoolVol; %#ok
    clear curVoxelSize curPoolVol;
end

% Remove `1-1-1` from list
magsToWrite(1, :) = [];

magGroups = {startWithMag};
for curIdx = 1:size(magsToWrite)
    curMag = magsToWrite(curIdx, :);

    if any(curMag ./ magGroups{end}(1, :) > 8)
        magGroups{end + 1} = vertcat( ...
            magGroups{end}(end, :), curMag); %#ok
    else
        magGroups{end} = vertcat( ...
            magGroups{end}, curMag);
    end
end

% Create output folder if it does not exist
if ~exist(outParam.root, 'dir')
    mkdir(outParam.root);
end

% Initialize wkw datasets
if isfield(outParam, 'backend') ...
        && strcmp(outParam.backend, 'wkwrap')
    allMagRoots = cellfun( ...
        @(magIds) sprintf('%d-%d-%d', magIds), ...
        num2cell(magsToWrite, 2), 'UniformOutput', false);
    allMagRoots = fullfile(outParam.root, allMagRoots);
    
    for i = 1:numel(allMagRoots)
        try
            wkwInit('new', allMagRoots{i}, 32, blocksPerFileDim, outParam.dtype, 1);
        catch
            warning( ...
               ['Failed to initialize WKW dataset at "%s". ', ...
                'It might already exits.'], allMagRoots{i});
        end
    end
    
    clear allMagRoots;
end

% Do the work, submitted to scheduler
curBbox = bbox;
curInParam = inParam;

curCubeOffs = @(box, i) unique([ ...
    box(i, :) + [0, 1], 1 + cubeSize(i) * ...
    (ceil(box(i, 1) / cubeSize(i)):floor(box(i, 2) / cubeSize(i)))]);

for i = 1:numel(magGroups)
    curMag = magGroups{i}(end, :);
    curPoolVol = curMag ./ magGroups{i}(1, :);
    
    curBbox = [ ...
        floor((curBbox(:, 1) - 1) ./ curPoolVol(:)) .* curPoolVol(:) + 1, ...
        ceil(curBbox(:, 2) ./ curPoolVol(:)) .* curPoolVol(:)];
    
    X = curCubeOffs(curBbox, 1);
    Y = curCubeOffs(curBbox, 2);
    Z = curCubeOffs(curBbox, 3);
    
    idx = 1;
    inputCell = cell(prod(cellfun(@numel, {X, Y, Z}) - 1), 1);
    
    for x = 1:(numel(X) - 1)
        for y = 1:(numel(Y) - 1)
            for z = 1:(numel(Z) - 1)
                thisBBox = [ ...
                    X(x), (X(x + 1) - 1);
                    Y(y), (Y(y + 1) - 1);
                    Z(z), (Z(z + 1) - 1)];
                inputCell{idx} = {thisBBox};
                idx = idx + 1;
            end
        end
    end
  % NOTE(amotta): At the time of writing our Slurm configuration is such
    % that arrays of at most 100'000 task can be submitted. Let's work
    % around that...
  % NOTE(schurrm): Reduce taksGroupSize to 1000 to speed up job
    % submission, for large datasets this might require to increase the 
    % time limit
    
    taskGroupSize = ceil(numel(inputCell) / 1000);  
    job = Cluster.startJob( ...
        @writeSupercubes, inputCell, ...
        'sharedInputs', {curInParam, magGroups{i}, outParam, isSeg}, ...
        'sharedInputsLocation', [1, 3:5], ...
        'taskGroupSize', taskGroupSize, ...
        'cluster', { ...
            'time', '12:00:00', ...
            'memory', gbRamPerTask, ...
            'taskConcurrency', taskConcurrency}, ...
        'name', 'supercubes');
    Cluster.waitForJob(job);
    
    % Update root path
    curInParam.root = fullfile( ...
        outParam.root, sprintf('%d-%d-%d', curMag));
    curInParam.root(end + 1) = filesep;
    
    % Update prefix
    if isfield(curInParam, 'prefix')
        curInParam.prefix = regexprep( ...
            curInParam.prefix, '(\d+)$', num2str(curMag));
    end
    
    % Update bounding box
    curBbox = [ ...
        floor((curBbox(:, 1) - 1) ./ curPoolVol(:)) + 1, ...
        ceil(curBbox(:, 2) ./ curPoolVol(:))];
end

end
