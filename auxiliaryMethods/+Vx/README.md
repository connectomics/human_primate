# Voxelytics (Vx) module

When using Scalable Minds' Voxelytics, the resulting data structures may
differ from these of our MATLAB-based pipeline. This module contains
utilities that partially abstract these differences.
