function out = getPointsBySegIds(param, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    out = Vx.Seg.getMetaData(param, 'point', varargin{:});
end
