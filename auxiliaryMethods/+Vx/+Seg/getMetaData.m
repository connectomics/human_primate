function varargout = getMetaData(param, varNames, segIds, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    varDict = struct;
    % NOTE(amotta): Names were chosen for backward compatibility with the
    % Seg.Global.getSegToXYZ functions of the SBEM pipeline.
    varDict.size.dset = '/volumes';
    varDict.size.postProc = @(p) p;
    varDict.point.dset = '/positions';
    varDict.point.postProc = @(p) p + 1;
    varDict.centroid.dset = '/center_of_mass';
    varDict.centroid.postProc = @(p) p + 1;
    % NOTE(amotta): For sanity checks
    varDict.segid.dset = '/ids';
    varDict.segid.postProc = @(p) p;
    
    opts = struct;
    opts.toDouble = false;
    opts = Util.modifyStruct(opts, varargin{:});
    
    metaFile = param.files.segmentStats;
    
    assert(not(isempty(varNames)));
    if ischar(varNames); varNames = {varNames}; end
    varNames = cellfun(@lower, varNames, 'UniformOutput', false);
    assert(all(ismember(varNames, fieldnames(varDict))));
    
    % See email from Jonathan of 26.05.2021
    % TODO(amotta): We should read a file version number instead.
    firstSegId = h5read(metaFile, '/ids', 1, 1);
    assert(firstSegId == 0 || firstSegId == 1);
    
    if not(exist('segIds', 'var'))
        segIdsGiven = false;
    elseif isscalar(segIds) && segIds < 0
        segIdsGiven = false;
    else
        assert(all(segIds(:) > 0));
        segIdsGiven = true;
    end
    
    varargout = cell(1, numel(varNames));
    
    for curIdx = 1:numel(varNames)
        curVarName = varNames{curIdx};
        curDsetName = varDict.(curVarName).dset;
        curPostProc = varDict.(curVarName).postProc;
        
        if segIdsGiven
            if firstSegId == 0; segIds = segIds + 1; end
            curData = HDF5.readChunked(metaFile, curDsetName, segIds);
        else
            curDsetRank = h5info(metaFile, curDsetName);
            curDsetRank = numel(curDsetRank.Dataspace.MaxSize);
            
            curOffset = 1 + double(firstSegId == 0);
            curOffset = [ones(1, curDsetRank - 1), curOffset]; %#ok
            curCount = inf(1, curDsetRank);
            
            curData = h5read(metaFile, curDsetName, curOffset, curCount);
        end

        if opts.toDouble; curData = double(curData); end
        curData = curPostProc(curData);
        varargout{curIdx} = curData;
    end
end
