function [typeNames, typeProbs] = getTypeProbs(param, segIds)
    % [typeNames, typeProbs] = getTypeProbs(param, segIds)
    %   Returns the neurite type probabilities per segment.
    %
    % Input arguments
    %   param:
    %     Dataset parameter structure. The neurite type probabilities will
    %     be loaded from the file specified in param.files.segmentTypes.
    %
    %   segIds:
    %     Optional list of segment IDs.
    %     * If specified, only the type probabilities for these segments
    %       will be loaded and returned. typeProbs(:, i) contains the type
    %       probabilities for segIds(i).
    %     * If unspecified or specified as a negative scalar, the type
    %       probabilities for all segments in the segmentation will be
    %       loaded and returned. typeProbs(:, 1) contains the type
    %       probabilities for segment 1.
    %
    % Output arguments:
    %   typeNames:
    %     1xT cell array with type names. This is a subset of:
    %     Background, Astrocyte, Axon, Dendrite, SpineHead, Other.
    %
    %   typeProbs:
    %     TxS matrix of type probabilities. Row i corresponds to type
    %     typeNames{i}. Column j corresponds to either segment j or
    %     segIds(j). For details, see above.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    if not(exist('segIds', 'var'))
        segIdsGiven = false;
    elseif isscalar(segIds) && segIds < 0
        segIdsGiven = false;
    else
        assert(all(segIds(:) > 0));
        segIdsGiven = true;
    end
    
    typeFile = param.files.segmentTypes;
    dsetName = '/average_scores';
    
    typeNames = getTypeNames(typeFile);
    typeNames = categorical(typeNames, typeNames);
    
    % See email from Jonathan of 26.05.2021
    % TODO(amotta): We should read a file version number instead.
    firstSegId = h5read(typeFile, '/segment_ids', 1, 1);
    assert(firstSegId == 0 || firstSegId == 1);
    
    if segIdsGiven
        if firstSegId == 0; segIds = segIds + 1; end
        typeProbs = HDF5.readChunked(typeFile, dsetName, segIds);
    else
        offset = [1, 1 + double(firstSegId == 0)];
        typeProbs = h5read(typeFile, dsetName, offset, [inf, inf]);
    end
    
    % Sanity check
    assert(isequal(numel(typeNames), size(typeProbs, 1)));
end

function typeNames = getTypeNames(typeFile)
    typeNames = getTypeNamesLegacy(typeFile);
    if iscell(typeNames); return; end
    
    % NOTE(amotta): Here, we are reading the type names from the HDF5 file.
    % Unfortunately, the capitalization of these names is not entirely
    % consistent across datasets. To fix this and to make sure that the
    % names are backward-compatible, we map them onto known names.
    knownTypeNames = { ...
        'Background', 'Astrocyte', ...
        'Axon', 'Dendrite', 'SpineHead', 'Other', 'Myelin'};
    typeNames = h5readatt(typeFile, '/', 'type_names');
    
   [~, typeIds] = ismember( ...
       cellfun(@lower, typeNames, 'UniformOutput', false), ...
       cellfun(@lower, knownTypeNames, 'UniformOutput', false));
   
    if not(all(typeIds))
        unknownTypeNames = typeNames(not(typeIds));
        unknownTypeNames = strjoin(unknownTypeNames, ', ');
        error('Unknown types: %s', unknownTypeNames);
    end
    
    typeNames = knownTypeNames(typeIds);
    typeNames = reshape(typeNames, 1, []);
end

function types = getTypeNamesLegacy(typeFile)
    % NOTE(amotta): The following HDF5 files are so old that Scalable Minds
    % did not yet store type name names. That's why we had to hard-code
    % them here..
    switch typeFile
        case { ...
               ['/tmpscratch/georgwie/artifacts/default/compute_segment_types', ...
                '/segment_types_sorted_probabilities__bd5ed1a57a/segment_types_all.hdf5'], ...
               ['/tmpscratch/georgwie/artifacts/default/compute_segment_types', ...
                '/segment_types__235f8bd911/segment_types_all.hdf5']}
            % NOTE(amotta): See
            % https://github.com/scalableminds/msem/wiki/Datasets#layer-4-barrel
            types = { ...
                'Background', 'SpineHead', 'Dendrite', ...
                'Astrocyte', 'Axon', 'Other'};
            
        case { ...
               ['/conndata/georgwie/artifacts/default/compute_segment_types', ...
                '/segment_types_v4__81c38e881f-v1/segment_types_all.hdf5']}
            % NOTE(amotta): See email from Jonathan of 23.10.2020.
            types = { ...
                'SpineHead', 'Dendrite', ...
                'Astrocyte', 'Axon', 'Other'};
        otherwise
            types = [];
    end
end

