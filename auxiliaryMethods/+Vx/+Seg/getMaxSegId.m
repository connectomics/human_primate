function maxSegId = getMaxSegId(param)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    % Count number of segment IDs
    segmentStatsFile = param.files.segmentStats;
    segCount = h5info(segmentStatsFile, '/ids');
    segCount = segCount.Dataspace.Size;
    
    % Look up ID of last segment
    maxSegId = h5read(segmentStatsFile, '/ids', segCount, 1);
    assert(maxSegId == segCount);
end
