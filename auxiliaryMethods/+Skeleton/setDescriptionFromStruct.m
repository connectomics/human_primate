function [skel] = setDescriptionFromStruct(skel, descStruct, varargin)
% [skel] = setDescriptionFromStruct(skel, descStruct, varargin)
% Turns fields/values from a struct array into a string and
% sets/appends the skeleton description to this string.
% 
% Inputs:
%   skel       [1D skeleton object]
%   descStruct [1D struct]
%   varargin:
%     append   [bool] (optional, Defaults to false => replace)
%              Whether to replace or append the description.
%     fieldMarks [char] (optional, Defaults to '*' => italic)
%              Which markdown marker to use for the fieldnames
% 
% Author:
%   Martin Schmidt <martin.schmidt@brain.mpg.de>

% Default opts
opts = struct();
opts.validTypes = {@isnumeric, @iscell, @isstring, @ischar, @islogical};
opts.fieldMarker = '*'; % => italic
opts.append = false;
opts = Util.modifyStruct(opts, varargin{:});

assert(isstruct(descStruct));

descStr = '';
fnames = fieldnames(descStruct);
for k = 1:numel(fnames)
    curValue = descStruct.(fnames{k});
    if ~any(cellfun(@(fn) fn(curValue), opts.validTypes))
        continue;
    end
    curValueAsString = Util.char(curValue);
    descStr = strjoin( ...
        {descStr, ...
        [opts.fieldMarker fnames{k} ':' opts.fieldMarker ' ' ...
         curValueAsString]}, ...
        newline);
end

if isempty(descStr)
    return;
end

% wK does not accept <, > and \n in nml files => replace with HTML analogues
replaceChar = {'<', '>', newline};
replaceCharWith = {'&lt;', '&gt;', '&#xa;'};
for idx = 1:numel(replaceChar)
    descStr = ...
        replace(descStr, replaceChar{idx}, replaceCharWith{idx});
end

skel = skel.setDescription(descStr, 'append', opts.append);

end
