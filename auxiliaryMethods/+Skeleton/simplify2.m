function simple = simplify2(skel, centerNodeId, varargin)
    % Attempt at a version of `simplify` that better preserves the
    % branchpoint morphology. `simplify` biases branchpoints towards the
    % center. This should be avoided here.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.voxelSize = [1, 1, 1];
    opts.minBranchLength = 5000;
    opts = Util.modifyStruct(opts, varargin{:});
    assert(isequal(size(opts.voxelSize), [1, 3]));
    assert(ismatrix(skel.edges) && size(skel.edges, 2) == 2);

    %% Build graph
    g = double(skel.nodes);
    g = opts.voxelSize .* ( ...
        g(skel.edges(:, 1), 1:3) ...
      - g(skel.edges(:, 2), 1:3));
    g = sqrt(sum(g .* g, 2));

    g = graph( ...
        skel.edges(:, 1), ...
        skel.edges(:, 2), ...
        g, size(skel.nodes, 1));
    
    centerDists = distances(g, centerNodeId);
    centerDists = reshape(centerDists, [], 1);
    
    %% Find endings
    curEdges = skel.edges;
    curFlipMask = centerDists(curEdges);
    curFlipMask = curFlipMask(:, 1) > curFlipMask(:, 2);
    curEdges(curFlipMask, :) = flip(curEdges(curFlipMask, :), 2);
    
    endNodeIds = true(size(skel.nodes, 1), 1);
    endNodeIds(curEdges(:, 1)) = false;
    endNodeIds = find(endNodeIds);
    
    % Sort endings by decreasing size from soma
   [~, curSortIds] = sort(centerDists(endNodeIds), 'descend');
    endNodeIds = endNodeIds(curSortIds);
    
    assert(not(any(endNodeIds == centerNodeId)));
    endNodeIds = [endNodeIds(:); centerNodeId];
    
    %% Filter endings
    clear cur*;
    
    % LCN = last common node
    lcnDists = centerDists(endNodeIds);
    pwDists = distances(g, endNodeIds, endNodeIds);
    lcnDists = (lcnDists + transpose(lcnDists) - pwDists) / 2;
    
    % Check triangle inequality
    curMask = centerDists(endNodeIds);
    curMask = min(curMask, transpose(curMask)) - lcnDists;
    curMask = curMask < opts.minBranchLength;
    lcnDists(curMask) = nan;
    
    % NOTE(amotta): If we find that pairs of ending candidates are located
    % on the same branch, then we only keep the most distal (the first)
    % candidate.
    curMask(1:(numel(endNodeIds) + 1):end) = false;
    curDelEnd = false(size(endNodeIds));
    
    for curPairId = reshape(find(curMask), 1, [])
       [curIdA, curIdB] = ind2sub(size(lcnDists), curPairId);
        % NOTE(amotta): We exploit that candidate nodes are ordered
        curDelEnd(max(curIdA, curIdB)) = true;
    end
    
    curDelEnd(end) = false;
    endNodeIds(curDelEnd) = [];
    lcnDists(:, curDelEnd) = [];
    lcnDists(curDelEnd, :) = [];
    
    %% Connect endings
    clear cur*;
    
    simple = struct;
    simple.nodeIds = [];
    simple.nodes = nan(0, 3);
    simple.edges = nan(0, 2);
    
    while true
       [curLcnDist, curPairId] = max(lcnDists(:));
        if isnan(curLcnDist); break; end
        
       [curIdB, curIdA] = ind2sub(size(lcnDists), curPairId);
        if curIdA >= curIdB; [curIdA, curIdB] = deal(curIdB, curIdA); end
        
        curNodeIdA = endNodeIds(curIdA);
        curNodeIdB = endNodeIds(curIdB);
        
        curPath = shortestpath(g, curNodeIdA, curNodeIdB);
       [curLcnDist, curLcnNodeId] = min(centerDists(curPath));
        curLcnNodeId = curPath(curLcnNodeId);

        simple.edges = [simple.edges; [ ...
            reshape(curPath((1 + 0):(end - 1)), [], 1), ...
            reshape(curPath((1 + 1):(end - 0)), [], 1)]];
        
        % TODO(amotta): Optimize?
        endNodeIds(curIdB)  = [];
        lcnDists(:, curIdB) = [];
        lcnDists(curIdB, :) = [];
        
        if any(endNodeIds == curLcnNodeId)
            % LCN is already in list of endings
            if curNodeIdA == curLcnNodeId
                % Old LCN is new LCN. Keep it!
            else
                % Avoid introducing a duplicate!
                endNodeIds(curIdA)  = [];
                lcnDists(:, curIdA) = [];
                lcnDists(curIdA, :) = [];
            end
        else
            % Introduce new ending
            endNodeIds(curIdA) = curLcnNodeId;

            curPwDists = distances(g, curLcnNodeId, endNodeIds);
            curLcnDists = centerDists(endNodeIds) + curLcnDist;
            curLcnDists = (curLcnDists(:) - curPwDists(:)) / 2;
            curLcnDists(curIdA) = nan;

            lcnDists(:, curIdA) = curLcnDists;
            lcnDists(curIdA, :) = curLcnDists;
        end
    end
    
   [simple.nodeIds, ~, simple.edges(:)] = unique(simple.edges(:));
    if isempty(simple.nodeIds); simple.nodeIds = centerNodeId; end
    assert(any(simple.nodeIds(:) == centerNodeId));
    
    simple.edges = unique(sort(simple.edges, 2), 'rows');
    simple.nodes = skel.nodes(simple.nodeIds, :);
end
