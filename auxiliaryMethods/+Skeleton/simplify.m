function [simple, inToOutNodeIds] = simplify(skel, centerNodeId, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.voxelSize = [1, 1, 1];
    opts.minBranchLength = 5000;
    opts = Util.modifyStruct(opts, varargin{:});
    assert(isequal(size(opts.voxelSize), [1, 3]));
    
    %% Compute shortest paths
    paths = double(skel.nodes);
    
    paths = ...
        paths(skel.edges(:, 1), 1:3) ...
      - paths(skel.edges(:, 2), 1:3);
  
    paths = paths .* opts.voxelSize;
    paths = sqrt(sum(paths .* paths, 2));
    paths = graph( ...
        skel.edges(:, 1), skel.edges(:, 2), ...
        paths, size(skel.nodes, 1));

    if not(exist('centerNodeId', 'var')) || isempty(centerNodeId)
        [~, centerNodeId] = max(distances(paths), [], 'all', 'linear');
        [centerNodeId, ~] = ind2sub(size(centerNodeId), centerNodeI);
    end

   [paths, dists] = ...
        shortestpathtree( ...
            paths, centerNodeId, ...
            'Method', 'positive', ...
            'OutputForm', 'vector');
        
    if nargout > 1
        % We will modify `paths`
        origPaths = paths;
    end

    %% Trace terminal nodes to branchpoints
    maxTermDist = zeros(size(skel.nodes, 1), 1);
    maxTermNodeId = nan(size(skel.nodes, 1), 1);

    numChildren = reshape(nonzeros(paths), [], 1);
    numChildren = accumarray(numChildren, 1, [numel(paths), 1]);

    termT = table;
    termT.nodeId = reshape(setdiff(1:numel(paths), paths), [], 1);
    termT.branchDist(:) = nan;

    for curIdx = 1:height(termT)
        curTermNodeId = termT.nodeId(curIdx);
        curTermDist = dists(curTermNodeId);

        curTermBranchDist = nan;
        curNodeId = paths(curTermNodeId);
        while logical(curNodeId)
            curDist = curTermDist - dists(curNodeId);
            curIsBranch = numChildren(curNodeId) > 1;

            if curDist > maxTermDist(curNodeId)
                maxTermDist(curNodeId) = curDist;
                maxTermNodeId(curNodeId) = curTermNodeId;
            end

            if isnan(curTermBranchDist) && curIsBranch
                curTermBranchDist = curDist;
            end

            curNodeId = paths(curNodeId);
        end

        termT.branchDist(curIdx) = curTermBranchDist;
    end

    %% Iteratively remove short branches
    clear cur*;
    
   [~, curSortIds] = sort(termT.branchDist, 'ascend');
    termT = termT(curSortIds, :);

    termT.kept(:) = false;
    termT.dirty(:) = true;
    termT.stoppedAt(:) = nan;
    
    % HACKHACKHACK(amotta): MATLAB's tables are waaaaay slower.
    termT = table2struct(termT, 'ToScalar', true);
    
    curIdx = find(termT.dirty, 1);
    while not(isempty(curIdx))
        curTermNodeId = termT.nodeId(curIdx);
        curTermDist = dists(curTermNodeId);
        
        % TODO(amotta): If we are re-processing a dirtied terminal node, we
        % could (probably?) resume the walk at the `stoppedAt` node. But
        % let's play it safe for now and re-do the entire walk.

        curKeep = false;
        curNodeId = paths(curTermNodeId);
        while logical(curNodeId)
            curDist = curTermDist - dists(curNodeId);
            
            if numChildren(curNodeId) > 1 ...
                    || curNodeId == centerNodeId
                curKeep = maxTermNodeId(curNodeId) == curTermNodeId;
                curKeep = curKeep | (curDist > opts.minBranchLength);
                break;
            end

            curNodeId = paths(curNodeId);
        end
        
        curStoppedAt = curNodeId;
        termT.stoppedAt(curIdx) = curNodeId;

        if not(curKeep)
            % Remove branch
            curNodeId = curTermNodeId;
            while curNodeId ~= curStoppedAt
                curNextId = paths(curNodeId);
                paths(curNodeId) = 0;

                if curNextId > 0
                    numChildren(curNextId) = numChildren(curNextId) - 1;
                    % NOTE(amotta): Stop if next node is branchpoint
                    if numChildren(curNextId) > 0; break; end
                end

                curNodeId = curNextId;
            end
        end
        
        % NOTE(amotta): Walks that stopped at branchpoint that are no
        % longer branchpoints need to be repeated. It could be that they
        % need to be removed now.
        curMask = termT.stoppedAt > 0; % Could be zero or NaN
        curMask(curMask) = numChildren(termT.stoppedAt(curMask)) < 2;
        termT.dirty = termT.dirty | curMask;
        
        termT.dirty(curIdx) = false;
        termT.kept(curIdx) = curKeep;
        
        curIdx = find(termT.dirty, 1);
    end

    %% Build output skeleton
    edges = [1:numel(paths); reshape(paths, 1, [])];
    edges = edges(:, all(logical(edges), 1));
    
    % NOTE(amotta): Make sure that the center node is preserved
    assert(isempty(edges) || any(edges(:) == centerNodeId));
    
   [uniNodeIds, ~, edges] = unique(edges);
    edges = transpose(reshape(edges, 2, []));
    
    simple = struct;
    simple.nodeIds = uniNodeIds(:);
    simple.nodes = skel.nodes(uniNodeIds, :);
    
    if isfield(skel, 'segIds')
        simple.segIds = skel.segIds(uniNodeIds);
    end
    
    simple.edges = edges;
    
    %% Build mapping from input to output nodes
    if nargout < 2; return; end
    
    % NOTE(amotta): Depth-first search along the shortest-tree path. This
    % is necessary to make sure that the input-to-output node ID LUT entry
    % for the predecessor node has been set.
    curG = reshape(1:size(skel.nodes, 1), [], 1);
    curG = [origPaths(:), curG];
    curG(centerNodeId, :) = [];
    assert(all(curG(:)));
    
    curG = digraph( ...
        curG(:, 1), curG(:, 2), ...
        [], size(skel.nodes, 1));
    sortIds = dfsearch(curG, centerNodeId);
    Util.clear(curG);
    
    inToOutNodeIds = zeros(size(skel.nodes, 1), 1);
    inToOutNodeIds(simple.nodeIds) = 1:numel(simple.nodeIds);
    assert(logical(inToOutNodeIds(centerNodeId)));
    
    for dstNodeId = reshape(sortIds, 1, [])
        if dstNodeId == centerNodeId; continue; end
        if logical(inToOutNodeIds(dstNodeId)); continue; end
        inToOutNodeIds(dstNodeId) = inToOutNodeIds(origPaths(dstNodeId));
    end
    
    assert(all(inToOutNodeIds));
end
