function simple = reduceNodeDensity(skel, minNodeDistNm, voxelSize)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    if not(exist('voxelSize', 'var')) || isempty(voxelSize)
        voxelSize = [1, 1, 1];
    end
    
    edgeLens = ...
        double(skel.nodes(skel.edges(:, 1), 1:3)) ...
      - double(skel.nodes(skel.edges(:, 2), 1:3));
    
    edgeLens = voxelSize .* edgeLens;
    edgeLens = sqrt(sum(edgeLens .* edgeLens, 2));

    degreeOneNodeIds = accumarray( ...
        skel.edges(:), 1, [size(skel.nodes, 1), 1]);
    degreeOneNodeIds = find(degreeOneNodeIds == 1);

    % Build graph
    g = graph( ...
        skel.edges(:, 1), skel.edges(:, 2), ...
        edgeLens, size(skel.nodes, 1));
    g.Nodes.id(:) = 1:height(g.Nodes);

    curSeedNodeId = 1;
    if not(isempty(degreeOneNodeIds))
        curSeedNodeId = degreeOneNodeIds(1);
    end

    % Split graph into chunks
    g.Nodes.distNm(:) = distances(g, curSeedNodeId);
    g.Nodes.compId = 1 + floor(g.Nodes.distNm / minNodeDistNm);

    % Remove edges across chunks
    removeEdgeIds = find( ...
        g.Nodes.compId(g.Edges.EndNodes(:, 1)) ...
     ~= g.Nodes.compId(g.Edges.EndNodes(:, 2)));
    g = rmedge(g, removeEdgeIds);

    % Split into connected components
    g.Nodes.compId(:) = conncomp(g);

    % Find "closest" node for each component
    compNodeIds = sortrows(g.Nodes, 'distNm');
   [~, curUniIds] = unique(compNodeIds.compId);
    compNodeIds = compNodeIds.id(curUniIds);

    % Build simplified skeleton
    simple = struct;
    simple.nodeIds = compNodeIds(:);
    simple.nodes = skel.nodes(compNodeIds, :);
    
    if isfield(skel, 'segIds')
        simple.segIds = skel.segIds(compNodeIds);
    end

    simple.edges = reshape(g.Nodes.compId(skel.edges), [], 2);
    simple.edges(simple.edges(:, 1) == simple.edges(:, 2), :) = [];
    simple.edges = sort(reshape(simple.edges, [], 2), 2);
    simple.edges = unique(simple.edges, 'rows');
end
