function skel = setDescriptionFromRunInfo(skel, runInfo, descStruct)
    % skel = setDescriptionFromRunInfo(skel, runInfo, descStruct)
    %   Sets the skeleton's description based on the `runInfo` struct
    %   generated using `Util.runInfo`.
    %   If descStruct (struct, optional) is passed, struct fields
    %   and values are parsed to markdown text and appended to
    %   the description.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    skel = skel.setDescription(sprintf( ...
        '%s (%s)', runInfo.filename, runInfo.git_repos{1}.hash));
    if exist('descStruct', 'var') && ~isempty(descStruct) && isstruct(descStruct)
        skel = Skeleton.setDescriptionFromStruct(skel, descStruct, 'append', true);
    end
end
