function skel = setParams4Pipeline(skel, param)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    vxSize = param.raw.voxelSize;
    if isfield(param, 'mag'); vxSize = vxSize ./ param.mag; end
    skel = skel.setParams(param.experimentName, vxSize, 1);
end
