function seg = applyMappingToSegmentationVolume(seg, mapping)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    if ischar(mapping)
        % NOTE(amotta): Yeah, this could also be achieved by a single call
        % to `unique`. But this approach here is more memory efficient.
        uniSegIds = unique(seg);
       [~, seg] = ismember(seg, uniSegIds);
        
        if not(isempty(uniSegIds)) && uniSegIds(1) == 0
            % NOTE(amotta): Ignore segment ID zero
            uniSegIds(1) = [];
            seg = seg - 1;
        end
        
        uniSegIds = uniSegIds + 1;
        mapping = HDF5.readChunked(mapping, ...
            '/segment_to_agglomerate', uniSegIds);
    end
    
    if not(isa(seg, class(mapping)))
        seg = cast(seg, 'like', mapping);
    end
    
    mask = seg ~= 0;
    seg(mask) = mapping(seg(mask));
end
