function applyMappingToSegmentation(p, mapping, outParam)
    % Written by
    %   Manuel Berning <manuel.berning@brain.mpg.de>
    %   Kevin M. Boergens <kevin.boergens@brain.mpg.de>
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
   [boxes, boxSize] = buildBoxes(p, outParam);
    inArgs = arrayfun(@(b) b, boxes, 'UniformOutput', false);
    sharedInArgs = {p.seg, outParam, mapping};
    
    maxTaskCount = 1000;
    taskGroupSize = ceil(numel(inArgs) / maxTaskCount);
    runTime = char(taskGroupSize * duration(0, 30, 0));
    
    % Estimate RAM requirements
    boxVol = prod(boxSize);
    if boxVol <= 512 * 512 * 512
        memory = 12;
    else
        % NOTE(amotta): Known-good data points:
        % * 48 GB for a (1024 vx)³ volume
        memory = boxVol / (1024 * 1024 * 1024);
        memory = ceil(48 * memory);
    end
    
    job = Cluster.startJob( ...
        @doIt, inArgs, ...
        'sharedInputs', sharedInArgs, ...
        'taskGroupSize', taskGroupSize, ...
        'cluster', {'memory', memory, 'time', runTime}, ...
        'name', mfilename());
    Cluster.waitForJob(job);
end

function doIt(inParam, outParam, mapping, bbox)
    seg = loadSegDataGlobal(inParam, bbox);
    seg = Seg.Global.applyMappingToSegmentationVolume(seg, mapping);
    
    if isfield(outParam, 'dtype') ...
            && not(isa(seg, outParam.dtype))
        % NOTE(amotta): If the user requested a specific output data type,
        % make sure that we respect their wishes.
        seg = cast(seg, outParam.dtype);
    end
    
    saveSegDataGlobal(outParam, bbox(:, 1)', seg);
end

function [boxes, boxSize] = buildBoxes(param, outParam)
    boxes = arrayfun(@(l) {l.bboxSmall}, param.local);
    boxSize = param.tileSize;
    
    % NOTE(amotta): If `outParam` points to a compressed WKW dataset, then
    % the boxes must be aligned to the grid of WKW files.
    if isfield(outParam, 'backend') ...
            && strcmp(outParam.backend, 'wkwrap')
        wkwHeader = wkwLoadHeader(outParam.root);
        wkwBlockType = wkwHeader.blockType;
        
        switch wkwBlockType
            case {'LZ4', 'LZ4HC'}
                wkwSize = ...
                    wkwHeader.blocksPerFileDim ...
                  * wkwHeader.voxelsPerBlockDim;
                boxSize = repelem(wkwSize, 1, 3);
              
                boxRanges = ceil(param.bbox / wkwSize);
                boxes = cell(1 + diff(transpose(boxRanges)));
                
                for x = 1:(diff(boxRanges(1, :)) + 1)
                    for y = 1:(diff(boxRanges(2, :)) + 1)
                        for z = 1:(diff(boxRanges(3, :)) + 1)
                            box = [x; y; z] + boxRanges(:, 1) - 1;
                            box = (box - [1, 0]) * wkwSize + [1, 0];
                            boxes{x, y, z} = box;
                        end
                    end
                end
                assert(isequal(size(boxSize), [1, 3]));                
            case 'RAW'
                % nothing to do
                
            otherwise
                error('Unknown block type "%s"', wkwBlockType)
        end
    end
    
end
