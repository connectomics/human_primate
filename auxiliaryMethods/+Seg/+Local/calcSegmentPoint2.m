function [points, segIds, seg] = calcSegmentPoint2(seg, voxelSize)
    % points = calcSegmentPoint2(seg)
    %   This function returns for each segment the coordinates of its
    %   voxel that is closest to the center of mass.
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    if ~exist('voxelSize', 'var') || isempty(voxelSize)
        voxelSize = [1, 1, 1];
    end
    
    segT = table;
    segSize = size(seg);
    segClass = class(seg);
   [segT.id(:), ~, seg] = unique(seg);
    seg = reshape(cast(seg, segClass), segSize);
    
    if segT.id(1) == 0
        segT(1, :) = [];
        seg = seg - 1;
    end
    
    % Sanity check
    assert(all(segT.id(:) > 0));
    
    segT = [segT, regionprops3(seg, 'Centroid', 'VoxelList')];
    if nargout < 3; Util.clear(seg); end

    segT.pos = nan(height(segT), 3);
    for curSegIdx = 1:height(segT)
        curCentroid = segT.Centroid(curSegIdx, :);
        curVoxels = segT.VoxelList{curSegIdx};
        segT.VoxelList{curSegIdx} = [];

        curCentroid = curCentroid(:, [2, 1, 3]);
        curVoxels = curVoxels(:, [2, 1, 3]);

       [~, curSegPos] = pdist2( ...
            voxelSize .* curVoxels, ...
            voxelSize .* curCentroid, ...
            'squaredeuclidean', ...
            'Smallest', 1);

        curSegPos = curVoxels(curSegPos, :);
        segT.pos(curSegIdx, :) = curSegPos;
    end
    
    assert(all(all(segT.pos >= 1)));
    assert(all(all(segT.pos <= segSize)));
    
    points = segT.pos;
    segIds = segT.id;
end
