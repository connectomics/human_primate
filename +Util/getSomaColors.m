function somaColors = getSomaColors(matFlag)
if ~exist('matFlag','var') | isempty(matFlag)
    matFlag = false;
end

% function to define colors for IN groups
somaColors = {[0, 0.5, 0, 1],... %1 green
              [230/255, 159/255, 0/255, 1],... %2 orange
              [0.4940,0.1840,0.5560, 1],... %3 dark purple
              [213/255, 94/255, 0, 1],... %4 vermillion
              [0.6350,0.0780,0.1840, 1],... %5 marroon
              [0.5, 0.5, 0.5, 1],... %6 gray
              [0.75, 0.75, 0, 1],... %7 dark yellow
              [0, 0, 0, 1],... %9 black
              [0.3010, 0.7450, 0.933,1],... %10 skyblue
              [0,158/255,115/255, 1],... % 8 bluish green
              [1, 0, 1, 1],... %11 magenta Stp
              [0, 0, 1, 1]}; %12 blue Sps

if matFlag
    somaColors = cell2mat(somaColors');
    somaColors = somaColors(:,1:3);
end
end
