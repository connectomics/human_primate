function skel = addNodesSomasAsGroup(skel, points, groupName, color )

    if ~exist('color','var') | isempty(color)
        color = [1,0,0,1];
    end

    [skel, id] = skel.addGroup(groupName);
    skel = skel.addNodesAsTrees(points,'','',repelem(color, size(points,1),1));
    treesNew = skel.numTrees - size(points,1) + 1 : skel.numTrees;
    skel = skel.addTreesToGroup(treesNew , id);
end

