function a = setPlotDefault( a , xLimRestrict, yLimRestrict)
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
% modified by SL
if ~exist('a', 'var') || isempty(a)
    a = gca;
end
if ~exist('xLimRestrict', 'var') || isempty(xLimRestrict)
    xLimRestrict = false;
end      
if ~exist('yLimRestrict', 'var') || isempty(yLimRestrict)
    yLimRestrict = false;
end

a.FontName = 'Arial';
a.TickDir = 'out';

if xLimRestrict
    a.XLim = [0 1];
    a.XTick = [0 0.5 1];
end

if yLimRestrict
    a.YLim = [0 1];
    a.YTick = [0 0.5 1];
end

a.Box = 'off';

end

