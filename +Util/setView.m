function a = setView(index,doMore, a)
% Author: Sahil Loomba <sahil.loomba@brain.mpg.de>
% set views for the HP/NHP background
if ~exist('doMore', 'var') || isempty(doMore)
    doMore = false;
end
if ~exist('a', 'var') || isempty(a)
    a = gca;
end

switch index
    case 1
        %xy
        view([0, -90]);
    case 2
         %xz
         view([0, 180]);
    case 3
        %yz
        view([-90, 180]);
        camroll(90);
end

if doMore
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
set(gca,'ztick',[])
set(gca,'zticklabel',[])
axis off
box off
end
end

