function plotAxonProfile(curAxonsData, ax, targetNames)
    curAxonsData = table2array(curAxonsData);
    % convert numbers to fractions
    individualSpecificityValues = curAxonsData./sum(curAxonsData,2);
    n = size(curAxonsData,1);
    colors = Util.getRangedColors(0,1,0,1,0,1,n);
    for j = 1:size(individualSpecificityValues,1)
        curDataToPlot = individualSpecificityValues(j,:);
        noise = 0.1*rand(1) + (1:numel(curDataToPlot));        % add noise 
        plot(noise, curDataToPlot, 'LineStyle' ,'-', 'Marker','o',...
            'Color', colors(j,:),'MarkerFaceColor', 'none',...
            'MarkerEdgeColor' ,'none','Parent',ax)
    end
    if size(individualSpecificityValues,1)>2
        means = Util.Stat.mean(individualSpecificityValues,'',1);
        sems = Util.Stat.sem(individualSpecificityValues,'',1);
        errorbar(means,sems,'Color','k', 'Parent', ax);
    end
       
    ax.YAxis.TickValues = 0:0.2:1;
    ax.YAxis.Limits = [0,1];
%     ax.YAxis.Label.String = 'Fractional innervation by other synapses';
    ax.LineWidth = 2;
    ax.XAxis.TickValues = 1:1:numel(targetNames);
    ax.XAxis.Limits = [0, numel(targetNames)+0.5];
    ax.XAxis.TickLabels = targetNames;
%     ax.XAxis.Label.String = 'Post synaptic targets';
    Util.setPlotDefault(ax,'','');
end
