function skel = mergeSkelsMinDist( skel1, skel2, scale )
%MERGEMINDIST Merge two skeletons at the nodes with minimal distance.
% INPUT skel: struct
%           First skeleton.
%       skel2: struct
%           Second skeleton.
%       sacles: (Optional) [1x3] double
%           Node scale used for distance calculation.
%           (Default: [11.24, 11.24, 28])
% OUTPUT skel: struct
%           The merged skeleton.
% Author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>
%       : Sahil Loomba <sahil.loomba@brain.mpg.de>

% cat fields from old agglos

if nargin < 3
    scale = [str2num(skel1.parameters.scale.x),str2num(skel1.parameters.scale.y),str2num(skel1.parameters.scale.z)];
end
scale = scale(:)';

nodes = cat(1, skel1.nodes{1}, skel2.nodes{1});
offset = size(skel1.nodes{1}, 1);
edges = cat(1, skel1.edges{1}, skel2.edges{1} + offset);

% introduce a new node with shortest distance
d = pdist2(bsxfun(@times, skel1.nodes{1}(:,1:3), scale), ...
           bsxfun(@times, skel2.nodes{1}(:,1:3), scale));
[~, idx] = min(d(:));
[x, y] = ind2sub(size(d), idx);

% add new edge between shortest nodes
edges(end+1, :) = [x, y + offset];

% create new agglo
expName = skel1.parameters.experiment.name;
skel = skeleton();
skel = skel.setParams(expName,scale,[0 0 0]);
skel = skel.addTree('mergedTree',nodes(:,1:3),edges);

end


