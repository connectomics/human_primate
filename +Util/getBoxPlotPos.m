function [pos, posS] = getBoxPlotPos(numGroups,startS)
% positions for placing n groups for a box plot
% always start first box at 1
pos = 2:4:(2*numGroups-1)*2;
posS = startS:4:(2*numGroups-1)*2;
end
