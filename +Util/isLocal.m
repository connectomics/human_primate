function flag = isLocal()
    % find whether you are on local machine or gaba
    root = matlabroot;
    flag = isempty(regexpi(root,'gaba')) & isempty(regexpi(root,'garching'));
end


