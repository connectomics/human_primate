function [coords, indices] = getNodeCoordsWithComment(skel,comments,type, treeIdx)
% Description:
% get coordinates of the nodes with comment
%   Input: 
%       skel: skeleton
%       comments = cell-array of strings or a string eg. {'sp','sh'} or 'sp'
%       type: same as in getNodesWithComment function
%   Output:
%       coords: Nx3 array of list of coordinates

coords = []; somaCoord = [];
if ~iscell(comments)
    comments = {comments};
end
if iscolumn(comments)
    comments = comments';
end

if ~exist('treeIdx','var') | isempty(treeIdx)
    treesToSearch = 1:skel.numTrees;
else
    treesToSearch = treeIdx;
end


indices = [];
for comment = comments
    for i = treesToSearch
        m = skel.getNodesWithComment(comment{1},i,type,true);
        mm = ~cellfun(@isempty,m); % in case that tree doesn't have the comment
        loc = find(mm);
        if ~isempty(loc)
            indices = vertcat(indices, m{loc(1)});
            somaCoord = skel.nodes{i}(m{loc(1)},1:3);
            coords = vertcat(coords,somaCoord);
        end
    end
end    
end


