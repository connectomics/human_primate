function histogramAndKsdensity(data, ax, curColor, bounds, bw)
% from Karimi et al fig.2
    if ~exist('ax','var') || isempty(ax)
        ax = gca;
    end
    hold on
    
    % histogram
    yyaxis(ax,'left');
    histogram(ax, ...
         data,0:0.1:1, ...
        'EdgeColor', curColor, ...
        'FaceAlpha', 0);
    
    % ksdensity
    yyaxis(ax,'right');
    if ~exist('bounds','var') || isempty(bounds)
        bounds = [min(data),max(data)];
    end
    if ~exist('bandwidth','var') || isempty(bounds)
        bw = [];
    end
    
    range = diff(bounds);
    [f,xi] = ksdensity(data,bounds(1):range/100:bounds(2),...
        'Support',[bounds(1)-1e-5 bounds(2)+1e-5],...
        'BoundaryCorrection','reflection',...
        'Bandwidth',bw);
    plot(xi,f,'-','Color', curColor, 'Parent',ax);
    if ((1-trapz(xi,f))>1e-3)
        warning (['Kernel Density sums up to: ',num2str(trapz(xi,f))])
    end

end