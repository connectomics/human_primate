function [types, aggloMeans] = buildProbsForAggloFile(param, aggloFile)
    % Copy of mSEM.TypeEM.buildProbsForAggloFile
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
   [types, segMeans] = HNHP.Auto.TypeEM.loadSegmentStats(param);
    segVols = Vx.Seg.getMetaData(param, 'size');
    
    segIds = h5read(aggloFile, '/agglomerate_to_segments');
    aggloOffs = h5read(aggloFile, '/agglomerate_to_segments_offsets');
    assert(isscalar(aggloOffs) || aggloOffs(2) == 0);
    
    aggloCounts = numel(aggloOffs) - 1;
    aggloMeans = [size(segMeans, 1), aggloCounts];
    aggloMeans = nan(aggloMeans, 'single');
    
    for curAggloIdx = (1 + 1):aggloCounts
        curStart = 1 + aggloOffs(curAggloIdx);
        curEnd = aggloOffs(curAggloIdx + 1);
        curSegIds = segIds(curStart:curEnd);
        
        curVols = double(segVols(curSegIds));
        curVols = curVols / sum(curVols(:));
        curVols = reshape(curVols, 1, []);
        
        curMeans = segMeans(:, curSegIds);
        curMeans = sum(curVols .* curMeans, 2);
        aggloMeans(:, curAggloIdx) = curMeans;
    end
    
    % NOTE(amotta): The per-agglomerate probabilities sum up to a total
    % that is close to one, but not exactly one. (The error is on the order
    % of percentiles or less). Still, let's smoothen out these numerical
    % instabilities.
    aggloMeans = aggloMeans ./ sum(aggloMeans, 1);
end
