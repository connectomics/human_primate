function [varargout] = loadSegmentStats(varargin)
    % Copy of mSEM.TypeEM.loadSegmentStats
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    varargout = cell(1, nargout);
   [varargout{:}] = Vx.Seg.getTypeProbs(varargin{:});
end
