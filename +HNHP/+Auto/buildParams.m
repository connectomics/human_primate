function params = buildParams()
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    params = struct;

    % Mouse S1
    params.mouseS1 = struct;
    % See l23_connectome_v3 workflow:
    % https://static.voxelytics.com/workflows/viewers/v22/index.html?workflowName=ac360144fa.json
    params.mouseS1.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__f20425459c-v1/agglomerate_view/agglomerate_view_80.hdf5';
    params.mouseS1.connFile = '/conndata/georgwie/artifacts/default/create_connectome/connectome_v3_mapping_80__0946bdc9a9-v1/connectome/connectome.hdf5';
    params.mouseS1.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__1c1255b32c-v1/segment_stats/merged_segment_stats.hdf5';
    params.mouseS1.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__9ab38c02c4-v1/segment_types/segment_types_all.hdf5';
    params.mouseS1.gtSkelFileRel = 'data/tracings/ex144-08x2/hnhp/synapse-ground-truth_box-01.nml';
    params.mouseS1.axonsMinTypeProb = 0.5;
    params.mouseS1.dendritesMinTypeProb = 0.5;

    % Mouse A2
    params.mouseA2 = struct;
    % See a2_connectome_v1 workflow:
    % https://static.voxelytics.com/workflows/viewers/v27/index.html?workflowName=8d70a34f22.json
    params.mouseA2.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__5a9d9feca2-v1/agglomerate_view/agglomerate_view_80.hdf5';
    params.mouseA2.connFile = '/conndata/georgwie/artifacts/default/create_connectome/connectome_a2_mapping_80__f1989cb74f-v1/connectome/connectome.hdf5';
    params.mouseA2.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__af3cc87e41-v1/segment_stats/merged_segment_stats.hdf5';
    params.mouseA2.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__def27929db-v1/segment_types/segment_types_all.hdf5';
    params.mouseA2.gtSkelFileRel = 'data/tracings/mouse-a2-l23/synapse-ground-truth_box-01.nml';
    params.mouseA2.axonsMinTypeProb = 0.65;
    params.mouseA2.dendritesMinTypeProb = 0.40;

    % Mouse V2
    params.mouseV2 = struct;
    % See V2_connectome_v2 workflow:
    % https://static.voxelytics.com/workflows/viewers/v22/index.html?workflowName=5c728ca2b9.json
    params.mouseV2.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__29cdeff7e8-v1/agglomerate_view/agglomerate_view_80.hdf5';
    params.mouseV2.connFile = '/conndata/georgwie/artifacts/default/create_connectome/connectome_v2_mapping_80__3bfc57cf7d-v1/connectome/connectome.hdf5';
    params.mouseV2.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__99a53365df-v1/segment_stats/merged_segment_stats.hdf5';
    params.mouseV2.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__9912cc001d-v1/segment_types/segment_types_all.hdf5';
    params.mouseV2.gtSkelFileRel = 'data/tracings/mouse-v2-l23/synapse-ground-truth_box-01.nml';
    params.mouseV2.axonsMinTypeProb = 0.65;
    params.mouseV2.dendritesMinTypeProb = 0.50;

    % Mouse PPC
    params.mousePPC = struct;
    % See ppcAK99_connectome_v3 workflow:
    % https://static.voxelytics.com/workflows/viewers/v22/index.html?workflowName=386b4c22dd.json
    params.mousePPC.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__bac82cc436-v1/agglomerate_view/agglomerate_view_95.hdf5';
    params.mousePPC.connFile = '/conndata/georgwie/artifacts/default/create_connectome/connectome_v3_mapping_80_with_spine_head_agglomeration__aa64e1e8f2-v1/connectome/connectome.hdf5';
    params.mousePPC.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__0f741ae239-v1/segment_stats/merged_segment_stats.hdf5';
    params.mousePPC.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__ed25c381d8-v1/segment_types/segment_types_all.hdf5';
    params.mousePPC.gtSkelFileRel = 'data/tracings/2017-04-03_ppcAK99_76x79/synapse-ground-truth_box-01.nml';
    params.mousePPC.axonsMinTypeProb = 0.5;
    params.mousePPC.dendritesMinTypeProb = 0.25;

    % Mouse ACC
    params.mouseACC = struct;
    % See acc_z_connectome_v2 workflow:
    % https://static.voxelytics.com/workflows/viewers/v22/index.html?workflowName=8b85d3b90d.json
    params.mouseACC.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__94f1b3b6e4-v1/agglomerate_view/agglomerate_view_80.hdf5';
    params.mouseACC.connFile = '/conndata/georgwie/artifacts/default/create_connectome/connectome_v2_mapping_80__31790e9cb2-v1/connectome/connectome.hdf5';
    params.mouseACC.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__b411d69c0f-v1/segment_stats/merged_segment_stats.hdf5';
    params.mouseACC.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__56ecb5d0d9-v1/segment_types/segment_types_all.hdf5';
    params.mouseACC.gtSkelFileRel = 'data/tracings/mouse-acc-l23/synapse-ground-truth_box-02.nml';
    params.mouseACC.axonsMinTypeProb = 0.5;
    params.mouseACC.dendritesMinTypeProb = 0.5;

    % Macaque (Mk1) S1 L2/3
    params.macaqueS1 = struct;
    % See mk1_l23_connectome_v3 workflow:
    % https://static.voxelytics.com/workflows/viewers/v30/index.html?workflowName=bc1f52f712.json
    params.macaqueS1.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__d24fa48f6f-v1/agglomerate_view/agglomerate_view_80.hdf5';
    params.macaqueS1.connFile = '/conndata/georgwie/artifacts/default/create_connectome/connectome_v3_mapping_80__7f10b3030c-v1/connectome/connectome.hdf5';
    params.macaqueS1.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__16d56e436c-v1/segment_stats/merged_segment_stats.hdf5';
    params.macaqueS1.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__27feb0ccb1-v1/segment_types/segment_types_all.hdf5';
    params.macaqueS1.gtSkelFileRel = 'data/tracings/mk1-s1-l23/synapse-ground-truth_box-01.nml';
    params.macaqueS1.axonsMinTypeProb = 0.35;
    params.macaqueS1.dendritesMinTypeProb = 0.25;

    % Macaque (Mk1) STG L2/3
    params.macaqueSTG = struct;
    % See mk1_t2_connectome_v2 workflow:
    % https://static.voxelytics.com/workflows/viewers/v30/index.html?workflowName=f0df59300b.json
    params.macaqueSTG.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__eac28a688c-v1/agglomerate_view/agglomerate_view_80.hdf5';
    params.macaqueSTG.connFile = '/conndata/georgwie/artifacts/default/create_connectome/connectome_v2_mapping_80__ecff04092a-v1/connectome/connectome.hdf5';
    params.macaqueSTG.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__e262e5b7bb-v1/segment_stats/merged_segment_stats.hdf5';
    params.macaqueSTG.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__f13d6a8dbe-v1/segment_types/segment_types_all.hdf5';
    params.macaqueSTG.gtSkelFileRel = 'data/tracings/mk1-stg-l23/synapse-ground-truth_box-01.nml';
    params.macaqueSTG.axonsMinTypeProb = 0.25;
    params.macaqueSTG.dendritesMinTypeProb = 0.25;

    % Human H5
    params.humanH5 = struct;
    % See st700_connectome_v3 workflow:
    % https://static.voxelytics.com/workflows/viewers/v29/index.html?workflowName=3f8dc3af0c.json
    params.humanH5.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view__2849f14e55-v1/agglomerate_view/agglomerate_view_95.hdf5';
    params.humanH5.connFile = '/conndata/georgwie/artifacts/default/create_connectome/st700_connectome_v3__8bc69d5f60-v1/connectome/connectome.hdf5';
    params.humanH5.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__5733a855f9-v1/segment_stats/merged_segment_stats.hdf5';
    params.humanH5.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__1de8c01b50-v1/segment_types/segment_types_all.hdf5';
    params.humanH5.gtSkelFileRel = 'data/tracings/h5-amk-st700/synapse-ground-truth_box-01.nml';
    params.humanH5.axonsMinTypeProb = 0.4;
    params.humanH5.dendritesMinTypeProb = 0.4;

    % Human H6
    params.humanH6 = struct;
    % See h6_4s_connectome_v2 workflow:
    % https://static.voxelytics.com/workflows/viewers/v29/index.html?workflowName=4412c4de2f.json
    params.humanH6.aggloFile = '/conndata/georgwie/artifacts/default/create_agglomerate_view/agglomerate_view_fixed_mapping__bf499c2b80-v1/agglomerate_view/agglomerate_view_95.hdf5';
    params.humanH6.connFile = '/conndata/georgwie/artifacts/default/create_connectome/h6_42_connectome_v2__b4ba7d0f6f-v1/connectome/connectome.hdf5';
    params.humanH6.param.files.segmentStats = '/conndata/georgwie/artifacts/default/compute_segment_stats/segment_stats__4e2bcc869f-v1/segment_stats/merged_segment_stats.hdf5';
    params.humanH6.param.files.segmentTypes = '/conndata/georgwie/artifacts/default/compute_segment_types/segment_types__9fd7be9180-v1/segment_types/segment_types_all.hdf5';
    params.humanH6.gtSkelFileRel = 'data/tracings/human-h6/synapse-ground-truth_box-01.nml';
    params.humanH6.axonsMinTypeProb = 0.25;
    params.humanH6.dendritesMinTypeProb = 0.4;
end
