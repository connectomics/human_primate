function [spineProbs, postDist, lut] = ...
        inferSpineSynapseFraction( ...
            errorRates, axons, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.verbose = false;
    opts.axonWeights = 'constant';
    opts.maxSynCountForSimulation = 50;
    opts.spineSynProbs = linspace(0, 1, 101);
    opts = Util.modifyStruct(opts, varargin{:});
    
    % Extract options
    maxSynCount = opts.maxSynCountForSimulation;
    spineProbs = reshape(opts.spineSynProbs, 1, []);
    
    % NOTE(amotta): The columns of `axons` correspond to
    % 1. the number of spine synapses, and
    % 2. the number of shaft synapses. 
    assert(ismatrix(axons) && size(axons, 2) == 2);
    assert(all(sum(axons, 2) <= maxSynCount));
    assert(all(axons(:) >= 0));
    
    if opts.verbose; Util.log('Computing look-up table'); end
    lut = buildLUT(errorRates, maxSynCount, spineProbs);
    assert(isequal(size(lut, 1), numel(spineProbs)));
    
    if opts.verbose; Util.log('Computing posterior distribution'); end
    
    switch lower(opts.axonWeights)
        case 'constant'; axonWeights = ones(size(axons, 1), 1);
        case 'synapsecount'; axonWeights = sum(axons, 2);
        otherwise; error('Unknown weight method "%s"', opts.axonWeights);
    end
    
    assert(isequal(size(axons, 1), numel(axonWeights)));
    
    postDist = zeros(1, size(lut, 1));
    for curAxonIdx = 1:size(axons, 1)
        curAxon = axons(curAxonIdx, :);
        curWeight = axonWeights(curAxonIdx);
        curPostDist = curWeight * lut(:, 1 + curAxon(1), 1 + curAxon(2));
        postDist = postDist + reshape(curPostDist, size(postDist));
    end
end

function P = buildLUT(errorRates, maxSynCount, spineProbs)
    % Precomputations
   [rows, cols] = ndgrid(0:maxSynCount, 0:maxSynCount);
    spineRecallMat = binopdf(rows, cols, errorRates.spineRecall);
    shaftRecallMat = binopdf(cols, rows, errorRates.shaftRecall);
    
    spineToShaftLUT = cell(1 + maxSynCount);
    shaftToSpineLUT = cell(1 + maxSynCount);
    for curSynCount = 0:maxSynCount
        spineToShaftLUT{1 + curSynCount} = binopdf( ...
            0:curSynCount, curSynCount, errorRates.spineToShaft);
        shaftToSpineLUT{1 + curSynCount} = binopdf( ...
            0:curSynCount, curSynCount, errorRates.shaftToSpine);
    end
    
    spinePrecisionMat = binopdf(cols, rows, errorRates.spinePrecision);
    shaftPrecisionMat = binopdf(rows, cols, errorRates.shaftPrecision);
    
    tic;
    % NOTE(amotta): Dimensions are permuted from N×N×L to L×N×N at the end
    P = nan(1 + maxSynCount, 1 + maxSynCount, numel(spineProbs));
    for spineProbIdx = 1:numel(spineProbs)
        Util.progressBar(spineProbIdx, numel(spineProbs));
        spineProb = spineProbs(spineProbIdx);

        % TODO(amotta): Zero entries in bottom-right corner?
        p = binopdf(rows, rows + cols, spineProb);
        clear cur*;
        
        p = spineRecallMat * p;
        p = p * shaftRecallMat;

        % Synapse type confusion
        q = zeros(1 + maxSynCount);
        for curSpBefore = 0:maxSynCount
            for curShBefore = 0:maxSynCount
                curP = ...
                    reshape(spineToShaftLUT{1 + curSpBefore}, [], 1) ...
                 .* reshape(shaftToSpineLUT{1 + curShBefore}, 1, []);
               [curSpAfter, curShAfter] = ndgrid( ...
                    0:curSpBefore, 0:curShBefore);

                % NOTE(amotta): Above,
                % * rows corr. to # spine synapses that flip to shaft
                % * columns corr. to # shaft synapses that flip to spine
                %
                % Now, we change the format such that
                % * rows corr. to # spine synapses left after confusion
                % * columns corr. to # shaft synapses left after confusion

               [curSpAfter, curShAfter] = deal( ...
                    curSpBefore - curSpAfter + curShAfter, ...
                    curShBefore - curShAfter + curSpAfter);

                curMask = ...
                    (0 <= curSpAfter) & (curSpAfter <= maxSynCount) ...
                  & (0 <= curShAfter) & (curShAfter <= maxSynCount);

                curP = curP(curMask);
                curSpAfter = curSpAfter(curMask);
                curShAfter = curShAfter(curMask);

                curIds = sub2ind(size(q), 1 + curSpAfter, 1 + curShAfter);
               [curIds, ~, curRelIds] = unique(curIds);
                curP = accumarray(curRelIds, curP);

                curP = curP * p(1 + curSpBefore, 1 + curShBefore);
                q(curIds(:)) = q(curIds(:)) + curP(:);
            end
        end
        p = q;
        Util.clear(q);
        
        p = spinePrecisionMat * p;
        p = p * shaftPrecisionMat;

        P(:, :, spineProbIdx) = p;
    end
    
    P = permute(P, [3, 1, 2]);
    P = P ./ sum(P, 1);
end
