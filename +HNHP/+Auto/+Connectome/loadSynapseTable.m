function synT = loadSynapseTable(connFile)
    % Modified copy of mSEM.Connectome.loadSynapseTable
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de
    
    dsetNames = h5info(connFile);
    dsetNames = {dsetNames.Datasets.Name};
    spineHeadIdDsetName = '/synapse_spine_head_ids';
    hasSpineHeadIds = ismember(spineHeadIdDsetName(2:end), dsetNames);
    
    aggloIds = [ ...
        flatten(h5read(connFile, '/synapse_to_src_agglomerate')), ...
        flatten(h5read(connFile, '/synapse_to_dst_agglomerate'))];
    synPos = transpose(h5read(connFile, '/synapse_positions'));
    synTypes = flatten(readSynapseTypes(connFile));

    % Load spine head IDs
    if hasSpineHeadIds
        spineHeadIds = flatten(h5read(connFile, spineHeadIdDsetName));
    else
        warning('Spine head IDs are missing from %s', connFile);
        spineHeadIds = nan(size(aggloIds, 1), 1);
    end
    
    synT = table;
    synT.id = flatten(1:size(aggloIds, 1));
    synT.aggloIds = aggloIds;
    synT.spineHeadId = spineHeadIds;
    synT.type = synTypes;
    synT.pos = synPos;
end

function v = flatten(v)
    v = v(:);
end

function synTypes = readSynapseTypes(connFile)
    synTypeTranslation = struct;
    synTypeTranslation.dendritic_shaft_synapse = 'Shaft';
    synTypeTranslation.spine_head_synapse = 'SpineHead';
    synTypeTranslation.soma_synapse = 'Soma';
    
    typeNames = h5readatt(connFile, '/', 'synapse_types');
    for curIdx = 1:numel(typeNames)
        curTypeName = typeNames{curIdx};
        curTypeName = strrep(curTypeName, '-', '_');
        curTypeName = synTypeTranslation.(curTypeName);
        typeNames{curIdx} = curTypeName;
    end
    typeNames = categorical(typeNames);
    
    synTypes = h5read(connFile, '/synapse_types');
    synTypes = typeNames(1 + synTypes);
end
