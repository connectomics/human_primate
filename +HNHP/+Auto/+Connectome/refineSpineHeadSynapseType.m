function synT = refineSpineHeadSynapseType(synT)
    % Copy of mSEM.Connectome.refineSpineHeadSynapseType
    %
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
   [uniSpineHeadIds, ~, relSpineHeadIds] = unique(synT.spineHeadId);
    assert(uniSpineHeadIds(1) == 0);

    % TODO(amotta): Count all synapses or only 'SpineHead' synapses?
    synCountsPerSpineHead = accumarray(relSpineHeadIds, 1);

    mask = ...
        synT.type == 'SpineHead' ...
      & logical(synT.spineHeadId) ...
      & synCountsPerSpineHead(relSpineHeadIds) > 1;
    synT.type(mask) = 'MultiSynSpineHead';

    % NOTE(amotta): Treat `SpineHead` synapses without postsynaptic spine
    % head segment ID as single-synaptic spine head synapses.
    mask = synT.type == 'SpineHead' & not(mask);
    synT.type(mask) = 'SingleSynSpineHead';
    
    % Now there should be no more `SpineHead` synapses, only
    % `SingleSynSpineHead` and `MultiSynSpineHead`.
    assert(not(any(synT.type == 'SpineHead')));
    synT.type = removecats(synT.type, {'SpineHead'});
end
