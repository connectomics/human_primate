% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
% NOTE(amotta): Path to outputs of HNHP.Auto.estimateErrorRates
dataDir = '/conndata/amotta/hnhp/2022-04-11-analysis-of-automated-reconstructions/2022-04-16-run-02';

minSynCount = 5;
maxSynCount = 25;
excProbThreshold = 0.5;

binEdges = linspace(0, 1, 11);

% NOTE(amotta): See email from MH of 12.04.2022
colorLims = [0, 0.20];

clear cur*;
info = Util.runInfo();
Util.showRunInfo(info);

%% Loading data
params = HNHP.Auto.buildParams();
datasetNames = reshape(fieldnames(params), 1, []);

mouseNames = datasetNames(startsWith(datasetNames, 'mouse'));
macaqueNames = datasetNames(startsWith(datasetNames, 'macaque'));
humanNames = datasetNames(startsWith(datasetNames, 'human'));
primateNames = union(macaqueNames, humanNames, 'stable');

cmap = Util.twoColorMap([1, 1, 1], [0, 0, 0], 101);

%% Loading data
inhAxonT = cell(0, 1);
excAxonT = cell(0, 1);
for curDatasetName = datasetNames
    curDatasetName = curDatasetName{1}; %#ok
    
    curInFile = fullfile(dataDir, sprintf( ...
        '%s_data.mat', lower(curDatasetName)));
    curAxonT = Util.load(curInFile, 'axonT');
    
    curAxonT = curAxonT( ...
        minSynCount <= curAxonT.outSynCountAll ...
      & curAxonT.outSynCountAll <= maxSynCount, :);
    
    % Inhibitory axons
    curInhAxonT = curAxonT(curAxonT.excProb < excProbThreshold, :);
    assert(not(any(isnan(curInhAxonT.ontoSpinyDendProbCorrected))));
    
    curInhAxonT.ontoSmoothDendProbCorrected = ...
        1 - curInhAxonT.ontoSpinyDendProbCorrected;
    inhAxonT{end + 1} = curInhAxonT; %#ok
    
    % Excitatory axons
    curExcAxonT = curAxonT(curAxonT.excProb > excProbThreshold, :);
    curExcAxonT.ontoSmoothDendProbUncorrected = ...
        1 - curExcAxonT.ontoSpinyDendProbUncorrected;
    excAxonT{end + 1} = curExcAxonT; %#ok
end

%% Summary statistics
clear cur*;

curConfigs = struct;
curConfigs(1).tag = 'mouse';   curConfigs(1).dsetNames = mouseNames;
curConfigs(2).tag = 'macaque'; curConfigs(2).dsetNames = macaqueNames;
curConfigs(3).tag = 'human';   curConfigs(3).dsetNames = humanNames;
curConfigs(4).tag = 'primate'; curConfigs(4).dsetNames = primateNames;

curConfigData = cell(size(curConfigs));
fprintf('P(onto smooth dendrite | inhibitory axon)\n');
for curConfigIdx = 1:numel(curConfigs)
    curConfig = curConfigs(curConfigIdx);
    
   [~, curData] = ismember(curConfig.dsetNames, datasetNames);
    curData = cat(1, inhAxonT{curData});
    
    curData = curData.ontoSmoothDendProbCorrected;
    curConfigData{curConfigIdx} = curData;
    
    fprintf('  For %s (n=%d datasets, n=%d axons):\n', ...
        curConfig.tag, numel(curConfig.dsetNames), numel(curData));
    fprintf('    mean +- sd: %.1f%% +- %.1f%%\n', ...
        100 * mean(curData), 100 * std(curData));
    fprintf('    median: %.1f%%\n', ...
        100 * median(curData));
end

curPValues = nan(numel(curConfigs));
for curRefIdx = 1:numel(curConfigs)
    curRefData = curConfigData{curRefIdx};
    
    for curTestIdx = 1:numel(curConfigs)
        if curRefIdx == curTestIdx; continue; end
        curTestData = curConfigData{curTestIdx};
       [~, curPValue] = kstest2(curRefData, curTestData, 'Tail', 'larger');
        curPValues(curRefIdx, curTestIdx) = curPValue;
    end
end

curPValues = array2table(curPValues, ...
    'VariableNames', {curConfigs.tag}, ...
    'RowNames', {curConfigs.tag});

fprintf('\n');
fprintf('  One-sided Kolmogorov-Smirnov test\n');
fprintf('\n');
curOldFormat = matlab.internal.display.format;
format('short'); disp(curPValues); format(curOldFormat);

%% Building heatmap
clear cur*;

heatMap = nan( ...
    numel(binEdges) - 1, ...
    numel(datasetNames));

for curDatasetIdx = 1:numel(datasetNames)
    curDatasetName = datasetNames{curDatasetIdx};
    
    curHist = inhAxonT{curDatasetIdx};
    curHist = discretize(curHist.ontoSmoothDendProbCorrected, binEdges);
    curHist = accumarray(curHist, 1, [numel(binEdges) - 1, 1]);
    curHist = curHist / sum(curHist);
    
    heatMap(:, curDatasetIdx) = curHist;
end

%% Show heatmap
clear cur*;

curFig = figure();
curAx = axes(curFig);

colormap(curAx, cmap);
imagesc(curAx, 100 * heatMap);

axis(curAx, 'xy');
axis(curAx, 'equal');
axis(curAx, 'tight');

xticks(curAx, 1:numel(datasetNames));
xticklabels(curAx, datasetNames);
xtickangle(curAx, 45);

yticks(curAx, (1:numel(binEdges)) - 0.5);
yticklabels(curAx, arrayfun(@num2str, ...
    100 * binEdges, 'UniformOutput', false));
ylabel(curAx, 'P(onto smooth dendrite | inhibitory axon) [%]');

curCbar = colorbar(curAx);
curCbar.Label.String = 'Fraction of inhibitory axons [%]';

for curCol = 1:size(heatMap, 2)
    curAnn = round(100 * heatMap(1, curCol));
    curAnn = sprintf('%d%%', curAnn);
    
    text( ...
        curAx, curCol, 1, curAnn, ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'middle', ...
        'Color', 'white');
end

curAx.CLim = 100 * colorLims;
curCbar.Ticks = 0:5:curAx.CLim(2);
curCbar.TickLabels{end} = sprintf( ...
    '≥%s', curCbar.TickLabels{end});

Figure.config(curFig, info);
curFig.Position(3:4) = [450, 440];
