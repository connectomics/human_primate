% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Building parameters
params = HNHP.Auto.buildParams();
dataDir = '/conndata/amotta/hnhp/2022-04-11-analysis-of-automated-reconstructions/2022-04-16-run-02';

minSynCount = 5;
maxSynCount = 25;
maxExcProb = 0.5;

info = Util.runInfo();
Util.showRunInfo(info);

%% Computing numbers
curDatasetNames = fieldnames(params);
summaryT = cell(numel(curDatasetNames), 1);
for curDatasetIdx = 1:numel(curDatasetNames)
    curDatasetName = curDatasetNames{curDatasetIdx};
    
    curInFile = fullfile(dataDir, sprintf( ...
        '%s_data.mat', lower(curDatasetName)));
    curAxonT = Util.load(curInFile, 'axonT');
    
    curAxonT = curAxonT( ...
        minSynCount <= curAxonT.outSynCountAll ...
      & curAxonT.outSynCountAll <= maxSynCount, :);
    curAxonT.isInh = curAxonT.excProb < maxExcProb;
    
    curSummaryT = table;
    curSummaryT.dataset = {curDatasetName};
    curSummaryT.nAxons = height(curAxonT);
    curSummaryT.nSynapses = sum(curAxonT.outSynCountAll);
    curSummaryT.nInhAxons = sum(curAxonT.isInh);
    curSummaryT.nInhSynapses = sum(curAxonT.outSynCountAll(curAxonT.isInh));
    summaryT{curDatasetIdx} = curSummaryT;
end

summaryT = cat(1, summaryT{:});

curTotalT = sum(summaryT{:, 2:end}, 1);
curTotalT = [{'TOTAL'}, num2cell(curTotalT)];
summaryT(end + 1, :) = curTotalT;

fprintf('\n');
disp(summaryT);
