% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
datasetTag = 'mouseS1';

do = struct;
do.plotPreAndPostsynapticAgglomerateTypes = false;
do.restrictToGroundTruthSynapsesWithPresynapticAxon = true;
do.plotConnectome = false;

out = struct;
out.dir = '/conndata/amotta/hnhp/2022-04-11-analysis-of-automated-reconstructions/2022-04-16-run-02';
out.doSave = true && not(isempty(out.dir));
out.buildFilePath = @(fileName) fullfile( ...
    out.dir, sprintf('%s_%s', lower(datasetTag), fileName));

if out.doSave; diary(out.buildFilePath('output.log')); end

% Bring dataset parameters into workspace
params = HNHP.Auto.buildParams();
params = params.(datasetTag);
for curName = reshape(fieldnames(params), 1, [])
    assignin('base', curName{1}, params.(curName{1}));
end
Util.clear(params);

% NOTE(amotta): We use the model of excitatory and inhibitory axons that
% was optimized on manual annotations to classify the automated axon
% reconstructions. This is the path to the model parameters.
modelParamsCache = struct;
modelParamsCache.dir = '/conndata/amotta/hnhp/2022-02-26-model-bootstrapping';
modelParamsCache.runId = '20220416T102733';

axonMaxSynCount = 25;
synTypeNames = {'Spine', 'Shaft'};

clear cur*;
info = Util.runInfo();
Util.showRunInfo(info);

curGitDir = info.git_repos{1}.local;
gtSkelFile = fullfile(curGitDir, gtSkelFileRel);

%% Loading data
clear cur*;

Util.log('Dataset: %s', datasetTag);
Util.log('Agglomerate file: %s', aggloFile);
Util.log('Connectome file: %s', connFile);

Util.log('Defining axon and dendrite agglomerates');
[curTypes, curProbs] = HNHP.Auto.TypeEM.buildProbsForAggloFile(param, aggloFile);
curProbs = transpose(curProbs);
curProbs(1, :) = [];

curProbs = [ ...
    curProbs(:, curTypes == 'Axon'), ...
    curProbs(:, curTypes == 'Dendrite') ...
  + curProbs(:, curTypes == 'SpineHead')];

[curMaxProb, curMaxIdx] = max(curProbs, [], 2);
axonIds = find(curMaxIdx == 1 & curMaxProb > axonsMinTypeProb);
dendIds = find(curMaxIdx == 2 & curMaxProb > dendritesMinTypeProb);

Util.log('Identified %d axon agglomerates', numel(axonIds));
Util.log('Identified %d dendrite agglomerates', numel(dendIds));

assert(isempty(intersect(axonIds, dendIds)));
curAggloCount = AggloFile(aggloFile).agglomerateCount();

aggloT = table;
aggloT.id = reshape(1:curAggloCount, [], 1);
aggloT.type(:) = categorical({'Unknown'});
aggloT.type(axonIds) = 'Axon';
aggloT.type(dendIds) = 'Dendrite';

synT = HNHP.Auto.Connectome.loadSynapseTable(connFile);

% NOTE(amotta): Remove "autapses" (vast majority are FPs)
synT(synT.aggloIds(:, 1) == synT.aggloIds(:, 2), :) = [];

% Separate into 'SinglySynSpineHead' and 'MultiSynSpineHead'
synT = HNHP.Auto.Connectome.refineSpineHeadSynapseType(synT);

% Restrict to synapses from axons onto dendrites
synT = synT( ...
    aggloT.type(synT.aggloIds(:, 1)) == 'Axon' ...
  & aggloT.type(synT.aggloIds(:, 2)) == 'Dendrite', :);

%% Simplify synapse types
% From here on out, we only consider two types of synapses:
% * "Spine" synapses, which are synapses onto spine heads with single
%   innervations. Above, these were tagged as 'SingleSynSpineHead'.
% * "Shaft" synapses (as above).
clear cur*;

% Simplify synapse type names
curTypes = repelem(categorical({'Ignore'}), height(synT), 1);
curTypes(synT.type == 'SingleSynSpineHead') = 'Spine';
curTypes(synT.type == 'Shaft') = 'Shaft';
synT.type = curTypes;
Util.clear(curTypes);

% Restrict to 'Spine' and 'Shaft' synapses
synT = synT(ismember(synT.type, synTypeNames), :);

curDropCats = setdiff(categories(synT.type), synTypeNames);
synT.type = removecats(synT.type, curDropCats);

%% Separate axons and dendrites
clear cur*;

dendT = aggloT(aggloT.type == 'Dendrite', :);
dendT = addSynapseCounts(dendT, synT, 'in');

axonT = aggloT(aggloT.type == 'Axon', :);
axonT = addSynapseCounts(axonT, synT, 'out');
axonT = axonT(axonT.outSynCountAll <= axonMaxSynCount, :);

Util.clear(aggloT);

%% Loading ground truth
clear cur*;

gtSkel = skeleton(gtSkelFile);
curTreeCount = gtSkel.numTrees();

gtSkelDsetName = gtSkel.parameters.experiment.name;
gtSkelVoxelSize = struct2cell(gtSkel.parameters.scale);
gtSkelVoxelSize = cellfun(@str2double, gtSkelVoxelSize);
gtSkelVoxelSize = reshape(gtSkelVoxelSize, 1, 3);
gtSkelBox = gtSkel.getBoundingBox();

gtT = table;
gtT.treeIdx  = reshape(1:curTreeCount, [], 1);
gtT.pos      = nan(height(gtT), 3);
gtT.prePos   = nan(height(gtT), 3);
gtT.postPos  = nan(height(gtT), 3);
gtT.trunkPos = nan(height(gtT), 3);
gtT.type     = cell(height(gtT), 1);
gtT.dendType = cell(height(gtT), 1);

for curIdx = 1:curTreeCount
    curName = gtSkel.names{curIdx};
    curNodes = gtSkel.nodes{curIdx}(:, 1:3);
    curNodeCount = size(curNodes, 1);
    assert(curNodeCount >= 3);
    
    curComments = {gtSkel.nodesAsStruct{curIdx}.comment};
    assert(isequal(curNodeCount, numel(curComments)));
    
    curPreNodeIdx = find(strcmpi(curComments, 'pre'));
    assert(isscalar(curPreNodeIdx));
    
    curEdges = gtSkel.edges{curIdx};
    
    % Make sure that tracing is linear
    % HACKHACKHACK(amotta): This is violated in Vijay's Mk1 tracings.
    % curNodeDegrees = accumarray(curEdges(:), 1, [curNodeCount, 1]);
    % assert(sum(curNodeDegrees == 2) == curNodeCount - 2);
    % assert(sum(curNodeDegrees == 1) == 2);
    
    % Sort nodes pre → interface → post
    curSortIds = graph(curEdges(:, 1), curEdges(:, 2));
    curSortIds = dfsearch(curSortIds, curPreNodeIdx);
    curNodes = curNodes(curSortIds, :);
    
    gtT.pos(curIdx, :) = curNodes(2, :);
    gtT.prePos(curIdx, :) = curNodes(1, :);
    gtT.postPos(curIdx, :) = curNodes(3, :);
    
    if size(curNodes, 1) > 3
        % NOTE(amotta): Left as NaN otherwise
        gtT.trunkPos(curIdx, :) = curNodes(end, :);
    end
    
    % Extract synapse type
    curStarts = find(curName == '(') + 1;
    curEnds   = find(curName == ')') - 1;
    assert(isequal(size(curStarts), size(curEnds)));
    
    curSynType = strtrim([ ...
     ... part within first pair of parentheses
        curName(curStarts(1):curEnds(1)), ...
     ... part after last pair of parentheses (with prefix space)
        curName((curEnds(end) + 2):end)]);
    
    curDendType = 'unknown';
    if numel(curStarts) >= 2
        curDendType = curName(curStarts(2):curEnds(2));
    end
    
    gtT.type{curIdx} = lower(curSynType);
    gtT.dendType{curIdx} = lower(curDendType);
end

gtT.type = categorical(gtT.type);
gtT.dendType = categorical(gtT.dendType);

curSegParam = struct;
curSegParam.root = fullfile( ...
    '/tmpscratch', 'webknossos', ...
    'Connectomics_Department', gtSkelDsetName, ...
    'segmentation', '1');
curSegParam.backend = 'wkwrap';

curParam = struct;
curParam.seg = curSegParam;

curAf = AggloFile(aggloFile);
gtT.preSegId = Seg.Global.getSegIds(curParam, gtT.prePos);
gtT.preAggloId = curAf.segmentToAgglomerateIds(gtT.preSegId);

gtT.postSegId = Seg.Global.getSegIds(curParam, gtT.postPos);
gtT.postAggloId = curAf.segmentToAgglomerateIds(gtT.postSegId);

curMask = any(gtT.trunkPos, 2);
curPos = gtT.trunkPos(curMask, :);
gtT.trunkSegId(:) = cast(0, 'like', gtT.postSegId);
gtT.trunkSegId(curMask) = Seg.Global.getSegIds(curParam, curPos);
gtT.trunkAggloId = curAf.segmentToAgglomerateIds(gtT.trunkSegId);

% Sanity check
curKnownTypes = { ...
    'filospine', 'prim', 'sec', 'sh', 'shaft', 'sph', ...
    'sph neck', 'sph prim', 'sph sec', 'stub'};
assert(all(ismember(gtT.type, curKnownTypes)));

%% Evaluate spine head attachment
clear cur*;

% NOTE(amotta): Secondary spine synapses and spine neck synapses are
% excluded because they should have a companion primary spine synapse.
% Without this restriction, these spines would be counted multiple times.
curSpineHeadTypes = {'sph', 'sph prim', 'prim', 'stub', 'filospine'};

curSpineHeadMask = ismember(gtT.type, curSpineHeadTypes);
curTracedToTrunkMask = logical(gtT.trunkAggloId);

if not(all(curTracedToTrunkMask(curSpineHeadMask)))
    curBadCount = sum(not(curTracedToTrunkMask(curSpineHeadMask)));
    curAllCount = sum(curSpineHeadMask);
    
    Util.log( ...
        '%.1f%% (%d out of %d) spine heads without attachment', ...
        100 * curBadCount / curAllCount, curBadCount, curAllCount);
end

curMask = curSpineHeadMask & curTracedToTrunkMask;
curAttached = gtT.postAggloId == gtT.trunkAggloId;
curAttachRate = sum(curAttached & curMask) / sum(curMask);
Util.log('%.1f%% of spine heads attached\n', 100 * curAttachRate);

%% Clean up synapse type names
clear cur*;

% TODO(amotta): In H5_AMK_st700, a number of 'stub' synapses were
% automatically detected and classified as spine head synapses. Leaving out
% the 'stub' class results in quite some (false?) false positives.
curSpineHeadTypes = {'sph', 'sph prim', 'prim'};
curShaftTypes = {'sh', 'shaft'};

assert(isempty(intersect(curSpineHeadTypes, curShaftTypes)));

curTypes = repelem(categorical({'Ignore'}), height(gtT), 1);
curTypes(ismember(gtT.type, curSpineHeadTypes)) = 'Spine';
curTypes(ismember(gtT.type, curShaftTypes)) = 'Shaft';
gtT.type = curTypes;

% NOTE(amotta): By keeping secondary spine synapses, synapses onto stubs,
% etc. in the ground truth, we allow eventual automated detections of these
% synapses to be assigned to the corresponding ground truth. Without this,
% we could count the correct detections as false positives and thereby
% underestimate the precision.
%
% However, we these ground truth synapses as type 'Ignore'. Accordingly,
% they will be ignored when computing the recall and confusion rates.
%
% gtT(gtT.type == 'Ignore', :) = [];
% gtT.type = removecats(gtT.type);

%% Clean up dendrite type names
clear cur*;

curSpinyDendTypes = {'exc'};
curSmoothDendTypes = {'inh', 'in'};

assert(isempty(intersect(curSpinyDendTypes, curSmoothDendTypes)));

curTypes = repelem(categorical({'Unknown'}), height(gtT), 1);
curTypes(ismember(gtT.dendType, curSpinyDendTypes)) = 'Spiny';
curTypes(ismember(gtT.dendType, curSmoothDendTypes)) = 'Smooth';
gtT.dendType = curTypes;

%% Match ground truth synapses with automated detections
clear cur*;

evalT = synT;

% Restrict to detected synapses within box + margin
curMargin = 500;
curMargin = ceil(curMargin ./ gtSkelVoxelSize);
curBox = gtSkelBox + [-1, +1] .* curMargin(:);

evalT = evalT( ...
    all(reshape(curBox(:, 1), 1, 3) <= evalT.pos, 2) ...
  & all(evalT.pos <= reshape(curBox(:, 2), 1, 3), 2), :);

gtT.proxSynId(:) = nan;
gtT.proxSynDist(:) = nan;
for curIdx = 1:height(gtT)
    curAggloIds = [gtT.preAggloId(curIdx), gtT.postAggloId(curIdx)];
    curEvalT = evalT(all(evalT.aggloIds == curAggloIds, 2), :);
    
    if isempty(curEvalT); continue; end
    
    curPos = gtT.pos(curIdx, :);
    curEvalT.dist = gtSkelVoxelSize .* (curPos - double(curEvalT.pos));
    curEvalT.dist = sqrt(sum(curEvalT.dist .* curEvalT.dist, 2));
    
   [curMinDist, curMinIdx] = min(curEvalT.dist);
    gtT.proxSynId(curIdx) = curEvalT.id(curMinIdx);
    gtT.proxSynDist(curIdx) = curMinDist;
end

% NOTE(amotta): For ex144, I've checked the most extreme cases.
% assert(max(gtT.proxSynDist) < 750);

% Sanity check
[curUniSynIds, ~, curUniSynCounts] = unique(gtT.proxSynId);
curUniSynCounts = accumarray(curUniSynCounts, 1);

if any(curUniSynCounts > 1)
    curBadSynIds = curUniSynIds(curUniSynCounts > 1);
    
    curBadSynIdsStr = strjoin(arrayfun( ...
        @num2str, curBadSynIds, 'UniformOutput', false), ', ');
    
    warning( ...
       ['%d synapse(s) got assigned to multiple ground truth synapses: %s.', ...
        newline, 'Please check these cases in webKnossos! For now, ', ...
        'conflicts are solved by brute force!'], ...
        numel(curBadSynIds), curBadSynIdsStr);
    
    for curSynId = reshape(curBadSynIds, 1, [])
        curGtRows = find(gtT.proxSynId == curSynId);
       [~, curSortIds] = sort(gtT.proxSynDist(curGtRows));
       
        curMaskOutGtRows = curGtRows(curSortIds(2:end));
        gtT.proxSynId(curMaskOutGtRows) = nan;
        gtT.proxSynDist(curMaskOutGtRows) = nan;
    end
end

[curUniSynIds, ~, curUniSynCounts] = unique(gtT.proxSynId);
curUniSynCounts = accumarray(curUniSynCounts, 1);
assert(not(any(curUniSynCounts > 1)));

% Restrict to
% - true positives in box with margin and
% - false positives in box without margin
curMask = ...
    all(reshape(gtSkelBox(:, 1), 1, 3) <= evalT.pos, 2) ...
  & all(evalT.pos <= reshape(gtSkelBox(:, 2), 1, 3), 2);
curMask = curMask | ismember(evalT.id, gtT.proxSynId);
evalT = evalT(curMask, :);

%% Generate skeleton for inspection of FPs and FNs synapses
clear cur*;

curRed   = [1, 0, 0, 1];
curGreen = [0, 1, 0, 1];
curBlue  = [0, 0, 1, 1];

evalSkel = skeleton();
evalSkel = evalSkel.setParams(gtSkelDsetName, gtSkelVoxelSize, 1);
evalSkel = Skeleton.setDescriptionFromRunInfo(evalSkel, info);
evalSkel = evalSkel.setBbox(gtSkelBox, true);

curNumDigits = ceil(log10(1 + height(gtT)));
for curIdx = 1:height(gtT)
    curGtT = gtT(curIdx, :);
    curRecalled = curGtT.proxSynId > 0;
    
    curName = '%0*d. Ground truth synapse';
    curName = sprintf(curName, curNumDigits, curIdx);
    if not(curRecalled); curName = sprintf('%s (FN)', curName); end
   [evalSkel, curGroupId] = evalSkel.addGroup(curName);
    
    % Add ground truth annotation
    curAggloIds = [curGtT.preAggloId, curGtT.postAggloId];
    curName = sprintf( ...
        'A. Manual (id: %d; type: %s; agglos: %d-%d)', ...
        curGtT.treeIdx, curGtT.type, curAggloIds);
    evalSkel = evalSkel.addTree(curName, ...
        [curGtT.prePos; curGtT.pos; curGtT.postPos], ...
        [], curBlue, [], [], curGroupId);
    
    % Add matched synapse
    if curRecalled
        curEvalT = evalT(evalT.id == curGtT.proxSynId, :);
        
        curName = sprintf( ...
            'B. Auto (id: %d; type: %s; agglos: %d-%d)', ...
            curEvalT.id, curEvalT.type, curEvalT.aggloIds);
        evalSkel = evalSkel.addTree(curName, ...
            curEvalT.pos, [], curGreen, [], [], curGroupId);
    end
end

[evalSkel, curGroupId] = evalSkel.addGroup('False positives');
curFpT = evalT(not(ismember(evalT.id, gtT.proxSynId)), :);

for curIdx = 1:height(curFpT)
    curT = curFpT(curIdx, :);

    curName = sprintf( ...
        'B. Auto (id: %d; type: %s; agglos: %d-%d)', ...
        curT.id, curT.type, curT.aggloIds);
    evalSkel = evalSkel.addTree(curName, ...
        curT.pos, [], curRed, [], [], curGroupId);
end

if out.doSave
    curEvalSkelFile = out.buildFilePath('synapse-evaluation.nml');
    evalSkel.write(curEvalSkelFile);
    Util.protect(curEvalSkelFile);
end

%% Evaluate type predictions of pre- and postsynaptic agglomerates
clear cur*;

if do.plotPreAndPostsynapticAgglomerateTypes
   [curTypes, curProbs] = ...
        HNHP.Auto.TypeEM.buildProbsForAggloFile(param, aggloFile);
    curProbs = transpose(curProbs);
    curProbs(1, :) = [];

    curPreAxonProbs = curProbs( ...
        gtT.preAggloId, curTypes == 'Axon');
    curPreDendProbs = sum(curProbs(gtT.preAggloId, ...
        ismember(curTypes, {'Dendrite', 'SpineHead'})), 2);

    curPostAxonProbs = curProbs( ...
        gtT.postAggloId, curTypes == 'Axon');
    curPostDendProbs = sum(curProbs(gtT.postAggloId, ...
        ismember(curTypes, {'Dendrite', 'SpineHead'})), 2);

    % Plotting
    curFig = figure();

    curAx = subplot(3, 1, 1);
    hold(curAx, 'on');
    histogram(curAx, curPreAxonProbs, 'BinEdges', linspace(0, 1, 21));

    xlabel(curAx, {
        'Axon probability';
        'of presynaptic agglomerate'});
    ylabel(curAx, 'Ground truth synapses');
    axis(curAx, 'square');

    curAx = subplot(3, 1, 2);
    hold(curAx, 'on');
    histogram(curAx, curPostDendProbs, 'BinEdges', linspace(0, 1, 21));

    xlabel(curAx, {
        'Dendrite + spine head probability';
        'of postsynaptic agglomerate'});
    ylabel(curAx, 'Ground truth synapses');
    axis(curAx, 'square');

    curAx = subplot(3, 1, 3);
    hold(curAx, 'on');

    scatter(curAx, curPreAxonProbs, curPreDendProbs, 36, '.');
    scatter(curAx, curPostAxonProbs, curPostDendProbs, 36, '.');
    
    curLeg = legend(curAx, {'Pre', 'Post'});
    curLeg.Location = 'NorthEast';

    xlabel(curAx, 'Axon probability');
    ylabel(curAx, 'Dendrite + spine head probability');

    xlim(curAx, [0, 1]);
    ylim(curAx, [0, 1]);
    axis(curAx, 'square');
    
    Figure.config(curFig, info, 'additionalInfos', datasetTag);
    curFig.Position(3:4) = [300, 990];

    if out.doSave
        saveFigure(curFig, out.buildFilePath( ...
            'type-probabilities-of-pre-and-post-agglomerates'));
    end
end

%% Compute error rates of synapse detections
clear cur*;

assert(all(ismember(evalT.type, synTypeNames)));

gtT.autoType(:) = categorical({'Missed'});
[curMask, curRows] = ismember(gtT.proxSynId, evalT.id);
gtT.autoType(curMask) = evalT.type(curRows(curMask));

curRecalled = gtT.autoType ~= 'Missed';
curCorrect = ismember(evalT.id, gtT.proxSynId);

errorRates = struct;

% Precision
for curName = synTypeNames
    curName = curName{1}; %#ok
    curMask = evalT.type == curName;
    curPrecision = sum(curMask & curCorrect) / sum(curMask);
    curOut = strcat(lower(curName(1)), curName(2:end), 'Precision');
    errorRates.(curOut) = curPrecision;
end

% Recall
for curName = synTypeNames
    curName = curName{1}; %#ok
    curMask = gtT.type == curName;
    curRecall = sum(curMask & curRecalled) / sum(curMask);
    curOut = strcat(lower(curName(1)), curName(2:end), 'Recall');
    errorRates.(curOut) = curRecall;
end

% Confusion
for curSrc = synTypeNames
    curSrc = curSrc{1}; %#ok
    
    curMask = gtT.type == curSrc;
    curMask = curMask & curRecalled;
    
    for curDst = synTypeNames
        curDst = curDst{1}; %#ok
        
        if strcmp(curSrc, curDst); continue; end
        
        curConfusion = gtT.autoType == curDst;
        curConfusion = sum(curMask & curConfusion) / sum(curMask);
        curOut = strcat(lower(curSrc(1)), curSrc(2:end), 'To', curDst);
        errorRates.(curOut) = curConfusion;
    end
end

fprintf('Error rates\n\n');
disp(errorRates);

%% Prepare ground truth for spiny vs. smooth dendrites
clear cur*;

% Sanity checks
assert(all(ismember(gtT.type, {'Spine', 'Shaft', 'Ignore'})));
assert(all(ismember(gtT.dendType, {'Spiny', 'Smooth', 'Unknown'})));

gtDendT = table;
gtDendT.id = [ ...
    gtT.postAggloId(gtT.type == 'Spine' | gtT.type == 'Shaft');
    gtT.trunkAggloId(gtT.type == 'Spine' & gtT.trunkAggloId > 0)];
gtDendT.type = [ ...
    gtT.dendType(gtT.type == 'Spine' | gtT.type == 'Shaft');
    gtT.dendType(gtT.type == 'Spine' & gtT.trunkAggloId > 0)];
assert(all(gtDendT.id));

% Ignore dendrites of unknown type
gtDendT(gtDendT.type == 'Unknown', :) = [];

gtDendT = unique(gtDendT, 'rows');

% NOTE(amotta): Dendrite agglomerates that occur multiple times have
% contradictory dendrite type labels. Let's remove these.
[~, curUniRows, curUniCounts] = unique(gtDendT.id);
curUniCounts = accumarray(curUniCounts, 1);
curUniRows = curUniRows(curUniCounts == 1);
gtDendT = gtDendT(curUniRows, :);

Util.log( ...
    'Found %d spiny and %d smooth dendrites in ground truth', ...
    sum(gtDendT.type == 'Spiny'), sum(gtDendT.type == 'Smooth'));
Util.log( ...
    'Found %d unclassified dendrites (these will be ignored)', ...
    sum(not(ismember(gtDendT.type, {'Spiny', 'Smooth'}))));

% Restrict to postsynaptic 'Dendrite' agglomerates
gtDendT = gtDendT(ismember(gtDendT.id, dendT.id), :);
gtDendT = addSynapseCounts(gtDendT, synT, 'in');

%% Infer properties of true spiny and smooth dendrites
% NOTE(amotta): For now, let's model spiny and smooth dendrites by a
% binomial distribution. Here, we infer the maximum likelihood estimates.
%
% This is a purely "phenomenological model" based on the automated synapse
% detection, including all its errors. We do not aim to infer the true and
% error-free underlying spine rates for spiny and smooth dendrites.
%
% This latter approach would be necessary if we wanted to classify
% dendrites based on the model parameters inferred from manual data.
%
% However, this model-based approach would have other drawbacks. For
% example, model parameters were inferred based on spiny dendrites from
% which the most proximal / perisomatic region was excluded. The model
% would thus systematically overestimate the spine rate of spiny dendrites.
clear cur*;

assert(all(ismember(gtDendT.type, {'Spiny', 'Smooth'})));

priorSpinyDendProb = [ ...
    sum(gtDendT.type == 'Spiny'), ...
    sum(gtDendT.type == 'Smooth')];
priorSpinyDendProb = ...
    priorSpinyDendProb(1) ...
  / sum(priorSpinyDendProb);

Util.log( ...
    'Inferred prior for spiny dendrites: %.1f%%', ...
    100 * priorSpinyDendProb);

spinyDendSpineProb = ...
    sum(gtDendT.inSynCountSpine(gtDendT.type == 'Spiny')) ...
  / sum(gtDendT.inSynCountAll(gtDendT.type == 'Spiny'));

smoothDendSpineProb = ...
    sum(gtDendT.inSynCountSpine(gtDendT.type == 'Smooth')) ...
  / sum(gtDendT.inSynCountAll(gtDendT.type == 'Smooth'));

Util.log('Inferred automated spine rates:');
Util.log('  for spiny dendrites: %.1f%%', 100 * spinyDendSpineProb);
Util.log('  for smooth dendrites: %.1f%%', 100 * smoothDendSpineProb);

%% Infer P(spiny | dendrite)
clear cur*;

curBinoLogPdf = @(k, n, p) ...
    gammaln(n + 1) - gammaln(k + 1) - gammaln(n - k + 1) ...
  + k * log(p) + (n - k) * log(1 - p);

curSpinyProb = [ ...
    curBinoLogPdf( ...
        dendT.inSynCountSpine, ...
        dendT.inSynCountAll, ...
        spinyDendSpineProb), ...
    curBinoLogPdf( ...
        dendT.inSynCountSpine, ...
        dendT.inSynCountAll, ...
        smoothDendSpineProb)];
curSpinyProb = ...
    curSpinyProb + [ ...
        log(priorSpinyDendProb), ...
        log(1 - priorSpinyDendProb)];

curNorm = max(curSpinyProb, [], 2);
curNorm = curNorm + log(sum(exp(curSpinyProb - curNorm), 2));
curSpinyProb = exp(curSpinyProb(:, 1) - curNorm);
dendT.spinyProb = curSpinyProb;

%% Plot P(spiny | dendrite)
clear cur*;

curMinSynCount = 5;
curBinEdges = linspace(0, 1, 11);

curDendT = dendT(dendT.inSynCountAll >= curMinSynCount, :);
curDendT.spineFrac = curDendT.inSynCountSpine ./ curDendT.inSynCountAll;
curSpinyMask = curDendT.spinyProb > 0.5;

curFig = figure();
curAx = axes(curFig);
hold(curAx, 'on');

histogram(curAx, ...
    curDendT.spineFrac(curSpinyMask), ...
    'BinEdges', curBinEdges);
histogram(curAx, ...
    curDendT.spineFrac(not(curSpinyMask)), ...
    'BinEdges', curBinEdges);

xlabel(curAx, {'P(onto spine | input synapse)'; '(uncorrected)'});
ylabel(curAx, sprintf('Dendrites with ≥%d synapses', curMinSynCount));
axis(curAx, 'square');

curLeg = legend(curAx, { ...
    sprintf('Spiny dendrites (n=%d)', sum(curSpinyMask));
    sprintf('Smooth dendrites (n=%d)', sum(not(curSpinyMask)))});
curLeg.Location = 'SouthOutside';

Figure.config(curFig, info, 'additionalInfos', datasetTag);
curFig.Position(3:4) = [335, 425];

if out.doSave
    saveFigure(curFig, out.buildFilePath( ...
        'spine-rate-of-spiny-vs-smooth-dendrites'));
end

%% Inference of P(spine rate | axon)
clear cur*;

% NOTE(amotta): See meeting notes and emails of 30.03.2022
curMinSynCount = 5;

% NOTE(amotta): For visualization, also impose minimum synapse count
curAxons = axonT(axonT.outSynCountAll >= curMinSynCount, :);
curAxons = curAxons{:, {'outSynCountSpine', 'outSynCountShaft'}};

[spineProbs, spinePosteriorProbs, spineProbLUT] = ...
    HNHP.Auto.inferSpineSynapseFraction( ...
            errorRates, curAxons, ...
            'axonWeights', 'constant', ...
            'spineSynProbs', linspace(0, 1, 101), ...
            'maxSynCountForSimulation', 4 * axonMaxSynCount);

%% Plot P(spine rate | axon)
curBinEdges = linspace(0, 1, 11);
curBinCounts = curAxons(:, 1) ./ sum(curAxons, 2);
curBinCounts = discretize(curBinCounts, curBinEdges);
curBinCounts = accumarray(curBinCounts, 1, [numel(curBinEdges) - 1, 1]);

% Identical normalization to inference results
curBinCounts = curBinCounts / sum(curBinCounts);
curBinCounts = curBinCounts .* diff(curBinEdges(:));

curPostProbs = spinePosteriorProbs / sum(spinePosteriorProbs);
curMaxX = max(max(curBinCounts(:)), max(curPostProbs(:)));
curMaxX = 0.01 * ceil(curMaxX / 0.01);

curFig = figure();
curAx = axes(curFig);
hold(curAx, 'on');

curHist = histogram(curAx, ...
    'BinCounts', curBinCounts, 'BinEdges', curBinEdges, ...
    'Orientation', 'horizontal', 'EdgeColor', 'none', ...
    'FaceColor', [0.75, 0.75, 0.75]);

plot(curAx, ...
    curPostProbs, spineProbs, ...
    'Color', 'black', 'LineWidth', 2);

ylim(curAx, [0, 1]);
yticks(curAx, 0:0.1:1);
ylabel(curAx, { ...
    'P(spine synapse fraction | axon)';
    sprintf( ...
        'based on axons with %d to %d synapses', ...
        curMinSynCount, axonMaxSynCount);
    sprintf('(n=%d)', size(curAxons, 1))});
xlabel(curAx, 'Probability density');

Figure.config(curFig, info, 'additionalInfos', datasetTag);
curFig.Position(3:4) = [220, 350];

% NOTE(amotta): Figure.config switches histograms to
% DisplayStyle = 'stairs', so let's style the histogram here.
curHist.DisplayStyle = 'bar';
curHist.FaceColor(:) = 0.75;
curHist.EdgeColor = 'none';

% NOTE(amotta): Must be last. Not sure why.
xlim(curAx, [0, curMaxX]);

if out.doSave
    saveFigure(curFig, out.buildFilePath( ...
        'spine-rates-of-axons_corrected'));
end
        
%% Inference of P(excitatory | axon)
clear cur*;

modelParams = fullfile( ...
    modelParamsCache.dir, sprintf('%s_%s.mat', ...
        modelParamsCache.runId, lower(datasetTag)));
modelParams = Util.load(modelParams, 'optimalModelParams');

% NOTE(amotta): This is P(excitatory | synapse) and might be different from
% P(excitatory | axon) in case there is a correlation between the number of
% synapses along an axon and the probability of the axon being excitatory.
curPriorExcProb = modelParams.pExc;

curExcBinoProbs = (spineProbs(1:(end - 1)) + spineProbs((1 + 1):end)) / 2;
curExcBinoProbs = [0, reshape(curExcBinoProbs, 1, []), 1];

curExcAlpha = modelParams.aExcSpine;
curExcBeta = modelParams.aExcSpinyShaft + modelParams.aExcSmoothShaft;

curExcBinoProbs = diff(betacdf(curExcBinoProbs, curExcAlpha, curExcBeta));
assert(isequal(numel(curExcBinoProbs), numel(spineProbs)));
assert(sum(curExcBinoProbs) == 1);

[~, curInhLutIdx] = min(abs(modelParams.pInhSpine - spineProbs));

% NOTE(amotta): For performance reasons, we're computing the "posterior"
% probability only once for each unique axon configuration.
curAxons = axonT{:, {'outSynCountSpine', 'outSynCountShaft'}};
[curAxons, ~, curRelAxonIds] = unique(curAxons, 'rows');
curAxonsExcProb = nan(size(curAxons, 1), 1);

for curAxonIdx = 1:size(curAxons, 1)
    curSpines = curAxons(curAxonIdx, 1);
    curShafts = curAxons(curAxonIdx, 2);
    
    curLUT = spineProbLUT(:, 1 + curSpines, 1 + curShafts);
    curExcLik = curPriorExcProb * sum(curLUT .* curExcBinoProbs(:));
    curInhLik = (1 - curPriorExcProb) * curLUT(curInhLutIdx);
    
    curExcProb = curExcLik / (curExcLik + curInhLik);
    curAxonsExcProb(curAxonIdx) = curExcProb;
end

axonT.excProb = reshape(curAxonsExcProb(curRelAxonIds), [], 1);

%% Plotting P(onto spine | excitatory vs. inhibitory axon)
clear cur*;

curMinSynCount = 5;
curBinEdges = linspace(0, 1, 11);

curAxonT = axonT(axonT.outSynCountAll >= curMinSynCount, :);
curAxonT.spineFrac = curAxonT.outSynCountSpine ./ curAxonT.outSynCountAll;
curExcMask = curAxonT.excProb > 0.5;

curFig = figure();
curAx = axes(curFig);
hold(curAx, 'on');

histogram(curAx, ...
    curAxonT.spineFrac(curExcMask), ...
    'BinEdges', curBinEdges);
histogram(curAx, ...
    curAxonT.spineFrac(not(curExcMask)), ...
    'BinEdges', curBinEdges);

xlabel(curAx, ...
    'P(onto spine | output synapse)');
ylabel(curAx, sprintf( ...
    'Axons with %d to %d synapses', ...
    curMinSynCount, axonMaxSynCount));
axis(curAx, 'square');

curLeg = legend(curAx, { ...
    sprintf('Excitatory axons (n=%d)', sum(curExcMask));
    sprintf('Inhibitory axons (n=%d)', sum(not(curExcMask)))});
curLeg.Location = 'SouthOutside';

Figure.config(curFig, info, 'additionalInfos', datasetTag);
curFig.Position(3:4) = [335, 425];

if out.doSave
    saveFigure(curFig, out.buildFilePath( ...
        'spine-rates-of-exc-and-inh-axons-uncorrected'));
end

%% Plotting P(onto smooth dendrites | excitatory vs. inhibitory axon)
clear cur*;

curMinSynCount = 5;
curBinEdges = linspace(0, 1, 11);

curSynT = synT;

[~, curSynT.relAxonId] = ismember(curSynT.aggloIds(:, 1), axonT.id);
% NOTE(amotta): We've only computed `excProb` for axons with <= N synapses
curSynT = curSynT(logical(curSynT.relAxonId), :);
curSynT.excProb = axonT.excProb(curSynT.relAxonId);

[~, curSynT.spinyProb] = ismember(curSynT.aggloIds(:, 2), dendT.id);
curSynT.spinyProb = dendT.spinyProb(curSynT.spinyProb);

axonT.ontoSpinyDendProbUncorrected = accumarray( ...
    curSynT.relAxonId, curSynT.spinyProb, ...
   [height(axonT), 1], @mean, nan);

curAxonT = axonT(axonT.outSynCountAll >= curMinSynCount, :);
curAxonT.isExc = curAxonT.excProb >= 0.5;

fprintf( ...
    'P(onto spiny dendrite | excitatory synapse) = %.1f%%\n', ...
    100 * mean(curSynT.spinyProb(curSynT.excProb > 0.5)));
fprintf( ...
    'P(onto spiny dendrite | inhibitory synapse) = %.1f%%\n', ...
    100 * mean(curSynT.spinyProb(curSynT.excProb < 0.5)));

% Plot
curFig = figure();
curAx = axes(curFig);
hold(curAx, 'on');

curHist = ...
    @(data) histogram( ...
        curAx, data, ...
        'BinEdges', curBinEdges, ...
        'Normalization', 'probability');
    
curHist(1 - curAxonT.ontoSpinyDendProbUncorrected(curAxonT.isExc));
curHist(1 - curAxonT.ontoSpinyDendProbUncorrected(not(curAxonT.isExc)));

xlabel(curAx, { ...
    'P(onto smooth dendrite | output synapse)';
    '(uncorrected)'});
ylabel(curAx, { ...
    'Probability based on';
    sprintf( ...
        'axons with %d to %d synapses', ...
        curMinSynCount, axonMaxSynCount)});
axis(curAx, 'square');

curLeg = legend(curAx, { ...
    sprintf('Excitatory axons (n=%d)', sum(curAxonT.isExc));
    sprintf('Inhibitory axons (n=%d)', sum(not(curAxonT.isExc)))});
curLeg.Location = 'SouthOutside';

Figure.config(curFig, info, 'additionalInfos', datasetTag);
curFig.Position(3:4) = [335, 425];

if out.doSave
    saveFigure(curFig, out.buildFilePath( ...
        'smooth-dendrite-rates-of-axons_uncorrected'));
end

%% Further analyze P(onto spiny dendrite | inhibitory axon)
clear cur*;

% NOTE(amotta): Despite this minimum synapse count, we still compute
% P(onto spiny dendrite | inhibitory axon) for all inhibitory axons.
curMinSynCountForPlot = 5;

ontoSpinyDendProb = linspace(0, 1, 101);
spinyDendOntoShaftProb = linspace(0, 1, 101);

% Precompute synapse type probabilities with errors
curProbs = cell(1, 1, 4);
[curProbs{1:2}] = ndgrid( ...
    ontoSpinyDendProb, ...
    spinyDendOntoShaftProb);

curProbs{4} = 1 - curProbs{1}; % Shafts of smooth dendrites
curProbs{3} = zeros(size(curProbs{1})); % Spines of smooth dendrites
curProbs{2} = curProbs{1} .* curProbs{2}; % Shafts of spiny dendrites
curProbs{1} = curProbs{1} .* (1 - curProbs{2}); % Spines of spiny dendrites
curProbs = reshape(cell2mat(curProbs), [], numel(curProbs));

% Simulate imperfect recall
curProbs = curProbs .* [ ...
    errorRates.spineRecall, ...
    errorRates.shaftRecall, ...
    errorRates.spineRecall, ...
    errorRates.shaftRecall];
curProbs = curProbs ./ sum(curProbs, 2);

% Simulate synapse type confusion
curProbs = curProbs * [ ...
    (1 - errorRates.spineToShaft), errorRates.spineToShaft, 0, 0;
    errorRates.shaftToSpine, (1 - errorRates.shaftToSpine), 0, 0;
    0, 0, (1 - errorRates.spineToShaft), errorRates.spineToShaft;
    0, 0, errorRates.shaftToSpine, (1 - errorRates.shaftToSpine)];
curProbs = curProbs ./ sum(curProbs, 2);

% Simulate imperfect precision
curProbs = curProbs ./ [ ...
    errorRates.spinePrecision, ...
    errorRates.shaftPrecision, ...
    errorRates.spinePrecision, ...
    errorRates.shaftPrecision];
curProbs = curProbs ./ sum(curProbs, 2);

curProbs = [ ...
    reshape(sum(curProbs(:, 1:2), 2), 1, 1, []); ...
    reshape(sum(curProbs(:, 3:4), 2), 1, 1, [])];

% Compute P(synapse onto spiny dendrite | inhibitory axon)
curInhAxonT = axonT(axonT.excProb < 0.5, :);
Util.log('Found %d inhibitory axons', height(curInhAxonT));

curInhAxonSynT = synT;
[~, curInhAxonSynT.inhAxonId] = ismember( ...
    curInhAxonSynT.aggloIds(:, 1), curInhAxonT.id);
curInhAxonSynT(not(curInhAxonSynT.inhAxonId), :) = [];

[~, curInhAxonSynT.spinyProb] = ismember( ...
    curInhAxonSynT.aggloIds(:, 2), dendT.id);
curInhAxonSynT.spinyProb = dendT.spinyProb(curInhAxonSynT.spinyProb);

curInhAxonSynT = sortrows(curInhAxonSynT, 'inhAxonId');
curInhAxonSynOffs = accumarray(curInhAxonSynT.inhAxonId, 1);
curInhAxonSynOffs = cumsum([0; curInhAxonSynOffs(:)]);

curInhAxonT.ontoSpinyDendProbCorrected(:) = nan;
inhAxonsSpinyDendProbsCorrected = zeros(size(ontoSpinyDendProb));
for curInhAxonIdx = 1:height(curInhAxonT)
    curOffs = curInhAxonSynOffs(curInhAxonIdx + [0, 1]);
    curSynT = curInhAxonSynT((1 + curOffs(1)):curOffs(2), :);
    
    curLiks = reshape(curSynT.spinyProb, 1, []);
    curLiks = sum(curProbs .* [curLiks; (1 - curLiks)], 1);
    curLiks = prod(curLiks, 2);

    curLiks = reshape(curLiks, ...
        numel(ontoSpinyDendProb), ...
        numel(spinyDendOntoShaftProb));

    % NOTE(amotta): I am not yet sure about the validity of this!
    curSpineLiks = ...
        reshape(ontoSpinyDendProb, [], 1) ...
     .* reshape(1 - spinyDendOntoShaftProb, 1, []);
   [~, curSpineLiks] = min(abs(...
        reshape(spineProbs, [], 1) ...
      - reshape(curSpineLiks, 1, [])), [], 1);
    curSpineLiks = spineProbLUT(curSpineLiks, ...
        1 + curInhAxonT.outSynCountSpine(curInhAxonIdx), ...
        1 + curInhAxonT.outSynCountShaft(curInhAxonIdx));
    curSpineLiks = reshape(curSpineLiks, size(curLiks));
    curLiks = curLiks .* curSpineLiks;

    curLiks = sum(curLiks, 2) / sum(curLiks(:));
    curLiks = reshape(curLiks, size(inhAxonsSpinyDendProbsCorrected));
    
   [~, curMapProb] = max(curLiks);
    curMapProb = ontoSpinyDendProb(curMapProb);
    curInhAxonT.ontoSpinyDendProbCorrected(curInhAxonIdx) = curMapProb;
    
    if height(curSynT) >= curMinSynCountForPlot
        inhAxonsSpinyDendProbsCorrected = ...
            inhAxonsSpinyDendProbsCorrected + curLiks;
    end
end

[~, curIds] = ismember(curInhAxonT.id, axonT.id);
axonT.ontoSpinyDendProbCorrected(:) = nan;
axonT.ontoSpinyDendProbCorrected(curIds) = ...
    curInhAxonT.ontoSpinyDendProbCorrected;

inhAxonsSpinyDendProbsCorrected = ...
    inhAxonsSpinyDendProbsCorrected ...
  / sum(inhAxonsSpinyDendProbsCorrected);

%% Plot corrected P(synapse onto smooth dendrite | inhibitory axon)
clear cur*;

curMinSynCount = 5;
curBinEdges = linspace(0, 1, 11);

curAxonT = axonT(axonT.outSynCountAll >= curMinSynCount, :);
curAxonT.isExc = curAxonT.excProb > 0.5;

curFig = figure();
curAx = axes(curFig);
hold(curAx, 'on');

curHist = ...
    @(data) histogram( ...
        curAx, data, ...
        'BinEdges', curBinEdges, ...
        'Normalization', 'probability');

curHist(1 - curAxonT.ontoSpinyDendProbCorrected(curAxonT.isExc));
curHist(1 - curAxonT.ontoSpinyDendProbCorrected(not(curAxonT.isExc)));

xlabel(curAx, { ...
    'P(onto smooth dendrite | output synapse)';
    '(corrected)'});
ylabel(curAx, { ...
    'Probability based on';
    sprintf( ...
        'axons with %d to %d synapses', ...
        curMinSynCount, axonMaxSynCount)});
axis(curAx, 'square');

curLeg = legend(curAx, { ...
    'Excitatory axons (not computed)';
    %{ sprintf('Excitatory axons (n=%d)', sum(curAxonT.isExc)); %}
    sprintf('Inhibitory axons (n=%d)', sum(not(curAxonT.isExc)))});
curLeg.Location = 'SouthOutside';

Figure.config(curFig, info, 'additionalInfos', datasetTag);
curFig.Position(3:4) = [335, 425];

if out.doSave
    saveFigure(curFig, out.buildFilePath( ...
        'smooth-dendrite-rates-of-inh-axons_corrected'));
end

%% Plot corrected P(synapse onto smooth dendrite | inhibitory axon)
clear cur*;

curFig = figure();
curAx = axes(curFig);
hold(curAx, 'on');

curX = 1 - ontoSpinyDendProb;
curY = inhAxonsSpinyDendProbsCorrected;
plot(curAx, curX, curY, 'Color', 'black', 'LineWidth', 2);

xlabel(curAx, { ...
    'P(onto smooth dendrite | inhibitory axon)';
    '(corrected)'});
ylabel(curAx, 'Probability density');
axis(curAx, 'square');
curAx.XLim(1) = 0;

Figure.config(curFig, info, 'additionalInfos', datasetTag);
curFig.Position(3:4) = [345, 365];

if out.doSave
    saveFigure(curFig, out.buildFilePath( ...
        'smooth-dendrite-rates-of-inh-axons_corrected-distribution'));
end

%% Seal the deal!
clear cur*;

curOut = struct;
curOut.dataset = datasetTag;
curOut.axonT = axonT;
curOut.dendT = dendT;
curOut.synT = synT;
curOut.info = info;

curOutFile = out.buildFilePath('data.mat');
Util.log('Writing results to %s', curOutFile);
Util.saveStruct(curOutFile, curOut);
Util.protect(curOutFile);
Util.log('Done!');

if strcmp(get(0, 'Diary'), 'on')
    curDiaryFile = get(0, 'DiaryFile');
    diary('off');
    Util.protect(curDiaryFile);
end

%% Utilities
function saveFigure(fig, filePathWithoutExt)
    withExt = @(ext) sprintf('%s.%s', filePathWithoutExt, ext);
    
    figFile = withExt('fig');
    savefig(fig, figFile);
    Util.protect(figFile);
    
    epsFile = withExt('eps');
    export_fig(fig, epsFile, '-nocrop', '-m2');
    Util.protect(epsFile);
    
    pngFile = withExt('png');
    export_fig(fig, pngFile, '-nocrop', '-m2');
    Util.protect(pngFile);
end

function aggloT = addSynapseCounts(aggloT, synT, dirs)
    dirNameToColIdx = struct;
    dirNameToColIdx.out = 1;
    dirNameToColIdx.in = 2;
    
    if not(exist('dirs', 'var'))
        dirs = fieldnames(dirNameToColIdx);
    elseif ischar(dirs)
        dirs = {dirs};
    end
    
    assert(iscell(dirs));
    dirs = reshape(unique(dirs, 'stable'), 1, []);
    assert(all(ismember(dirs, fieldnames(dirNameToColIdx))));
    
    synTypes = categories(synT.type);
   [~, synT.aggloIds(:)] = ismember(synT.aggloIds(:), aggloT.id);
   
    for curDirIdx = 1:numel(dirs)
        curDirName = dirs{curDirIdx};
        curColIdx = dirNameToColIdx.(curDirName);
        
        curCounts = [synT.aggloIds(:, curColIdx), double(synT.type)];
        % NOTE(amotta): Handle agglomerates missing from `aggloT`
        curCounts = curCounts(logical(curCounts(:, 1)), :);
        
        curSize = [height(aggloT), numel(synTypes)];
        curCounts = accumarray(curCounts, 1, curSize);
        
        curVarNames = [synTypes(:); {'All'}];
        curCounts = [curCounts, sum(curCounts, 2)]; %#ok
        
        curVarNames = strcat(curDirName, 'SynCount', curVarNames);
        curCounts = array2table(curCounts, 'VariableNames', curVarNames);
        
        % NOTE(amotta): Remove variables from previous call to function
        curMask = sprintf('%sSynCount', curDirName);
        curMask = startsWith(aggloT.Properties.VariableNames, curMask);
        aggloT(:, curMask) = [];
        
        aggloT = [aggloT, curCounts]; %#ok
    end
end
