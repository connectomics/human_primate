% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
% Define configurations to run
% * To run all configurations, set to []
% * To run a subset of configurations, set to cell array of config names
runConfigs = [];

dataTypes = { ...
    'volume', ...
    'spinyDendrites', ...
    'spineSeeded', ...
    'spinyShaftSeeded', ...
    'smoothShaftSeeded'};

info = Util.runInfo();
Util.showRunInfo(info);

%% Loading data
clear cur*;

configs = HNHP.Manual.buildConfigs();

if isequal(runConfigs, [])
    runConfigs = fieldnames(configs);
end

mouseConfigs = runConfigs(startsWith(runConfigs, 'mouse'));
macaqueConfigs = runConfigs(startsWith(runConfigs, 'macaque'));
humanConfigs = runConfigs(startsWith(runConfigs, 'human'));
primateConfigs = union(macaqueConfigs, humanConfigs, 'stable');

inputData = cell(numel(dataTypes), numel(runConfigs));
smoothShaftSeededAxons = cell(1, numel(runConfigs));
for curConfigIdx = 1:numel(runConfigs)
    curName = runConfigs{curConfigIdx};
    curConfig = configs.(curName);
    
    curData = HNHP.Manual.loadInputData(curConfig);
    
    curData.spineSeeded.spine = curData.spineSeeded.spine + 1;
    curData.spinyShaftSeeded.shaft = curData.spinyShaftSeeded.shaft + 1;
    curData.smoothShaftSeeded.shaft = curData.smoothShaftSeeded.shaft + 1;
    
    if not(ismember('shaft', curData.volume.Properties.VariableNames))
        curData.volume.shaft = ...
            curData.volume.shaftExN ...
          + curData.volume.shaftIN;
    end
    
    smoothShaftSeededAxons{curConfigIdx} = ...
        curData.smoothShaftSeeded;
    
    curData = cellfun( ...
        @(name) curData.(name){:, {'spine', 'shaft'}}, ...
        dataTypes, 'UniformOutput', false);
    inputData(:, curConfigIdx) = curData;
end

% Group by data type
for curRow = 1:size(inputData, 1)
    inputData{curRow, 1} = cell2struct( ...
        inputData(curRow, :), runConfigs, 2);
end

inputData = cell2struct( ...
    inputData(:, 1), dataTypes, 1);

%% Numbers
clear cur*;

curTotalAxons = fieldnames(inputData);
curTotalAxons = curTotalAxons(endsWith(curTotalAxons, 'Seeded'));

curTotalAxons = sum(cellfun( ...
    @(dataType) sum(structfun( ...
        @(data) size(data, 1), ...
        inputData.(dataType))), ...
    curTotalAxons));

curTotalSynCount = sum( ...
    structfun( ...
        @(data) sum(structfun( ...
            @(data) sum(data(:)), data)), ...
    inputData));

fprintf('Manual reconstructions\n');
fprintf('  Seeded axons: %d\n', curTotalAxons);
fprintf('  Synapses (volume + dendrites + axons): %d\n', curTotalSynCount);
fprintf('\n');

for curDataType = reshape(dataTypes, 1, [])
    curDataType = curDataType{1}; %#ok
    curData = inputData.(curDataType);
    fprintf('  %s\n', curDataType);
    
    for curConfigs = { ...
            {'Mouse', mouseConfigs}, ...
            {'Primate', primateConfigs}}
        curConfigs = curConfigs{1}; %#ok
        fprintf('    %s: ', curConfigs{1});
        
       [curCounts, curSynsAvg, curSynsMin, curSynsMax] = cellfun( ...
            @(name) deal( ...
                size(curData.(name), 1), ...
                mean(sum(curData.(name), 2)), ...
                min(sum(curData.(name), 2)), ...
                max(sum(curData.(name), 2))), ...
            curConfigs{2});
        
        if strcmpi(curDataType, 'volume')
            curSyns = curCounts .* curSynsAvg;
            fprintf('%d to %d synapses\n', min(curSyns), max(curSyns));
        elseif strcmpi(curDataType, 'spinyDendrites')
            fprintf( ...
                '%d to %d dendrites, with %d to %d synapses perdendrite\n', ...
                prctile(curCounts, [0, 100]), min(curSynsMin), max(curSynsMax));
        else
            fprintf( ...
                '%d to %d axons, with %d to %d synapses / axon\n', ...
                prctile(curCounts, [0, 100]), min(curSynsMin), max(curSynsMax));
        end
    end
    
    fprintf('\n');
end

for curConfigs = { ...
        {'Mouse', mouseConfigs}, ...
        {'Primate', primateConfigs}}
    curConfigs = curConfigs{1}; %#ok
    
   [~, curData] = ismember(curConfigs{2}, runConfigs);
    curData = smoothShaftSeededAxons(curData);
        
    for curDendType = { ...
            {'MP', 'Multipolar'}, ...
            {'BP', 'Bipolar'}}
        curDendType = curDendType{1}; %#ok
        
        curMasks = cellfun( ...
            @(d) d.dendType == curDendType{2}, ...
            curData, 'UniformOutput', false);
        
       [curCounts, curSynsMin, curSynsMax] = cellfun( ...
            @(data, mask) feval( ...
                @(data) deal( ...
                    size(data, 1), ...
                    min(sum(data, 2)), ...
                    max(sum(data, 2))), ...
                data{mask, {'spine', 'shaft'}}), ...
            curData, curMasks);
        
        curAxonCount = mean(curCounts);
        curAverageSynCount = mean(curSyns);
        
        fprintf('%s %s-seeded axons:\n', curConfigs{1}, curDendType{1});
        fprintf('  %d to %d axons\n', min(curCounts), max(curCounts));
        fprintf('  %d to %d synapses / axon\n', min(curSynsMin), max(curSynsMax));
        fprintf('\n');
    end
end

%% Plotting
clear cur*;

binEdges = linspace(0, 1, 11);

for curTypeIdx = 1:numel(dataTypes)
    curType = dataTypes{curTypeIdx};
    curTypeData = inputData.(curType);
    
    curMatrix = nan( ...
        numel(binEdges) - 1, ...
        numel(runConfigs));
    for curConfigIdx = 1:numel(runConfigs)
        curName = runConfigs{curConfigIdx};
        
        curData = curTypeData.(curName);
        curData = curData(:, 1) ./ sum(curData, 2);
        
        curData = discretize(curData, binEdges);
        curData = accumarray(curData, 1, [numel(binEdges) - 1, 1]);
        curMatrix(:, curConfigIdx) = curData;
    end
    
   [fig, ax] = HNHP.Manual.Figure.plotDataMatrix( ...
        curMatrix, 'binEdges', binEdges, 'showZeros', false);
    
    xticklabels(ax, runConfigs);
    xtickangle(ax, 45);
    
    title(ax, curType);
    Figure.config(fig, info);
end
