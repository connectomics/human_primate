% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
bootstrap = struct;
% See `bootstrap.outDir` in runAnalysis.m
bootstrap.dir = '/conndata/amotta/hnhp/2022-02-26-model-bootstrapping';
bootstrap.runId = '20220416T102733';

curConfigs = HNHP.Manual.buildConfigs();
curConfigs = fieldnames(curConfigs);

mouseConfigs = curConfigs(startsWith(curConfigs, 'mouse'));
macaqueConfigs = curConfigs(startsWith(curConfigs, 'macaque'));
humanConfigs = curConfigs(startsWith(curConfigs, 'human'));
primateConfigs = union(macaqueConfigs, humanConfigs, 'stable');

% NOTE(amotta): For interneuron (IN) fractions, see species bulk in
% celltypes_classification_compiled_wk_links_v22_12_2021_v3.xlsx
curMouseConfig.title = 'mouse';
curMouseConfig.configs = mouseConfigs;
curMouseConfig.inFrac = 88 / (88 + 646);

curPrimateConfig = struct;
curPrimateConfig.title = 'primate';
curPrimateConfig.configs = primateConfigs;
curPrimateConfig.inFrac = (190 + 88) / ((190 + 88) + (420 + 216));

plotConfigs = [curMouseConfig, curPrimateConfig];

% See email from MH of 21.04.2022
[plotConfigs.neurons] = deal(100);
[plotConfigs.connProb] = deal(0.3);

clear cur*;
info = Util.runInfo();
Util.showRunInfo(info);

%% Load inferred connectivity
clear cur*;

curExcVars = {'aExcSpine', 'aExcSpinyShaft', 'aExcSmoothShaft'};
curInhVars = {'pInhSpine', 'pInhSpinyShaft'};

[plotConfigs.conn] = deal([]);
for curPlotConfigIdx = 1:numel(plotConfigs)
    curPlotConfig = plotConfigs(curPlotConfigIdx);
    curConfigs = curPlotConfig.configs;
    
    curConns = cell(1, 1, 1, numel(curConfigs));
    for curConfigIdx = 1:numel(curConfigs)
        curConfigName = curConfigs{curConfigIdx};

        curInFile = fullfile(bootstrap.dir, sprintf( ...
            '%s_%s.mat', bootstrap.runId, lower(curConfigName)));

        curSampleT = Util.load(curInFile, 'bootstrapSamples');
        curSampleT = struct2table(curSampleT, 'AsArray', true);
        
        % Remove invalid bootstrap samples
        curSampleT = curSampleT(curSampleT.exitFlag > 0, :);
        assert(all(ismember(curSampleT.exitFlag, [1, 2])));
        
        % Connectivity matrix
        curExcConn = curSampleT{:, curExcVars};
        curExcConn = curExcConn ./ sum(curExcConn, 2);
        curExcConn = curSampleT.pExc .* curExcConn;
        
        curInhConn = curSampleT{:, curInhVars};
        curInhConn = [curInhConn, (1 - sum(curInhConn, 2))]; %#ok
        curInhConn = (1 - curSampleT.pExc) .* curInhConn;
        
        curConn = [ ...
            reshape(transpose(curExcConn), 3, 1, []), ...
            reshape(transpose(curInhConn), 3, 1, [])];
        curConns{curConfigIdx} = curConn;
    end
    
    curConns = cell2mat(curConns);
    curConns = mean(curConns, 4);
    curConn = mean(curConns, 3);
    
    % Show results
    curConnT = arrayfun( ...
        @(mean, std) sprintf('%.1f%% ± %.1f%%', mean, std), ...
        100 * curConn, 100 * std(curConns, [], 3), ...
        'UniformOutput', false);
    
    curRowNames = {
        'Onto spine';
        'Onto spiny shaft';
        'Onto smooth shaft'};
    curColNames = { ...
        'Excitatory', ...
        'Inhibitory'};
    curConnT = array2table( ...
        curConnT, 'RowNames', curRowNames, ...
        'VariableNames', curColNames);
    
    fprintf('Connectivity for %s\n', curPlotConfig.title);
    fprintf('\n'); disp(curConnT);
    
    plotConfigs(curPlotConfigIdx).conn = curConn;
end

%% Plot
clear cur*;
rng(0);

neuronPos = max([plotConfigs.neurons]);
neuronPos = rand(neuronPos, 2);

fig = figure();
for configIdx = 1:numel(plotConfigs)
    c = plotConfigs(configIdx);

    neuronTypeProbs = [(1 - c.inFrac), c.inFrac];
    
    % 1 = ExN, 2 = IN
    neuronTypes = cumsum([0, neuronTypeProbs]);
    neuronTypes = discretize(linspace(0, 1, c.neurons), neuronTypes);
    neuronTypes = reshape(neuronTypes, [], 1);
    
    % Columns: type of presynaptic axon (exc. vs. inh)
    % Rows: target type (ExN spine vs. ExN shaft vs. IN shaft)
    connProbs = c.conn;
    
    % Rows: type of presynaptic neuron (ExN vs. IN)
    % Column: type of postsynaptic neuron (ExN vs. IN)
    connProbs = [ ...
        connProbs(1, 1) + connProbs(2, 1), connProbs(3, 1);
        connProbs(1, 2) + connProbs(2, 2), connProbs(3, 2)];
    
    % Correct for prevalence of neurons
    curConnCorrFactors = neuronTypeProbs .* transpose(neuronTypeProbs);
    connProbs = connProbs ./ curConnCorrFactors;
    connProbs = connProbs / sum(connProbs(:));
    
    % HACKHACKHACK(amotta): Let's ignore excitatory connections for now.
    % See email from MH of 21.04.2022 for context.
    connProbs(1, :) = nan;

    conn = cell(1, 2);
   [conn{:}] = ndgrid(neuronTypes, neuronTypes);
    conn = arrayfun(@(pre, post) connProbs(pre, post), conn{:});
    conn = conn * (c.connProb / mean(conn(:), 'omitnan'));
    
    conn = rand(size(conn)) < conn;
    conn(1:(size(conn, 1) + 1):end) = false;

    connPrePostIds = cell(1, 2);
   [connPrePostIds{:}] = find(conn);
    connPrePostIds = cell2mat(connPrePostIds);

    % NOTE(amotta): Draw inhibitory connections last
    connPrePostIds = sortrows(connPrePostIds, 'ascend');
    
%{
    % Sanity check
    for curNeuronType = 1:2
        curTargets = neuronTypes(connPrePostIds(:, 1)) == curNeuronType;
        curTargets = neuronTypes(connPrePostIds(curTargets, 2));
        curTargets = accumarray(curTargets, 1, size(neuronTypeProbs(:)));
        disp(sum(curTargets) / sum(neuronTypes == curNeuronType));
        curTargets = curTargets / sum(curTargets);
        disp(reshape(curTargets, 1, []));
    end
%}

    %% Let's plot
    ax = subplot(1, numel(plotConfigs), configIdx);
    hold(ax, 'on');
    
    for curConnIdx = 1:size(connPrePostIds, 1)
        curPrePostIds = connPrePostIds(curConnIdx, :);

        if neuronTypes(curPrePostIds(1)) == 1
            % excitatory connections
            curColor = [1, 0, 1];
        elseif neuronTypes(curPrePostIds(2)) == 1
            % inhibitory connection onto ExN
            curColor = [0, 0, 0];
        else
            % inhibitory connection onto IN
            curColor = [0, 1, 1];
        end

        plot(ax, ...
            neuronPos(curPrePostIds, 1), ...
            neuronPos(curPrePostIds, 2), ...
            'Color', curColor);
    end
    
    curPlot = @(mask, varargin) scatter(ax, ...
        neuronPos(mask, 1), neuronPos(mask, 2), varargin{:});
    curPlot(neuronTypes == 1, 24, [1, 0, 1], '^', 'filled');
    curPlot(neuronTypes == 2, 24, [0, 0, 0], 'o', 'filled');

    title(ax, c.title);
    ax.XAxis.Visible = 'off';
    ax.YAxis.Visible = 'off';
    
    axis(ax, 'square');
    xlim(ax, [0, 1]);
    ylim(ax, [0, 1]);
end

Figure.config(fig, info);
fig.Position(3:4) = [870, 310];
