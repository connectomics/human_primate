function data = loadInputData(inputFiles)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    extraTypes = struct('col', cell(0), 'var', cell(0));
    extraTypes(1).col = 'prim';   extraTypes(1).var = 'primarySpine';
    extraTypes(2).col = 'second'; extraTypes(2).var = 'secondarySpine';
    extraTypes(3).col = 'ter';    extraTypes(3).var = 'tertiarySpine';
    extraTypes(4).col = 'neck';   extraTypes(4).var = 'spineNeck';
    extraTypes(5).col = 'stub';   extraTypes(5).var = 'stub';
    
    data = struct;
    
    %% Volumetric data
    clear cur*;
    
    if isfield(inputFiles, 'volume')
        curT = HNHP.Manual.Data.getFile(inputFiles.volume);
        curT = readtable(curT, 'ReadVariableNames', true);
        curT = curT(startsWith(curT.name, 'syn_box'), :);
        curT = curT(:, {'name', 'sph', 'shaft', 'shaftExc', 'shaftIN'});
        
        if isequal(curT.shaft, curT.shaftExc + curT.shaftIN)
            curData = table;
            curData.sph = curT.sph;
            curData.shaftExN = curT.shaftExc;
            curData.shaftIN = curT.shaftIN;
        else
            assert(all(curT.shaftExc == 0));
            assert(all(curT.shaftIN == 0));
            curData = curT(:, {'sph', 'shaft'});
        end

        data.volume = curData;
    else
        warning('Volume measurements are missing');
    end
    
    %% Loading spiny dendrites
    clear cur*;
    
    curT = HNHP.Manual.Data.getFile(inputFiles.dendrites.file);
    curT = readtable(curT, 'ReadVariableNames', true);

    % See email from Sahil of 27.10.2021
    curMinDist = inputFiles.dendrites.minDistToCellBody;
    curT = curT(curT.distToCellbody >= curMinDist, :); % exclude proximal dendrites
    curT = curT(contains(curT.cellbodyType, 'pyr'), :); % restrict to pyramids
    curT = curT(logical(curT.cellbodyAttached), :); % restrict to cell-body attached
    curT = curT(not(contains(curT.name, 'apical', 'IgnoreCase', true)), :); % exclude apicals
    
    % Exclude bubbly dendrites
    % See email from Sahil of 07.03.2022
    curT = curT(not(contains(curT.name, 'bubbly', 'IgnoreCase', true)), :);
    
    % Extra synapses (e.g., onto spine necks)
    curExtraT = curT(:, {extraTypes.col});
    curExtraT.Properties.VariableNames = {extraTypes.var};
    
    curData = curT(:, {'sph', 'shaft'});
    curData = [curData, curExtraT];
    
    data.spinyDendrites = curData;
    
    %% Seeded axon reconstructions
    clear cur*;
    
    curT = HNHP.Manual.Data.getFile(inputFiles.seededAxons);
    curT = readtable(curT, 'ReadVariableNames', true);

    % IMPORTANT(amotta): Sahil told me that the seed synapse has been
    % removed. This is undone before inference, depending on the model.
    curT = curT(:, {'name', 'sph', 'shaft'});

    % Axons seeded at spiny synapses (onto spiny dendrites)
    curMask = ...
        contains(curT.name, 'seed_sph') ...
      | contains(curT.name, 'sph_seed');
    data.spineSeeded = curT(curMask, {'sph', 'shaft'});

    % Axons seeded at shaft synapses onto spiny dendrites
    curMask = ...
        contains(curT.name, 'seed_sh') ...
      | contains(curT.name, 'sh_seed');
    data.spinyShaftSeeded = curT(curMask, {'sph', 'shaft'});
    
    % Axons seeded at shaft synapses onto smooth dendrites
    curData = curT(contains(curT.name, 'seed_IN_smooth'), :);
    
    curMPMask = contains(curData.name, 'seed_IN_smooth_MP');
    curBPMask = contains(curData.name, 'seed_IN_smooth_BP');
    assert(not(any(curMPMask & curBPMask)));
    
    curData.dendType(:) = categorical({'Unknown'});
    curData.dendType(curMPMask) = 'Multipolar';
    curData.dendType(curBPMask) = 'Bipolar';
    
    data.smoothShaftSeeded = curData(:, {'sph', 'shaft', 'dendType'});
    
    %% Rename 'sph' to 'spine'
    clear cur*;
    
    for curField = reshape(fieldnames(data), 1, [])
        curData = data.(curField{1});
        assert(isequal(curData.Properties.VariableNames{1}, 'sph'));
        curData.Properties.VariableNames{1} = 'spine';
        data.(curField{1}) = curData;
    end
end
