function configs = buildConfigs()
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    
    %% P(exc | synapse) for extra synapse types
    % Must correspond to `extraTypes` in `loadInputData` function.
    % See email from / to Sahil of 01.04.2022
    
    extraSynExcProbs = struct;
    extraSynExcProbs.mouse = struct;
    extraSynExcProbs.mouse.primarySpine = 1;
    extraSynExcProbs.mouse.secondarySpine = 0;
    extraSynExcProbs.mouse.tertiarySpine = 0;
    extraSynExcProbs.mouse.spineNeck = 0;
    extraSynExcProbs.mouse.stub = 1;
    
    extraSynExcProbs.macaque = struct;
    extraSynExcProbs.macaque.primarySpine = 1;
    extraSynExcProbs.macaque.secondarySpine = 0;
    extraSynExcProbs.macaque.tertiarySpine = 0;
    extraSynExcProbs.macaque.spineNeck = 0;
    extraSynExcProbs.macaque.stub = 1;
    
    extraSynExcProbs.humanH5 = struct;
    extraSynExcProbs.humanH5.primarySpine = 1;
    extraSynExcProbs.humanH5.secondarySpine = 0;
    extraSynExcProbs.humanH5.tertiarySpine = 0;
    extraSynExcProbs.humanH5.spineNeck = 0;
    extraSynExcProbs.humanH5.stub = 0.5;
    
    % See email from Sahil of 06.04.2022
    extraSynExcProbs.humanH6 = struct;
    extraSynExcProbs.humanH6.primarySpine = 1;
    extraSynExcProbs.humanH6.secondarySpine = 0;
    extraSynExcProbs.humanH6.tertiarySpine = 0;
    extraSynExcProbs.humanH6.spineNeck = 0;
    extraSynExcProbs.humanH6.stub = 0.89;
    
    %% Build config
    configs = struct;

    configs.mouseS1 = struct;
    configs.mouseS1.seededAxons = 'dendSeededAxonDataAll-ex144_v14.xlsx';
    configs.mouseS1.volume = 'ex144_data_synapse_distribution_in_volume.xlsx';
    configs.mouseS1.dendrites = struct;
    configs.mouseS1.dendrites.file = 'dendData-ex144_v06.xlsx';
    configs.mouseS1.dendrites.minDistToCellBody = 30;
    configs.mouseS1.extraSynExcProbs = extraSynExcProbs.mouse;

    configs.mouseA2 = struct;
    configs.mouseA2.seededAxons = 'dendSeededAxonDataAll-Mouse_A2_v07.xlsx';
    configs.mouseA2.volume = 'Mouse_A2_data_synapse_distribution_in_volume_v01.xlsx';
    configs.mouseA2.dendrites = struct;
    configs.mouseA2.dendrites.file = 'dendData-Mouse_A2_v06.xlsx';
    configs.mouseA2.dendrites.minDistToCellBody = 30;
    configs.mouseA2.extraSynExcProbs = extraSynExcProbs.mouse;
    configs.mouseA2.validateOnAxonReconstructions = { ...
        '/gaba/u/sahilloo/data/Mouse_A2/data/tracings/axons/cellbody_attached/older_versions/Mouse_A2_cellbody_attached_axons_v02.nml'};
    
    configs.mouseV2 = struct;
    configs.mouseV2.seededAxons = 'dendSeededAxonDataAll-V2_L23_v03.xlsx';
    configs.mouseV2.volume = 'V2_L23_data_synapse_distribution_in_volume_v01.xlsx';
    configs.mouseV2.dendrites = struct;
    configs.mouseV2.dendrites.file = 'dendData-V2_L23_v03.xlsx';
    configs.mouseV2.dendrites.minDistToCellBody = 30;
    configs.mouseV2.extraSynExcProbs = extraSynExcProbs.mouse;

    configs.mousePPC = struct;
    configs.mousePPC.seededAxons = 'dendSeededAxonDataAll-PPC_AK_v05.xlsx';
    configs.mousePPC.volume = 'PPC_AK_data_synapse_distribution_in_volume_v01.xlsx';
    configs.mousePPC.dendrites = struct;
    configs.mousePPC.dendrites.file = 'dendData-PPC_AK_v04.xlsx';
    configs.mousePPC.dendrites.minDistToCellBody = 30;
    configs.mousePPC.extraSynExcProbs = extraSynExcProbs.mouse;
    configs.mousePPC.validateOnAxonReconstructions = { ...
        '/gaba/u/sahilloo/data/PPC2/data/tracings/axons/cellbody_attached/older_versions/PPC2_cellbody_attached_axons_v06.nml'};
    
    configs.mouseACC = struct;
    configs.mouseACC.seededAxons = 'dendSeededAxonDataAll-ACC_JO_v05.xlsx';
    configs.mouseACC.volume = 'ACC_JO_data_synapse_distribution_in_volume_v01.xlsx';
    configs.mouseACC.dendrites = struct;
    configs.mouseACC.dendrites.file = 'dendData-ACC_JO_v04.xlsx';
    configs.mouseACC.dendrites.minDistToCellBody = 30;
    configs.mouseACC.extraSynExcProbs = extraSynExcProbs.mouse;
    
    configs.macaqueS1 = struct;
    configs.macaqueS1.seededAxons = 'dendSeededAxonDataAll-mk_L23_v16.xlsx';
    configs.macaqueS1.volume = 'mk_L23_data_synapse_distribution_in_volume_v01.xlsx';
    configs.macaqueS1.dendrites = struct;
    configs.macaqueS1.dendrites.file = 'dendData-mk_L23_v04.xlsx';
    configs.macaqueS1.dendrites.minDistToCellBody = 45;
    configs.macaqueS1.extraSynExcProbs = extraSynExcProbs.macaque;
    configs.macaqueS1.validateOnAxonReconstructions = { ...
        '/gaba/u/sahilloo/data/mk_L23/data/tracings/axons/cellbody_attached/older_versions/mk-L23_cellbody_attached_axons_v06.nml'};
    
    configs.macaqueSTG = struct;
    configs.macaqueSTG.seededAxons = 'dendSeededAxonDataAll-Mk1_T2_v05.xlsx';
    configs.macaqueSTG.volume = 'Mk1_T2_data_synapse_distribution_in_volume_v02.xlsx';
    configs.macaqueSTG.dendrites = struct;
    configs.macaqueSTG.dendrites.file = 'dendData-Mk1_T2_v04.xlsx';
    configs.macaqueSTG.dendrites.minDistToCellBody = 45;
    configs.macaqueSTG.extraSynExcProbs = extraSynExcProbs.macaque;

    configs.humanH5 = struct;
    configs.humanH5.seededAxons = 'dendSeededAxonDataAll-H5_10_L23_v17.xlsx';
    configs.humanH5.volume = 'H5_5_AMK_data_synapse_distribution_in_volume_v03.xlsx';
    configs.humanH5.dendrites = struct;
    configs.humanH5.dendrites.file = 'dendData-H5_10_L23_v08.xlsx';
    configs.humanH5.dendrites.minDistToCellBody = 80;
    configs.humanH5.extraSynExcProbs = extraSynExcProbs.humanH5;
    configs.humanH5.validateOnAxonReconstructions = { ...
        % NOTE(amotta): Excluded because axonogram enters infinite loop
        % '/gaba/u/sahilloo/data/H5_5_AMK/data/tracings/axons/cellbody_attached/older_versions/H5_5_AMK_cellbody_attached_axons_v07.nml', ...
        '/gaba/u/sahilloo/data/H5_10_ext/data/tracings/axons/cellbody_attached/older_versions/H5_10_ext_cellbody_attached_axons_v12.nml'};
    
    configs.humanH6 = struct;
    configs.humanH6.seededAxons = 'dendSeededAxonDataAll-H6_4S_L23_v11.xlsx';
    configs.humanH6.volume = 'H6_4S_L23_data_synapse_distribution_in_volume_v01.xlsx';
    configs.humanH6.dendrites = struct;
    configs.humanH6.dendrites.file = 'dendData-H6_4S_L23_v10Distal.xlsx';
    configs.humanH6.dendrites.minDistToCellBody = 80;
    configs.humanH6.extraSynExcProbs = extraSynExcProbs.humanH6;
end
