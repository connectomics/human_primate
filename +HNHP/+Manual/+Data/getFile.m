function path = getFile(name)
    path = fileparts(mfilename('fullpath'));
    path = fullfile(path, name);
end
