% Written by
%   Alessandro Motta <alessandro.motta@brain.mpg.de>
clear;

%% Configuration
% To show all, set to `[]`
showConfigs = [];

plotConfigs = struct;
plotConfigs(1).var = 'inhFracSpinyDend';
plotConfigs(1).label = 'P(inhibitory | synapse onto spiny dendrite)';

plotConfigs(2).var = 'inhFracSpinyDendWithExtraSyns';
plotConfigs(2).label = 'P(inhibitory | synapse onto spiny dendrite, w. extra)';

plotConfigs(3).var = 'inhFracSmoothDend';
plotConfigs(3).label = 'P(inhibitory | synapse onto smooth dendrite)';

plotConfigs(4).var = 'inhFracMultipolarDendOnly';
plotConfigs(4).label = 'P(inhibitory | synapse onto MP IN dendrite)';

plotConfigs(5).var = 'inhFracBipolarDendOnly';
plotConfigs(5).label = 'P(inhibitory | synapse onto BP IN dendrite)';

bootstrap = struct;
% See `bootstrap.outDir` in runAnalysis.m
bootstrap.dir = '/conndata/amotta/hnhp/2022-02-26-model-bootstrapping';
bootstrap.runId = '20220416T102733';

% TODO(amotta): Read from Excel file?
% See celltypes_classification_compiled_wk_links_v22_12_2021_v3.xlsx
% and email from Sahil of 16.04.2022.
excInhSomata.mouseS1    = [135,  14]; % ex144
excInhSomata.mouseA2    = [184,  30]; % Mouse_A2
excInhSomata.mouseV2    = [120,  13]; % V2_L23
excInhSomata.mousePPC   = [125,  21]; % PPC_AK
excInhSomata.mouseACC   = [ 82,  10]; % ACC_JO

excInhSomata.macaqueS1  = [235, 107]; % mk_L23
excInhSomata.macaqueSTG = [185,  83]; % Mk1_T2

excInhSomata.humanH5    = [120,  55]; % H5_5_AMK
excInhSomata.humanH6    = [ 96,  33]; % H6_4S_L23

% NOTE(amotta): This is roughly the value automatically chosen by MATLAB's
% `ksdensity` function on the bootstrap samples for I/(I+E) on smooth
% dendrites. Let's use this same bandwidth for all configurations.
violinBandwidth = 0.005;

info = Util.runInfo();
Util.showRunInfo(info);

%% Loading data
configs = HNHP.Manual.buildConfigs();

if isequal(showConfigs, [])
    showConfigs = fieldnames(configs);
end

assert(iscell(showConfigs));
assert(all(ismember(showConfigs, fieldnames(configs))));

mouseConfigs = showConfigs(startsWith(showConfigs, 'mouse'));
macaqueConfigs = showConfigs(startsWith(showConfigs, 'macaque'));
humanConfigs = showConfigs(startsWith(showConfigs, 'human'));
primateConfigs = union(macaqueConfigs, humanConfigs, 'stable');

samples = struct;
axonBasedValid = struct;
for curConfigIdx = 1:numel(showConfigs)
    curConfigName = showConfigs{curConfigIdx};
    
    curInFile = fullfile(bootstrap.dir, sprintf( ...
        '%s_%s.mat', bootstrap.runId, lower(curConfigName)));
    
    curSampleT = Util.load(curInFile, 'bootstrapSamples');
    curSampleT = struct2table(curSampleT, 'AsArray', true);
    
    % Remove bootstrap samples where model parameter optimization failed
    curSampleT = curSampleT(curSampleT.exitFlag > 0, :);
    assert(all(ismember(curSampleT.exitFlag, [1, 2])));
    samples.(curConfigName) = curSampleT;
    
    try
        curValidT = Util.load(curInFile, 'axonBasedValidation');
        axonBasedValid.(curConfigName) = curValidT;
    catch
        % No validation data - nothing to do
    end
end

%% Violin plots of I/(I+E) for all datasets
clear cur*;

for curPlotConfig = plotConfigs
    curTickStepSize = 0.05;
    curBinSize = 0.01;

    curMax = max(structfun(@(s) max(s.(curPlotConfig.var)), samples));
    curMax = curTickStepSize * ceil(curMax / curTickStepSize);

    curFig = figure();
    curAx = axes(curFig); %#ok

    curAllSamples = cellfun( ...
        @(name) samples.(name).(curPlotConfig.var), ...
        showConfigs, 'UniformOutput', false);
    
    % NOTE(amotta): Because our samples can be very close to to the lower
    % limit of the support, the 'log' boundary correction normally used by
    % MATLAB can result in values that approach negative infinity. That's
    % why we are using the 'reflection' boundary correction here.
    Figure.violin( ...
        curAx, curAllSamples, ...
        'Percentile', 25, 'Bandwidth', violinBandwidth, ...
        'Support', [0, 1], 'BoundaryCorrection', 'reflection');
    
    xlim(curAx, [0, numel(showConfigs)] + 0.5);
    xticks(curAx, 1:numel(showConfigs));
    xticklabels(curAx, showConfigs);
    xtickangle(curAx, 45);
    
    ylabel(curAx, curPlotConfig.label);
    ylim(curAx, [0, curMax]);

    Figure.config(curFig, info);
    curFig.Position(3:4) = [310, 420];
end

%% Violin plots of I/(I+E), without and with extra synapses
clear cur*;

curPlotConfig = plotConfigs(1);
curVars = strcat(curPlotConfig.var, {'', 'WithExtraSyns'});

curTickStepSize = 0.05;

curAllSamples = cell(numel(curVars), numel(showConfigs));
for curVarIdx = 1:numel(curVars)
    curAllSamples(curVarIdx, :) = cellfun( ...
        @(name) samples.(name).(curVars{curVarIdx}), ...
        showConfigs, 'UniformOutput', false);
end

curMax = max(cellfun(@max, curAllSamples(:)));
curMax = curTickStepSize * ceil(curMax / curTickStepSize);

curFig = figure();
curAx = axes(curFig);
hold(curAx, 'on');

% NOTE(amotta): Because our samples can be very close to to the lower
% limit of the support, the 'log' boundary correction normally used by
% MATLAB can result in values that approach negative infinity. That's
% why we are using the 'reflection' boundary correction here.
Figure.violin( ...
    curAx, curAllSamples, ...
    'Percentile', 25, 'Bandwidth', violinBandwidth, ...
    'Support', [0, 1], 'BoundaryCorrection', 'reflection');

curViolins = flip(curAx.Children);
for curIdx = 2:2:numel(curViolins)
    curViolins(curIdx).Children(end).FaceColor(:) = 0.85;
end

xlim(curAx, [0, numel(curAllSamples)] + 0.5);
curTicks = (0:(numel(showConfigs) - 1)) * numel(curVars);
curTicks = (1 + numel(curVars)) / 2 + curTicks;
xticks(curAx, curTicks);
xticklabels(curAx, showConfigs);
xtickangle(curAx, 45);

ylabel(curAx, curPlotConfig.label);
ylim(curAx, [0, curMax]);

% Add mouse-to-primate prediction
curPair = struct;
curPair(1).title = 'mouse';   curPair(1).configs = mouseConfigs;
curPair(2).title = 'primate'; curPair(2).configs = primateConfigs;

[~, curPredLimX] = ismember(curPair(2).configs, showConfigs);
curPredLimX = 1 + numel(curVars) * (min(curPredLimX) - 1);
curPredLimX = [curPredLimX - 0.5, curAx.XLim(2)];

% Compute probability density of predictions
curPred = buildTestData(curPair, ...
    samples, excInhSomata, curVars{end});
curPred = ksdensity( ...
    curPred.prediction, linspace(0, curMax, 1001), ...
    'Support', [0, 1], 'BoundaryCorrection', 'reflection');
curPred = curPred / max(curPred);

[~, curPredMAP] = max(curPred);
curPredMAP = curMax * ((curPredMAP - 1) / (numel(curPred) - 1));

% Turn predictions into heatmap
curGamma = 2.2;
curToColor = @(prob) ( ...
    ([0.25, 1.0, 1.0] .^ curGamma) .* prob(:) ...
  + ([1.0, 1.0, 1.0] .^ curGamma) .* (1 - prob(:)) ...
) .^ (1 / curGamma);

curPred = curToColor(curPred);
curPred = reshape(curPred, [], 1, 3);
curPred = repmat(curPred, 1, 2, 1);

plot(curAx, ...
    curPredLimX, repelem(curPredMAP, 2), ...
    'Color', 'black', 'LineWidth', 2);
curAx.Children = circshift(curAx.Children, -1);

% HACKHACKHACK(amotta): `XData` does not contain the X coordinages range
% occupied by the image, but the center coordinates of the pixels in the
% first and last columns. Our image has two pixel columns, such that
% `XData` corresponds to 1/4 and 3/4 of the X coordinage range.
% 
% Let's apply the inverse transformation to get the desired result.
curIm = image(curAx, ...
    'XData', [3/4, 1/4; 1/4, 3/4] * curPredLimX(:), ...
    'YData', curAx.YLim, 'CData', curPred);
curAx.Children = circshift(curAx.Children, -1);

% Add colorbar
curCmap = linspace(0, 1, 101);
curCmap = curToColor(curCmap);
colormap(curAx, curCmap);
curAx.CLim = [0, 1];

curCbar = colorbar(curAx);
curCbar.Ticks = curCbar.Ticks([1, end]);
curCbar.TickLabels = {'0', 'Max'};
curCbar.Label.String = { ...
    sprintf('Prediction from %s to %s', curPair.title);
    '(scaled probability density)'};

% Add legend
curLeg = legend(curAx, ...
   [curViolins(1).Children(end), ...
    curViolins(2).Children(end)], ...
   {'without extra synapses', ...
    'with extra synapses'});
curLeg.Location = 'NorthWest';

curAx.Layer = 'top';
Figure.config(curFig, info);
curFig.Position(3:4) = [550, 420];

%% Violin plots of I/(I+E) for IN subtypes
clear cur*;
rng(0);

curViolins = cell(0, 2);
    
for curConfigs = { ...
        {'Mouse', mouseConfigs}, ...
        {'Primate', primateConfigs}}
    curConfigs = curConfigs{1}; %#ok
    
    for curDendType = { ...
            {'MP', 'Multipolar'}, ...
            {'BP', 'Bipolar'}}
        curDendType = curDendType{1}; %#ok
        
        curLabel = sprintf('%s %s', curConfigs{1}, curDendType{1});
        curVar = sprintf('inhFrac%sDendOnly', curDendType{2});
        
        curData = cell2mat(cellfun( ...
            @(config) reshape(samples.(config).(curVar), [], 1), ...
            reshape(curConfigs{2}, 1, []), 'UniformOutput', false));
        % NOTE(amotta): Average across datasets per species
        curData = mean(curData, 2);
        
        curViolins(end + 1, :) = {curLabel, curData}; %#ok
    end
end

% Summary
curSummaryT = table;
curSummaryT.name = curViolins(:, 1);
curSummaryT.mean = round(100 * cellfun(@mean, curViolins(:, 2)), 3);
curSummaryT.std = round(100 * cellfun(@std, curViolins(:, 2)), 3);

fprintf('I/(I+E) for MP and BP dendrites\n');
fprintf('\n');
disp(curSummaryT);

% Statistical test
curPValues = nan(size(curViolins, 1));
for curIdxRef = 1:size(curViolins, 1)
    for curIdxTest = 1:size(curViolins, 1)
        if curIdxRef == curIdxTest; continue; end
        
        curRefData = curViolins{curIdxRef, 2};
        curTestData = curViolins{curIdxTest, 2};
        
        % HACKHACKHACK(amotta): Could result in over-estimation of p
        curCount = min(numel(curRefData), numel(curTestData));
        curRefData = curRefData(randperm(numel(curRefData), curCount));
        curTestData = curTestData(randperm(numel(curTestData), curCount));
        
        curPValue = mean(curTestData(:) <= curRefData(:));
        curPValues(curIdxRef, curIdxTest) = curPValue;
    end
end

curVarNames = strrep(curViolins(:, 1), ' ', '');
curPValues = array2table(curPValues, ...
    'RowNames', curVarNames, ...
    'VariableNames', curVarNames);

fprintf('Testing I/(I+E) for MP and BP dendrites\n');
fprintf('\n');
disp(curPValues);

% Plotting
curFig = figure();
curAx = axes(curFig);

Figure.violin( ...
    curAx, curViolins(:, 2), ...
    'Percentile', 25, 'Bandwidth', violinBandwidth, ...
    'Support', [0, 1], 'BoundaryCorrection', 'reflection');

xlim(curAx, [1 - 0.5, size(curViolins, 1) + 0.5]);
xticks(curAx, 1:size(curViolins, 1));
xticklabels(curAx, curViolins(:, 1));
xtickangle(curAx, 45);

curAx.YLim(1) = 0;
ylabel(curAx, 'P(inhibitory | input synapse)');

Figure.config(curFig, info);
curFig.Position(3:4) = [240, 420];

%% Extrapolation from reference to test species
clear cur*;
rng(0);

curPair = struct;
curPair(1).title = 'Mouse';   curPair(1).configs = mouseConfigs;
curPair(2).title = 'Primate'; curPair(2).configs = primateConfigs;
% NOTE(amotta): Uncomment to compute upper bound on true primate I/(I+E).
% This is the rate with which true inhibitory synapses get misclassified as
% excitatory. It was computed via the `applyModelToAxonReconstructions`
% function in HNHP.Manual.runAnalysis.
% curPair(2).inhToExc = 1 - 0.916;

% Analyze results
for curPlotConfig = plotConfigs
    curVar = curPlotConfig.var;
    curSamples = samples;

    if isfield(curPair(2), 'inhToExc')
        % NOTE(amotta): Upper bound on error-corrected I/(I+E) for primate:
        % Let's assume that the true I/(I+E) is X. Instead, we observe a
        % correupted version. Specifically, a fraction C of the true I
        % synapses gets misclassified as E.
        %
        % The corrupted I/(I+E) ratio Y is:
        % Y = (1-C)×I / [(1-C)×I + (C×I + E)]
        %   = (1-C)×I / (I + E)
        %   = (1-C)×X
        %
        % Thus, the error-correted I/(I+E) ratio X can be recovered from
        % the corrupted I/(I+E) ratio Y as X = Y / (1-C).
        for curConfig = reshape(curPair(2).configs, 1, [])
            curData = curSamples.(curConfig{1}).(curVar);
            curData = curData / (1 - curPair(2).inhToExc);
            curSamples.(curConfig{1}).(curVar) = curData;
        end
    end
    
    curDataT = buildTestData(curPair, curSamples, excInhSomata, curVar);
    curPValue = mean(curDataT.test >= curDataT.prediction);
    
    curPlot = struct;
    % Species #1
    curPlot(1).title = curPair(1).title;
    curPlot(1).data  = curDataT.reference;
    % Species #2
    curPlot(2).title = curPair(2).title;
    curPlot(2).data  = curDataT.test;
    % Prediction from species #1 to #2
    curPlot(3).title = 'Prediction';
    curPlot(3).data  = curDataT.prediction;

    % Figure
    curFig = figure();
    curAx = axes(curFig); %#ok

    Figure.violin( ...
        curAx, {curPlot.data}, ...
        'Percentile', 25, 'Bandwidth', violinBandwidth, ...
        'Support', [0, 1], 'BoundaryCorrection', 'reflection');

    xticks(curAx, 1:3);
    xticklabels(curAx, {curPlot.title});
    xlim(curAx, [0.5, 3.5]);
    
    curAx.YLim(1) = 0;
    ylabel(curAx, curPlotConfig.label);

    curAdditionalInfos = sprintf( ...
        'P(%s ≥ prediction from %s) = %.3f', ...
        curPair(2).title, curPair(1).title, curPValue);
    Figure.config(curFig, info, ...
        'additionalInfos', curAdditionalInfos);
    curFig.Position(3:4) = [310, 420];
    
    % Summary
    fprintf('%s\n', curPlotConfig.label);
    
    curPrint = @(p) fprintf( ...
        '  %s: %.1f%% +- %.1f%%\n', p.title, ...
        100 * mean(p.data, 'omitnan'), ...
        100 * std(p.data, 'omitnan'));
    curPrint(curPlot(1));
    
    curPrint(curPlot(2));
    fprintf('    p-value: %f\n', mean( ...
        curDataT.test <= curDataT.reference));
    
    curPrint(curPlot(3));
    fprintf('    p-value: %f\n', curPValue);
    fprintf('\n');
end

%% Quantification of axon-based validation
clear cur*;

curConfigs = fieldnames(axonBasedValid);
curMouseConfigs = intersect(mouseConfigs, curConfigs);
curPrimateConfigs = intersect(primateConfigs, curConfigs);

curInhExcStrings = {'inhibitory', 'excitatory'};
fprintf('Evaluation of synapse classifcation\n');

for curConfig = { ...
        {'Mouse', curMouseConfigs}, ...
        {'Primate', curPrimateConfigs}}
    curConfig = curConfig{1}; %#ok
    fprintf('  For %s\n', curConfig{1});
    
    curValidT = cellfun( ...
        @(n) axonBasedValid.(n), ...
        curConfig{2}, 'UniformOutput', false);
    curValidT = cat(1, curValidT{:});
    
    curValidT.inferredIsExc = curValidT.excProb > 0.5;
    curValidT.isConfused = not(curValidT.isExc == curValidT.inferredIsExc);
    
    for curIsExc = { ...
            {'Excitatory', true}, ...
            {'Inhibitory', false}}
        curIsExc = curIsExc{1}; %#ok
        
        curMask = curValidT.isExc == curIsExc{2};
        curConfusionRate = mean(curValidT.isConfused(curMask));
        
        fprintf( ...
            '    P(predicted as %s | truly %s) = %.1f%%\n', ...
            curInhExcStrings{2 - curIsExc{2}}, ...
            curInhExcStrings{1 + curIsExc{2}}, ...
            100 * curConfusionRate);
    end
end

fprintf('\n');

%% IN targeting rate of excitatory axons
clear cur*;

curVars = {'aExcSpine', 'aExcSpinyShaft', 'aExcSmoothShaft'};

fprintf('P(smooth shaft | excitatory synapse)\n');

for curConfig = { ...
        {'Mouse', mouseConfigs}, ...
        {'Primate', primateConfigs}}
    curConfig = curConfig{1}; %#ok
    
    curData = cellfun( ...
        @(c) feval( ...
            @(aExc) aExc(:, 3) ./ sum(aExc, 2), ...
            samples.(c){:, curVars}), ...
        reshape(curConfig{2}, 1, []), ...
        'UniformOutput', false);
    curData = cell2mat(curData);
    curData = mean(curData, 2);
    
    fprintf( ...
        '  %s: %.1f%%+-%.1f%%\n', curConfig{1}, ...
        100 * mean(curData), 100 * std(curData));
end

fprintf('\n');

%% IN-to-IN connectivity
clear cur*;

curVars = {'pInhSpine', 'pInhSpinyShaft'};
fprintf('P(inhibitory smooth shaft synapse)\n');

curPValue = cell(0);
for curConfig = { ...
        {'Macaque', macaqueConfigs}, ...
        {'Human', humanConfigs}}
    curConfig = curConfig{1}; %#ok
    
    curData = cellfun( ...
        @(c) feval( ...
            @(pExc, pInh) (1 - pExc) .* (1 - sum(pInh, 2)), ...
            samples.(c).pExc, samples.(c){:, curVars}), ...
        reshape(curConfig{2}, 1, []), ...
        'UniformOutput', false);
    curData = cell2mat(curData);
    curData = mean(curData, 2);
    
    curPValue{end + 1} = curData; %#ok
    
    fprintf( ...
        '  %s: %.1f%%+-%.1f%%\n', curConfig{1}, ...
        100 * mean(curData), 100 * std(curData));
end

curPValue = mean(curPValue{2} <= curPValue{1});
fprintf('  p-value: %f\n', curPValue);
fprintf('\n');

%% Utilities
function dataT = buildTestData(pair, samples, excInhSomata, varName)
    rng(0);
    
    numSamples = nan(2, 1);
    pairExcInhSomata = nan(2, 2);
    for curPairIdx = 1:2
        curConfigs = pair(curPairIdx).configs;
        
        curNumSamples = min(cellfun( ...
            @(n) height(samples.(n)), curConfigs));
        numSamples(curPairIdx) = curNumSamples;
        
        curExcInhSomata = cell2mat(cellfun( ...
            @(n) reshape(excInhSomata.(n), 1, 2), ...
            reshape(curConfigs, [], 1), ...
            'UniformOutput', false));
        curExcInhSomata = sum(curExcInhSomata, 1);
        pairExcInhSomata(curPairIdx, :) = curExcInhSomata;
    end

    numSamples = min(numSamples);

    %% Collect samples
    clear cur*;
    
    refTestData = cell(1, 2);
    for curPairIdx = 1:2
        curConfigs = pair(curPairIdx).configs;
        curPairData = nan(numSamples, numel(curConfigs));

        for curConfigIdx = 1:numel(curConfigs)
            curConfig = curConfigs{curConfigIdx};
            
            curData = samples.(curConfig).(varName);
            curData = curData(randperm(numel(curData), numSamples));
            curPairData(:, curConfigIdx) = curData;
        end
        
        assert(not(any(isnan(curPairData(:)))));
        refTestData{curPairIdx} = curPairData;
    end
    
   [refData, testData] = deal(refTestData{:});
    Util.clear(refTestData);
    
    refData = mean(refData, 2);
    testData = mean(testData, 2);

    %% Make predictions from reference to test set
    clear cur*;

    curRefExcInhSomata = pairExcInhSomata(1, :);
    curTestExcInhSomata = pairExcInhSomata(2, :);
    
    curResampledInhFrac = @(ei) binornd( ...
        sum(ei), ei(2) / sum(ei)) / sum(ei);

    predData = nan(size(refData));
    for curSampleIdx = 1:numel(refData)
        curRefInhSynFrac = refData(curSampleIdx);
        
        % Resampling to account for uncertainty in soma counts
        curRefInhSomaFrac = curResampledInhFrac(curRefExcInhSomata);
        curTestInhSomaFrac = curResampledInhFrac(curTestExcInhSomata);

        % If curRefInhSynFrac = P(inhibitory | synapse onto spiny
        % dendrite) and curRefInhSomaFrac = P(interneuron | soma in the
        % volume), the expected synapse contribution of an interneuron
        % relative to excitatory neuron is:
        curRefInhSomaSynFrac = ...
            ((1 - curRefInhSomaFrac) / curRefInhSomaFrac) ...
          * (curRefInhSynFrac / (1 - curRefInhSynFrac));

        curPredInhSynFrac = ...
            curRefInhSomaSynFrac * curTestInhSomaFrac / ( ...
            curRefInhSomaSynFrac * curTestInhSomaFrac ...
          + 1 * (1 - curTestInhSomaFrac));
        
        predData(curSampleIdx) = curPredInhSynFrac;
    end
    
    %% Build output
    clear cur*;
    
    dataT = table;
    dataT.reference = refData;
    dataT.prediction = predData;
    dataT.test = testData;
end
