function [fig, ax] = plotDataMatrix(data, varargin)
    % Written by
    %   Alessandro Motta <alessandro.motta@brain.mpg.de>
    opts = struct;
    opts.info = [];
    opts.binEdges = [];
    opts.showZeros = true;
    opts.numberFormat = '%.0f';
    opts.columnNames = num2cell(char( ...
        double('A') + (1:size(data, 2)) - 1));
    opts = Util.modifyStruct(opts, varargin{:});
    
    assert(size(data, 1) == numel(opts.binEdges) - 1);
    assert(size(data, 2) == numel(opts.columnNames));
    
    fig = figure();
    ax = axes(fig);
    hold(ax, 'on');
    
    cmap = linspace(1, 0, 101);
    cmap = repmat(cmap(:), 1, 3);
    colormap(ax, cmap);
    ax.CLim = [0, 1];
    
    for curCol = 1:size(data, 2)
        curTotal = sum(data(:, curCol), 'omitnan');
        
        for curRow = 1:size(data, 1)
            curVal = data(curRow, curCol);
            
            curPos = opts.binEdges(curRow + [0, 1]);
            curPos = [curCol - 0.5, curPos(1), 1, diff(curPos)];
            curColor = 1 - curVal / curTotal;

            rectangle(...
                ax, 'Position', curPos, ...
                'FaceColor', repelem(curColor, 1, 3), ...
                'EdgeColor', 'none');
            
            curX = curPos(1) + curPos(3) / 2;
            curY = curPos(2) + curPos(4) / 2;
            if curColor > 0.5; curColor = 0; else; curColor = 1; end

            curNumStr = num2str(curVal, opts.numberFormat);
            if opts.showZeros || not(strcmp(curNumStr, '0'))
                text(...
                    ax, curX, curY, curNumStr, ...
                    'Color', repelem(curColor, 1, 3), ...
                    'HorizontalAlignment', 'center');
            end
        end
    end
    
    daspect(ax, [size(data, 1), 1, 1]);
    xlim(ax, [0.5, size(data, 2) + 0.5]);
    ylim(ax, [0, 1]);
    
    xticks(ax, 1:size(data, 2));
    xticklabels(ax, opts.columnNames);
    
    ylabel(ax, 'Fraction of synapses onto spine heads');
    yticks(ax, opts.binEdges);
    
    cbar = colorbar(ax);
    cbar.Label.String = 'Fraction of data points';
end
