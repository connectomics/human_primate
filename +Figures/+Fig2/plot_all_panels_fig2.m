% Written by
%  Sahil Loomba <sahil.loomba@brain.mpg.de>
%% panel B
% IN fractions
clear; clc;
Figures.setConfig;

cx_path = fullfile('data','excel_files');
cx_data = readtable(fullfile(cx_path,'celltypes_classification_compiled_wk_links_v22_12_2021_v3_out.xlsx'));

cx_data(cellfun(@isempty, cx_data.Datasets),:) = []; % remove empty rows

cx_data.INFrac = cx_data.IN ./ (cx_data.Pyr_ExN_Stellate + cx_data.IN);

mouseColor = [0,0,0]; 
humanColor = [0,0,0];% black
mkColor = 'b';
homeColorEdge = 'k';

cx_speciesTick = [0.8, 7, 10]; % for species center
cx_speciesTickWithLayers = [0.70,0.90, 6.90, 7.10, 9.90, 10.10, 8.5]; % L4 and L23 around centers
cx_species = {'Mouse',...
              'Macaque','Human'};

allRegionNames = {'ex145', 'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO', 'JO_A1_L34',...
                    'Mk L4','mk_L23','Mk1_T2',...
                    'H5 L4', 'H5 L23','H5_5_AMK', 'H2_3 L4','H6_4S_L23'};
allRegionNamesText = {'S1 L4', 'S1 L23', 'A2', 'V2 L23', 'PPC L23', 'ACC L23', 'A1 L34',...
                    'MQ S1 L4', 'MQ S1 L23', 'MQ STG L23',...
                    'Hum ST L4', 'Hum ST L23','Hum ST L23', 'Hum MT L4', 'Hum FC'};
allRegionNamesPos = [0.70, 0.80, 0.90, 1, 1.10, 1.20, 1.30,...
                    6.9, 7.1,7.3,...
                    9.9, 10.1, 10.3, 9.9, 10.5];
allRegionNamesMarker = {'o','x','x','x','x','x','x',...
                    'o','x','x',...
                    'o','x','x','o','x'};
allRegionNamesMarkerSize = [20,20,20,20,20,20,20,...
                    20,20,20,...
                    20,20,20,20,20];

Util.log('Now plotting:')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;

% ex144
doForThisRegionB(cx_data, 'ex144', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% ex145
%doForThisRegionB(cx_data, 'ex145', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% PPC
doForThisRegionB(cx_data, 'PPC_AK', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% ACC
doForThisRegionB(cx_data, 'ACC_JO', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% V2 
doForThisRegionB(cx_data, 'V2_L23', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% Mouse_A2
doForThisRegionB(cx_data, 'Mouse_A2', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)


% MQ
doForThisRegionB(cx_data, 'mk_L23', mkColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
doForThisRegionB(cx_data, 'Mk1_T2', mkColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% Human
%doForThisRegionB(cx_data, 'H5 L23', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
doForThisRegionB(cx_data, 'H5_5_AMK', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
doForThisRegionB(cx_data, 'H6_4S_L23', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

%% plot bulk per species
idx = contains(cx_data.Datasets, {'ex144','PPC_AK','ACC_JO','V2_L23','Mouse_A2'});
mouseB = plotForSpeciesB('mouse', cx_data, idx, cx_speciesTickWithLayers(2), 'r', 'x', 20);

idx = contains(cx_data.Datasets, {'mk_L23','Mk1_T2'});
plotForSpeciesB('nhp', cx_data, idx, cx_speciesTickWithLayers(4), 'r', 'x', 20);

idx = contains(cx_data.Datasets, {'H5_5_AMK','H6_4S_L23'}); % {'H5_5_AMK','H5 L23','H6_4S_L23'}
plotForSpeciesB('human', cx_data, idx, cx_speciesTickWithLayers(6), 'r', 'x', 20);

idx = contains(cx_data.Datasets,  {'mk_L23','Mk1_T2', 'H5_5_AMK','H6_4S_L23'}); % {'MkL23','Mk1_T2_STG', 'H5_5_AMK','H5 L23','H6_4S_L23'}
nhpHumanB = plotForSpeciesB('nhpHuman', cx_data, idx, cx_speciesTickWithLayers(7), 'r', 'x', 20);

% test on bootstrapped samples
rng(0)

factor = mean(nhpHumanB)/mean(mouseB);
pVal1 = sum(mouseB>=min(nhpHumanB))/numel(mouseB);
pVal2 = sum(nhpHumanB./mouseB >= factor)/numel(mouseB);
Util.log('Mouse vs nhpHuman: pVal = %e (test increase),\n  pVal = %e (test increase by factor: %.2f)',pVal1, pVal2, factor)

% save
ax.YAxis.Label.String = 'Fraction of interneurons (of all neurons) (%)';
ax.YAxis.Limits = [0,40];
ax.YAxis.TickValues = 0:20:40;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:10:80;
ax.XAxis.Limits = [0,15];
ax.XAxis.TickValues = cx_speciesTick;
ax.XAxis.TickLabels = cx_species;
ax.LineWidth = 2;
Util.setPlotDefault(gca,'','');
set(gca,'FontSize',18)

%% panel E
% dendrite synapse densities
clear; clc;
synapseTypes = {'sph','shaft'};
synapseColors = {'m','k'};
allData = cell(numel(synapseTypes),1);
for st = 1:numel(synapseTypes)
    synapseType = synapseTypes{st};
    synapseColor = synapseColors{st};
    % load saved data from parsed nmls in matfiles per region

    Figures.setConfig
    allColors = Util.getSomaColors(true);

    fig = figure;
    fig.Color = 'white';

    Util.log('Plotting soma data:')
    ax1 = gca; hold on;

    allRegionNames = {'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO',...
                'mk_L23', 'Mk1_T2', ...
                'H5_10_L23', 'H6_4S_L23'};

    legendNames = allRegionNames;
    allRegionPos = [1,1.5,2,2.5, 3, ...
                    6,6.5, ...
                    8,8.5];
    allRegionMarkers = {'x','x','x','x','x', ...
                    'x','x', ...
                    'x','x'};
    tickNames = legendNames;
    out = cell(numel(allRegionNames),1);
    for i=1:numel(allRegionNames)
        curOut=doForThisRegionE(ax1, synapseType, legendNames{i}, allRegionPos(i), allRegionMarkers{i},synapseColor);
        legendNames{i} = sprintf('%s (N=%d)',legendNames{i},numel(curOut));
        out{i} = curOut;
    end

    c = cellfun(@(x) {sprintf('%s',x), ''}, legendNames, 'UniformOutput', false); % duplicate for median lines
    legendNames = cat(2, c{:});

    % cosmetics
    ticklen = [0.025, 0.025];
    ax1.YAxis.TickValues = 0:0.5:4;
    ax1.YAxis.Limits = [0,2]; % [0,4];
    ax1.YAxis.MinorTick = 'on';
    ax1.YAxis.Label.String = sprintf('Synapses per um %s', synapseType);
    ax1.XAxis.Limits = [0,max(allRegionPos)+1];
    ax1.XAxis.TickValues = allRegionPos;
    ax1.XAxis.TickLabels = tickNames;
    ax1.XAxis.Label.String = 'Species';
    ax1.LineWidth = 2;
    ax1.XAxis.TickLabelInterpreter = 'none';
    ax1.XAxis.TickLabelRotation = 90;
    curLeg = legend(ax1, legendNames);
    set(curLeg, 'Box', 'Off', 'Location', 'best', 'Interpreter','none');
    title(ax1, ...
        {info.filename; info.git_repos{1}.hash; ''}, ...
        'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
    ax1.Title.Visible = 'on';
    set(ax1, 'TickLength',ticklen,'LineWidth',2)
    Util.setPlotDefault(ax1,'','');
    daspect([4,1,1]) % manually
    
%     outfile = fullfile(outDir,sprintf('synapse_densities-%s',synapseType));
    set(gcf, 'Position', get(0, 'Screensize'));
%     saveas(gcf, strcat(outfile,'.png'));
%     if not(Util.isLocal)
%         export_fig(strcat(outfile,'.eps'),'-q101', '-nocrop', '-transparent');
%     end
%         close all
    allData{st} = out;
end

% text
mouseSph = reportDensities(allRegionNames, allData, synapseTypes, 'sph', 'mouse');
nhpSph = reportDensities(allRegionNames, allData, synapseTypes, 'sph', 'nhp');
humanSph = reportDensities(allRegionNames, allData, synapseTypes, 'sph', 'human');

x1 = mouseSph;
x2 = nhpSph;
[~,pVal] = kstest2(x1,x2);
Util.log('Mouse vs Nhp: Sph syn densities p=%e (factor %.2f)', pVal, mean(x1)/mean(x2))

x1 = mouseSph;
x2 = humanSph;
[~,pVal] = kstest2(x1,x2);
Util.log('Mouse vs Human: Sph syn densities p=%e (factor %.2f)', pVal, mean(x1)/mean(x2))

x1 = mouseSph;
x2 = cat(1, nhpSph,humanSph);
[~,pVal] = kstest2(x1,x2);
Util.log('Mouse vs NhpHuman: Sph syn densities p=%e (factor %.2f)', pVal, mean(x1)/mean(x2))

mouseSh = reportDensities(allRegionNames, allData, synapseTypes, 'shaft', 'mouse');
nhpSh = reportDensities(allRegionNames, allData, synapseTypes, 'shaft', 'nhp');
humanSh = reportDensities(allRegionNames, allData, synapseTypes, 'shaft', 'human');

x1 = mouseSh;
x2 = nhpSh;
[~,pVal] = kstest2(x1,x2);
Util.log('Mouse vs Nhp: Shaft syn densities p=%f', pVal)

x1 = mouseSh;
x2 = humanSh;
[~,pVal] = kstest2(x1,x2);
Util.log('Mouse vs Human: Shaft syn densities p=%f', pVal)

x1 = mouseSh;
x2 = cat(1, nhpSh,humanSh);
[~,pVal] = kstest2(x1,x2);
Util.log('Mouse vs NhpHuman: Shaft syn densities p=%f', pVal)

%% panel F
% Input synapse fractions of ExNs
clear; clc;
% dendrite input synapse types
info = Util.runInfo();
Util.showRunInfo(info);

allRegionNames = {'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO',...
            'mk_L23', 'Mk1_T2', ...
            'H5_10_L23', 'H6_4S_L23'};
            
mapColor = [1,0,1; ...   % magenta sph
            0,0,0; ...   % grey shaft
            1,0,1; ...   % magenta double
            0,0,0; ...   % grey stub
            0,0,0];      % grey neck
allFieldNames = {'sph','shaft','double','stub','neck'}; 

allData = struct;
allDataBulk = struct;
for idxRegion = 1:numel(allRegionNames)
    curRegion = allRegionNames{idxRegion};
    [curData, curDataBulk] = doForThisRegion(curRegion);

    fieldNames = {'sph','shaft','double','stub','neck'}; yLims = [0,1]; yTicks = 0:0.5:1;
    
    % restrict to specific fileds
    cdFN = fieldnames(curData);
    curData = rmfield(curData, cdFN(~ismember(cdFN, fieldNames)));
    curDataBulk = rmfield(curDataBulk, cdFN(~ismember(cdFN, fieldNames)));
    
    % remove empty data columns
    fieldNames = fieldnames(curData);
    idxDel = structfun(@isempty, curData);
    curData = rmfield(curData, fieldNames(idxDel));
    curDataBulk = rmfield(curDataBulk, fieldNames(idxDel));

    curTitle = sprintf('Input data for %s', curRegion);
    dataMatrix = plotInputData(curData, curDataBulk, fieldNames(~idxDel),...
        'binEdges',linspace(0,1,21), 'mapColor', mapColor(~idxDel,:),...
        'title', curTitle, 'info', info);

    % cosmetics
    ax = gca;
    ax.XAxis.TickLabels = fieldNames;
    ax.XAxis.TickLabelRotation = 90;
    ax.XAxis.TickLabelInterpreter = 'none';
    ax.YAxis.Limits = yLims;
    ax.YAxis.TickValues = yTicks;
    ax.YAxis.MinorTick = 'on';
    ax.Title.Interpreter = 'none';
%     outfile = fullfile(outDir, sprintf('dendrites_input_synapses-%s',curRegion));
%     saveas(gcf, strcat(outfile,'.png'));
%     if not(Util.isLocal)
%         export_fig(strcat(outfile,'.pdf'),'-q101', '-nocrop', '-transparent');
%     end
%     close
    % save
    allData.(curRegion) = curData;
    allDataBulk.(curRegion) = curDataBulk;
end

% plot all regions together
allFieldNames = {'sph','shaft','double','stub','neck'}; 
synapseTypes = {'main','sub','double','stub','neck'};

for st = 1:numel(synapseTypes)
    mapColor = [1,0,1; ...   % magenta sph
            0,0,0; ...   % grey shaft
            1,0,1; ...   % grey double
            0,0,0; ...   % grey stub
            0,0,0];      % grey neck
    synapseType = synapseTypes{st};
    switch synapseType
        case 'main'
            fieldNames = {'sph','shaft'}; yLims = [0,1]; yTicks = 0:0.5:yLims(end); 
            binEdges = linspace(0,yTicks(end),21); outfileName = sprintf('dendrites_input_synapses-allRegions-main');
            daratio = [];
            mapColor = mapColor(ismember(allFieldNames,fieldNames),:);
        case 'sub'
            fieldNames = {'double','stub','neck'};  yLims = [0,0.4]; yTicks = 0:0.05:yLims(end); 
            binEdges = linspace(0,yTicks(end),11); outfileName = sprintf('dendrites_input_synapses-allRegions-sub');
            daratio = [50,1,1];
            mapColor = mapColor(ismember(allFieldNames,fieldNames),:);
        case 'double'
            fieldNames = {'double'};  yLims = [0,0.4]; yTicks = 0:0.05:yLims(end); 
            binEdges = linspace(0,yTicks(end),11); outfileName = sprintf('dendrites_input_synapses-allRegions-double');
            daratio = [10,1,1]; %[100,1,1];
            mapColor = mapColor(ismember(allFieldNames,fieldNames),:);      
        case 'stub'
            fieldNames = {'stub'};  yLims = [0,0.2]; yTicks = 0:0.05:yLims(end); 
            binEdges = linspace(0,yTicks(end),11); outfileName = sprintf('dendrites_input_synapses-allRegions-stub');
            daratio = [20,1,1]; %[100,1,1];
            mapColor = mapColor(ismember(allFieldNames,fieldNames),:);
        case 'neck'
            fieldNames = {'neck'};  yLims = [0,0.1]; yTicks = 0:0.05:yLims(end); 
            binEdges = linspace(0,yTicks(end),11); outfileName = sprintf('dendrites_input_synapses-allRegions-neck');
            daratio = [40,1,1]; %[100,1,1];
            mapColor = mapColor(ismember(allFieldNames,fieldNames),:);
    end

    % combine all data to one struct
    combinedData = struct;
    combinedDataBulk = struct;
    for idxRegion = 1:numel(allRegionNames)
        curRegion = allRegionNames{idxRegion};
        curData = allData.(curRegion);
        curDataBulk = allDataBulk.(curRegion);

        cdFN = fieldnames(curData);
        curData = rmfield(curData, cdFN(~ismember(cdFN, fieldNames)));
        curDataBulk = rmfield(curDataBulk, cdFN(~ismember(cdFN, fieldNames)));
        cdFN = fieldnames(curData);

        curFieldNames = cellfun(@(x) sprintf('%s_%s',curRegion,x),cdFN, 'uni',0);
        for idxField=1:numel(curFieldNames)
            combinedData.(curFieldNames{idxField}) = curData.(cdFN{idxField});
            combinedDataBulk.(curFieldNames{idxField}) = curDataBulk.(cdFN{idxField});
        end
    end

    curData = combinedData;
    curDataBulk = combinedDataBulk;
    mapColor = repmat(mapColor, numel(allRegionNames),1);

    % remove empty data columns
    fieldNames = fieldnames(curData);
    idxDel = structfun(@isempty, curData);
    curData = rmfield(curData, fieldNames(idxDel));
    curDataBulk = rmfield(curDataBulk, fieldNames(idxDel));

    curTitle = sprintf('Input data for all regions');
    dataMatrix = plotInputData(curData, curDataBulk, fieldNames(~idxDel), 'binEdges',binEdges, ...
        'showNumbers', false, 'mapColor', mapColor(~idxDel,:), ...
        'title', curTitle, 'info', info);

    % cosmetics
    ax = gca;
    ax.XAxis.TickLabels = fieldNames;
    ax.XAxis.TickLabelRotation = 90;
    ax.XAxis.TickLabelInterpreter = 'none';
    ax.YAxis.Limits = yLims;
    ax.YAxis.TickValues = yTicks;
    ax.YAxis.MinorTick = 'on';
    ax.YAxis.Label.String = {'Fraction of synapses', sprintf('onto %s',synapseType)};
    ax.Title.Interpreter = 'none';
%     outfile = fullfile(outDir, outfileName);
    if not(isempty(daratio))
        daspect(ax,daratio)
    end
%     saveas(gcf, strcat(outfile,'.png'));
%     if not(Util.isLocal)
%         export_fig(strcat(outfile,'.pdf'),'-q101', '-nocrop', '-transparent');
%     end
%     close
end

%% panel G
% Fraction of stub, neck, double synapses
Figures.setConfig;

cx_path = fullfile('data','excel_files');
cx_data = readtable(fullfile(cx_path,'celltypes_classification_compiled_wk_links_v22_12_2021_v3_out.xlsx'));
cx_data(cellfun(@isempty, cx_data.Datasets),:) = []; % remove empty rows
cx_data.INFrac = cx_data.IN ./ (cx_data.Pyr_ExN_Stellate + cx_data.IN);

% compile dend data from all regions
cx_data_dend = Figures.Fig2.loadDendDataFromAllRegions;

cx_speciesTick = [0.8, 7, 10]; % for species center
cx_speciesTickWithLayers = [0.70,0.90, 6.90, 7.10, 9.90, 10.10, 8.5]; % L4 and L23 around centers
cx_species = {'Mouse','NHP','Human'};
         
doScatter = true;

% plot bulk IN % per species
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;

Util.log('Now plotting IN%:')
plotForSpeciesForSynapseG(cx_data, cx_speciesTickWithLayers, 'IN', 'k', 20, '-', 'full', doScatter);

% plot bulk Synapse % per species
Util.log('Now plotting Shaft/Spine+Shaft synapse:')
plotForSpeciesForSynapseG(cx_data_dend, cx_speciesTickWithLayers, 'shaft', [86,180,233]/255, 20, '--', 'restricted', doScatter)
Util.log('Now plotting Shaft synapse:')
plotForSpeciesForSynapseG(cx_data_dend, cx_speciesTickWithLayers, 'shaft', 'b', 20, '-', 'full', doScatter)

% save
ax.YAxis.Label.String = 'Fraction (%)';
ax.YAxis.Limits = [0,40];
ax.YAxis.TickValues = 0:5:100;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:1:100;
ax.XAxis.Limits = [0,15];
ax.XAxis.TickValues = cx_speciesTick;
ax.XAxis.TickLabels = cx_species;
ax.LineWidth = 2;
Util.setPlotDefault(gca,'','');
set(gca,'FontSize',18)

% plot bulk other targets % per species 
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;
Util.log('Now plotting Double synapse:')
plotForSpeciesForSynapseG(cx_data_dend, cx_speciesTickWithLayers, 'double', 'm', 10, '--', 'full', doScatter)
Util.log('Now plotting Stub synapse:')
plotForSpeciesForSynapseG(cx_data_dend, cx_speciesTickWithLayers, 'stub', [0.5,0.5,0.5], 10, '--', 'full', doScatter)

Util.log('Now plotting Neck synapse:')
plotForSpeciesForSynapseG(cx_data_dend, cx_speciesTickWithLayers, 'neck', 'k', 10, '--', 'full', doScatter)

ax.YAxis.Label.String = 'Fraction (%)';
ax.YAxis.Limits = [0,10];
ax.YAxis.TickValues = 0:5:100;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:1:100;
ax.XAxis.Limits = [0,15];
ax.XAxis.TickValues = cx_speciesTick;
ax.XAxis.TickLabels = cx_species;
ax.LineWidth = 2;
Util.setPlotDefault(gca,'','');
set(gca,'FontSize',18)

%% Utility functions panel G
function plotForSpeciesForSynapseG(cx_data, cx_speciesTickWithLayers, synapseType, color, markerSize, lineStyle, method, doScatter)
    Util.log('Doing for %s:', synapseType)
    
    mouseRegions = {'ex144','PPC_AK','ACC_JO','V2_L23','Mouse_A2'};
    nhpRegions = {'mk_L23','Mk1_T2'};
    humanRegions = {'H5_5_AMK','H6_4S_L23'};
    nhpHumanRegions = {'mk_L23','Mk1_T2', 'H5_5_AMK','H6_4S_L23'}; % {'MkL23','MkL23','Mk1_T2_STG', 'H5_5_AMK','H5 L23','H6_4S_L23'}
    
    if ~exist('method','var') || isempty(method)
            method = 'full';
    end
    if ~exist('doScatter','var') || isempty(doScatter)
            doScatter =  false;
    end
    if not(strcmp(synapseType, 'IN'))
        humanRegions = {'H5_10_L23','H6_4S_L23'};
        nhpHumanRegions = {'mk_L23','Mk1_T2', 'H5_10_L23','H6_4S_L23'}; % use H5_10_L23
        switch method        
            case 'full'
                cx_data.Pyr_ExN_Stellate = (cx_data.sph + cx_data.prim + ... 
                        cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck); % other synapses
            case 'restricted' % spine and shaft only
                cx_data.Pyr_ExN_Stellate = (cx_data.sph + cx_data.shaft); % other synapses
        end
        if strcmp(synapseType, 'double')
            cx_data.IN = cx_data.prim + cx_data.second;
        else
            cx_data.IN = cx_data.(synapseType);
        end
        % remove num from total
        cx_data.Pyr_ExN_Stellate = cx_data.Pyr_ExN_Stellate - cx_data.IN;
    end
    
    idx = contains(cx_data.Datasets, mouseRegions);
    [mouseMean, mouseCI, mouseB, mouseRatios] = plotForSpeciesG('mouse', cx_data, idx, cx_speciesTickWithLayers(2), color, 'x', markerSize);
    
    idx = contains(cx_data.Datasets,  nhpRegions); 
    [nhpMean, nhpCI, nhpB, nhpRatios] = plotForSpeciesG('nhp', cx_data, idx, cx_speciesTickWithLayers(5), color, 'x', markerSize, false);
    
    idx = contains(cx_data.Datasets,  humanRegions); 
    [humanMean, humanCI, humanB, humanRatios] = plotForSpeciesG('human', cx_data, idx, cx_speciesTickWithLayers(6), color, 'x', markerSize, false);
    
    idx = contains(cx_data.Datasets,  nhpHumanRegions); 
    [nhpHumanMean, nhpHumanCI, nhpHumanB, nhpHumanRatios] = plotForSpeciesG('nhpHuman', cx_data, idx, cx_speciesTickWithLayers(7), color, 'x', markerSize);
    
    plot([cx_speciesTickWithLayers(2), cx_speciesTickWithLayers(7)], [mouseMean, nhpHumanMean], lineStyle, 'Color',color, 'LineWidth',2)
    
%     % test human vs MQ
%     factor = humanMean/nhpMean;
%     pVal1 = sum(nhpB>=min(humanB))/numel(nhpB);
%     Util.log('Nhp vs Human: pVal = %e (test increase) (factor: %.2f)',pVal1, factor)
    
    % test
    factor = nhpHumanMean/mouseMean;
    pVal1 = sum(mouseB>=min(nhpHumanB))/numel(mouseB);
    Util.log('Mouse vs nhpHuman: pVal = %e (test increase using bootstrap) (factor: %.2f)',pVal1, factor)
    [~,pVal1] =  kstest2(mouseRatios, nhpHumanRatios);
    Util.log('Mouse vs nhpHuman: pVal = %e (k.s. on raw ratios) (factor: %.2f)',pVal1, factor)
    
    if doScatter
        regionsToPlot = cat(2, mouseRegions, nhpHumanRegions);
        [allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize] = getRegionCosmetics; 
        for idx = 1:numel(regionsToPlot)
            doForThisRegionG(cx_data, regionsToPlot{idx}, color, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
        end
    end
end

function [curMean, confidenceInterval, curBootstrap, ratios] = plotForSpeciesG(curRegion, cx_data, idx, curPos, curColor, curMarker, curMarkerSize, plotFlag)
    if ~exist('plotFlag','var') || isempty(plotFlag)
        plotFlag = true;
    end
    ratios = cx_data(idx,:).IN ./ (cx_data(idx,:).IN + cx_data(idx,:).Pyr_ExN_Stellate);
    curData.Pyr_ExN_Stellate = sum(cx_data(idx,:).Pyr_ExN_Stellate,1);
    curData.IN = sum(cx_data(idx,:).IN,1);
    [curMean, confidenceInterval, curN, curBootstrap] = boostrapForThisG(curData);
    confidenceInterval = 100*confidenceInterval;
    curMean = 100*curMean;

    Util.log('%s: Mean +- s.d. (CI: 10th - 90th):  %.2f +- %.2f(%.2f - %.2f) (N=%d)',...
        curRegion, curMean, std(curBootstrap), confidenceInterval(1), confidenceInterval(2), curN)
    
    if plotFlag
        plot([curPos, curPos], confidenceInterval, 'Color', curColor, 'LineStyle','-', 'LineWidth',2);
    end
    %d = curMean;    % bulk mean
    %plot(curPos,d, curMarker,'MarkerSize',curMarkerSize,...
    %   'MarkerFaceColor','none', 'MarkerEdgeColor',curColor);
end

function doForThisRegionG(cx_data, curRegion, curColor, ...
            allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
    curIdx = contains(allRegionNames, curRegion);
    curPos = allRegionNamesPos(curIdx);
    curMarker = allRegionNamesMarker{curIdx};
    curMarkerSize = allRegionNamesMarkerSize(curIdx);
        
    idx = contains(cx_data.Datasets, curRegion);
    cx_data = cx_data(idx,:);
%     [curMean, confidenceInterval, curN] = boostrapForThis(cx_data);
%     confidenceInterval = 100*confidenceInterval;
%     curMean = 100*curMean;
%     Util.log('%s: Mean (CI: 10th - 90th):  %.2f (%.2f - %.2f) (N=%d)', curRegion, curMean, confidenceInterval(1), confidenceInterval(2), curN)
%     plot([curPos, curPos], confidenceInterval, 'Color', curColor, 'LineStyle','-', 'LineWidth',1);
%     d = curMean;

    % datasets as scatter
    M = sum(cx_data.Pyr_ExN_Stellate,1); % bulk if more than one
    B = sum(cx_data.IN,1); % bulk if more than one
    d = 100*B./(B+M);
    plot(curPos,d, curMarker,'MarkerSize',curMarkerSize,...
        'MarkerFaceColor','none', 'MarkerEdgeColor',curColor);
end

function [curMean, confidenceInterval, N, bootstrapped] = boostrapForThisG(curData)
    rng(0)
    % MP:0, BP =1
    M = curData.Pyr_ExN_Stellate;
    B = curData.IN;

    N= B+M;

    isBP = false(1, B + M);
    isBP(1:B) = true;

    bootstrapped = nan(1000, 1);
    for curIdx = 1:numel(bootstrapped)
      bootstrapped(curIdx) = mean(datasample(isBP, numel(isBP), 'Replace', true));
    end

    confidenceInterval = prctile(bootstrapped, [10, 90]);
    curMean = mean(bootstrapped);
end

function [allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize,allRegionNamesText] = getRegionCosmetics()
    allRegionNames = {'ex145', 'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO', 'JO_A1',...
                        'mk_L4','mk_L23','Mk1_T2',...
                        'H5_10_L4', 'H5_10_L23','H5_5_AMK', 'H2_3','H6_4S_L23'};
    allRegionNamesText = {'S1 L4', 'S1 L23', 'A2', 'V2 L23', 'PPC L23', 'ACC L23', 'A1 L34',...
                        'MQ S1 L4', 'MQ S1 L23', 'MQ STG L23',...
                        'Hum ST L4', 'Hum ST L23','Hum ST L23', 'Hum MT L4', 'Hum FC'};
    allRegionNamesPos = [0.40, 0.5, 1, 1.50, 2, 2.5, 3, ...
                        6.8, 7, 7.5,...
                        9.7, 10, 10.1, 10.4, 10.5];
    allRegionNamesMarker = {'o','x','x','x','x','x','x',...
                        'o','x','x',...
                        'o','x','x','o','x'};
    allRegionNamesMarkerSize = [20,20,20,20,20,20,20,...
                        20,20,20,...
                        20,20,20,20,20];       
end


%% Utility functions panel F,G
function [out, bulk] = doForThisRegion(regionName)
    Figures.setConfig
    
    % load dendrite data
    cx_path_dend = config.cx_path_dend;
    distThr = config.distThr;
    cx_data_dend = readtable(cx_path_dend);
    cx_data_dend = cx_data_dend(cx_data_dend.distToCellbody>=distThr,:); % remove proximal
    cx_data_dend = cx_data_dend(contains(cx_data_dend.cellbodyType,'pyr'),:) ; % keep only pyr
    cx_data_dend = cx_data_dend(logical(cx_data_dend.cellbodyAttached),:); % keep cellbody attached
    cx_data_dend = cx_data_dend(~contains(cx_data_dend.name, {'apical','Apical'}),:); % remove apical, basal?
    cx_data_dend = cx_data_dend(~contains(cx_data_dend.name, {'bubbly'}),:); % remove bubbly
    
    curDendData = cx_data_dend;
    [sph, shaft, stub, neck, double]  = estimateTargetFractions(curDendData);
    out.sph = sph;
    out.shaft = shaft;
    out.double = double;
    out.stub = stub;
    out.neck = neck;    
    
    [sph, shaft, stub, neck, double] = estimateBulkTargetFractions(curDendData);
    bulk.sph = sph;
    bulk.shaft = shaft;
    bulk.double = double;
    bulk.stub = stub;
    bulk.neck = neck;
end

function [sph, shaft, stub, neck, double] = estimateTargetFractions(cx_data) 
    sph = cx_data.sph ./ (cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck);
    shaft = cx_data.shaft ./ (cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck);
    stub = (cx_data.stub) ./ (cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck);
    neck = (cx_data.neck) ./ (cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck);
    double = (cx_data.prim + cx_data.second) ./...
        (cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck);
end

function [sph, shaft, stub, neck, double] = estimateBulkTargetFractions(cx_data) 
    sph = sum(cx_data.sph) ./ sum((cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck));
    shaft = sum(cx_data.shaft) ./ sum((cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck));
    stub = sum(cx_data.stub) ./ sum((cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck));
    neck = sum(cx_data.neck) ./ sum((cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck));
    double = sum(cx_data.prim + cx_data.second) ./...
        sum((cx_data.sph + cx_data.prim + cx_data.shaft + cx_data.second + cx_data.stub + cx_data.neck));
end

function dataMatrix = plotInputData(data, dataBulk, fieldNames, varargin)
    opts = struct;
    opts.info = [];
    opts.title = [];
    opts.binEdges = linspace(0, 1, 11);
    opts.showNumbers = true;
    opts.mapColor = repmat([0,0,0],numel(fieldNames),1);
    opts.ylabelName = 'Fraction of synapses onto targets';
    opts = Util.modifyStruct(opts, varargin{:});

    dataMatrix = nan( ...
        numel(opts.binEdges) - 1, ...
        numel(fieldNames));
    
    for curIdx = 1:numel(fieldNames)
        curField = fieldNames{curIdx};
        curData = data.(curField);

        curFracs = curData;
        curBinCounts = histcounts(curFracs, opts.binEdges);
        dataMatrix(:, curIdx) = curBinCounts(:);
    end
    if exist('dataBulk','var') && ~isempty(dataBulk)
        dataBulkMatrix = struct2array(dataBulk);
    else
        dataBulkMatrix = [];
    end
    
   [fig, ax] = plotDataMatrix(dataMatrix, dataBulkMatrix, ...
        'binEdges', opts.binEdges, 'showZeros', false, ...
        'showNumbers', opts.showNumbers, 'mapColor', opts.mapColor, ...
        'ylabelName', opts.ylabelName);
    
    Figure.config(fig, opts.info);
    ax.Title.String{end + 1} = opts.title;
    fig.Position(3:4) = [320, 420];
end

function [fig, ax] = plotDataMatrix(data, dataBulk, varargin)
    opts = struct;
    opts.binEdges = [];
    opts.showNumbers = true;
    opts.showZeros = true;
    opts.numberFormat = '%.0f';
    opts.mapColor = repmat([0,0,0],size(data,2),1);
    opts.ylabelName = 'Fraction of synapses onto targets';
    opts.columnNames = num2cell(char( ...
        double('A') + (1:size(data, 2)) - 1));
    opts = Util.modifyStruct(opts, varargin{:});
    
    assert(size(data, 1) == numel(opts.binEdges) - 1);
    assert(size(data, 2) == numel(opts.columnNames));
    assert(size(data, 2) == size(opts.mapColor,1));
    
    fig = figure();
    fig.Color = 'white';
    ax = axes(fig);
    hold(ax, 'on');
    
    for curCol = 1:size(data, 2)
        curTotal = sum(data(:, curCol), 'omitnan');
        
        for curRow = 1:size(data, 1)
            curVal = data(curRow, curCol);
            
            curPos = opts.binEdges(curRow + [0, 1]);
            curPos = [curCol - 0.5, curPos(1), 1, diff(curPos)];
            curColor = 1 - curVal / curTotal;
            
            rectangle(...
                ax, 'Position', curPos, ...
                'FaceColor', cat(2, opts.mapColor(curCol,:), 1-curColor), ...
                'EdgeColor', 'none');
            
            curX = curPos(1) + curPos(3) / 2;
            curY = curPos(2) + curPos(4) / 2;

            if opts.showNumbers
                if curColor > 0.5; curColor = 0; else; curColor = 1; end

                curNumStr = num2str(curVal, opts.numberFormat);
                if opts.showZeros || not(strcmp(curNumStr, '0'))
                    text(...
                        ax, curX, curY, curNumStr, ...
                        'Color', repelem(curColor, 1, 3), ...
                        'HorizontalAlignment', 'center');
                end
            end
        end
        if exist('dataBulk','var') && ~isempty(dataBulk)
            % bulk
            plot(ax, [curX-0.5, curX+0.5], repelem(dataBulk(:,curCol),1,2),...
                '-k', 'MarkerSize',100,'LineWidth',2)
        end
    end
    
    daspect(ax, [size(data, 1), 1, 1]);
    xlim(ax, [0.5, size(data, 2) + 0.5]);
    ylim(ax, [0, 1]);
    
    xticks(ax, 1:size(data, 2));
    xticklabels(ax, opts.columnNames);
    
    ylabel(ax, opts.ylabelName);
    yticks(ax, opts.binEdges);
end



%% utlity functions panel E
function curData = reportDensities(allRegionNames, allData, synapseTypes, curSynType, curSpeciesType)
    switch curSpeciesType
        case 'mouse'
            idx = contains(allRegionNames, {'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO'});
        case 'nhp'
            idx = contains(allRegionNames,{'mk_L23', 'Mk1_T2'});
        case 'human'
            idx =  contains(allRegionNames,{'H5_10_L23', 'H6_4S_L23'});
    end
    curData = allData{contains(synapseTypes,curSynType)}; % cur synpase type data
    curData = curData(idx); % all regions for this species
    curData = cat(1,curData{:});
    Util.log('%s densities: %s :  %.2f +- %.2f per um (N=%d)', curSynType, curSpeciesType, mean(curData), std(curData), numel(curData))
end

function out = doForThisRegionE(ax1, synapseType, regionName, curPos, curMarker, curColor)
    Figures.setConfig
    
    % load dendrite data
    cx_path_dend = config.cx_path_dend;
    
    if ismember(synapseType, {'IN','BP','MP'})
        cx_data_dend = readtable(cx_path_dend);
        cx_data_dend = cx_data_dend(contains(cx_data_dend.cellbodyType,'IN'),:) ; % keep only IN
        cx_data_dend = cx_data_dend(logical(cx_data_dend.cellbodyAttached),:); % keep cellbody attached
        cx_data_dend = cx_data_dend(contains(cx_data_dend.name, synapseType),:); % keep IN or BP or MP
        out = cx_data_dend.shaft ./ cx_data_dend.pL;
        outBulk = sum(cx_data_dend.shaft,1) ./ sum(cx_data_dend.pL,1);
    elseif ismember(synapseType, {'sph-proximal','shaft-proximal','sph-proximal-apical','shaft-proximal-apical', 'sph-proximal-basal','shaft-proximal-basal'})
        distThr = config.distThr; % root close to soma % distThr = config.distThr;
        cx_data_dend = readtable(cx_path_dend);
        cx_data_dend = cx_data_dend(contains(cx_data_dend.cellbodyType,'pyr'),:) ; % keep only pyr
        cx_data_dend = cx_data_dend(logical(cx_data_dend.cellbodyAttached),:); % keep cellbody attached
        cx_data_dend = cx_data_dend(~contains(cx_data_dend.name, {'bubbly'}),:); % remove bubbly
        switch synapseType
            case 'sph-proximal'
                cx_data_dend(cx_data_dend.distToCellbody+cx_data_dend.pL<distThr,:);
                out = cx_data_dend.sph ./ cx_data_dend.pL;
                outBulk =  sum(cx_data_dend.sph,1) ./ sum(cx_data_dend.pL,1);   
            case 'shaft-proximal'
                cx_data_dend(cx_data_dend.distToCellbody+cx_data_dend.pL<distThr,:);
                out = cx_data_dend.shaft ./ cx_data_dend.pL;
                outBulk =  sum(cx_data_dend.shaft,1) ./ sum(cx_data_dend.pL,1);   
            case 'sph-proximal-apical'
                idx  = contains(cx_data_dend.name, {'apical_proximal'});  % keep apical before bifurcation
                cx_data_dend = cx_data_dend(idx,:);
                out = cx_data_dend.sph ./ cx_data_dend.pL;
                outBulk =  sum(cx_data_dend.sph,1) ./ sum(cx_data_dend.pL,1);   
            case 'shaft-proximal-apical'
                idx  = contains(cx_data_dend.name, {'apical_proximal'});  % keep apical before bifurcation
                cx_data_dend = cx_data_dend(idx,:);
                out = cx_data_dend.shaft ./ cx_data_dend.pL;
                outBulk =  sum(cx_data_dend.shaft,1) ./ sum(cx_data_dend.pL,1);   
            case 'sph-proximal-basal'
                cx_data_dend(cx_data_dend.distToCellbody+cx_data_dend.pL<distThr,:);
                idx = contains(cx_data_dend.name, {'basal_proximal'});
                cx_data_dend = cx_data_dend(idx,:);
                out = cx_data_dend.sph ./ cx_data_dend.pL;
                outBulk =  sum(cx_data_dend.sph,1) ./ sum(cx_data_dend.pL,1);   
            case 'shaft-proximal-basal'
                cx_data_dend(cx_data_dend.distToCellbody+cx_data_dend.pL<distThr,:);
                idx = contains(cx_data_dend.name, {'basal_proximal'});
                cx_data_dend = cx_data_dend(idx,:);
                out = cx_data_dend.shaft ./ cx_data_dend.pL;
                outBulk =  sum(cx_data_dend.shaft,1) ./ sum(cx_data_dend.pL,1);      
            otherwise
                error('Proximal synapse type not defined!')
        end
    else
        distThr = config.distThr;
        cx_data_dend = readtable(cx_path_dend);
        cx_data_dend = cx_data_dend(cx_data_dend.distToCellbody>=distThr,:); % remove proximal
        cx_data_dend = cx_data_dend(contains(cx_data_dend.cellbodyType,'pyr'),:) ; % keep only pyr
        cx_data_dend = cx_data_dend(logical(cx_data_dend.cellbodyAttached),:); % keep cellbody attached
        cx_data_dend = cx_data_dend(~contains(cx_data_dend.name, {'apical','Apical'}),:); % remove apical, basal?
        cx_data_dend = cx_data_dend(~contains(cx_data_dend.name, {'bubbly'}),:); % remove bubbly

        out = cx_data_dend.(synapseType) ./ cx_data_dend.pL;
        outBulk =  sum(cx_data_dend.(synapseType),1) ./ sum(cx_data_dend.pL,1);
    end
    
    scatter(ax1, repelem(curPos,numel(out),1), out,...
        75, curMarker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',curColor,'LineWidth',1);

    plot(ax1, [curPos-0.15, curPos+0.15], repelem(outBulk,1,2), '-k','LineWidth',4)
    Util.log('%s %s: Bulk: %.2f, Mean+-s.d.: %.2f +- %.2f (N=d)', ...
        synapseType, regionName, outBulk, mean(out), std(out), numel(out))
end



%% Utility functions panel B
function bootstrapped = plotForSpeciesB(curRegion, cx_data, idx, curPos, curColor, curMarker, curMarkerSize)

    curData.Pyr_ExN_Stellate = sum(cx_data(idx,:).Pyr_ExN_Stellate,1);
    curData.IN = sum(cx_data(idx,:).IN,1);
    [curMean, confidenceInterval, curN, bootstrapped] = boostrapForThis(curData);
    confidenceInterval = 100*confidenceInterval;
    curMean = 100*curMean;

    Util.log('%s: Mean (CI: 10th - 90th):  %.2f (%.2f - %.2f) (N=%d)', curRegion, curMean, confidenceInterval(1), confidenceInterval(2), curN)
    
    plot([curPos, curPos], confidenceInterval, 'Color', curColor, 'LineStyle','-', 'LineWidth',1);
    d = curMean;
    plot(curPos,d, curMarker,'MarkerSize',curMarkerSize,...
        'MarkerFaceColor','none', 'MarkerEdgeColor',curColor);

end
function doForThisRegionB(cx_data, curRegion, curColor, ...
            allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
    curIdx = contains(allRegionNames, curRegion);
    curPos = allRegionNamesPos(curIdx);
    curMarker = allRegionNamesMarker{curIdx};
    curMarkerSize = allRegionNamesMarkerSize(curIdx);
        
    idx = contains(cx_data.Datasets, curRegion);

    [curMean, confidenceInterval, curN] = boostrapForThis(cx_data(idx,:));
    confidenceInterval = 100*confidenceInterval;
    curMean = 100*curMean;

    Util.log('%s: Mean (CI: 10th - 90th):  %.2f (%.2f - %.2f) (N=%d)', curRegion, curMean, confidenceInterval(1), confidenceInterval(2), curN)

    plot([curPos, curPos], confidenceInterval, 'Color', curColor, 'LineStyle','-', 'LineWidth',1);
%   errorbar(curPos, curMean, curSEM, 'Color', curColor, 'LineStyle','-', 'LineWidth',1)
%   Util.log('%s: %.2f +- %.2f', curRegion, curMean, curSEM)
    
    %d = 100 * cx_data(idx,:).INFrac;
    d = curMean;
    plot(curPos,d, curMarker,'MarkerSize',curMarkerSize,...
        'MarkerFaceColor','none', 'MarkerEdgeColor',curColor);
end

function [curMean, confidenceInterval, N, bootstrapped] = boostrapForThis(curData)
    rng(0)
    % MP:0, BP =1
    M = curData.Pyr_ExN_Stellate;
    B = curData.IN;

    N= B+M;

    isBP = false(1, B + M);
    isBP(1:B) = true;

    bootstrapped = nan(1000, 1);
    for curIdx = 1:numel(bootstrapped)
      bootstrapped(curIdx) = mean(datasample(isBP, numel(isBP), 'Replace', true));
    end

    confidenceInterval = prctile(bootstrapped, [10, 90]);
    curMean = mean(bootstrapped);
%    curMean = mean(bp_fraction);
%    curSEM = Util.Stat.sem(bp_fraction); % std(bp_fraction) ./ sum(~isnan(bp_fraction));
end

