cx_data_dend_exc  = Figures.Proofread.loadDendDataFromAllRegions('pyr');
cx_data_dend_inh  = Figures.Proofread.loadDendDataFromAllRegions('IN');

mouseRegions = {'ex144','PPC_AK','ACC_JO','V2_L23','Mouse_A2'};
nhpHumanRegions = {'mk_L23','Mk1_T2', 'H5_10_L23','H6_4S_L23'};

%% pyr dendrites
cx_data = cx_data_dend_exc;
idx = contains(cx_data.Datasets, mouseRegions);
curData = cx_data(idx,:);
Util.log('Mouse Pyr seeded dendrites: %.2f, %.2f to %.2f (N=%d)',mean(curData.pL), min(curData.pL), max(curData.pL), height(curData))

idx = contains(cx_data.Datasets, nhpHumanRegions);
curData = cx_data(idx,:);
Util.log('Nhp/Human Pyr  seeded dendrites: %.2f, %.2f to %.2f (N=%d)',mean(curData.pL), min(curData.pL), max(curData.pL), height(curData))

%% IN dendrites
cx_data = cx_data_dend_inh;
idx = contains(cx_data.Datasets, mouseRegions);
curData = cx_data(idx,:);
Util.log('Mouse IN seeded dendrites: %.2f, %.2f to %.2f (N=%d)',mean(curData.pL), min(curData.pL), max(curData.pL), height(curData))

idx = contains(cx_data.Datasets, nhpHumanRegions);
curData = cx_data(idx,:);
Util.log('Nhp/Human IN seeded dendrites: %.2f, %.2f to %.2f (N=%d)',mean(curData.pL), min(curData.pL), max(curData.pL), height(curData))

%% IN + Pyr
cx_data = cat(1,cx_data_dend_exc,cx_data_dend_inh);
idx = contains(cx_data.Datasets, mouseRegions);
curData = cx_data(idx,:);
Util.log('Mouse Pyr+IN seeded dendrites: %.2f, %.2f to %.2f (N=%d)',mean(curData.pL), min(curData.pL), max(curData.pL), height(curData))

idx = contains(cx_data.Datasets, nhpHumanRegions);
curData = cx_data(idx,:);
Util.log('Nhp/Human Pyr+IN seeded dendrites: %.2f, %.2f to %.2f (N=%d)',mean(curData.pL), min(curData.pL), max(curData.pL), height(curData))