function cx_data_dend_all = loadDendDataFromAllRegions(cellType)
    if ~exist('cellType','var') || isempty(cellType)
        cellType = 'pyr';
    end

    allRegionNames = {'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO',...
                'mk_L23', 'Mk1_T2', ...
                'H5_10_L23', 'H6_4S_L23'};
    cx_data_dend_all = table;        
    for i=1:numel(allRegionNames)
        curRegion = allRegionNames{i};
        cx_data_dend = doForThisRegion(curRegion, cellType);
        col1 = repelem({curRegion},height(cx_data_dend),1);
        col1 = table(col1,'VariableNames',{'Datasets'});
        cx_data_dend = [col1, cx_data_dend];
        cx_data_dend_all = cat(1,cx_data_dend_all, cx_data_dend);
        clear cx_data_dend
    end
end
function cx_data_dend = doForThisRegion(regionName, cellType)
    Figures.setConfig
    
    % load dendrite data
    cx_path_dend = config.cx_path_dend;
    distThr = config.distThr;
    cx_data_dend = readtable(cx_path_dend);
    
    switch cellType
        case 'pyr'
            cx_data_dend = cx_data_dend(cx_data_dend.distToCellbody>=distThr,:); % remove proximal
            cx_data_dend = cx_data_dend(contains(cx_data_dend.cellbodyType,'pyr'),:) ; % keep only pyr
            cx_data_dend = cx_data_dend(logical(cx_data_dend.cellbodyAttached),:); % keep cellbody attached
            cx_data_dend = cx_data_dend(~contains(cx_data_dend.name, {'apical','Apical'}),:); % remove apical, basal?
            cx_data_dend = cx_data_dend(~contains(cx_data_dend.name, {'bubbly'}),:); % remove bubbly
        case 'IN'
            cx_data_dend = cx_data_dend(contains(cx_data_dend.cellbodyType,'IN'),:) ; % keep only IN
    end
end