% soma for pyr vs
clear 

config = struct;

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'ACC_JO';
doAxons = false;

Figures.setConfig; % set mainFolder, datasetName, ownerName

tracingFolder = fullfile(mainFolder,'soma');
outDir = fullfile(mainFolder, 'results', 'soma');
if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'soma_all_input_syn*.nml'));
func = @(x) str2double(x.id);

%assert(numel(files) == 1)
% just one nml, can be multiple for H5_10_L23

% do per soma
Util.log('Now extracting...')
nml = fullfile(tracingFolder, files(1).name);
skelAll = skeleton(nml);
skelAll = skelAll.deleteEmptyTrees;

if numel(files)>1
    Util.log('Found %d Soma files. Merging!', numel(files))
    for i=2:numel(files)
        curSkel = skeleton(fullfile(tracingFolder, files(i).name));
        skelAll = skelAll.mergeSkels(curSkel);
    end
end
config.scaleEM = skelAll.scale;

% soma syn statistics
groupIds = skelAll.groups.id;
groupNames = skelAll.groups.name;

for i = 1:numel(groupIds)
    curGroupId = groupIds(i);
    curGroupName = groupNames{i};
    idxKeep = skelAll.groupId == curGroupId;
    curSkel = skelAll.deleteTrees(idxKeep, true); % remove all rest

    somaData = extractSynComments(curSkel, curGroupName);
    outData(i).soma = somaData;

    sprintf('%s - multiplicity = %.2f +- %.2f (n=%d)', curGroupName, somaData.mu, somaData.sd, somaData.N)
    clear somaData
end
disp(vertcat(outData.soma))

outTable = struct2table(outData);
Util.save(fullfile(outDir, sprintf('somaData-%s.mat', regionName)), outTable);

mkdir( fullfile(outDir, 'data'))
writetable(vertcat(outData.soma), fullfile(outDir, 'data', sprintf('somaData-InputSyns-%s.xlsx', regionName)));

%{
% plot histogram
Util.log('Plotting histogram:')
fig = figure();
fig.Color = 'white';
ax = axes(fig);
hold on
for i=1:numel(outData)
    aisData = outData(i).ais;
    x = aisData.synDist{:};
    switch aisData.type
        case 'pyr'
            curColor = 'r';
        case 'IN'
            curColor = 'g';
    end
    
    if i~=1
        curAxPos = ax.Position;
        curAxPos(1) = curAxPos(1)+0.01*rand(1);
        curAx = axes('Position', curAxPos, 'color','none');
    else
        curAx = ax;
    end
    hold on
    histogram(x, 'DisplayStyle','stairs', 'Binwidth',5,'EdgeColor', curColor, ...
         'LineWidth', 2, 'Parent', curAx);
    curAx.YAxis.TickValues = 0:2:20; % manually
    curAx.YAxis.Limits = [0,20]; % manually
    curAx.XAxis.TickValues = 0:20:100; % manually
    curAx.XAxis.Limits = [0,100]; % manually

    if i~=1
        set(gca,'xcolor','none', 'ycolor','none')
    end
    Util.setPlotDefault(gca,'','');
end
ax.XAxis.Label.String = 'Distance from soma(um)';
ax.YAxis.Label.String = 'Frequency of input synapses)';
ax.LineWidth = 2;
export_fig(fullfile(outDir, 'somaData-hist-synDistances.png'),'-q101', '-nocrop','-transparent', '-m8');
close all
%}
function out = extractSynComments(skel, groupName)
    c = skel.getAllComments;
    c(cellfun(@isempty, c)) = []; 

    out = table;
    out.name = {groupName};
    out.total = sum(contains(c,'pre'));
    idxMultiHit = cellfun(@(x) any(regexp(x,'pre(\s)*\d+')),c);
    out.multiHit = sum(idxMultiHit);
    out.multiHitFrac = out.multiHit / out.total; 


    % multiplicity per axon
    commentsMulti = c(idxMultiHit);
    countMulti = [];
    for i = 1:10
        curCount = sum(cellfun(@(x) any(regexp(x,['pre(\s)*',sprintf('%d',i)])),commentsMulti));
        countMulti(i) = curCount;
        clear curCount
    end 
    out.countMulti = countMulti;   
    out.mu  = mean(nonzeros(countMulti));
    out.sd = std(nonzeros(countMulti));
    out.N = numel(nonzeros(countMulti));
end 
