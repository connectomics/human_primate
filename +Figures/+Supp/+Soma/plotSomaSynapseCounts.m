% soma inputs snys box plot and histogram
cellType = 'pyr';
% load saved data from parsed nmls in matfiles per region

info = Util.runInfo();
Util.showRunInfo(info);

Figures.setConfig
allColors = Util.getSomaColors(true);

fig = figure;
fig.Color = 'white';

Util.log('Plotting soma data:')
ax1 = gca; hold on;

legendNames = {'ex144', 'ACC_JO','mk_L23','H5_10_L23', 'H6_4S_L23'};
allRegionPos = [1,1.5,3, 4,4.5];
allRegionMarkers = {'o','o','o','o','o'};
tickNames = legendNames;

i=1; mouseS1=doForThisRegion(ax1, legendNames{i}, allRegionPos(i), allRegionMarkers{i},'k'); legendNames{i} = sprintf('%s (N=%d)',legendNames{i},numel(mouseS1));
i=2; mouseACC=doForThisRegion(ax1, legendNames{i}, allRegionPos(i), allRegionMarkers{i},'k'); legendNames{i} = sprintf('%s (N=%d)',legendNames{i},numel(mouseACC));
i=3; macaqueS1=doForThisRegion(ax1, legendNames{i}, allRegionPos(i), allRegionMarkers{i},'k'); legendNames{i} = sprintf('%s (N=%d)',legendNames{i},numel(macaqueS1));
i=4; humanSTG=doForThisRegion(ax1, legendNames{i}, allRegionPos(i), allRegionMarkers{i},'k'); legendNames{i} = sprintf('%s (N=%d)',legendNames{i},numel(humanSTG));
i=5; humanIFC=doForThisRegion(ax1, legendNames{i}, allRegionPos(i), allRegionMarkers{i},'k'); legendNames{i} = sprintf('%s (N=%d)',legendNames{i},numel(humanIFC));

c = cellfun(@(x) {sprintf('%s',x), ''}, legendNames, 'UniformOutput', false); % duplicate for median lines
legendNames = cat(2, c{:});

%% test
mouse = cat(1, mouseS1, mouseACC);
nhpHuman = cat(1, macaqueS1, humanSTG, humanIFC);
factor = mean(mouse) / mean(nhpHuman);
[~, pValue] = kstest2(mouse, nhpHuman);

Util.log('Mouse: %d somata. Input synapses mean +/- s.d. = %.0f +- %.0f', numel(mouse), mean(mouse), std(mouse))
Util.log('NhpHuman: %d somata. Input synapses mean +/- s.d. = %.0f +- %.0f', numel(nhpHuman), mean(nhpHuman), std(nhpHuman))
Util.log('Mouse to Human: factor = %.2f (pValue = %e, k.s. test)', factor, pValue)


%% cosmetics
ticklen = [0.025, 0.025];
ax1.YAxis.TickValues = 0:20:160;
ax1.YAxis.Limits = [0,160];
ax1.YAxis.Label.String = 'Soma synapses';
ax1.XAxis.Limits = [0,max(allRegionPos)+1];
ax1.XAxis.TickValues = allRegionPos;
ax1.XAxis.TickLabels = tickNames;
ax1.XAxis.Label.String = 'Species';
ax1.LineWidth = 2;
ax1.XAxis.TickLabelInterpreter = 'none';
ax1.XAxis.TickLabelRotation = 90;
curLeg = legend(ax1, legendNames);
set(curLeg, 'Box', 'Off', 'Location', 'best', 'Interpreter','none');
title(ax1, ...
    {info.filename; info.git_repos{1}.hash; ''}, ...
    'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
ax1.Title.Visible = 'on';
set(ax1, 'TickLength',ticklen)
Util.setPlotDefault(ax1,'','');

outfile = fullfile(outputDir,'figures','fig4',sprintf('panel_soma_synapse_counts-%s',cellType));
% saveas(gcf, strcat(outfile,'.png'));
% export_fig(strcat(outfile,'.eps'), '-q101', '-nocrop', '-transparent', '-m8');
% close all

function out = doForThisRegion(ax1,regionName, curPos, curMarker, curColor)
    Figures.setConfig;

    outDir = fullfile(mainFolder, 'results', 'soma');
    m = load(fullfile(outDir, sprintf('somaData-%s.mat', regionName)));
    outTable = vertcat(m.outTable.soma);
    total = outTable.total; % input syns
    scatter(ax1, repelem(curPos,numel(total),1), total,...
        75, curMarker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',curColor);
    plot(ax1, [curPos-0.15, curPos+0.15], repelem(median(total),1,2), '-k','MarkerSize',100,'LineWidth',2)
    
    % export
    out = total;
end
