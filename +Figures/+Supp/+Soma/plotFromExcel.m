% plot soma/ais reinnervation figures from excel data

t = readtable('C:\Users\loombas\Downloads\somaAxonsDataPooled-H5_10_L4_v01.xlsx');

synThr = 4;
Util.log('Exclude axons with low N syns %d',synThr)
t(t.total<synThr,:) = [];

somaSpec = t.soma ./ t.total;
pL = t.pL;
synTotal = t.total;
synDensity = (synTotal+1) ./pL;

fig = figure;
ax = subplot(1,4,1);
histogram(somaSpec,'BinWidth',0.02)
ax.YAxis.Limits = [0,20];
ax.YAxis.TickValues = 0:2:20;
ax.XAxis.Limits = [0,1];
ax.XAxis.TickValues = 0:0.1:1;
Util.setPlotDefault(ax)
xlabel(sprintf('Fraction of soma reinnervation \n by soma-seeded axons'))
ylabel('# of axons')


ax = subplot(1,4,2);
bw = 2; % 10 (log scale)
histogram(synTotal,'BinWidth',bw )
ax.YAxis.Limits = [0,20];
ax.YAxis.TickValues = 0:2:20;
% ax.XAxis.Limits = [0,1e3];
ax.XAxis.Limits = [0,30];
Util.setPlotDefault(ax)
% xlabel('Number of synapses (log)')
xlabel('Number of synapses')
ylabel('# of axons')
% set(gca,'Xscale','log')

ax = subplot(1,4,3);
plot(synDensity, pL, 'o')
% ax.YAxis.Limits = [0,8000];
% ax.YAxis.TickValues = 0:1000:8000;
ax.YAxis.Limits = [0,500];
ax.YAxis.TickValues = 0:50:500;
ax.XAxis.Limits = [0,0.5];
Util.setPlotDefault(ax)
xlabel('Number of synapses per \mum')
ylabel('Path length (\mum)')
% set(gca,'Yscale','log')

ax = subplot(1,4,4);
plot(synTotal, somaSpec, 'o')
ax.YAxis.Limits = [0,1];
ax.YAxis.TickValues = 0:0.1:1;
% ax.XAxis.Limits = [0,1e3];
ax.XAxis.Limits = [0,30];
Util.setPlotDefault(ax)
xlabel('Number of synapses')
ylabel(sprintf('Fraction of soma reinnervation \n by soma-seeded axons'))

suptitle('H5-L4 soma seeded axons')