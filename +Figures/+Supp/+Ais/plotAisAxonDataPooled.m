% Use tracings on axons inputting to ais to get their output profile
% pooled over all AIS seeded axons
clear 

config = struct;

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'mk_L4';
Figures.setConfig; % set mainFolder, datasetName, ownerName

doAxons = true;

tracingFolder = fullfile(mainFolder,'ais');
outDir = fullfile(mainFolder, 'results', 'ais');
if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'ais_with_input_axons*.nml'));
func = @(x) str2double(x.id);

assert(numel(files) == 1) % just one nml

Util.log('Now extracting...')
nml = fullfile(tracingFolder, files(1).name);
skelAll = skeleton(nml);
config.scaleEM = skelAll.scale;

% axon statistics    
axonTreeIds = contains(skelAll.names, 'axon');
axonData = table;
if doAxons
    skelAxons = skelAll.deleteTrees(axonTreeIds, true); % remove all rest
    for curTree = 1:skelAxons.numTrees
        out = extractSynComments(skelAxons, curTree, 'axon', false);
        axonData = [axonData;out];
        clear out
    end
end
% sort tree names
[~,idxSort] = sort(axonData.name);
axonData = axonData(idxSort,:);

outData = struct;
outData(1).axon = axonData;
outData(1).name = 'pooled';
Util.save(fullfile(outDir, sprintf('aisAxonDataPooled-%s.mat', regionName)), outData);

% export to excel
outTable = outData.axon;
writetable(outTable, fullfile(outDir, 'data', sprintf('aisAxonsDataPooled-%s.xlsx', regionName)));

% num AIS seeded
c = skelAxons.getAllComments;
idxSeed = contains(c,'seed ais');
aisSeeded = unique(c(idxSeed));
numAisSeeded = numel(unique(c(idxSeed)));

%% plot axon profiles
rng(0)
Util.log('Now plotting per dendrite:')
targetNames = {'sph','prim','sec','shaft','neck','stub', 'soma', 'AIS', 'ter'};
thrNumSyns = 4;
Util.log('Doing for axons >%d synapses only!', thrNumSyns)
idxStart = 2;
idxEnd = 10;
for i=1:numel(outData)
    fig = figure;
    fig.Color = 'white';
    ax = axes(fig); hold on
    axonTable = outData(i).axon;

    % remove those axons below thr # of syns
    idxDel = axonTable.total < thrNumSyns;
    axonTable(idxDel,:) = [];

%    assert(all(idxExc|idxInh))
    curAxonsData = axonTable(:,idxStart:idxEnd);
    Util.plotAxonProfile(curAxonsData, ax, targetNames);
    
    ax.YAxis(1).Label.String = 'Fractional innervation by other synapses';
    ax.XAxis.Label.String = sprintf('Post synaptic targets');
    ax.Title.String = sprintf('N=%d Ais seeded N=%d axons', numAisSeeded, size(curAxonsData,1));
    ax.XAxis.FontSize = 8;
    outfile = fullfile(outDir,[outData(i).name '.png']);
    export_fig(outfile, '-q101', '-nocrop', '-transparent', '-m8');
end

%% plot fraction of AIS synapses per axon
Util.log('Doing for axons >%d synapses only!', thrNumSyns)
idxStart = 2;
idxEnd = 10;
for i=1:numel(outData)
    fig = figure;
    fig.Color = 'white';
    ax = axes(fig); hold on
    axonTable = outData(i).axon;

    % remove those axons below thr # of syns
    idxDel = axonTable.total < thrNumSyns;
    axonTable(idxDel,:) = [];

%    assert(all(idxExc|idxInh))
    curAxonsData = axonTable(:,idxStart:idxEnd);
    x = axonTable.total ./ axonTable.pL;
    y = axonTable.ais ./ axonTable.total;
    scatter(x,y,100,'k','x');
    ax.YAxis.Limits = [0,1];
    ax.YAxis.TickValues = 0:0.1:1;
    ax.XAxis.Scale = 'log';
    ax.XAxis.Limits = [0,1];
    ax.XAxis.MinorTick = 'on';
    ax.XAxis.MinorTickValues = [0:0.01:0.1, 0.2:0.1:1]
    ax.XAxis.TickValues = [0,0.1,1];
    ylabel('Fractional re-innervation of AIS')
    xlabel('Synapse density per \mum')
    ax.Title.String = sprintf('N=%d Ais seeded N=%d axons', numAisSeeded, size(curAxonsData,1));
    ax.XAxis.FontSize = 8;
    ax.LineWidth = 2;
    Util.setPlotDefault(gca,'','');
    outfile = fullfile(outDir,[outData(i).name '_fraction_AIS_syn.png']);
    export_fig(outfile, '-q101', '-nocrop', '-transparent', '-m8');
end

%% identify Ch axons
aisFraction = axonTable.ais ./ axonTable.total;
thrChAxons = 0.5; % manually
idxChAxons = aisFraction > thrChAxons;
chAxonNames = axonTable.name(idxChAxons);

%% load ais data and plot frac of AIS input syn from axo-axonic axon
treeNames = skelAll.names;
fracAisInput = [];
for i=1:numel(aisSeeded)
    % num input syns to AIS
    curAis = aisSeeded{i};
    t = regexp(curAis, 'ais (?<name>\w+)','names');
    curAisName = t.name;
    idxTree = contains(treeNames,curAisName(2:end)) & ~contains(treeNames,'axon');
    curSkelAis = skelAll.deleteTrees(idxTree, true);
    c = curSkelAis.getAllComments;
    numSynInput = sum(cellfun(@(x) any(regexpi(x,'^pre')),c)); % input syns to AIS

    % num input syns from ChAxons
    idxTree = skelAll.getTreeWithName(chAxonNames,'exact');
    curSkelCh = skelAll.deleteTrees(idxTree, true);
    c = curSkelCh.getAllComments;
    numSynChInput = sum(cellfun(@(x) any(regexpi(x,['ais ' curAisName])),c));

    fracAisInput(i) = numSynChInput / numSynInput;
    totalAisInput(i) = numSynInput;
    sprintf('%f input from ChAxons out of total %d syns', fracAisInput(i), numSynInput)
end

fig = figure;
fig.Color = 'white';
ax = axes(fig); hold on
g = {'1'};
x = fracAisInput;
widthS = 2; startS = 1;
[pos, posS] = Util.getBoxPlotPos(numel(unique(g)), startS);
scatter(widthS.*rand(numel(x),1)+posS(1), x,...
    200,'x','filled', 'MarkerFaceColor', 'k','MarkerEdgeColor', 'k');
h = boxplot(x,g,'Colors','k','Widths',2,'Positions',pos,'Symbol','r+');
set(h,{'linew'},{2})
ax.YAxis.Limits = [0,1];
ax.YAxis.TickValues = 0:0.1:1;
set(gca,'xcolor','none')
ylabel(sprintf('Fraction of AIS input syns\n from axo-axonic axons'))
ax.Title.String = sprintf('N=%d Ais seeded N=%d axons', numAisSeeded, size(curAxonsData,1));
ax.XAxis.FontSize = 8;
ax.LineWidth = 2;
Util.setPlotDefault(gca,'','');
outfile = fullfile(outDir,[outData(1).name '_fraction_AIS_syn_input.png']);
daspect([8,1,1])
export_fig(outfile, '-q101', '-nocrop', '-transparent', '-m8');
close all

function out = extractSynComments(skel, curTree, tracingType, plotDendrogram, outDir)
    c = {skel.nodesAsStruct{curTree}.comment};
    c(cellfun(@isempty, c)) = []; 

    % remove seed synapses
    idxSeed = cellfun(@(x) any(regexpi(x,'^seed')),c);
    c(idxSeed) = [];

    out = table;
    out.name = skel.names(curTree);
    out.sph = sum(cellfun(@(x) any(regexpi(x,'sph')),c));
    out.prim  =  sum(cellfun(@(x) any(regexpi(x,'rim')),c));
    out.second =  sum(cellfun(@(x) any(regexpi(x,'sec')),c));
    out.shaft = sum(cellfun(@(x) any(regexpi(x,'sh')),c));
    out.neck = sum(cellfun(@(x) any(regexpi(x,'neck')),c));
    out.stub =  sum(cellfun(@(x) any(regexpi(x,'stub')),c));
    out.soma =  sum(cellfun(@(x) any(regexpi(x,'soma')),c));
    out.ais = sum(cellfun(@(x) any(regexpi(x,'ais')),c));
    out.ter = sum(cellfun(@(x) any(regexpi(x,'ter')),c));
    out.total = out.sph + out.prim + out.second + out.shaft + out.neck + ...
                out.stub + out.soma + out.ais + out.ter;
 
    out.excFraction = (out.sph + out.prim) ./ out.total;
    switch tracingType
         case 'axon'
            [~,idxDel] = Util.getNodeCoordsWithComment(skel, {'post'},'partial', curTree);
            skel = skel.deleteNodes(curTree,idxDel,true);
            out.pL = skel.pathLength(curTree)./1e3; %um
 
         case 'dendrite'
            startIdx = skel.getNodesWithComment('_root',1,'exact');
            endIdx  =  skel.getNodesWithComment('_end',1,'exact');
 
            startId = str2double(skel.nodesAsStruct{1}(startIdx).id);
            endId = str2double(skel.nodesAsStruct{1}(endIdx).id);
   
            [~, ~, out.pL] = skel.getShortestPath(startId, endId); %nm
            out.pL = out.pL./1e3;
     end
     out.synD = out.total./ out.pL; 

    if plotDendrogram
        % delete pre nodes comment
        skel = skel.deleteNodesWithComment('pre','partial');
        synSize = 100; tubeSize = 1; dotSize= 3;
        synColors = {[1,0,0],... % red shaft
                     [0,1,0],... % green sph
                     [1,0,1],... % blue stub 
                     [230,159,0]./255,... % orange prim
                     [240,228,66]./255,... %Yellow second
                     [204,121,167]./255,... % reddish purple ter
                     [0,114,178]./255}; % bluish neck

        % make axonogram (PLASS-like)
        startpoint = skel.getNodesWithComment('_root',1,'regexp');
        endpoint  =  skel.getNodesWithComment('_end',1,'regexp');
    
        % attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
        comments = {skel.nodesAsStruct{1}.comment}';
        assert(startpoint == find(contains(comments,'_root')))
        
        tic;
        config = struct;
        config.scaleEM = skel.scale;
        Util.log('Now scatterting for %s',skel.names{1})
        [TREE, f] = Axonogram.axonogram(skel,'',startpoint,config.scaleEM, true);
        xlabel('Path length from root (um)')
        ylabel('')
        synComments = {'sh', 'sph', 'stub', 'prim', 'sec', 'ter', 'neck'};
        for i=1:numel(synComments)
            curSynLocs = find(cellfun(@(x) any(regexp(x,synComments{i})),comments));
            curSynColor = synColors{i};
            if ~isempty(curSynLocs)
                XY = Axonogram.findPointsInAxonogram(curSynLocs, TREE, skel, config.scaleEM);
                gcf, scatter(XY(:,1),XY(:,2),synSize, 'o','MarkerEdgeColor',curSynColor, 'MarkerFaceColor',curSynColor);
                clear XY
            end
        end

        % cosmetics
        box off
        camroll(90);
        set(gca,'linewidth',2, 'color', 'white');
        title( skel.names{curTree}, 'Interpreter', 'none')
        outfile = fullfile(outDir, ...
            sprintf('%s.png', skel.names{curTree}));
        export_fig(outfile,'-q101', '-nocrop', '-transparent');
        Util.log('Saving file %s', outfile);
        close(f);
        toc;
    end

end 
