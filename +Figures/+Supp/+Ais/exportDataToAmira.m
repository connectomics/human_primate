% export data to amira to plot ais with input syns

outputDir = '/u/sahilloo/data/primate_and_human/outData/';

Util.log('Loading data from matfiles saved:')

regionName = 'mk_L4';
mk_L4_ais = doForThisRegion(regionName);

regionName = 'mk_L23';
mk_L23_ais = doForThisRegion(regionName);

regionName = 'H5_10_L4';
H5_10_L4_ais = doForThisRegion(regionName);

regionName = 'H5_10_L23';
H5_10_L23_ais = doForThisRegion(regionName);

% write excel
outDataAis.mk_L4 = mk_L4_ais;
outDataAis.mk_L23 = mk_L23_ais;
outDataAis.H5_10_L4 = H5_10_L4_ais;
outDataAis.H5_10_L23 = H5_10_L23_ais;

Util.saveStruct(fullfile(outputDir,'figures','fig4','amira',sprintf('data_ais_input_numbers_all_regions.mat')), outDataAis)

function outDataAis = doForThisRegion(regionName)
    Figures.setConfig;

    outDir = fullfile(mainFolder, 'results', 'ais');
    m = load(fullfile(outDir, sprintf('aisData-%s.mat', regionName)));
    outData = m.outData;

    outDataAis = rmfield(outData,'ais');    
end


