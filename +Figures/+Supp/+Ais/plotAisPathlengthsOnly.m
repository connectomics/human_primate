% ais pathlengths only. We dont know about the input 'pre' here
clear 

config = struct;
version = datestr(now,'yyyy-mm-dd'); % hiwi tasks version

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'mk_L4';
doAxons = false;

Figures.setConfig; % set mainFolder, datasetName, ownerName

tracingFolder = fullfile(mainFolder,'ais');
outDir = fullfile(mainFolder, 'results', 'ais');
if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'ais_with_input_axons*.nml')); % use same tracing for ais and ais_with_axons
func = @(x) str2double(x.id);

assert(numel(files) == 1) % just one nml
% do per ais
Util.log('Now extracting...')
nml = fullfile(tracingFolder, files(1).name);
skelAll = skeleton(nml);
% removing axons and others
treeNames = skelAll.names;
idxKeep =  contains(treeNames, 'ais') & ~contains(treeNames, 'axon');
skelAll = skelAll.deleteTrees(idxKeep,true);
config.scaleEM = skelAll.scale;

% ais statistics
for curTree = 1:skelAll.numTrees
    curSkel = skelAll.deleteTrees(curTree, true); % remove all rest
    [aisData, nodesPre, nodeCellbody] = extractSynComments(curSkel, 1, false, outDir);
    outData(curTree).ais = aisData;
    outData(curTree).skel = curSkel;
    outData(curTree).nodesPre = nodesPre;
    outData(curTree).nodeCellbody = nodeCellbody;
    clear aisData nodesPre nodeCellbody
end
Util.save(fullfile(outDir, sprintf('aisDataPathlengths-%s.mat', regionName)), outData);

% export to excel
outTable = table;
for i=1:numel(outData)
    if any(contains(outData(i).ais.Properties.VariableNames,'pL'))
        outTable = [outTable; outData(i).ais(:,1:3)];% only export pl data here
    end
end
[~,idxSort] = sort(outTable.name);
outTable = outTable(idxSort,:);
writetable(outTable, fullfile(outDir, 'data', sprintf('aisDataPathlengths-%s.xlsx', regionName)));

function [out, nodesPre, nodeCellbody] = extractSynComments(skel, curTree, plotDendrogram, outDir)
    c = {skel.nodesAsStruct{curTree}.comment};
    c(cellfun(@isempty, c)) = []; 
    nodesPre = [];
    nodeCellbody= [];
    out = table;
    out.name = skel.names(curTree);
    if regexpi(out.name{1},'pyr')
        out.type = {'pyr'};
    elseif regexpi(out.name{1},'IN')
        out.type = {'IN'};
    else
        out.type = {'UC'};
    end
    
    startIdx = skel.getNodesWithComment('_root',1,'exact');
    endIdx  =  skel.getNodesWithComment('_end',1,'exact');
 
    if isempty(startIdx) | isempty(endIdx)
        warning('No start-end found in %s', skel.names{1})
        return
    end
    startId = str2double(skel.nodesAsStruct{curTree}(startIdx).id);
    endId = str2double(skel.nodesAsStruct{curTree}(endIdx).id);
   
    [~, ~, out.pL] = skel.getShortestPath(startId, endId); %nm
    out.pL = out.pL./1e3;
    out.total = sum(contains(c,'pre'));
    out.synD = out.total./ out.pL;
    
    preIdx = skel.getNodesWithComment('pre',1,'partial');
    nodesPre = Util.getNodeCoordsWithComment(skel, 'pre', 'partial');
    nodeCellbody = Util.getNodeCoordsWithComment(skel, 'cellbody', 'partial');
    if isempty(preIdx)
        warning(sprintf('Skel %s has no pre comments',skel.names{curTree}))
    end
    synDist = [];
    for i=1:numel(preIdx)
        curPreIdx = preIdx(i);
        curPreId  = str2double(skel.nodesAsStruct{curTree}(curPreIdx).id);
        [~, ~, pL] = skel.getShortestPath(startId, curPreId); %nm
        pL = pL./1e3;
        synDist(i) = pL;% distances of syns from root 
        clear pL
    end
    out.synDist = {synDist};
    if plotDendrogram
        % delete pre nodes comment
        synSize = 100; tubeSize = 1; dotSize= 3;
        synColors = {[1,0,0],... % red shaft
                     [0,1,0],... % green sph
                     [1,0,1],... % blue stub 
                     [230,159,0]./255,... % orange prim
                     [240,228,66]./255,... %Yellow second
                     [204,121,167]./255,... % reddish purple ter
                     [0,114,178]./255}; % bluish neck

        % make axonogram (PLASS-like)
        startpoint = skel.getNodesWithComment('_root',1,'regexp');
        endpoint  =  skel.getNodesWithComment('_end',1,'regexp');
    
        % attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
        comments = {skel.nodesAsStruct{1}.comment}';
        assert(startpoint == find(contains(comments,'_root')))
        
        tic;
        config = struct;
        config.scaleEM = skel.scale;
        Util.log('Now scatterting for %s',skel.names{1})
        [TREE, f] = Axonogram.axonogram(skel,'',startpoint,config.scaleEM, true);
        xlabel('Path length from root (um)')
        ylabel('')
        synComments = {'pre'};
        for i=1:numel(synComments)
            curSynLocs = find(cellfun(@(x) any(regexp(x,synComments{i})),comments));
            curSynColor = synColors{i};
            if ~isempty(curSynLocs)
                XY = Axonogram.findPointsInAxonogram(curSynLocs, TREE, skel, config.scaleEM);
                gcf, scatter(XY(:,1),XY(:,2),synSize, 'o','MarkerEdgeColor',curSynColor, 'MarkerFaceColor',curSynColor);
                clear XY
            end
        end

        % cosmetics
        box off
        camroll(90);
        set(gca,'linewidth',2, 'color', 'white');
        title( skel.names{curTree}, 'Interpreter', 'none')
        outfile = fullfile(outDir, ...
            sprintf('%s.png', skel.names{curTree}));
        export_fig(outfile,'-q101', '-nocrop', '-transparent');
        Util.log('Saving file %s', outfile);
        close(f);
        toc;
    end
end 
