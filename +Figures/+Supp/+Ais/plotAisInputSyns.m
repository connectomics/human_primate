% ais input for pyr vs IN
% empty where no input syns
clear 

config = struct;

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'Mouse_A2';
doAxons = false;

Figures.setConfig; % set mainFolder, datasetName, ownerName

tracingFolder = fullfile(mainFolder,'ais');
outDir = fullfile(mainFolder, 'results', 'ais');
if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'ais_with_input_axons*.nml')); % manually select AIS with pre comments only / keep all with empty rows
func = @(x) str2double(x.id);

%assert(numel(files) == 1)
% just one nml, can be multiple for H5_10_L23

% do per ais
Util.log('Now extracting...')
nml = fullfile(tracingFolder, files(1).name);
skelAll = skeleton(nml);

if numel(files)>1
    Util.log('Found %d AIS files. Merging!', numel(files))
    for i=2:numel(files)
        curSkel = skeleton(fullfile(tracingFolder, files(i).name)); 
        skelAll = skelAll.mergeSkels(curSkel);
    end
end

% removing axons and others
treeNames = skelAll.names;
idxKeep =  contains(treeNames, 'ais') & ~contains(treeNames, 'axon');
skelAll = skelAll.deleteTrees(idxKeep,true);
config.scaleEM = skelAll.scale;

% ais statistics
for curTree = 1:skelAll.numTrees
    curSkel = skelAll.deleteTrees(curTree, true); % remove all rest
    [aisData, nodesPre, nodeCellbody] = extractSynComments(curSkel, 1, false, outDir);
    if ~isempty(aisData)
        outData(curTree).ais = aisData;
        outData(curTree).skel = curSkel;
        outData(curTree).nodesPre = nodesPre;
        outData(curTree).nodeCellbody = nodeCellbody;
    end
    clear aisData nodesPre nodeCellbody
end
idxDel = arrayfun(@(x) isempty(x.ais), outData);
outData(idxDel) = [];
Util.save(fullfile(outDir, sprintf('aisData-InputSyns-%s.mat', regionName)), outData);

% export to excel
outTable = table;
for i=1:numel(outData)
    outTable = [outTable; outData(i).ais];
end

mkdir(fullfile(outDir,'data'))

writetable(outTable, fullfile(outDir, 'data', sprintf('aisData-InputSyns-%s.xlsx', regionName)));

% plot histogram
Util.log('Plotting histogram:')
fig = figure();
fig.Color = 'white';
ax = axes(fig);
hold on
for i=1:numel(outData)
    aisData = outData(i).ais;
    x = aisData.synDist{:};
    switch aisData.type{:}
        case 'pyr'
            curColor = 'r';
        case 'IN'
            curColor = 'g';
    end
    
    if i~=1
        curAxPos = ax.Position;
        curAxPos(1) = curAxPos(1)+0.01*rand(1);
        curAx = axes('Position', curAxPos, 'color','none');
    else
        curAx = ax;
    end
    hold on
    histogram(x, 'DisplayStyle','stairs', 'Binwidth',5,'EdgeColor', curColor, ...
         'LineWidth', 2, 'Parent', curAx);
    curAx.YAxis.TickValues = 0:2:20; % manually
    curAx.YAxis.Limits = [0,20]; % manually
    curAx.XAxis.TickValues = 0:20:100; % manually
    curAx.XAxis.Limits = [0,100]; % manually

    if i~=1
        set(gca,'xcolor','none', 'ycolor','none')
    end
    Util.setPlotDefault(gca,'','');
end
ax.XAxis.Label.String = 'Distance from soma(um)';
ax.YAxis.Label.String = 'Frequency of input synapses)';
ax.LineWidth = 2;
export_fig(fullfile(outDir, 'aisData-hist-synDistances.png'),'-q101', '-nocrop','-transparent', '-m8');
close all

function [out, nodesPre, nodeCellbody] = extractSynComments(skel, curTree, plotDendrogram, outDir)
    c = {skel.nodesAsStruct{curTree}.comment};
    c(cellfun(@isempty, c)) = []; 
    out = table;
    out.name = skel.names(curTree);

    if regexpi(out.name{1},'pyr')
        out.type = {'pyr'};
    elseif regexpi(out.name{1},'IN')
        out.type = {'IN'};
    elseif regexpi(out.name{1},'(uc|unknown)')
        out.type = {'UC'};
    else
        error('No type given for cell')
    end
    
    startIdx = skel.getNodesWithComment('_root',1,'regexp');

    endIdx  =  skel.getNodesWithComment('_end',1,'regexp');
    
    if isempty(startIdx) | isempty(endIdx)
        warning('No start-end found in %s', skel.names{1})
        out = table;
        nodesPre = NaN;
        nodeCellbody = NaN;
        return
    end

    startId = str2double(skel.nodesAsStruct{curTree}(startIdx).id);
    endId = str2double(skel.nodesAsStruct{curTree}(endIdx).id);
    
    [~, ~, out.pL] = skel.getShortestPath(startId, endId); %nm
    out.pL = out.pL./1e3;

    % start or end at _eods
    eodsIdx = skel.getNodesWithComment('(_root|_end)_eods',1,'regexp');
    if ~isempty(eodsIdx)
        out.EODS = 1;
    else
        out.EODS = 0;
    end

    % myelinating vs branching ais
    ais_type_node = skel.getNodesWithComment('type_(myelin|branch|syn)',1,'regexp');
    if isempty(ais_type_node) & out.EODS % eods
        out.typeMyelin = 0;
        out.typeBranching = 0;
        out.typeSynapse = 0;
    else
        ais_type_comment = skel.nodesAsStruct{1}(ais_type_node).comment;
        aisType = regexp(ais_type_comment,'type_(\w+)', 'tokens', 'once');

        switch aisType{:}
            case 'myelin'
                out.typeMyelin = 1;
                out.typeBranching = 0;
                out.typeSynapse = 0;
            case 'branching'
                out.typeMyelin = 0;
                out.typeBranching = 1;
                out.typeSynapse = 0;
            case 'synapse'
                out.typeMyelin = 0;
                out.typeBranching = 0;
                out.typeSynapse = 1;
            otherwise
                error('AIS type not defined for %s',skel.names{:})
        end
    end
    % soma location
    nodeCellbody = Util.getNodeCoordsWithComment(skel, 'cellbody', 'partial');

    % measured col
    measured_node = skel.getNodesWithComment('_measured',1,'regexp');
    if isempty(measured_node)
        out.measured_input = false;
    else
        out.measured_input = true;
    end

    % get pre syn nodes
    preIdx = skel.getNodesWithComment('pre',1,'partial');
    nodesPre = Util.getNodeCoordsWithComment(skel, 'pre', 'partial');

    if isempty(preIdx)
        warning(sprintf('Skel %s has no pre comments',skel.names{curTree}))
    end

    out.total = numel(preIdx);
    out.synD = out.total./ out.pL;

    synDist = [];
    for i=1:numel(preIdx)
        curPreIdx = preIdx(i);
        curPreId  = str2double(skel.nodesAsStruct{curTree}(curPreIdx).id);
        [~, ~, pL] = skel.getShortestPath(startId, curPreId); %nm
        pL = pL./1e3;
        synDist(i) = pL;% distances of syns from root 
        clear pL
    end
    out.synDist = {synDist};


    if plotDendrogram
        % delete pre nodes comment
        synSize = 100; tubeSize = 1; dotSize= 3;
        synColors = {[1,0,0],... % red shaft
                     [0,1,0],... % green sph
                     [1,0,1],... % blue stub 
                     [230,159,0]./255,... % orange prim
                     [240,228,66]./255,... %Yellow second
                     [204,121,167]./255,... % reddish purple ter
                     [0,114,178]./255}; % bluish neck

        % make axonogram (PLASS-like)
        startpoint = skel.getNodesWithComment('_root',1,'regexp');
        endpoint  =  skel.getNodesWithComment('_end',1,'regexp');
    
        % attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
        comments = {skel.nodesAsStruct{1}.comment}';
        assert(startpoint == find(contains(comments,'_root')))
        
        tic;
        config = struct;
        config.scaleEM = skel.scale;
        Util.log('Now scatterting for %s',skel.names{1})
        [TREE, f] = Axonogram.axonogram(skel,'',startpoint,config.scaleEM, true);
        xlabel('Path length from root (um)')
        ylabel('')
        synComments = {'pre'};
        for i=1:numel(synComments)
            curSynLocs = find(cellfun(@(x) any(regexp(x,synComments{i})),comments));
            curSynColor = synColors{i};
            if ~isempty(curSynLocs)
                XY = Axonogram.findPointsInAxonogram(curSynLocs, TREE, skel, config.scaleEM);
                gcf, scatter(XY(:,1),XY(:,2),synSize, 'o','MarkerEdgeColor',curSynColor, 'MarkerFaceColor',curSynColor);
                clear XY
            end
        end

        % cosmetics
        box off
        camroll(90);
        set(gca,'linewidth',2, 'color', 'white');
        title( skel.names{curTree}, 'Interpreter', 'none')
        outfile = fullfile(outDir, ...
            sprintf('%s.png', skel.names{curTree}));
        export_fig(outfile,'-q101', '-nocrop', '-transparent');
        Util.log('Saving file %s', outfile);
        close(f);
        toc;
    end
end 

