% ais inputs snys box plot and histogram
cellType = 'pyr';
% load saved data from parsed nmls in matfiles per region

info = Util.runInfo();
Util.showRunInfo(info);

Figures.setConfig
allColors = Util.getSomaColors(true);

Util.log('Plotting AIS data:')
fig1 = figure; 
fig1.Color = 'white';
ax1 = axes(fig1); hold on;

fig2 = figure; 
fig2.Color = 'white';
ax2 = axes(fig2); hold on;

% fig3 = figure; 
% fig3.Color = 'white';
% ax3 = axes(fig3); hold on;
ax3 = [];

legendNames = {'ex144', 'Mouse_A2','mk_L23','H5_10_L23', 'H6_4S_L23'};
allRegionPos = [1,1.5,3, 4,4.5];
allRegionMarkers = {'o','o','o','o','o'};
tickNames = legendNames;
legendNames1 = legendNames;
legendNames2 = legendNames;

i=1; [out1, mouseS1]=doForThisRegion(ax1, ax2, ax3, legendNames{i}, cellType, allRegionPos(i), allRegionMarkers{i},'k');
legendNames1{i} = sprintf('%s (N=%d)',legendNames1{i},numel(out1)); legendNames2{i} = sprintf('%s (N=%d)',legendNames2{i},numel(mouseS1));

i=2; [out1, mouseA2]=doForThisRegion(ax1, ax2, ax3,legendNames{i}, cellType, allRegionPos(i), allRegionMarkers{i},'k');
legendNames1{i} = sprintf('%s (N=%d)',legendNames1{i},numel(out1)); legendNames2{i} = sprintf('%s (N=%d)',legendNames2{i},numel(mouseA2));

i=3; [out1, macaqueS1]=doForThisRegion(ax1, ax2, ax3,legendNames{i}, cellType, allRegionPos(i), allRegionMarkers{i},'k');
legendNames1{i} = sprintf('%s (N=%d)',legendNames1{i},numel(out1)); legendNames2{i} = sprintf('%s (N=%d)',legendNames2{i},numel(macaqueS1));

i=4; [out1, humanSTG]=doForThisRegion(ax1, ax2, ax3,legendNames{i}, cellType, allRegionPos(i), allRegionMarkers{i},'k');
legendNames1{i} = sprintf('%s (N=%d)',legendNames1{i},numel(out1)); legendNames2{i} = sprintf('%s (N=%d)',legendNames2{i},numel(humanSTG));

i=5; [out1, humanIFC]=doForThisRegion(ax1, ax2, ax3,legendNames{i}, cellType, allRegionPos(i), allRegionMarkers{i},'k');
legendNames1{i} = sprintf('%s (N=%d)',legendNames1{i},numel(out1)); legendNames2{i} = sprintf('%s (N=%d)',legendNames2{i},numel(humanIFC));

c = cellfun(@(x) {sprintf('%s',x), ''}, legendNames1, 'UniformOutput', false); % duplicate for median lines
legendNames1 = cat(2, c{:});

c = cellfun(@(x) {sprintf('%s',x), ''}, legendNames2, 'UniformOutput', false); % duplicate for median lines
legendNames2 = cat(2, c{:});
% ex144.x1 = [27,30,23,39,37]; % AG P28 mouse L23
% H2_3.x1 = [66,74,15]; % JS

%% test
mouse = cat(1, mouseS1, mouseA2);
nhpHuman = cat(1, macaqueS1, humanSTG, humanIFC);
factor = mean(nhpHuman) / mean(mouse);
[~, pValue] = kstest2(mouse, nhpHuman);

Util.log('Mouse: %d AIS. Input synapses mean +/- s.d. = %.0f +- %.0f', numel(mouse), mean(mouse), std(mouse))
Util.log('NhpHuman: %d AIS. Input synapses mean +/- s.d. = %.0f +- %.0f', numel(nhpHuman), mean(nhpHuman), std(nhpHuman))
Util.log('Mouse to Human: factor = %.2f (pValue = %e, k.s. test)', factor, pValue)

%% cosmetics
ticklen = [0.025, 0.025];
ax1.YAxis.TickValues = 0:20:160;
ax1.YAxis.Limits = [0,160];
ax1.YAxis.Label.String = 'AIS length (\mum)';
ax1.XAxis.Limits = [0,max(allRegionPos)+1];
ax1.XAxis.TickValues = allRegionPos;
ax1.XAxis.TickLabels = tickNames;
ax1.XAxis.Label.String = 'Species';
ax1.XAxis.TickLabelInterpreter = 'none';
ax1.XAxis.TickLabelRotation = 90;
ax1.LineWidth = 2;
curLeg = legend(ax1, legendNames1);
set(curLeg, 'Box', 'Off', 'Location', 'best', 'Interpreter','none');
title(ax1, ...
    {info.filename; info.git_repos{1}.hash; ''}, ...
    'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
ax1.Title.Visible = 'on';
set(ax1, 'TickLength',ticklen)
Util.setPlotDefault(ax1,'','');

outfile = fullfile(outputDir,'figures','fig4',sprintf('panel_ais_lengths-%s',cellType));
saveas(ax1, strcat(outfile,'.png'));
export_fig(strcat(outfile,'.eps'),ax1, '-q101', '-nocrop', '-transparent', '-m8');

ax2.YAxis.TickValues = 0:20:160;
ax2.YAxis.Limits = [0,160];
ax2.YAxis.Label.String = 'AIS input syns';
ax2.XAxis.Limits = [0,max(allRegionPos)+1];
ax2.XAxis.TickValues = allRegionPos;
ax2.XAxis.TickLabels = tickNames;
ax2.XAxis.Label.String = 'Species';
ax2.XAxis.TickLabelInterpreter = 'none';
ax2.XAxis.TickLabelRotation = 90;
ax2.LineWidth = 2;
curLeg = legend(ax2, legendNames2);
set(curLeg, 'Box', 'Off', 'Location', 'best', 'Interpreter','none');
title(ax2, ...
    {info.filename; info.git_repos{1}.hash; ''}, ...
    'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
ax2.Title.Visible = 'on';
set(ax2, 'TickLength',ticklen)
Util.setPlotDefault(ax2,'','');

outfile = fullfile(outputDir,'figures','fig4',sprintf('panel_ais_synapse_counts-%s',cellType));
saveas(ax2, strcat(outfile,'.png'));
export_fig(strcat(outfile,'.eps'),ax2, '-q101', '-nocrop', '-transparent', '-m8');

% ax3.YAxis.TickValues = 0:20:160;
% ax3.YAxis.Limits = [0,160];
% ax3.YAxis.Label.String = 'AIS length (um)';
% ax3.XAxis.Limits = [0,150];
% ax3.XAxis.TickValues = 0:50:150;
% ax3.XAxis.Label.String = 'AIS input syns';
% ax3.LineWidth = 2;
% ax3.XAxis.TickLabelInterpreter = 'none';
% curLeg = legend(ax3, tickNames, 'Interpreter','none');
% set(curLeg, 'Box', 'Off', 'Location', 'best');
% title(ax3, ...
%     {info.filename; info.git_repos{1}.hash; ''}, ...
%     'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
% ax3.Title.Visible = 'on';
% set(ax3, 'TickLength',ticklen)
% Util.setPlotDefault(ax3,'','');
% 
% outfile = fullfile(outputDir,'figures','fig4',sprintf('panel_ais_length_and_synapse_counts-%s',cellType));
% set(gcf, 'Position', get(0, 'Screensize'));
% saveas(ax3, strcat(outfile,'.png'));

function [out1, out2] = doForThisRegion(ax1, ax2, ax3, regionName, cellType, curPos, curMarker, curColor)    
    Figures.setConfig;

    outDir = fullfile(mainFolder, 'results', 'ais');
    m = load(fullfile(outDir, sprintf('aisData-InputSyns-%s.mat', regionName)));
    outData = m.outData;

    %% plot pathlengths (with or without input syn measurements
    outTable = table;
    for i=1:numel(outData)
        % keep AIS completely inside the dataset
        if outData(i).ais.measured_input && not(outData(i).ais.EODS)
            outTable = [outTable; outData(i).ais];
        end
    end
    outTable = outTable(contains(outTable.type, cellType),:); % keep cell type specific AIS only
    [~,idxSort] = sort(outTable.name);
    outTable = outTable(idxSort,:);
    pL = outTable.pL; % pathlength
    scatter(ax1, repelem(curPos,numel(pL),1), pL,...
        75, curMarker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',curColor);
    plot(ax1, [curPos-0.15, curPos+0.15], repelem(median(pL),1,2), '-k','MarkerSize',100,'LineWidth',2)
    % export
    out1 = pL;
    Util.log('PL for %s: %.2f (N=%d)', regionName, median(pL), numel(pL))

    %% plot input syn measurements
    outTable = table;
    for i=1:numel(outData)
        % keep AIS completely inside the dataset and whose input was measured
        if outData(i).ais.measured_input && not(outData(i).ais.EODS)
            outTable = [outTable; outData(i).ais];
        end
    end
    outTable = outTable(contains(outTable.type, cellType),:); % leep cell type specific AIS only
    [~,idxSort] = sort(outTable.name);
    outTable = outTable(idxSort,:);
    total = outTable.total; % total input synapses
    scatter(ax2, repelem(curPos,numel(total),1), total,...
        75, curMarker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',curColor);
    plot(ax2, [curPos-0.15, curPos+0.15], repelem(median(total),1,2), '-k','MarkerSize',100,'LineWidth',2)
    % export
    out2 = total;
    Util.log('Input syns for %s: %.2f (N=%d)', regionName, median(total), numel(total))
    
%     %% plot pathlength vs input synapses 
%     pL = outTable.pL; % pathlength
%     scatter(ax3, total, pL, 75, curMarker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',curColor);
end
