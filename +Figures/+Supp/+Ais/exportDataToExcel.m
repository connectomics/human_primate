% export data to excel from ais input and axons
% ais input syns numbers
% ais seeded axons profiles

outputDir = '/u/sahilloo/data/primate_and_human/outData/';

Util.log('Loading data from matfiles saved:')

regionName = 'mk_L4';
[mk_L4_ais, mk_L4_axons] = doForThisRegion(regionName);

regionName = 'mk_L23';
[mk_L23_ais, mk_L23_axons] = doForThisRegion(regionName);

regionName = 'H5_10_L4';
[H5_10_L4_ais, H5_10_L4_axons] = doForThisRegion(regionName);

regionName = 'H5_10_L23';
[H5_10_L23_ais, H5_10_L23_axons] = doForThisRegion(regionName);

% write excel
outDataAis = [mk_L4_ais; mk_L23_ais; H5_10_L4_ais; H5_10_L23_ais];
writetable(outDataAis,fullfile(outputDir,'figures','fig4','data',sprintf('data_ais_input_syns_all_regions.xlsx')))

outDataAxons = [mk_L4_axons; mk_L23_axons; H5_10_L4_axons; H5_10_L23_axons];
writetable(outDataAxons,fullfile(outputDir,'figures','fig4','data',sprintf('data_ais_axons_all_regions.xlsx')))

function [outTableAis, outTableAxons] = doForThisRegion(regionName)
    Figures.setConfig;

    outDir = fullfile(mainFolder, 'results', 'ais');
    m = load(fullfile(outDir, sprintf('aisData-InputSyns-%s.mat', regionName)));
    outData = m.outData;
    
    % catenate
    outTableAis = table;
    for i=1:numel(outData)
        outTableAis = [outTableAis; outData(i).ais];
    end
    col1 = repelem({regionName},height(outTableAis),1);
    outTableAis = [col1, outTableAis];
    clear outData
 
    m = load(fullfile(outDir, sprintf('aisAxonDataPooled-%s.mat', regionName)));
    outData = m.outData;
    outTableAxons = outData.axon;
    col1 = repelem({regionName},height(outTableAxons),1);
    outTableAxons = [col1, outTableAxons];
    clear outData
end


