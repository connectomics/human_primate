% ais path lengths box plot and histogram
cellType = ''; % pyr IN UC

% load saved data from parsed nmls in matfiles per region
regionNames = {'mk_L4','mk_L23','H5_10_L4','H5_10_L23'};

binWidth = 10;

colorL4 = 'k';
colorL23 = [0.5,0.5,0.5];

info = Util.runInfo();
Util.showRunInfo(info);

outputDir = '/u/sahilloo/data/primate_and_human/outData/';
Util.log('AIS path lengths data:')
[mk_L4.x1, mk_L4.x2] = doThisForRegion('mk_L4', cellType);
[mk_L23.x1, mk_L23.x2] = doThisForRegion('mk_L23', cellType);
[H5_10_L4.x1, H5_10_L4.x2] = doThisForRegion('H5_10_L4', cellType);
[H5_10_L23.x1, H5_10_L23.x2] = doThisForRegion('H5_10_L23', cellType);

ex144.x1 = [33.35, 50.48, 30.33, 43.12, 42.34]; % AG P28
H2_3.x1 = [86.05, 104.71, 82.92]; % JS

% save data
m = struct;
m.mk_L4 = mk_L4;
m.mk_L23 = mk_L23;
m.H5_10_L4 = H5_10_L4;
m.H5_10_L23 = H5_10_L23;
Util.saveStruct(fullfile(outputDir,'figures','fig4','data',sprintf('data_ais_pathlengths_all_regions-%s.mat',cellType)),m);

% export data
outData = table;
count = 100;
% pl
outData.mk_L4_pl = cell(count,1);
outData.mk_L4_pl(1:numel(mk_L4.x1)) = num2cell( mk_L4.x1);
outData.mk_L23_pl = cell(count,1);
outData.mk_L23_pl(1:numel(mk_L23.x1)) = num2cell( mk_L23.x1);
outData.H5_10_L4_pl = cell(count,1);
outData.H5_10_L4_pl(1:numel(H5_10_L4.x1)) = num2cell( H5_10_L4.x1);
outData.H5_10_L23_pl = cell(count,1);
outData.H5_10_L23_pl(1:numel(H5_10_L23.x1)) = num2cell( H5_10_L23.x1);
outData.H2_3_L4_pl = cell(count,1);
outData.H2_3_L4_pl(1:numel(H2_3.x1)) = num2cell( H2_3.x1);
outData.ex144_L23_pl = cell(count,1);
outData.ex144_L23_pl(1:numel(ex144.x1)) = num2cell( ex144.x1);

writetable(outData,fullfile(outputDir,'figures','fig4','data',sprintf('data_ais_pathlengths_all_regions-%s.xlsx',cellType)))

% do plotting
Util.log('Plotting AIS pathlengths:')

%% boxplot fig = figure;
fig = figure;
fig.Color = 'white';

subplot(1,2,1)
ax = gca;
hold on;

% ex144 L4
widthS = 1;% startS = 1;
x = ex144.x1;
marker = 'x';
color = 'k';
pos = 1.5;
posS = 1;
scatter(ax, widthS.*rand(numel(x),1)+posS, x,...
       75,marker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',color);
h = boxplot(ax, x,'Colors',color,'Widths',widthS,'Positions',pos,'Symbol','r+');
set(h,{'linew'},{2})
ex144Name = sprintf('ex144 N(%d)',numel(x));

% L4 Mk vs H5
pos = [4.5,7.5];
posS = [4,7];
doPlottingOfTwoBoxes(mk_L4.x1, H5_10_L4.x1, pos, posS, colorL4, 'o');
mk_L4Name = sprintf('mkL4 (N=%d)',numel(mk_L4.x1));
H5_L4Name = sprintf('H5L4 (N=%d)',numel(H5_10_L4.x1));

% L23 Mk vs H5
pos = [5.5,8.5];
posS = [5,8];
doPlottingOfTwoBoxes(mk_L23.x1, H5_10_L23.x1, pos, posS, colorL23, 'x');
mk_L23Name = sprintf('mkL23 (N=%d)',numel(mk_L23.x1));
H5_L23Name = sprintf('H5L23 (N=%d)',numel(H5_10_L23.x1));


% H2_3 L4
widthS = 1;% startS = 1;
x = H2_3.x1;
marker = 'o';
color = [0.5,0.5,0.5];
pos = 75;
posS = 7;
scatter(ax, widthS.*rand(numel(x),1)+posS, x,...
       75,marker,'filled', 'MarkerFaceColor',color,'MarkerEdgeColor',color);
h = boxplot(ax, x,'Colors',color,'Widths',widthS,'Positions',pos,'Symbol','r+');
set(h,{'linew'},{2})
H2_3Name = sprintf('H2_3 (N=%d)',numel(H2_3.x1));

% cosmetics
ax.YAxis.TickValues = 0:10:120;
ax.YAxis.Limits = [0,120];
ax.YAxis.Label.String = 'AIS pathlengths (\mum)';

ax.XAxis.Limits = [0,10];
ax.XAxis.TickValues = [1.5, 4.5, 7.5];
ax.XAxis.TickLabels = {'Mouse', 'Mk','Human'};
ax.XAxis.Label.String = 'Species';
ax.LineWidth = 2;
curLeg = legend(ex144Name, mk_L4Name, H5_L4Name, mk_L23Name, H5_L23Name, H2_3Name);
set(curLeg, 'Box', 'Off', 'Location', 'best');
title(ax, ...
    {info.filename; info.git_repos{1}.hash; ''}, ...
    'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
ax.Title.Visible = 'off';
Util.setPlotDefault(gca,'','');

% histogram
subplot(1,2,2)
axTemp = gca;
set(axTemp,'Color','none','Visible','off')

% L4 and L23 Mk
ax1_pos = axTemp.Position;
ax1_pos(3) = ax1_pos(3)/2;
ax1 = axes('Position',ax1_pos,...
    'Color','none');
hold on
h = histogram(ax1,mk_L4.x1,'DisplayStyle','stairs','LineWidth',2,'EdgeColor','k','orientation','horizontal','BinWidth', binWidth);
h = histogram(ax1,mk_L23.x1,'DisplayStyle','stairs','LineWidth',2,'EdgeColor',[0.5,0.5,0.5],'orientation','horizontal','BinWidth', binWidth);

% cosmetics
ax1.YAxis.TickValues = 0:10:120;
ax1.YAxis.Limits = [0,120];
ax1.YAxis.Label.String = '';
ax1.YAxis.TickLabels = []; % turn off

ax1.XAxis.Limits = [0,20];
ax1.XAxis.Label.String = 'Freq.';
ax1.LineWidth = 2;
title(ax1, ...
    {info.filename; info.git_repos{1}.hash; ''}, ...
    'FontWeight', 'normal', 'FontSize', 6, 'Interpreter','none');
ax1.Title.Visible = 'off';
Util.setPlotDefault(ax1,'','');
hold off

% L4 and L23 H5
ax2_pos = ax1.Position;
ax2_pos(1) =  ax2_pos(1) + ax2_pos(3) + 0.02;
ax2 = axes('Position',ax2_pos,...
    'Color','none');
hold on
h = histogram(ax2,H5_10_L4.x1,'DisplayStyle','stairs','LineWidth',2,'EdgeColor','k','orientation','horizontal','BinWidth', binWidth);
h = histogram(ax2,H5_10_L23.x1,'DisplayStyle','stairs','LineWidth',2,'EdgeColor',[0.5,0.5,0.5],'orientation','horizontal','BinWidth', binWidth);

% cosmetics
ax2.YAxis.TickValues = 0:10:120;
ax2.YAxis.Limits = [0,120];
ax2.YAxis.Label.String = '';
ax2.YAxis.TickLabels = []; % turn off

ax2.XAxis.Limits = [0,20];
ax2.XAxis.Label.String = 'Freq.';
ax2.LineWidth = 2;
title(ax2, ...
    {info.filename; info.git_repos{1}.hash; ''}, ...
    'FontWeight', 'normal', 'FontSize', 6, 'Interpreter','none');
ax2.Title.Visible = 'off';
Util.setPlotDefault(ax2,'','');
hold off

outfile = fullfile(outputDir,'figures','fig4',sprintf('panel_ais_pathlengths-%s',cellType));
export_fig(strcat(outfile,'.eps'), '-q101', '-nocrop', '-transparent', '-m8');
export_fig(strcat(outfile,'.png'), '-q101', '-nocrop', '-transparent', '-m8');
close all

function doPlottingOfTwoBoxes(x1,x2, pos, posS, color, marker)
    if isempty(x1) & isempty(x2)
        warning('Empty box plot')
        return
    end

    x = [x1(:); x2(:)];
    curIdxx1 = 1:numel(x1);
    curIdxx2 = curIdxx1(end)+(1:numel(x2));
    g = cell(numel(x),1);
    g(curIdxx1) = {'x1'}; % first box
    g(curIdxx2) = {'x2'}; % second box

    widthS = 1;% startS = 1;
    %[pos, posS] = Util.getBoxPlotPos(numel(unique(g)), startS);
    scatter(widthS.*rand(numel(curIdxx1),1)+posS(1), x(curIdxx1),...
        75,marker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',color);
    scatter(widthS.*rand(numel(curIdxx2),1)+posS(2), x(curIdxx2),...
        75,marker,'filled', 'MarkerFaceColor','none','MarkerEdgeColor',color);

    if isempty(x1)
        pos(1) = [];
    elseif isempty(x2)
        pos(2) = [];
    end
    h = boxplot(x,g,'Colors',color,'Widths',widthS,'Positions',pos,'Symbol','r+');
    set(h,{'linew'},{2})
end


function [x1,x2] = doThisForRegion(regionName, cellType)
    % x1: ais pl, x2: input syns
    Figures.setConfig;

    outDir = fullfile(mainFolder, 'results', 'ais');
    m = load(fullfile(outDir, sprintf('aisDataPathlengths-%s.mat', regionName)));
    outData = m.outData;

    outTable = table;
    for i=1:numel(outData)
        if any(contains(outData(i).ais.Properties.VariableNames,'pL'))
            outTable = [outTable; outData(i).ais];
        end
    end
    [~,idxSort] = sort(outTable.name);
    outTable = outTable(idxSort,:);
    
    % cell type
%    idxKeep = contains(outTable.type, cellType);
%    outTable = outTable(idxKeep,:);
    x1 = outTable.pL;
    x2 = [];
end
