clear; clc;
if Util.isLocal
    outDir = 'C:\Users\loombas\Downloads\u\data\screening\figures\meeting_notes\2022_03_16\figure_panels\IN_galleries';
    mkdir(outDir)       
else
    Figures.setConfig;
    outDir = fullfile(outputDir,'figures','fig2','panels');
    mkdir(outDir)
    clear outputDir
end

%% soma based IN data
cellTypes = {'soma-based-IN-MP-singleSph','soma-based-IN-BP-singleSph', 'soma-based-IN-MP-doubleSph','soma-based-IN-BP-doubleSph'};
cellTypesCurData = cell(1,numel(cellTypes));

info = Util.runInfo();
Util.showRunInfo(info);

for ct = 1:numel(cellTypes)
    allRegionNames = {'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO', ...
                'mk_L23', 'Mk1_T2', ...
                'H5_10_L23', 'H6_4S_L23'};
    
    cellType = cellTypes{ct};
    
    Util.log('Doing for: %s', cellType)
    curData = struct;
    
    for idxRegion = 1:numel(allRegionNames)
        curRegion = allRegionNames{idxRegion};
        [curData.(curRegion), mapColor, marker] = doForThisRegion(curRegion, cellType);
    end
    
    % remove empty data columns
    idxDel = structfun(@isempty, curData);
    curData = rmfield(curData, allRegionNames(idxDel));
    allRegionNames = allRegionNames(~idxDel);
    mapColor = repmat(mapColor, numel(fieldnames(curData)),1);

    cellTypesCurData{ct} = curData;
end

%% plot scatter for bp/mp mouse/nhpHuman singleSph vs doubleSph
% cellTypes = {'soma-based-IN-MP-singleSph','soma-based-IN-BP-singleSph', 'soma-based-IN-MP-doubleSph','soma-based-IN-BP-doubleSph'};
mpColor = [107 76 154]/255; % purple;
bpColor = [0,1,1]; % cyan
mouseMarker = 'x';
nhpHumanMarker = 'o';

fig = figure; hold on;
ax = gca;

% MP
singleSph = cellTypesCurData{contains(cellTypes, 'MP') & contains(cellTypes, 'singleSph')};
doubleSph = cellTypesCurData{contains(cellTypes, 'MP') & contains(cellTypes, 'doubleSph')};

[dataSingleSph, dataDoubleSph] = getData(allRegionNames, singleSph, doubleSph, 'mouse');
plotInputDataScatter(dataSingleSph, dataDoubleSph, mouseMarker, mpColor);

[dataSingleSph, dataDoubleSph] = getData(allRegionNames, singleSph, doubleSph, 'nhpHuman');
plotInputDataScatter(dataSingleSph, dataDoubleSph, nhpHumanMarker, mpColor);

% BP
singleSph = cellTypesCurData{contains(cellTypes, 'BP') & contains(cellTypes, 'singleSph')};
doubleSph = cellTypesCurData{contains(cellTypes, 'BP') & contains(cellTypes, 'doubleSph')};

[dataSingleSph, dataDoubleSph] = getData(allRegionNames, singleSph, doubleSph, 'mouse');
plotInputDataScatter(dataSingleSph, dataDoubleSph, mouseMarker, bpColor);

[dataSingleSph, dataDoubleSph] = getData(allRegionNames, singleSph, doubleSph, 'nhpHuman');
plotInputDataScatter(dataSingleSph, dataDoubleSph, nhpHumanMarker, bpColor);

% plot marginal histogram for all scatter points
allDataSingleSph = cellfun(@vertcat,struct2cell(cellTypesCurData{1}),struct2cell(cellTypesCurData{2}),'uni',0);
allDataSingleSph = cat(1, allDataSingleSph{:});
allDataDoubleSph = cellfun(@vertcat,struct2cell(cellTypesCurData{3}),struct2cell(cellTypesCurData{4}),'uni',0);
allDataDoubleSph = cat(1, allDataDoubleSph{:});

% cosmetics
ax.XAxis.Label.String = 'Fraction of synapses onto singleSph';
ax.XAxis.Limits = [0,0.6];
ax.XAxis.TickValues = 0:0.1:1;
ax.XAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = 0:0.02:1;
ax.YAxis.Label.String = 'Fraction of synapses onto doubleSph';
ax.YAxis.Limits = [0,0.6];
ax.YAxis.TickValues = 0:0.1:1;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:0.02:1;
ax.Title.Interpreter = 'none';
ax.LineWidth = 1;
Util.setPlotDefault(ax);
curLeg = legend({'mouse MP', 'nhpHuman MP', 'mouse BP' , 'nhpHuman BP'});
set(curLeg, 'Box', 'off')

%%
set(ax,'XAxisLocation','top');
ax.Position = [0.15, 0.15, 0.7, 0.7];

[axPost, axPre] = Figures.Conn.plotHistogramOverScatter(fig, ax, allDataSingleSph, allDataDoubleSph, ...
    0.02, 0.02, [0,0.6], [0,0.6]);
axPost.XAxis.Visible = 'on';
axPre.YAxis.Visible = 'on';

outfile = fullfile(outDir, 'soma_based_INs_single_vs_double_sph_targets');
if not(Util.isLocal)
   export_fig(strcat(outfile,'.eps'),'-q101', '-nocrop', '-transparent');
else
    saveas(gcf, strcat(outfile,'.png'));
end

%% histograms
% cellTypes = {'soma-based-IN-MP-singleSph','soma-based-IN-BP-singleSph', 'soma-based-IN-MP-doubleSph','soma-based-IN-BP-doubleSph'};
figure; hold on;
ax = gca;

% merge MP BP 
singleSph = cell2struct(cellfun(@vertcat,struct2cell(cellTypesCurData{1}),struct2cell(cellTypesCurData{2}),'uni',0),fieldnames(cellTypesCurData{1}),1);
doubleSph = cell2struct(cellfun(@vertcat,struct2cell(cellTypesCurData{3}),struct2cell(cellTypesCurData{4}),'uni',0),fieldnames(cellTypesCurData{3}),1);

[dataSingleSph, dataDoubleSph] = getData(allRegionNames, singleSph, doubleSph, 'mouse');
plotInputDataHist(dataSingleSph, dataDoubleSph, 'k', 'k');

[dataSingleSph, dataDoubleSph] = getData(allRegionNames, singleSph, doubleSph, 'nhpHuman');
plotInputDataHist(dataSingleSph, dataDoubleSph, [0.5,0.5,0.5], [0.5,0.5,0.5]);

% cosmetics
ax.XAxis.Label.String = {'Fraction of synapses onto', 'singleSph (solid - ) / doubleSph (dashed -- )'};
ax.XAxis.Limits = [0,1];
ax.XAxis.TickValues = 0:0.1:1;
ax.XAxis.MinorTick = 'on';
ax.XAxis.MinorTickValues = 0:0.05:1;

ax.YAxis.Label.String = 'Number of interneuron axons';
ax.YAxis.Limits = [0,16];
ax.YAxis.TickValues = 0:2:20;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:1:20;

ax.Title.Interpreter = 'none';
ax.LineWidth = 1;
Util.setPlotDefault(ax);

curLeg = legend({'mouse singleSph', 'mouse doubleSph' , 'nhpHuman singleSph', 'nhpHuman doubleSph'});
set(curLeg, 'Box', 'off')
outfile = fullfile(outDir, 'soma_based_INs_single_vs_double_sph_targets_histogram');
if not(Util.isLocal)
   export_fig(strcat(outfile,'.eps'),'-q101', '-nocrop', '-transparent');
else
    saveas(gcf, strcat(outfile,'.png'));
end

%% helper functions
function [dataSingle, dataDouble] = getData(allRegionNames, curDataSingle, curDataDouble, speciesName)
    switch speciesName
        case 'mouse'
            idxSpecies = contains(allRegionNames, {'Mouse_A2','PPC'});
        case 'nhpHuman'
            idxSpecies = contains(allRegionNames, {'mk_L23','H5_10_L23'});
    end
    
    curDataSingle = struct2cell(curDataSingle);
    dataSingle = curDataSingle(idxSpecies);
    dataSingle = cat(1,dataSingle{:});
    curDataDouble = struct2cell(curDataDouble);
    dataDouble = curDataDouble(idxSpecies);
    dataDouble = cat(1,dataDouble{:});
end

function plotInputDataScatter(data1, data2, marker, color)
    plot(data1, data2,...
        'Color', color, ...
        'Marker', marker, ...
        'MarkerSize', 10, ...
        'LineWidth', 2, ...
        'LineStyle', 'none')
end

function plotInputDataHist(data1, data2, color1, color2)
    histogram(data1,'BinWidth',0.05, 'DisplayStyle','stairs', ...
        'EdgeColor',color1,'LineWidth',2,...
        'Orientation','vertical', 'LineStyle','-');
    histogram(data2,'BinWidth',0.05, 'DisplayStyle','stairs', ...
        'EdgeColor',color2,'LineWidth',2,...
        'Orientation','vertical', 'LineStyle','--');
end

function [out, mapColor, marker] = doForThisRegion(regionName, cellType)
    marker = 'x';
    switch cellType
        case 'spiny'
            searchString = {'seed_sh','seed_sph','sh_seed','sph_seed'};
            mapColor = [0,0,0];
        case 'smooth-sh'
            searchString = {'seed_IN_smooth'};
             mapColor = [0,0,0];
        case 'BP'
            searchString = {'seed_IN_smooth_BP'};
            mapColor = [0,1,1]; % cyan
        case 'MP'
            searchString = {'seed_IN_smooth_MP'};
            mapColor = [107 76 154]/255; % purple
        case {'soma-based-IN-BP-singleSph', 'soma-based-IN-BP-doubleSph'}
            searchString = {'BP'};
            mapColor = [0,1,1]; % cyan
        case {'soma-based-IN-MP-singleSph', 'soma-based-IN-MP-doubleSph'}
            searchString = {'MP'};
            mapColor = [107 76 154]/255; % purple
        case 'spiny-sph'
            searchString = {'seed_sph','sph_seed'};
            mapColor = [1,0,1];
        case 'spiny-sh'
            searchString = {'seed_sh','sh_seed'};
            mapColor = [0,0,0];
        case 'spiny-double'
            searchString = {'prim','sec'};
            mapColor = [0,0,0];
        case 'spiny-neck'
            searchString = {'neck'};
            mapColor = [0,0,0];  
        case 'spiny-stub'
            searchString = {'stub'};
            mapColor = [0,0,0];    
        otherwise
            error('Seeded axon type not defined!')
    end

    if contains(cellType, 'soma-based')
        Figures.setConfig
        
        % handle exceptions
        if strcmp(regionName, 'H5_10_L23')
            regionName = {'H5_10_L23', 'H5_5_AMK','H5_10_ext'}; % compile datasets for H5
        end
        if strcmp(regionName, 'PPC_AK')
            regionName = {'PPC_AK','PPC2'}; % compile datasets for PPC
        end
        
        Util.log('load wholecell based data')
        nSynThr = 10;
        cx_path_axons = fullfile(outputDir,  'figures','fig2','data','data_spine_targets_fraction_axons_per_axon-cellbody-attached-combined_v32.xlsx');
        cx_data_axons = readtable(cx_path_axons);
        curIdx = contains(cx_data_axons.Var1, regionName);
        cx_data_axons = cx_data_axons(curIdx,:);
        d = cx_data_axons(cx_data_axons.total>=nSynThr,:);
        idxKeep = contains(d.name, searchString);
        curAxonData = d(idxKeep,:);
        if contains(cellType,'doubleSph')
            out = estimateSecTargetFractions(curAxonData);
            marker = 'x';
        elseif contains(cellType,'singleSph')
            out = estimateSphTargetFractions(curAxonData);
            marker = 'o';
        end
    else     
        nSynThr = 2;
        Figures.setConfig
        Util.log('load seeded axon data')
        cx_path_axons = config.cx_path_axons;
        cx_data_axons = readtable(cx_path_axons);
        cx_data_axons(cx_data_axons.sph + cx_data_axons.shaft<nSynThr,:)=[];   
        % keep cellType seeded axons only
        curAxonData = cx_data_axons(contains(cx_data_axons.name, searchString), :);  
        % estimate spine target fraction
        curSphFraction = estimateSpineTargetFractions(curAxonData);
        % export output
        out = curSphFraction;
    end
end

function x = estimateSpineTargetFractions(cx_data) 
    x = cx_data.sph ./ (cx_data.sph + cx_data.shaft);
end

function x = estimateSecTargetFractions(cx_data) 
    x = (cx_data.second + cx_data.prim + cx_data.ter)./ cx_data.total;
end
function x = estimateSphTargetFractions(cx_data) 
    x = (cx_data.sph)./ cx_data.total;
end
