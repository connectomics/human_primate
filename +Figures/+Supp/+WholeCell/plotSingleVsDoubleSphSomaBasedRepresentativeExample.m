% representative double=bouquet axon example with output doublespine synapses
clear; clc;
if Util.isLocal
    outDir = 'C:\Users\loombas\Downloads\u\data\screening\figures\meeting_notes\2022_03_16\figure_panels\SingleVsDoubleSpineTargets';
    mkdir(outDir)       
else
    Figures.setConfig;
    outDir = fullfile(outputDir,'figures','fig2','panels');
    mkdir(outDir)
    clear outputDir
end

%%
regionNames = {'PPC2','H5_10_ext','H5_5_AMK'};
cellIds = {'Cell_001_IN_BP','Cell_0263_IN_BP','cell_081_IN_BP'};
curCellType = 'BP';

for idxRegion = 1:numel(regionNames)
    regionName =  regionNames{idxRegion};
    cellId = cellIds{idxRegion};

    Figures.setConfig;

    Util.log('Reading cellbody attached axons')
    tracingFolder = fullfile(mainFolder,'data','tracings','axons','cellbody_attached');
    files = dir(fullfile(tracingFolder, '*.nml'));
    nml = fullfile(tracingFolder, files(1).name);
    skelAll = skeleton(nml); % two trees per cell

    % keep representative pyr cell axon + dend
    idxKeep = contains(skelAll.names, cellId);
    skel = skelAll.deleteTrees(idxKeep, true);

    % scale
    scale = skel.scale./1000;
    bbox = Util.convertWebknossosToMatlabBbox(config.bbox);
    bbox = bsxfun(@times, bbox, config.scale(:)./1000);

    outfile = fullfile(mainFolder, 'outData', 'figures','wholecells', sprintf('%s_representative_axon_dend_%s',regionName,cellId));

    %%
    somaSize = 50; tubeSize = 2;
    alphaVal = 1;
    [somaColor, dendColor, axonColor] = getColors(curCellType);

    f = figure();
    f.Color = 'white';
    ax = axes(f);
    hold on;
    idxAxon = contains(skel.names, 'axon');
    idxDend = contains(skel.names, 'dend');

    skel.plot(idxDend, dendColor, true, tubeSize,'','','','','',alphaVal);
    skel.plot(idxAxon, axonColor, true, tubeSize,'','','','','',alphaVal);

    somaCoords = Util.getNodeCoordsWithComment(skel,{'cellbody'},'regexpi', idxAxon);
    if isempty(somaCoords)
        error('Empty soma %s', skel.names{:})
    end
    somaLocs = skel.setScale(somaCoords,scale);
    % plot soma
    objectSomas = ...
                scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3),...
                somaSize,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                'MarkerFaceColor', somaColor(1:3));


    [tubeSize, commentTypes, synColors,synComments,synMarkers, synSizes] = getCosmetics();
    comments = {skel.nodesAsStruct{idxAxon}.comment}';

    % scatter output synapses
    for curCommentIdx=1:numel(synComments)
        curSynLocs = Util.getNodeCoordsWithComment(skel,synComments{curCommentIdx},'regexpi',idxAxon);
        curSynColor = synColors{curCommentIdx};
        if ~isempty(curSynLocs)
            curSynLocs = skel.setScale(curSynLocs,scale);
            scatter3(curSynLocs(:,1),curSynLocs(:,2),curSynLocs(:,3),...
                synSizes(curCommentIdx), synMarkers{curCommentIdx},...
                'MarkerEdgeColor',curSynColor, 'MarkerFaceColor',curSynColor,...
                'LineWidth', 2);
        end
    end
    scaleBarLength = 50; % um
    line(ax,[bbox(1,2)-scaleBarLength, bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis

    Util.setView(1, true); % xy
    daspect([1,1,1])
    Util.setPlotDefault();
    curTitle = title(ax, sprintf('%s (scalebar %d um)',cellId, scaleBarLength),...
            'FontName','Arial','Units','normalized','Position',[0.5, 0, 0],...
            'HorizontalAlignment', 'center','Interpreter','none',....
            'EdgeColor', 'none','Color', [0,0,0],'FontSize',8);

    %% save
    set(gcf,'Position',get(0, 'Screensize')) 
    export_fig(strcat(outfile,'.eps'),'-q101', '-nocrop','-transparent')
    close all
end

%%
function [tubeSize, commentTypes, synColors,synComments,synMarkers, synSizes ] = getCosmetics()
        tubeSize = 2;
        commentTypes = {'sh_Inh',...
                       'sh_Exc',...
                       'sph_Inh',...
                       'sph_Exc',...
                       'prim_Inh',...
                       'prim_Exc',...
                       'sec_Inh',...
                       'sec_Exc',...
                       'stub_Inh',...
                       'stub_Exc',...
                       'ter_Inh',...
                       'ter_Exc',...
                       'neck_Inh',...
                       'neck_Exc',...
                        };
        synColors = {[0,0,0],... % Sh IN black
                     [0,0,1],... % Sh ExN blue
                     [0,0,0],... % Sph IN black
                     [1,0,1],... % Sph ExN magenta
                     [0,0,0],... % prim IN black
                     [1,0,1],... % prim ExN magenta
                     [0,0,0],... % sec IN black
                     [1,0,1],... % sec ExN magenta
                     [0,0,0],... % stub IN black
                     [0,0,1],... % stub ExN blue
                     [0,0,0],... % ter IN black
                     [1,0,1],... % ter ExN magenta
                     [0,0,0],... % neck IN black
                     [0,0,1],... % neck ExN blue
                     };
        synComments = {'(sh(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|sh(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'sh(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(sph(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|sph(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'sph(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(prim(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|prim(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'prim(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(sec(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|sec(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'sec(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(stub(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|stub(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'stub(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(ter(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|ter(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'ter(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(neck(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|neck(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'neck(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                        };
        synMarkers = {'o','o',...
                      'o','o',...
                      'x','x',...
                      'x','x',...
                      'o','o',...
                      'x','x',...
                      's','s',...
                       };

        synSizes = [20, ...
                    20,...
                    20,...
                    20,...
                    60,...
                    60,...
                    60,...
                    60,...
                    20,...
                    20,...
                    60,...
                    60,...
                    20,...
                    20,...
                       ];
end

function [somaColor, dendColor, axonColor] = getColors(cellType)
    switch cellType
        case 'Pyr'
            somaColor = [1,0,1]; % m
            dendColor = [1,0,1]; % m
            axonColor = [0,158,115]/255; % green
        case 'BP'
            somaColor = [0,1,1]; % cyan
            dendColor = [0,1,1]; % cyan
            axonColor = [0,158,115]/255; % green
        case 'MP'
            somaColor = [107 76 154]./255; % purple
            dendColor = [107 76 154]./255; % purple
            axonColor = [0,158,115]/255; % green
        case 'IN'
            somaColor = [0,0,0]; % black
            dendColor = [0,0,0]; % purple
            axonColor = [0,158,115]/255; % green
    end
end

