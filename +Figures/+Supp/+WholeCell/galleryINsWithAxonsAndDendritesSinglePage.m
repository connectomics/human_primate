% export IN dendrites + axons as single page gallery
regionName = 'ACC_JO'; % ex144, Mouse_A2, V2_L23, PPC_AK, ACC_JO, Mk1_T2, 'H5_5_AMK', 'H6_4S_L23'

Figures.setConfig; % set main region based folder

bbox = Util.convertWebknossosToMatlabBbox(config.bbox);

tubeSize = 1;
do3D = false;

outDirPdf = fullfile(mainFolder,'outData/figures/wholecells/IN_single_page/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

Util.log('Loading nml with IN axon and dend tracings...')
nmlDir = fullfile(mainFolder,'data/tracings/wholecells/IN/');
file = dir(fullfile(nmlDir,'*.nml'));
skel = skeleton(fullfile(nmlDir,file.name)); % two trees per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% extract unique IDs
if strcmp(regionName, 'mk_L23') % cell types are not embedded in tree names
    skelDend = skel.keepTreeWithName('dend(?!_undefined)','regexp');
    inCellIds = nan(skelDend.numTrees,1);
    inCellTypes = cell(skelDend.numTrees,1);

    % load data from excel sheet
    xlsfile = config.wholeCellsExcel;
    [~,~,xl] = xlsread(xlsfile);
    xlTable = cell2table(xl);
    xlTable(1,:) = []; % remove headers

    % remove empty rows
    cellIds = xlTable.xl1;
    cellTypes = xlTable.xl2;
    multipolarType = xlTable.xl6;
    bipolarType = xlTable.xl7; %Note

    idxNan = cellfun(@(x) any(isnan(x)),cellTypes);
    cellIds(idxNan) = [];
    cellTypes(idxNan) = [];
    bipolarType(idxNan) = [];
    multipolarType(idxNan) = [];

    % extract bipolar flag for INs
    idxIN = cellfun(@(x) any(regexp(x,'IN')), cellTypes);
    bipolarFlag = bipolarType(idxIN);
    bipolarFlag = cell2mat(bipolarFlag);
    multipolarFlag = multipolarType(idxIN);
    multipolarFlag = cell2mat(multipolarFlag);

    for i=1:skelDend.numTrees
        curSkel = skelDend.deleteTrees(i,true);
        curId = regexpi(curSkel.names{1}, '(cell|soma|Cell|Soma)_(?<id>\d+)','names');
        curId = str2num(curId(1).id); % could be duplicate
        inCellIds(i) = curId;
        if bipolarFlag(i)
            inCellTypes{i} = 'BP';
        elseif multipolarFlag(i)
            inCellTypes{i} = 'MP';
        else
            inCellTypes{i} = 'UC';
        end
    end
else
    % remove axon trees
    idxDel = contains(skel.names, 'axon');
    skelDend = skel.deleteTrees(idxDel);
    % keep dend trees. Some may not have 'dend' label in tree name
    skelDend = skelDend.keepTreeWithName('(Cell|Soma)','regexpi');
    inCellIds = nan(skelDend.numTrees,1);
    inCellTypes = cell(skelDend.numTrees,1);
    for i=1:skelDend.numTrees
        curSkel = skelDend.deleteTrees(i,true);
        curId = regexpi(curSkel.names{1}, '(Cell|cell|soma)_(?<id>\d+)','names');
        curId = str2num(curId(1).id); % could be duplicate
        inCellIds(i) = curId;

        if contains(curSkel.names{1}, '_BP')
            inCellTypes{i} = 'BP';
        elseif contains(curSkel.names{1}, '_MP')
            inCellTypes{i} = 'MP';
        elseif contains(curSkel.names{1}, {'_UC', '_UP'})
            inCellTypes{i} = 'UC';
        elseif contains(curSkel.names{1}, '_border')
            inCellTypes{i} = 'UC';
        else 
            error('Cell type of IN %03d not defined',curId)
        end
    end
end


assert(numel(inCellIds) == skelDend.numTrees)
sprintf('Found %d IN cell Ids',numel(inCellIds))

% scale all spheres
scale = skel.scale./1000;
bbox = bsxfun(@times, bbox, config.scale(:)./1000);

% sort cell IDs from BP -> MP -> UC
[~, idxSort] = sort(inCellTypes);
inCellTypes = inCellTypes(idxSort);
inCellIds = inCellIds(idxSort);

%% plot cell bodies per type with dendrite
alphaVal = 1;

% number of subplots
if numel(inCellIds)>30
    widthDim = 6;
else
    widthDim = 4;
end
heightDim = ceil(numel(inCellIds)/widthDim);

% plot gallery
f = figure;
f.Units = 'centimeters';
f.Position = [1 1 21 29.7]; % A4 size
f.Color = 'white';

Util.log('Plotting INs for gallery:')
for idx = 1:numel(inCellIds)
    curCellId = inCellIds(idx);
    curCellType = inCellTypes{idx};
    curSkel =  skel.keepTreeWithName([sprintf('_%03d',curCellId),'(?!\d+)'],'regexp');

    [somaColor, dendColor, axonColor] = getColors(curCellType);
    
    idxTreeDend = curSkel.getTreeWithName('dend','regexp');
    idxTreeAxon = curSkel.getTreeWithName('axon','regexp');

    somaCoords = Util.getNodeCoordsWithComment(curSkel,{'soma','cellbody'},'regexpi', idxTreeDend);
    if isempty(somaCoords)
        warning('Empty soma %s', curSkel.names{:})
        somaLocs = [];
    else
        out.somaLocs(idx,:) = somaCoords;
        somaLocs = skel.setScale(somaCoords,scale);
    end


    curAx = subplot(heightDim, widthDim, idx);
    hold on

    % plot soma
    if contains(regionName, {'ex144','Mouse_A2','V2_L23','PPC_AK', 'ACC_JO', 'H6_4S_L23', 'PPC2'})
        if ~isempty(somaLocs)
            objectSomas = ...
                        scatter3(curAx,somaLocs(:,1),somaLocs(:,2),somaLocs(:,3),...
                        'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                        'MarkerFaceColor', somaColor(1:3));
        end
    end
    
    % plot dend tree
    curSkel.plot(idxTreeDend, dendColor, true, tubeSize,'','','','','',alphaVal);

    % plot axon
    if ~isempty(idxTreeAxon)
        curSkel.plot(idxTreeAxon, axonColor, true, tubeSize,'','','','','',alphaVal);
    end

    % switch view
    k = 1;
    switch k
        case 1 % xy
            Util.setView(1, true);
        case 2 % xz
            Util.setView(2, true);
        case 3 % yz
            Util.setView(3, true);
    end

    box on
    axis on
    axis equal
    set(gca,'XLim',bbox(1,:));
    set(gca,'YLim',bbox(2,:));
    set(gca,'ZLim',bbox(3,:));
    axis off
    box off
    set(gca,'color','white')

    if idx==1
    % scalebar
    scaleBarLength = 50; % um
    line(curAx,[bbox(1,2)-scaleBarLength, bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',2,'Color',[0,0,0]) % x axis
    %line([bbox(1,2), bbox(1,2)], [bbox(2,2)-scaleBarLength, bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % y axis
    %line([bbox(1,2), bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2)-scaleBarLength, bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % z axis
    end
    curTitle = title(curAx, sprintf('IN_%03d_%s',curCellId, curCellType),...
        'FontName','Arial','Units','normalized','Position',[0.5, -0.3, 0],...
        'HorizontalAlignment', 'center','Interpreter','none',....
        'EdgeColor', 'none','Color', [0,0,0],'FontSize',8);
end

outfile = fullfile(outDirPdf,sprintf('IN_gallery_%s.pdf',regionName));
export_fig(outfile,'-q101', '-nocrop', '-transparent','-m8');
Util.log('Saving file %s.', outfile);
close all

function [somaColor, dendColor, axonColor] = getColors(cellType)
    switch cellType
        case 'Pyr'
            somaColor = [1,0,1]; % m
            dendColor = [1,0,1]; % m
            axonColor = [0,158,115]/255; % green
        case 'BP'
            somaColor = [0,1,1]; % cyan
            dendColor = [0,1,1]; % cyan
            axonColor = [0,158,115]/255; % green
        case 'MP'
            somaColor = [107 76 154]./255; % purple
            dendColor = [107 76 154]./255; % purple
            axonColor = [0,158,115]/255; % green
        case {'IN','UC','Border'}
            somaColor = [0,0,0]; % black
            dendColor = [0,0,0]; % purple
            axonColor = [0,158,115]/255; % green
    end
end
