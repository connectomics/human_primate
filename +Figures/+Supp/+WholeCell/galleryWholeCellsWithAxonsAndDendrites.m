% export wholecells with axons and dendrites (where available)
info = Util.runInfo();
Util.showRunInfo(info);
somaSize = 175; tubeSize = 2;
do3D = false;
useOptimalBBox = true;

allRegionNames = {'mk_L23', 'H5_10_ext', 'H5_5_AMK', 'PPC2', 'Mouse_A2', ... % with axons + dendrites
                'ex144','V2_L23','ACC_JO','PPC_AK','Mk1_T2','H6_4S_L23'}; % with only dendrites

% use same bbox for all SBEM datasets to keep the scalebar unique, except for H5_10_ext
if useOptimalBBox
    optimalBbox = findOptimalBoxForGallery();
end

for idxRegion = 1:numel(allRegionNames)
    regionName = allRegionNames{idxRegion};
    
    Figures.setConfig; % set main region based folder
    outDirPdf = fullfile(mainFolder, 'results','axon','somaBased','gallery');
    mkdir(outDirPdf)

    Util.log('Reading cellbody attached axons')
    tracingFolder = fullfile(mainFolder,'data','tracings','axons','cellbody_attached');
    files = dir(fullfile(tracingFolder, '*.nml'));
    nml = fullfile(tracingFolder, files(1).name);
    skel = skeleton(nml); % two trees per cell

    % scale
    scale = skel.scale./1000;
    bbox = Util.convertWebknossosToMatlabBbox(config.bbox);
    bbox = bsxfun(@times, bbox, config.scale(:)./1000);
    
    if useOptimalBBox && not(strcmp(regionName,'H5_10_ext'))
        bbox = optimalBbox;
    end
    
    %% extract unique IDs
    skelAxonDend = skel.keepTreeWithName('axon|dend','regexp');
    skelAxonDendNames = skelAxonDend.names;
    allCellIds = cell(numel(skelAxonDendNames),1);
    allCellTypes = cell(numel(skelAxonDendNames),1);
    for idxCell=1:numel(skelAxonDendNames)
        curName = skelAxonDendNames{idxCell};
        curId = regexpi(curName, '(cell|Cell|soma)_(?<id>\d+)','names');
        %curId = str2num(curId(1).id); % could be duplicate
        allCellIds{idxCell} = curId(1).id;
        if contains(curName, {'pyr','Pyr'})
            allCellTypes{idxCell} = 'Pyr';
        elseif contains(curName, 'BP')
            allCellTypes{idxCell} = 'BP';
        elseif contains(curName, 'MP')
            allCellTypes{idxCell} = 'MP';
        elseif contains(curName, 'IN')
            allCellTypes{idxCell} = 'IN';
        end
    end

    % keep unique IDs and their types
    [allCellIds, idxU] = unique(allCellIds);
    allCellTypes = allCellTypes(idxU);
    
    Util.log('Plotting per cell gallery for %d cells:',numel(allCellIds))
    for idxCell = 1:numel(allCellIds)
        curCellId = allCellIds{idxCell};
        curCellType = allCellTypes{idxCell};
        curSkel =  skel.keepTreeWithName(sprintf('_%s_',curCellId),'regexp'); % '051' vs '0513'

        idxTreeDend = curSkel.getTreeWithName('dend','regexp');
        idxTreeAxon = curSkel.getTreeWithName('axon','regexp');
        
        % cosmetics
        alphaVal = 1;
        [somaColor, dendColor, axonColor] = getColors(curCellType);
        
        f = figure();
        f.Color = 'white';
        hold on;

        somaCoords = Util.getNodeCoordsWithComment(curSkel,{'cellbody'},'regexpi', idxTreeAxon);
        if isempty(somaCoords)
            error('Empty soma %s', curSkel.names{:})
        end
        somaLocs = skel.setScale(somaCoords,scale);

%         myelinLocs = Util.getNodeCoordsWithComment(curSkel,'(?<!un)myelin','regexpi');% negative lookbehind
%         if ~isempty(myelinLocs)
%                 myelinLocs = skel.setScale(myelinLocs,scale);
%         end
% 
%         endLocs = Util.getNodeCoordsWithComment(curSkel,'(real([\s_])?end|true([\s_])?end)','regexpi');
%         if ~isempty(endLocs)
%                 endLocs = skel.setScale(endLocs,scale);
%         end

        for k = 1 %[1,2]
            a(k) = subplot(1, 1, k);
            hold on

            % plot soma
            objectSomas = ...
                        scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3),...
                        somaSize,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                        'MarkerFaceColor', somaColor(1:3));
            % plot axon
            if ~isempty(idxTreeAxon)
                curSkel.plot(idxTreeAxon, axonColor, true, tubeSize,'','','','','',alphaVal);
            end
            % plot dend tree
            if ~isempty(idxTreeDend)
                curSkel.plot(idxTreeDend, dendColor, true, tubeSize,'','','','','',alphaVal);
            end
            
%             % check myelination start in axon
%             if ~isempty(myelinLocs)
%                 objectMyelin = ...
%                         scatter3(myelinLocs(:,1),myelinLocs(:,2),myelinLocs(:,3)...
%                         ,50,'x','MarkerEdgeColor', [1,1,1], ...
%                         'MarkerFaceColor', [1,1,1]);
%             end
% 
%             if ~isempty(endLocs)
%                 objectEnd = ...
%                         scatter3(endLocs(:,1),endLocs(:,2),endLocs(:,3)...
%                         ,50,'x','MarkerEdgeColor', [1,0,0], ...
%                         'MarkerFaceColor', [1,0,0]);
%             end

            % switch view
            switch k
                case 1 % xy
                    Util.setView(1, true);
                case 2 % xz
                    Util.setView(2, true);
                case 3 % yz
                    Util.setView(3, true);
            end
            if do3D
                % do once for sanity check then remove
                % axis on
                % box on
                % ax.BoxStyle = 'full';
                camorbit(10,0,'camera');
                camorbit(0,15,'data',[0 1 0]);
            end
            box on
            axis on
            axis equal
            set(gca,'XLim',bbox(1,:));
            set(gca,'YLim',bbox(2,:));
            set(gca,'ZLim',bbox(3,:));
            set(gca,'color','none')

        end
    %    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
         % scalebar
         scaleBarLength = 50; % um
         line(a(k),[bbox(1,2)-scaleBarLength, bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis
         %line([bbox(1,2), bbox(1,2)], [bbox(2,2)-scaleBarLength, bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % y axis
         %line([bbox(1,2), bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2)-scaleBarLength, bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % z axis

        annotation( ...
                f,...
                'textbox', [0, 0.01,1,0.1], ...
                'String', sprintf('%s_%s',curCellType,curCellId), ...
                'EdgeColor', 'none',...
                'Color', [0,0,0], ...
                'Interpreter','none',...
                'HorizontalAlignment', 'center')

        outfile = fullfile(outDirPdf, sprintf('%s_%s_%s.eps',regionName,curCellType,curCellId));
        export_fig(outfile,'-q101', '-transparent', '-nocrop', '-m8');
        Util.log('Saving file %s.', outfile);
        close(f)
    end
end
function [somaColor, dendColor, axonColor] = getColors(cellType)
    switch cellType
        case 'Pyr'
            somaColor = [1,0,1]; % m
            dendColor = [1,0,1]; % m
            axonColor = [0,158,115]/255; % green
        case 'BP'
            somaColor = [0,1,1]; % cyan
            dendColor = [0,1,1]; % cyan
            axonColor = [0,158,115]/255; % green
        case 'MP'
            somaColor = [107 76 154]./255; % purple
            dendColor = [107 76 154]./255; % purple
            axonColor = [0,158,115]/255; % green
        case 'IN'
            somaColor = [0,0,0]; % black
            dendColor = [0,0,0]; % purple
            axonColor = [0,158,115]/255; % green
    end
end
function optimalBbox = findOptimalBoxForGallery()
    allRegionNames = {'mk_L23', 'H5_5_AMK', 'PPC2', 'Mouse_A2', ... % with axons + dendrites
                    'ex144','V2_L23','ACC_JO','PPC_AK','Mk1_T2','H6_4S_L23'}; % with only dendrites
    % exclude H5_10_ext
    bbox = [];            
    for idxRegion=1:numel(allRegionNames)
        regionName = allRegionNames{idxRegion};
        Figures.setConfig; % set main region based folder
        box = Util.convertWebknossosToMatlabBbox(config.bbox);
        box = bsxfun(@times, box, config.scale(:)./1000);
        bbox(idxRegion,:) = box(:)';
    end

    optimalBbox  = [min(bbox(:,1)), max(bbox(:,4)); ...
                    min(bbox(:,2)), max(bbox(:,5)); ...
                    min(bbox(:,3)), max(bbox(:,6))]; 
end
