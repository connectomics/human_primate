clear; clc;
if Util.isLocal
    outDir = 'C:\Users\loombas\Downloads\u\data\screening\figures\meeting_notes\2022_03_16\figure_panels\human_vs_mouse_proximal_synapses_estimation';
    mkdir(outDir)       
else
    Figures.setConfig;
    outDir = fullfile(outputDir,'figures','fig2','panels');
    mkdir(outDir)
    clear outputDir
end

%% plot compartment wise input
dataTable = readtable(fullfile(outDir, 'human_vs_mouse_proximal_synapses_estimation_v07.xlsx'));
dataTypes = dataTable.species_compartments(1:16);

%%
curType = 'total_synapses_per_compartment';
curTypeIdx = contains(dataTypes, curType);

mouseData =  extractDataPerComparment(dataTable, 'Mouse', curTypeIdx);
humanData =  extractDataPerComparment(dataTable, 'Human', curTypeIdx);

fig = figure;
ax = subplot(1,2,1);
hold on;
allData = [struct2array(mouseData); struct2array(humanData)];
bar(ax, allData,'stacked')

ax.XAxis.Limits = [0,3];
ax.XAxis.TickValues = 1:2;
ax.XAxis.TickLabels = {'Mouse', 'Human'};
ax.YAxis.Label.String = 'Number of synapses';
ax.YAxis.Limits = [0,20000];
ax.YAxis.TickValues = 0:4000:20000;
ax.YAxis.MinorTick = 'on';
curLeg = legend(fieldnames(mouseData), 'Location','best');
set(curLeg, 'box','off','Interpreter','none')
Util.setPlotDefault(ax);

% plot soma, ais, prox basal, prox apical
ax = subplot(1,2,2);
curFields = {'AIS','soma','proximal_basal','proximal_apical'};
idx = contains(fieldnames(mouseData), curFields);
allData = [struct2array(mouseData); struct2array(humanData)];
allData = allData(:,idx);
bar(ax, allData,'stacked')

ax.XAxis.Limits = [0,3];
ax.XAxis.TickValues = 1:2;
ax.XAxis.TickLabels = {'Mouse', 'Human'};
ax.YAxis.Label.String = 'Number of synapses';
ax.YAxis.Limits = [0,400];
ax.YAxis.TickValues = 0:100:1000;
ax.YAxis.MinorTick = 'on';
curLeg = legend(curFields, 'Location','best');
set(curLeg, 'box','off','Interpreter','none')
Util.setPlotDefault(ax);

outfile = fullfile(outDir, sprintf('compartment_wise_input_mouse_vs_human_%s',curType));
if not(Util.isLocal)
    export_fig(strcat(outfile,'.pdf'),'-q101', '-nocrop', '-transparent');
else
    saveas(gcf, strcat(outfile,'.png'));
end

%% 
curType = 'exc_total_per_compartment';
curTypeIdx = contains(dataTypes, curType);

mouseData =  extractDataPerComparment(dataTable, 'Mouse', curTypeIdx);
humanData =  extractDataPerComparment(dataTable, 'Human', curTypeIdx);

fig = figure;
ax = subplot(1,2,1);
hold on;
allData = [struct2array(mouseData); struct2array(humanData)];
bar(ax, allData,'stacked')

ax.XAxis.Limits = [0,3];
ax.XAxis.TickValues = 1:2;
ax.XAxis.TickLabels = {'Mouse', 'Human'};
ax.YAxis.Label.String = 'Number of excitatory synapses';
ax.YAxis.Limits = [0,20000];
ax.YAxis.TickValues = 0:4000:20000;
curLeg = legend(fieldnames(mouseData), 'Location','best');
set(curLeg, 'box','off','Interpreter','none')
Util.setPlotDefault(ax);

% plot soma, ais, prox basal, prox apical
ax = subplot(1,2,2);
curFields = {'AIS','soma','proximal_basal','proximal_apical'};
idx = contains(fieldnames(mouseData), curFields);
allData = [struct2array(mouseData); struct2array(humanData)];
allData = allData(:,idx);
bar(ax, allData,'stacked')

ax.XAxis.Limits = [0,3];
ax.XAxis.TickValues = 1:2;
ax.XAxis.TickLabels = {'Mouse', 'Human'};
ax.YAxis.Label.String = 'Number of excitatory synapses';
ax.YAxis.Limits = [0,400];
ax.YAxis.TickValues = 0:100:1000;
curLeg = legend(curFields, 'Location','best');
set(curLeg, 'box','off','Interpreter','none')
Util.setPlotDefault(ax);

outfile = fullfile(outDir, sprintf('compartment_wise_input_mouse_vs_human_%s',curType));
if not(Util.isLocal)
    export_fig(strcat(outfile,'.pdf'),'-q101', '-nocrop', '-transparent');
else
    saveas(gcf, strcat(outfile,'.png'));
end

%%
curType = 'inh_total_per_compartment';
curTypeIdx = contains(dataTypes, curType);

mouseData =  extractDataPerComparment(dataTable, 'Mouse', curTypeIdx);
humanData =  extractDataPerComparment(dataTable, 'Human', curTypeIdx);

fig = figure;
ax = subplot(1,2,1);
hold on;
allData = [struct2array(mouseData); struct2array(humanData)];
bar(ax, allData,'stacked')

ax.XAxis.Limits = [0,3];
ax.XAxis.TickValues = 1:2;
ax.XAxis.TickLabels = {'Mouse', 'Human'};
ax.YAxis.Label.String = 'Number of inhibitory synapses';
ax.YAxis.Limits = [0,2000];
ax.YAxis.TickValues = 0:500:2000; 
ax.YAxis.MinorTick = 'on';
curLeg = legend(fieldnames(mouseData), 'Location','best');
set(curLeg, 'box','off','Interpreter','none')
Util.setPlotDefault(ax);

% plot  ais,soma, prox basal, prox apical
ax = subplot(1,2,2);
curFields = {'AIS','soma','proximal_basal','proximal_apical'};
idx = contains(fieldnames(mouseData), curFields);
allData = [struct2array(mouseData); struct2array(humanData)];
allData = allData(:,idx);
bar(ax, allData,'stacked')

ax.XAxis.Limits = [0,3];
ax.XAxis.TickValues = 1:2;
ax.XAxis.TickLabels = {'Mouse', 'Human'};
ax.YAxis.Label.String = 'Number of inhibitory synapses';
ax.YAxis.Limits = [0,400];
ax.YAxis.TickValues = 0:100:1000;
ax.YAxis.MinorTick = 'on';
curLeg = legend(curFields, 'Location','best');
set(curLeg, 'box','off','Interpreter','none')
Util.setPlotDefault(ax);

outfile = fullfile(outDir, sprintf('compartment_wise_input_mouse_vs_human_%s',curType));

if not(Util.isLocal)
    export_fig(strcat(outfile,'.pdf'),'-q101', '-nocrop', '-transparent');
else
    saveas(gcf, strcat(outfile,'.png'));
end

%%
function out = extractDataPerComparment(dataTable, speciesName, curTypeIdx)
    curData = dataTable(curTypeIdx,:);

    out.AIS = convertDataType(curData.(sprintf('%s_AIS',speciesName)));
    out.soma = convertDataType(curData.(sprintf('%s_soma',speciesName)));
    out.proximal_basal = convertDataType(curData.(sprintf('%s_proximal_basal',speciesName)));
    out.proximal_apical = convertDataType(curData.(sprintf('%s_proximal_apical',speciesName)));
    out.oblique = convertDataType(curData.(sprintf('%s_oblique',speciesName)));
    out.tuft = convertDataType(curData.(sprintf('%s_tuft',speciesName)));
    out.distal = convertDataType(curData.(sprintf('%s_distal',speciesName)));
end

function out = convertDataType(d)
    if isa(d, 'double')
        out = d;
    elseif iscell(d)
        d = cell2mat(d);
        if isa(d, 'double')
            out = d;
        elseif isa(d,'char')
            out = str2double(d);
        end
    end
end