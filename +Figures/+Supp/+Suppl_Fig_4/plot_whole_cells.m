%% Plotting cells from webknossos - Adapted from Sahil Loomba
% 
% on gaba: 
addpath(genpath('/gaba/u/natalieheike/code/auxiliaryMethods'));
addpath(genpath('/gaba/u/natalieheike/code/Export_figures'));
nmlDir = '/gaba/u/natalieheike/code/H5_10_analysis/tracings/H5_10_ext';
outDirPdf = fullfile('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_ext/whole_cells/illustrations');



% % local
% addpath(genpath('/Users/natalieheike/U/code/auxiliaryMethods')); 
% addpath(genpath('/Users/natalieheike/U/code/Export_figures'));
% nmlDir = '/Users/natalieheike/U/code/H5_10_analysis/tracings/H5_10_ext';
% outDirPdf = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_ext/whole_cells/illustrations');

skelAll = skeleton(fullfile(nmlDir, 'H5_ext_whole_cells.nml'));

somaSize = 50; tubeSize = 2;
do3D = false;

skelAll = skelAll.keepTreeWithName('Cell','regexp');

config.scaleEM = skelAll.scale;

%% plot cell bodies per type with dendrite
alphaVal = 1;
somaColor = [0.5,0.5,0.5]; % grey
dendColor = [0,0,1]; % cyan
axonColor = [0,158,115]/255; % green

Util.log('Plotting cells for gallery:')
for idx = 1:skelAll.numTrees;
    
    curSkel = skelAll.deleteTrees(idx, true);

    idxTreeDend = curSkel.getTreeWithName('dend','regexp');
    idxTreeAxon = curSkel.getTreeWithName('axon','regexp');
    idxName = curSkel.names{:};

    f = figure();
%     f.Color = 'black';
    hold on;
    

        % plot dend tree
        curSkel.plot(idxTreeDend, dendColor, true, tubeSize,'','','','','',alphaVal);
        out.skelsDend{idx} = curSkel.deleteTrees(idxTreeDend, true);

        % plot axon
        if ~isempty(idxTreeAxon)
            curSkel.plot(idxTreeAxon, axonColor, true, tubeSize,'','','','','',alphaVal);
            out.skelsAxon{idx} = curSkel.deleteTrees(idxTreeAxon, true);
        else
            out.skelsAxon{idx} = [];
        end
        
        set(gca, 'YDir','reverse') % flip vertically (plots skeleton in XY-axis as on wk)
        
        box on
%         axis on
%         axis equal
%        
%         set(gca,'color','k')

%    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
    outfile = fullfile(outDirPdf, sprintf('Whole_Cell_Illustration_%s.png', idxName));
    export_fig(fullfile(outDirPdf, sprintf('Whole_Cell_Illustration_%s.eps', idxName)),'-q101', '-nocrop','-transparent')
    
    close all
end
