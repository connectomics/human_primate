%% Written by NH
%%Version05_02: plot true endings and branchpoint in one plot and eods-endings
%%in plot below with reversed y-axis, minor adjustments
% 
% % on gaba: 
addpath(genpath('/gaba/u/natalieheike/code/auxiliaryMethods'));
addpath(genpath('/gaba/u/natalieheike/code/Export_figures'));
dataDir = ('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_comb/whole_cells/');
outDirPdf = fullfile('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_comb/whole_cells/plots/v05_2');

% % local
% addpath(genpath('/Users/natalieheike/U/code/auxiliaryMethods')); 
% addpath(genpath('/Users/natalieheike/U/code/Export_figures'));
% dataDir = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_comb/whole_cells');
% outDirPdf = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_comb/whole_cells/plots/v05_2');

%% BASALS
% get Data for distances between soma and BPs

dendSectionType = 'basals';

    % import data from xls sheets for distances Soma to BPs
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_1 = readtable(filename);
        
        % distances to BP1
        BP1s = T_1.CBtoBP1;
        BP1s = unique(BP1s); % delete redundant distances
        BP1s = nonzeros(BP1s);

        % distances to BP2
        BP2s = T_1.CBtoBP2;
        BP2s = unique(BP2s); % delete redundant distances
        BP2s = nonzeros(BP2s);
        
        % distances to BP3
        BP3s = T_1.CBtoBP3;
        BP3s = unique(BP3s); % delete redundant distances
        BP3s = nonzeros(BP3s);

        % distances to BP4
        BP4s = T_1.CBtoBP4;
        BP4s = unique(BP4s); % delete redundant distances
        BP4s = nonzeros(BP4s);

        % distances to BP5
        BP5s = T_1.CBtoBP5;
        BP5s = unique(BP5s); % delete redundant distances
        BP5s = nonzeros(BP5s);
        
        BPsAll = [BP1s; BP2s; BP3s; BP4s; BP5s];
    
        
    % import data from xls sheets with distances of true endings
    filename = fullfile(dataDir, 'pathlength_estimation', sprintf('pl_tend_%s.xls', dendSectionType));
    T_3 = readtable(filename);

        %PL of true endings
        plTEND = T_3.length; 

            
            
    % Make table for eods endings only
    eods = find(strcmp('(eods|zzz)', T_1.ending));
    T_eods = table([0],[0], ...
            'VariableNames', {'CBtoEnd', 'bp_order',});
        
    for i = 1:numel(eods);
        id = eods(i);
        T_eods.CBtoEnd(i)= T_1.CBtoEnd(id);
        T_eods.bp_order(i) = T_1.bp_order(id);
    end

    plEODS = T_eods.CBtoEnd;
    
   %Save distances for later
    plTENDBasal = plTEND;
    BPsAllBasal = BPsAll;
    plEODSBasal = plEODS;

% Make histogram with distance of true endings and branchpoints in top
% plot, and eods endings in bottom plot with reversed y-axis

        fig = figure();
        ax = axes(fig);
        hold on;
        
        subplot(2,1,1);
        x1 = plTEND;
        h1 = histogram(x1, 'DisplayStyle', 'stairs', 'EdgeColor', 'k', 'LineWidth', 1, 'BinWidth',25);
        xlim([0 500]);
        ylim([0 80]);
        hold on;
        x3 = BPsAll;
        h3 = histogram(x3,'DisplayStyle', 'stairs','EdgeColor', 'b', 'LineWidth', 1, 'BinWidth', 25);
        legend('true ending', 'branchpoints');
        suptitle('basal dendrites');
        xlabel('pathlength from soma [um]');
        ylabel('Frequency');
        set(gca, 'TickDir', 'out', 'color','none');
        box off
        
        subplot(2,1,2); 
        x2 = plEODS;
        h2 = histogram(x2,'DisplayStyle', 'stairs','EdgeColor', 'k', 'LineWidth', 1, 'BinWidth', 25);
        xlim([0 500]);
        ylim([0 80]);
        legend('eods');
        ylabel('Frequency');
        set(gca,'TickDir', 'out', 'XAxisLocation','top','YAxisLocation','left','ydir','reverse');
        set(gca, 'color','none');
        box off
        

% export_fig(fullfile(outDirPdf,'histo_stairs_basals_y_reversed.png'),'-q101', '-nocrop','-transparent', '-m8');
export_fig(fullfile(outDirPdf, 'histo_stairs_basals_y_reversed.eps'),'-q101', '-nocrop','-transparent');

close all
        
%% APICALS

% get Data for distances between soma and BPs
dendSectionType = 'apicals';

    % import data from xls sheets for distances Soma to BPs
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_1 = readtable(filename);
        
        % distances to BP1
        BP1s = T_1.BIFURtoBP1;
        BP1s = unique(BP1s); % delete redundant distances
        BP1s = nonzeros(BP1s);

        % distances to BP2
        BP2s = T_1.BIFURtoBP2;
        BP2s = unique(BP2s); % delete redundant distances
        B21s = nonzeros(BP2s);

        % distances to BP3
        BP3s = T_1.BIFURtoBP3;
        BP3s = unique(BP3s); % delete redundant distances
        BP3s = nonzeros(BP3s);

        % distances to BP4
        BP4s = T_1.BIFURtoBP4;
        BP4s = unique(BP4s); % delete redundant distances
        BP4s = nonzeros(BP4s);

        % distances to BP5
        BP5s = T_1.BIFURtoBP5;
        BP5s = unique(BP5s); % delete redundant distances
        BP5s = nonzeros(BP5s);
        
        % distances to BP6
        BP6s = T_1.BIFURtoBP6;
        BP6s = unique(BP6s); %delete redundant distances
        BP6s = nonzeros(BP6s);
        
        BPsAll = [BP1s; BP2s; BP3s; BP4s; BP5s; BP6s];
    
     
    % import data from xls sheets with distances of true endings
    filename = fullfile(dataDir, 'pathlength_estimation', sprintf('pl_tend_%s.xls', dendSectionType));
    T_3 = readtable(filename);

        %PL of true endings
        plTEND = T_3.length; 

        %PL of last BP to true ending
        BP_TEND = T_3.bplast_to_end;

    
    % Make table for eods endings only
    eods = find(strcmp('(eods|zzz)', T_1.ending));
    T_eods = table([0],[0], ...
            'VariableNames', {'BIFURtoEND', 'bp_order',});
        
    for i = 1:numel(eods);
        id = eods(i);
        T_eods.BIFURtoEND(i)= T_1.BIFURtoEND(id);
        T_eods.bp_order(i) = T_1.bp_order(id);
    end

    plEODS = T_eods.BIFURtoEND;
    
    

% Make histogram with distance of true endings and branchpoints in top
% plot, and eods endings in bottom plot with reversed y-axis
        fig = figure();        
        ax = axes(fig);
        hold on;
        
        subplot(2,1,1);
        x1 = plTEND;
        h1 = histogram(x1, 'DisplayStyle', 'stairs', 'EdgeColor', 'k', 'LineWidth', 1, 'BinWidth',25);
        xlim([0 800]);
        ylim([0 30]);
        hold on;
        x3 = BPsAll;
        h3 = histogram(x3,'DisplayStyle', 'stairs','EdgeColor', 'b', 'LineWidth', 1, 'BinWidth', 25);
        legend('true ending', 'branchpoints');
        suptitle('apical dendrites');
        xlabel('pathlength from main bifurcation [um]');
        ylabel('Frequency');
        set(gca, 'TickDir', 'out', 'color','none');
        box off
        
        subplot(2,1,2); 
        x2 = plEODS;
        h2 = histogram(x2,'DisplayStyle', 'stairs','EdgeColor', 'k', 'LineWidth', 1, 'BinWidth', 25);
        xlim([0 800]);
        ylim([0 30]);
        legend('eods');
        ylabel('Frequency');
        set(gca,'TickDir', 'out', 'XAxisLocation','top','YAxisLocation','left','ydir','reverse');
        set(gca, 'color','none');
        box off       

% export_fig(fullfile(outDirPdf, 'histo_stairs_apicals_y_reversed.png'),'-q101', '-nocrop','-transparent', '-m8');
export_fig(fullfile(outDirPdf, 'histo_stairs_apicals_pl_y_reversed.eps'),'-q101', '-nocrop','-transparent');

close all

%% OBLIQUES
        
dendSectionType = 'obliques';

    % import data from xls sheets for distances Soma to BPs
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_1 = readtable(filename);
        
        % distances to BP1
        BP1s = T_1.rootToBP1;
        BP1s = unique(BP1s); % delete redundant distances
        BP1s = nonzeros(BP1s);

        % distances to BP2
        BP2s = T_1.rootToBP2;
        BP2s = unique(BP2s); % delete redundant distances
        BP2s = nonzeros(BP2s);

        % distances to BP3
        BP3s = T_1.rootToBP3;
        BP3s = unique(BP3s); % delete redundant distances
        BP3s = nonzeros(BP3s);

        % distances to BP4
        BP4s = T_1.rootToBP4;
        BP4s = unique(BP4s); % delete redundant distances
        BP4s = nonzeros(BP4s);
        
        BPsAll = [BP1s; BP2s; BP3s; BP4s];
           
    % import data from xls sheets with distances of true endings
    filename = fullfile(dataDir, 'pathlength_estimation', sprintf('pl_tend_%s.xls', dendSectionType));
    T_3 = readtable(filename);

        %PL of true endings
        plTEND = T_3.length; 

        %PL of last BP to true ending
        BP_TEND = T_3.bplast_to_end;
        
        
    % Make table for eods endings only
    eods = find(strcmp('(eods|zzz)', T_1.ending));
    T_eods = table([0],[0], ...
            'VariableNames', {'rootToEnd', 'bp_order',});
        
    for i = 1:numel(eods);
        id = eods(i);
        T_eods.rootToEnd(i)= T_1.rootToEnd(id);
        T_eods.bp_order(i) = T_1.bp_order(id);
    end

    plEODS = T_eods.rootToEnd;
    
    
% Make histogram with distance of true endings and branchpoints in top
% plot, and eods endings in bottom plot with reversed y-axis
        fig = figure();
        ax = axes(fig);
        hold on;
        
        subplot(2,1,1);
        x1 = plTEND;
        h1 = histogram(x1, 'DisplayStyle', 'stairs', 'EdgeColor', 'k', 'LineWidth', 1, 'BinWidth',25);
        xlim([0 500]);
        ylim([0 50]);
        hold on;
        x3 = BPsAll;
        h3 = histogram(x3,'DisplayStyle', 'stairs','EdgeColor', 'b', 'LineWidth', 1, 'BinWidth', 25);
        legend('true ending', 'branchpoints');
        suptitle('oblique dendrites');
        xlabel('pathlength from root [um]');
        ylabel('Frequency');
        set(gca, 'TickDir', 'out', 'color','none');
        box off
        
        subplot(2,1,2); 
        x2 = plEODS;
        h2 = histogram(x2,'DisplayStyle', 'stairs','EdgeColor', 'k', 'LineWidth', 1, 'BinWidth', 25);
        xlim([0 500]);
        ylim([0 50]);
        legend('eods');
        ylabel('Frequency');
        set(gca,'TickDir', 'out', 'XAxisLocation','top','YAxisLocation','left','ydir','reverse');
        set(gca, 'color','none');
        box off
        
% export_fig(fullfile(outDirPdf, 'histo_stairs_oblqiue_y_reversed.png'),'-q101', '-nocrop','-transparent', '-m8');
export_fig(fullfile(outDirPdf, 'histo_stairs__oblique_y_reversed.eps'),'-q101', '-nocrop','-transparent');


close all
