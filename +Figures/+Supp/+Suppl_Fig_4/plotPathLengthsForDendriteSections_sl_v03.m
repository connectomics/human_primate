%%written by SL, small changes by NH
%% v03: pyramidal cell = magenta, no distances shown anymore, crosses for bps, triangle for true endings

% on gaba: 
addpath(genpath('/gaba/u/natalieheike/code/auxiliaryMethods'));
addpath(genpath('/gaba/u/natalieheike/code/Export_figures'));

outDirPdf = '/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_ext/whole_cells/illustrations/measured/v03';
nmlDir = '/gaba/u/natalieheike/code/H5_10_analysis/tracings/H5_10_ext';

% % local
% addpath(genpath('/Users/natalieheike/U/code/auxiliaryMethods')); 
% addpath(genpath('/Users/natalieheike/U/code/Export_figures'));
% nmlDir = '/Users/natalieheike/U/code/H5_10_analysis/tracings/H5_10_ext';
% outDirPdf = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_ext/whole_cells/illustrations/measured/v03')


regionName = 'H5_10_ext';
config = struct;
 
        datasetName = ' H5_10_270um_L23_NH_NJ_extended_Aug22_2021_v2';
        ownerName = 'NJ';
%         config.wholeCellsExcel = fullfile(mainFolder,'data','H5_10_ext_celltype_classification.xlsx');
        config.datasetName = datasetName;
        config.scale = [4, 4, 38];
        config.bbox = [100000, 160169, 290, 175000, 125000, 1550];
        config.bbox_center = [100000, 160169, 290, 175000, 125000, 1550];
        
regionName = 'H5_10_ext';
bbox = [100, 1200; 200, 1500; 0, 400];
% 
% 
% regionName = 'H5_10';
% bbox = [400, 1700; 450, 1200; 0, 200];

skelAll = skeleton(fullfile(nmlDir, 'H5_ext_Cell_008_pyr_whole.nml'));

dendSectionType = 'Cell'; startCommentType = 'cellbody';

scale = skelAll.scale ./ 1000;
idxKeep = contains(skelAll.names, dendSectionType);
skel = skelAll.deleteTrees(idxKeep, true);

%%
rng(0)
allColors = Util.getRangedColors(0.7,1, 0 ,0.4, 0,0.5, skel.numTrees); % magenta red range
for idxTree = 1:skel.numTrees
    curSkel = skel.deleteTrees(idxTree, true);
    curName = curSkel.names{:};
    startIdx = curSkel.getNodesWithComment(startCommentType,1,'regexp'); % comment where the tree will start
    startId = str2double(curSkel.nodesAsStruct{1}(startIdx).id);

    f = figure();
    f.Color = 'none';
    hold on;

    tubeSize = 2;     fontSize = 12;
    curColor = 'm'
    
    % plot skeleton
    curSkel.plot(1, curColor, true, tubeSize);
    
    % plot eods as open circles
    endCommentType = '(eods|zzz)';
    endCommentMarker = 'o';
    doForThisEnding(endCommentType, endCommentMarker, curSkel, fontSize, scale, startId);     

    % plot true endings with triangles
    endCommentType = 'true ending';
    endCommentMarker = '>';
    doForThisEnding(endCommentType, endCommentMarker, curSkel, fontSize, scale, startId); 
    
    % plot bps with crosses
    endCommentType = 'bp';
    endCommentMarker = 'x';
    doForBPs(endCommentType, endCommentMarker, curSkel, fontSize, scale, startId); 


    % cosmetics   
    Util.setView(1, true); % xy
    daspect([1,1,1])
    Util.setPlotDefault();
    title(curName, 'Interpreter','none')
    
    set(gcf,'Position',get(0, 'Screensize')) 
    
    %add scalebar
    hold on;
    box on
    axis on
    axis equal
    set(gca,'XLim',bbox(1,:));
    set(gca,'YLim',bbox(2,:));
    set(gca,'ZLim',bbox(3,:));
    set(gca,'color','none')


    scaleBarLength = 50; % um
    line([bbox(1,2)-scaleBarLength, bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[0,0,0]) % x axis
         
box off
axis off     
        
    
%     outfile = fullfile(outDirPdf, [curName,'_endings_with_distances.png']);
%     savefig(fullfile(outDirPdf, [curName,'_endings_with_distances.png']));
%     export_fig(outfile,'-q101', '-nocrop', '-m8');
    export_fig(fullfile(outDirPdf, [curName,'_endings_with_distances.eps']),'-q101', '-nocrop','-transparent')
    close all

end

function doForThisEnding(endCommentType, endCommentMarker, curSkel, fontSize, scale, startId)
    endIdx = curSkel.getNodesWithComment(endCommentType,1,'regexp');
    endLocs = curSkel.nodes{1}(endIdx,1:3);
    endLocs = curSkel.setScale(endLocs,scale);

    for idxNode = 1:numel(endIdx)
        curEndIdx = endIdx(idxNode);
        endId = str2double(curSkel.nodesAsStruct{1}(curEndIdx).id);

%         [~, ~, pL] = curSkel.getShortestPath(startId, endId); %nm
%         pL = pL./1e3;
%         curText = sprintf('%.0f', pL); % '\mum'];

        curLocs = endLocs(idxNode,:);
        % plot
        objectEndLocs = ...
                    scatter3(curLocs(:,1),curLocs(:,2),curLocs(:,3),...
                    150,'filled', 'Marker',endCommentMarker,'MarkerEdgeColor', 'k', ...
                    'MarkerFaceColor', 'none', 'LineWidth',1);
    end
    
end

function doForBPs(endCommentType, endCommentMarker, curSkel, fontSize, scale, startId)
    endIdx = curSkel.getNodesWithComment(endCommentType,1,'regexp');
    endLocs = curSkel.nodes{1}(endIdx,1:3);
    endLocs = curSkel.setScale(endLocs,scale);

    for idxNode = 1:numel(endIdx)
        curEndIdx = endIdx(idxNode);
        endId = str2double(curSkel.nodesAsStruct{1}(curEndIdx).id);

%         [~, ~, pL] = curSkel.getShortestPath(startId, endId); %nm
%         pL = pL./1e3;
%         curText = sprintf('%.0f', pL); % '\mum'];
%         curText = 'BP';

        curLocs = endLocs(idxNode,:);
        % plot
        objectEndLocs = ...
                    scatter3(curLocs(:,1),curLocs(:,2),curLocs(:,3),...
                    150,'filled', 'Marker',endCommentMarker,'MarkerEdgeColor', 'b', ...
                    'MarkerFaceColor', 'none', 'LineWidth',1); 
    end
    
end


