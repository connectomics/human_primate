function cx_data_axons_all = loadAxonDataFromAllRegions()

    allRegionNames = {'ex144', 'Mouse_A2','V2_L23', 'PPC_AK', 'ACC_JO',...
                'mk_L23', 'Mk1_T2', ...
                'H5_10_L23', 'H6_4S_L23'};
    cx_data_axons_all = table;        
    for i=1:numel(allRegionNames)
        curRegion = allRegionNames{i};
        cx_data_axons = doForThisRegion(curRegion);
        col1 = repelem({curRegion},height(cx_data_axons),1);
        col1 = table(col1,'VariableNames',{'Datasets'});
        cx_data_axons = [col1, cx_data_axons];
        cx_data_axons_all = cat(1,cx_data_axons_all, cx_data_axons);
        clear ccx_data_axons
    end
end
function cx_data_axons = doForThisRegion(regionName)
    nSynThr = 1;
    Figures.setConfig
    Util.log('load seeded axon data')
    cx_path_axons = config.cx_path_axons;
    cx_data_axons = readtable(cx_path_axons);
    cx_data_axons(cx_data_axons.total<nSynThr,:)=[];   
end