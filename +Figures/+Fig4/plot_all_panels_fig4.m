% Written by
%  Sahil Loomba <sahil.loomba@brain.mpg.de>
%% panel A
% Spine-target fraction of ExN spine vs shaft seeded axons.
clear; clc;
% load data
cx_data = Figures.Fig4.loadAxonDataFromAllRegions;

thr = 6;
idx = contains(cx_data.Datasets, 'ex144') & contains(cx_data.name, {'seed_sph','sph_seed','seed_sh','sh_seed'}) & cx_data.total>=thr;
ex144 = cx_data(idx,:);
sprintf('ex144: synThr>=%d, %d axons with %d synapses', thr, height(ex144),sum(ex144.total))

ex144SphSeeded = ex144(contains(ex144.name, {'seed_sph','sph_seed'}),:);
ex144ShSeeded = ex144(contains(ex144.name, {'seed_sh','sh_seed'}),:);

thr = 6;
idx = contains(cx_data.Datasets, 'H5_10_L23') & contains(cx_data.name, {'seed_sph','sph_seed','seed_sh','sh_seed'}) & cx_data.total>=thr;
H5 = cx_data(idx,:);
sprintf('H5: synThr>=%d, %d axons with %d synapses', thr, height(H5),sum(H5.total))
H5SphSeeded = H5(contains(H5.name, {'seed_sph','sph_seed'}),:);
H5ShSeeded = H5(contains(H5.name, {'seed_sh','sh_seed'}),:);

sprintf('Total %d seeded axons with %d output synapses', height(ex144)+height(H5), sum([ex144.total; H5.total]))

% plot 
rng(0)
widthS = 1; pos = [0,2];

fig = figure;
fig.Color = 'white';
ax1 = subplot(1,2,1);
hold on
data = 100*ex144SphSeeded.sph ./ ex144SphSeeded.total;
scatter(widthS.*rand(numel(data),1)+pos(1), data,...
        150,'x', 'MarkerFaceColor', 'm','MarkerEdgeColor', 'm','LineWidth',2);
data = 100*ex144ShSeeded.sph ./ ex144ShSeeded.total;
scatter(widthS.*rand(numel(data),1)+pos(2), data,...
        150,'x', 'MarkerFaceColor', 'k','MarkerEdgeColor', 'k','LineWidth',2);
doCosmetics(ax1)
ylabel({'Fraction of output synapses','onto spine along axon (%)'})

ax2 = subplot(1,2,2);
hold on
data = 100*H5SphSeeded.sph ./ H5SphSeeded.total;
scatter(widthS.*rand(numel(data),1)+pos(1), data,...
        150,'x', 'MarkerFaceColor', 'm','MarkerEdgeColor', 'm','LineWidth',2);
data = 100*H5ShSeeded.sph ./H5ShSeeded.total;
scatter(widthS.*rand(numel(data),1)+pos(2), data,...
        150,'x', 'MarkerFaceColor', 'k','MarkerEdgeColor', 'k','LineWidth',2);
doCosmetics(ax2)

%% panel B-E
% see HNHP.Manual.plotInputData.m

%% utility functions panel A
function doCosmetics(ax)
    ticklen = [0.025, 0.025];
    ax.YAxis.MinorTick = 'off';
    ax.YAxis.TickValues = 0:10:100;
    ax.YAxis.Limits = [0,100];
    ax.XAxis.Visible = 'off';
    ax.LineWidth = 2;
    ax.TickLength = ticklen;
    Util.setPlotDefault(gca,'','');
end