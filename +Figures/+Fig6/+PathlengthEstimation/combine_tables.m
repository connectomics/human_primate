%% Script to combine Tables from H5 and H5_ext
%% Written by NH
% % % on gaba: 
% addpath(genpath('/gaba/u/natalieheike/code/auxiliaryMethods'));
% addpath(genpath('/gaba/u/natalieheike/code/Export_figures'));
% dataDir = ('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_ext/whole_cells');
% outDirPdf = fullfile('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_comb/whole_cells');

%%% local
addpath(genpath('/Users/natalieheike/U/code/auxiliaryMethods')); 
addpath(genpath('/Users/natalieheike/U/code/Export_figures'));
dataDir = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_comb/whole_cells/L2');
outDirPdf = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_comb/whole_cells/L2')

%%

% dataDir_01 = ('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_ext/whole_cells');
% dataDir_02 = ('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_L23/whole_cells');

dataDir_01 = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_ext/whole_cells/L2');
dataDir_02 = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_L23/whole_cells');


dendSectionType = 'basals';

    % import data from xls sheets
    filename = fullfile(dataDir_01, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_01 = readtable(filename);
    
    filename = fullfile(dataDir_02, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_02 = readtable(filename);
    
    T_comb = [T_01;T_02];
    
    % Save and export table
    filename = fullfile(outDirPdf,'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    writetable(T_comb, filename);
    
dendSectionType = 'apicals';

    % import data from xls sheets
    filename = fullfile(dataDir_01, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_01 = readtable(filename);
    
    filename = fullfile(dataDir_02, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_02 = readtable(filename);
    
    T_comb = [T_01;T_02];
    
    % Save and export table
    filename = fullfile(outDirPdf,'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    writetable(T_comb, filename);
    
    
 dendSectionType = 'obliques';

    % import data from xls sheets
    filename = fullfile(dataDir_01, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_01 = readtable(filename);
    
    filename = fullfile(dataDir_02, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T_02 = readtable(filename);
    
    T_comb = [T_01;T_02];
    
    % Save and export table
    filename = fullfile(outDirPdf,'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    writetable(T_comb, filename);
    
    
    
 %T_all_numbers for prim basals and number of obliques
    % import data from xls sheets
    filename = fullfile(dataDir_01, 'distance_tables', 'T_all_numbers.xls');
    T_01 = readtable(filename);
    
    filename = fullfile(dataDir_02, 'distance_tables', 'T_all_numbers.xls');
    T_02 = readtable(filename);

    T_comb = [T_01;T_02];
    
    % Save and export table
    filename = fullfile(outDirPdf,'distance_tables', 'T_all_numbers.xls');
    writetable(T_comb, filename);
    

