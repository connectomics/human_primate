%% Script for measuring pathlenghts of different dendritic compartments (basal, apical, oblique) and between branchpoints
%% Adapted, adjusted and developed from SL script "measurePathLengthsForDendriteSections.m (NH)

% % on gaba: 
addpath(genpath('/gaba/u/natalieheike/code/auxiliaryMethods'));
addpath(genpath('/gaba/u/natalieheike/code/Export_figures'));

outDirPdf = '/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_L23/whole_cells/';
nmlDir = '/gaba/u/natalieheike/code/H5_10_analysis/tracings/H5_10_L23';

% local
% addpath(genpath('/Users/natalieheike/U/code/auxiliaryMethods')); 
% addpath(genpath('/Users/natalieheike/U/code/Export_figures'));
% nmlDir = '/Users/natalieheike/U/code/H5_10_analysis/tracings/H5_10_ext';
% outDirPdf = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_ext/whole_cells/L3');

outData = struct;

skelAll = skeleton(fullfile(nmlDir, 'H5_L23_Synaptic_Input_Estimation_v03.nml'));
% 
dendSectionType = 'Cell'; 
startCommentType = 'cellbody';

scale = skelAll.scale ./ 1000;
idxKeep = contains(skelAll.names, dendSectionType);
skel = skelAll.deleteTrees(idxKeep, true);


%%
for idxTree = 1:skel.numTrees
    curSkel = skel.deleteTrees(idxTree, true);
    curName = curSkel.names{:};
    startIdx = curSkel.getNodesWithComment(startCommentType,1,'regexp'); % comment where the tree will start
    startId = str2double(curSkel.nodesAsStruct{1}(startIdx).id);
    
    %% for basal dendrites

            % measure eod/zzz endings
            endCommentType = '(eods|zzz)';
            [outTable_basal_eods]= doForBasalEnding(endCommentType, curSkel, scale, startId);     

            % measure true endings
            endCommentType = 'true ending';
            [outTable_basal_tend]= doForBasalEnding(endCommentType, curSkel, scale, startId); 

            % put true ending and eods tables together
            cur_T_basal = [outTable_basal_eods; outTable_basal_tend];
            
            % write and save table for each cell
            filename = fullfile(outDirPdf, 'distance_tables', sprintf('distance_table_basal_%s.xls',curSkel.names{:}));
            writetable(cur_T_basal, filename);
            
            % store table for later to make complete table
            outData(idxTree).BasalTable = cur_T_basal;

    %% for oblique dendrites

            % measure eod/zzz endings
            endCommentType = '(eods|zzz)';
            [outTable_oblique_eods]= doForObliqueEnding(endCommentType, curSkel, scale, startId);     

            % measure true endings
            endCommentType = 'true ending';
            [outTable_oblique_tend]= doForObliqueEnding(endCommentType, curSkel, scale, startId); 

            % put true ending and eods tables together
            cur_T_oblique = [outTable_oblique_eods; outTable_oblique_tend];
            
            %write and save tables for each cell
            filename = fullfile(outDirPdf,'distance_tables', sprintf('distance_table_oblique_%s.xls',curSkel.names{:}));
            writetable(cur_T_oblique, filename);
            
            % store table for later to make complete table
            outData(idxTree).ObliqueTable = cur_T_oblique;

    %% for apical tuft dendrites

            % measure eod/zzz endings
            endCommentType = '(eods|zzz)';
            [outTable_apical_eods]= doForApicalEnding(endCommentType, curSkel, scale, startId);     

            % measure true endings
            endCommentType = 'true ending';
            [outTable_apical_tend]= doForApicalEnding(endCommentType, curSkel, scale, startId); 
            
            % put true ending and eods tables together
            cur_T_apical = [outTable_apical_eods; outTable_apical_tend];
            
            %write and save tables for each cell
            filename = fullfile(outDirPdf,'distance_tables', sprintf('distance_table_apical_%s.xls',curSkel.names{:}));
            writetable(cur_T_apical, filename);
            
            % store table for later to make complete table
            outData(idxTree).ApicalTable = cur_T_apical;
            
            
     %% Count number of obliques and basals
     
        Comment_basal = 'basal';
        comments_basal = curSkel.getNodesWithComment(Comment_basal,1,'regexp'); % comment to identify basal dendrites
        numb_prim_basal = numel(comments_basal);
        
        Comment_oblique = '_root';
        comments_oblique = curSkel.getNodesWithComment(Comment_oblique,1,'regexp');
        numb_oblique = numel(comments_oblique);
      
        T_numbers = table( {''},[0],[0],...
            'VariableNames', {'name','prim_basal', 'obliques'});
        
        T_numbers.name{1} = curSkel.names{:};
        T_numbers.prim_basal = numb_prim_basal;
        T_numbers.obliques = numb_oblique;
        
        %store data for later
        outData(idxTree).numbers = T_numbers;
      
            
end

%% create Tables for all cells together
%basals
T_all_basals = cat(1,outData.BasalTable);
filename = fullfile(outDirPdf,'distance_tables', 'pl_all_basals.xls');
writetable(T_all_basals, filename);

%obliques
T_all_obliques = cat(1,outData.ObliqueTable);
filename = fullfile(outDirPdf,'distance_tables', 'pl_all_obliques.xls');
writetable(T_all_obliques, filename);

%apicals
T_all_apicals = cat(1,outData.ApicalTable);
filename = fullfile(outDirPdf,'distance_tables', 'pl_all_apicals.xls');
writetable(T_all_apicals, filename);

%numbers of obliques and primary basals
T_all_numbers = cat(1,outData.numbers);
filename = fullfile(outDirPdf,'distance_tables', 'T_all_numbers.xls');
writetable(T_all_numbers, filename);

           
%%
function [outTable]= doForBasalEnding(endCommentType, curSkel, scale, startId)
    endIdx = curSkel.getNodesWithComment(endCommentType,1,'regexp');

    outTable = table( {''},{''},{''},[0],[0],[0],[0],[0],[0],[0],[0],...
            'VariableNames', {'name','type', 'ending', 'CBtoEnd', 'bp_order', 'CBtoBP1', 'CBtoBP2', 'CBtoBP3', 'CBtoBP4', 'CBtoBP5', 'CBtoBP6',});
        
% check for each ending id    
    for idxNode = 1:numel(endIdx)
        curEndIdx = endIdx(idxNode);
        endId = str2double(curSkel.nodesAsStruct{1}(curEndIdx).id);

        [path, ~, pL] = curSkel.getShortestPath(startId, endId); %nm
        pL = pL./1e3;
        plfromCB = pL;
        
    % check if "basal'comment is on path
        Comment_basal = 'basal';
        BasalCommentsIdx = curSkel.getNodesWithComment(Comment_basal,1,'regexp'); % comment to identify basal dendrites
    	
        for BasalIdx = 1:numel(BasalCommentsIdx);
            curBasalIdx = BasalCommentsIdx(BasalIdx);
            curName = curSkel.names{:};

            if ismember (curBasalIdx, path) == true
    
                [~, ~, pL] = curSkel.getShortestPath(startId, endId); %nm
                pL = pL./1e3;
                plfromCB = pL;
                
                outTable.CBtoEnd(idxNode) = plfromCB;
                outTable.ending{idxNode} = endCommentType;
                outTable.type{idxNode} = 'basal'; 
                outTable.name{idxNode} = curName;

        % Get order of branchpoints on this path/dendrite
                bpCommentType = 'bp'; 
                branchpoints_all = curSkel.getNodesWithComment(bpCommentType, 1, 'regexp');
        
                bps = ismember (branchpoints_all,path);
                bp_order = sum(bps(:));
                outTable.bp_order(idxNode) = bp_order;

        % check if/which branchpoints are in path and get distances
                %BP1
                bpCommentType = 'bp1';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                       curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(startId, curBpId); %nm
                        CBtoBP1 = pL./1e3;
                        outTable.CBtoBP1(idxNode) = CBtoBP1;
                    end
                end

                %BP2
                bpCommentType = 'bp2';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(startId, curBpId); %nm
                        CBtoBP2 = pL./1e3;
                        outTable.CBtoBP2(idxNode) = CBtoBP2;
                    end
                end

                %BP3
                bpCommentType = 'bp3';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(startId, curBpId); %nm
                        CBtoBP3 = pL./1e3;
                        outTable.CBtoBP3(idxNode) = CBtoBP3;
                    end
                end

                %BP4
                bpCommentType = 'bp4';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(startId, curBpId); %nm
                        CBtoBP4 = pL./1e3;
                        outTable.CBtoBP4(idxNode) = CBtoBP4;
                    end
                end

                %BP5
                bpCommentType = 'bp5';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(startId, curBpId); %nm
                        CBtoBP5 = pL./1e3;
                        outTable.CBtoBP5(idxNode) = CBtoBP5;
                    end
                end
                
                %BP6
                bpCommentType = 'bp6';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(startId, curBpId); %nm
                        CBtoBP6 = pL./1e3;
                        outTable.CBtoBP6(idxNode) = CBtoBP6;
                    end
                end
                

             end

        end
 
    end
    
%     Delete Empty Rows with no data
    toDelete = outTable.CBtoEnd == 0;
    outTable(toDelete,:) = [];

end


function [outTable]= doForObliqueEnding(endCommentType, curSkel, scale, startId)
    endIdx = curSkel.getNodesWithComment(endCommentType,1,'regexp');
    endLocs = curSkel.nodes{1}(endIdx,1:3);
    endLocs = curSkel.setScale(endLocs,scale);

    outTable = table( {''},{''},{''},[0],[0],[0],[0],[0],[0],[0],...
            'VariableNames', {'name', 'type','ending', 'plfromCB', 'bp_order', 'rootToEnd', 'rootToBP1', 'rootToBP2', 'rootToBP3', 'rootToBP4'});
   
        
    for idxNode = 1:numel(endIdx)
        curEndIdx = endIdx(idxNode);
        endId = str2double(curSkel.nodesAsStruct{1}(curEndIdx).id);

        [path, ~, pL] = curSkel.getShortestPath(startId, endId); %nm
        pL = pL./1e3;
        plfromCB = pL;
        
% check if _root is on path
        startComment_oblique = '_root';
        startObliqueIdx = curSkel.getNodesWithComment(startComment_oblique,1,'regexp'); % comment where the tree will start
    	
        for OblIdx = 1:numel(startObliqueIdx);
            curOblIdx = startObliqueIdx(OblIdx);
            curName = curSkel.names{:};

            if ismember (curOblIdx, path) == true
                rootId = str2double(curSkel.nodesAsStruct{1}(curOblIdx).id);
    
                [~, ~, pL] = curSkel.getShortestPath(rootId, endId); %nm
                rootToEnd = pL./1e3;
                
                outTable.rootToEnd(idxNode) = rootToEnd;
                outTable.plfromCB(idxNode) = plfromCB;
                outTable.ending{idxNode} = endCommentType;
                outTable.type{idxNode} = 'oblique'; 
                outTable.name{idxNode} = curName;
                
                % Get order of branchpoints on this path/dendrite
                bpCommentType = 'bp'; 
                branchpoints_all = curSkel.getNodesWithComment(bpCommentType, 1, 'regexp');
        
                bps = ismember (branchpoints_all,path);
                bp_order = sum(bps(:));
                outTable.bp_order(idxNode) = bp_order;


                % check if/which branchpoints are in path
                %BP1
                bpCommentType = 'bp1';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                       curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        rootToBP1 = pL./1e3;
                        outTable.rootToBP1(idxNode) = rootToBP1;
                    end
                end

                %BP2
                bpCommentType = 'bp2';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        rootToBP2 = pL./1e3;
                        outTable.rootToBP2(idxNode) = rootToBP2;
                    end
                end

                %BP3
                bpCommentType = 'bp3';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        rootToBP3 = pL./1e3;
                        outTable.rootToBP3(idxNode) = rootToBP3;
                    end
                end

                %BP4
                bpCommentType = 'bp4';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        rootToBP4 = pL./1e3;
                        outTable.rootToBP4(idxNode) = rootToBP4;
                    end
                end

                %BP5
                bpCommentType = 'bp5';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        rootToBP5 = pL./1e3;
                        outTable.rootToBP5(idxNode) = rootToBP5;
                    end
                end
                
                

             end

        end
 
    end
    
%     Delete Empty Rows with no data
    toDelete = outTable.plfromCB == 0;
    outTable(toDelete,:) = [];

end

function [outTable]= doForApicalEnding(endCommentType, curSkel, scale, startId)
    endIdx = curSkel.getNodesWithComment(endCommentType,1,'regexp');

    outTable = table( {''},{''},{''},[0],[0],[0],[0],[0],[0],[0],[0],[0],...
            'VariableNames', {'name', 'type','ending', 'CBtoBIFUR', 'bp_order', 'BIFURtoEND', 'BIFURtoBP1', 'BIFURtoBP2', 'BIFURtoBP3', 'BIFURtoBP4', 'BIFURtoBP5', 'BIFURtoBP6'});
        
        
    for idxNode = 1:numel(endIdx)
        curEndIdx = endIdx(idxNode);
        endId = str2double(curSkel.nodesAsStruct{1}(curEndIdx).id);

        [path, ~, pL] = curSkel.getShortestPath(startId, endId); %nm
        pL = pL./1e3;
        plfromCB = pL;
        
% check if "main bifurcation'comment is on path
        Comment_apical = 'main bifurcation';
        ApicalCommentsIdx = curSkel.getNodesWithComment(Comment_apical,1,'regexp'); % comment to identify apical dendrites
    	
        for ApicalIdx = 1:numel(ApicalCommentsIdx);
            curApicalIdx = ApicalCommentsIdx(ApicalIdx);
            curName = curSkel.names{:};

            if ismember (curApicalIdx, path) == true
               rootId = str2double(curSkel.nodesAsStruct{1}(curApicalIdx).id);
    
                [~, ~, pL] = curSkel.getShortestPath(rootId, endId); %nm
                pL = pL./1e3;
                BIFURtoEND = pL;
                
                % add distance from cellbody to main bifurcation
                [~, ~, pL] = curSkel.getShortestPath(startId, rootId); %nm
                pL = pL./1e3;
                CBtoBIFUR = pL;
                
                outTable.CBtoBIFUR(idxNode) = CBtoBIFUR;
                outTable.BIFURtoEND(idxNode) = BIFURtoEND;
                outTable.ending{idxNode} = endCommentType;
                outTable.type{idxNode} = 'apical';
                outTable.name{idxNode} = curName;
                
             % Get order of branchpoints on this path/dendrite
                bpCommentType = 'bp'; 
                branchpoints_all = curSkel.getNodesWithComment(bpCommentType, 1, 'regexp');
        
                bps = ismember (branchpoints_all,path);
                bp_order = sum(bps(:));
                outTable.bp_order(idxNode) = bp_order;

            % check if/which branchpoints are in path
                %BP1
                bpCommentType = 'bp1';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                       curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        BIFURtoBP1 = pL./1e3;
                        outTable.BIFURtoBP1(idxNode) = BIFURtoBP1;
                    end
                end

                %BP2
                bpCommentType = 'bp2';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        BIFURtoBP2 = pL./1e3;
                        outTable.BIFURtoBP2(idxNode) = BIFURtoBP2;
                    end
                end

                %BP3
                bpCommentType = 'bp3';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        BIFURtoBP3 = pL./1e3;
                        outTable.BIFURtoBP3(idxNode) = BIFURtoBP3;
                    end
                end

                %BP4
                bpCommentType = 'bp4';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        BIFURtoBP4 = pL./1e3;
                        outTable.BIFURtoBP4(idxNode) = BIFURtoBP4;
                    end
                end

                %BP5
                bpCommentType = 'bp5';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        BIFURtoBP5 = pL./1e3;
                        outTable.BIFURtoBP5(idxNode) = BIFURtoBP5;
                    end
                end
                
                %BP6
                bpCommentType = 'bp6';
                branchpoints = curSkel.getNodesWithComment(bpCommentType, 1, 'exact');
                for bpIdx = 1:numel(branchpoints);
                    curBp = branchpoints(bpIdx);

                    if ismember(curBp,path) == true
                        curBpId = str2double(curSkel.nodesAsStruct{1,1}(curBp).id);

                        [~, ~, pL] = curSkel.getShortestPath(rootId, curBpId); %nm
                        BIFURtoBP6 = pL./1e3;
                        outTable.BIFURtoBP6(idxNode) = BIFURtoBP6;
                    end
                end
                
                

             end

        end
 
    end
    
%     Delete Empty Rows with no data
    toDelete = outTable.BIFURtoEND == 0;
    outTable(toDelete,:) = [];

end

