%% Script to estimate total dendritic pathlength of pyramidal cell based on true ending pathlengths
% Prior: Use Script: measurePL_dend_sections_wholeCells to generate tables
% with distance measurements


%% Written by NH

% % on gaba: 
% addpath(genpath('/gaba/u/natalieheike/code/auxiliaryMethods'));
% addpath(genpath('/gaba/u/natalieheike/code/Export_figures'));
% dataDir = ('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_comb/whole_cells/');
% outDirPdf = fullfile('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_comb/whole_cells/');

% local
addpath(genpath('/Users/natalieheike/U/code/auxiliaryMethods')); 
addpath(genpath('/Users/natalieheike/U/code/Export_figures'));
dataDir = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_comb/whole_cells/');
outDirPdf = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_comb/whole_cells/');

    
%% basals

dendSectionType = 'basals';
av_prim_basal = 5;

    % import data from xls sheets
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T = readtable(filename);
    
    % Make table for true endings only
    T_tend = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', 'bp02_to_bp03', 'bp03_to_bp04', 'bp04_to_bp05', 'bplast_to_end'});
    
        
    true_endings = find(strcmp('true ending', T.ending));

    for i = 1:numel(true_endings);
        id = true_endings(i);
        T_tend.name{i}= T.name{id};
        T_tend.ending{i} = T.ending{id};
        T_tend.bp_order(i) = T.bp_order(id);
        T_tend.length(i) = T.CBtoEnd(id);
        
        % calculations for distances between branchpoints
        %to bp01
        if T_tend.bp_order(i) > 0
        T_tend.to_bp01(i) = T.CBtoBP1(id);
        else 
        T_tend.to_bp01(i) = 0;
        end
        
        %bp01 to bp02
        if T_tend.bp_order(i) > 1
        T_tend.bp01_to_bp02(i) = T.CBtoBP2(id) - T.CBtoBP1(id);
        else 
        T_tend.bp01_to_bp02(i) = 0;
        end
        
        %bp02 to bp03
        if T_tend.bp_order(i) > 2
        T_tend.bp02_to_bp03(i) = T.CBtoBP3(id) - T.CBtoBP2(id);
        else 
        T_tend.bp02_to_bp03(i) = 0;
        end
        
        %bp03 to bp04
        if T_tend.bp_order(i) > 3
        T_tend.bp03_to_bp04(i) = T.CBtoBP4(id) - T.CBtoBP3(id);
        else 
        T_tend.bp03_to_bp04(i) = 0;
        end
        
        %bp04 to bp05
        if T_tend.bp_order(i) > 4
        T_tend.bp04_to_bp05(i) = T.CBtoBP5(id) - T.CBtoBP4(id);
        else 
        T_tend.bp04_to_bp05(i) = 0;
        end
        
        %bplast to end
        T_tend.bplast_to_end(i) = T_tend.length(i) - sum(T_tend{i,5:9})
        
    end
    
    T_tend_basal = T_tend;
    
    % Save and export table
    filename = fullfile(outDirPdf,'pathlength_estimation', sprintf('pl_tend_%s.xls', dendSectionType));
    writetable(T_tend, filename);

    
    % Calculate and create table for means of bp distances
    T_means = table([0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'av_to_bp01', 'av_bp01_to_bp02', 'av_bp02_to_bp03', 'av_bp03_to_bp04', 'av_bp04_to_bp05', 'av_bplast_to_end','av_bps', 'av_length'});
    
        
    T_means.av_to_bp01 = mean(nonzeros(T_tend.to_bp01));
    T_means.av_bp01_to_bp02 = mean(nonzeros(T_tend.bp01_to_bp02));
    T_means.av_bp02_to_bp03 = mean(nonzeros(T_tend.bp02_to_bp03));
    T_means.av_bp03_to_bp04 = mean(nonzeros(T_tend.bp03_to_bp04));
    T_means.av_bp04_to_bp05 = mean(nonzeros(T_tend.bp04_to_bp05));
    T_means.av_bplast_to_end = mean(T_tend.bplast_to_end);
    T_means.av_length = mean(T_tend.length);
    T_means.av_bps = int32(mean(T_tend.bp_order));

    % Calculate Estimation for basal dendritic pathlength
    
    pl_seg(:,:) = 0; %create variable to store values from loop
    
    for i = 0:(T_means.av_bps-1); % we subtract -1 because we want the last segment to be the long segment from last bp to end, see below
        pl_seg(i+1) = T_means{1,i+1} * 2^i; % multiply each segment with 2 to the power of bp order
    end
    
    pl_prim_basal = sum(pl_seg) + T_means.av_bplast_to_end * 2^T_means.av_bps; % add the bplast to end segment
    pl_total_basals = pl_prim_basal * av_prim_basal; % multiply by average number of primary basal denrites
    
    T_means_basals = T_means;
    
    %% obliques

dendSectionType = 'obliques';
av_num_obliques = 6;

    % import data from xls sheets
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType))
    T = readtable(filename);
    
    % Make table for true endings only
    T_tend = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', 'bp02_to_bp03', 'bp03_to_bp04', 'bp04_to_bp05', 'bplast_to_end'});
    
        
    true_endings = find(strcmp('true ending', T.ending));

    for i = 1:numel(true_endings);
        id = true_endings(i);
        T_tend.name{i}= T.name{id};
        T_tend.ending{i} = T.ending{id};
        T_tend.bp_order(i) = T.bp_order(id);
        T_tend.length(i) = T.rootToEnd(id);
        
        % calculations for distances between branchpoints
        %to bp01
        if T_tend.bp_order(i) > 0
        T_tend.to_bp01(i) = T.rootToBP1(id);
        else 
        T_tend.to_bp01(i) = 0;
        end
        
        %bp01 to bp02
        if T_tend.bp_order(i) > 1
        T_tend.bp01_to_bp02(i) = T.rootToBP2(id) - T.rootToBP1(id);
        else 
        T_tend.bp01_to_bp02(i) = 0;
        end
        
        %bp02 to bp03
        if T_tend.bp_order(i) > 2
        T_tend.bp02_to_bp03(i) = T.rootToBP3(id) - T.rootToBP2(id);
        else 
        T_tend.bp02_to_bp03(i) = 0;
        end
        
        %bp03 to bp04
        if T_tend.bp_order(i) > 3
        T_tend.bp03_to_bp04(i) = T.rootToBP4(id) - T.rootToBP3(id);
        else 
        T_tend.bp03_to_bp04(i) = 0;
        end
        
        %bp04 to bp05
        if T_tend.bp_order(i) > 4
        T_tend.bp04_to_bp05(i) = T.rootToBP5(id) - T.rootToBP4(id);
        else 
        T_tend.bp04_to_bp05(i) = 0;
        end
        
        %bplast to end
        T_tend.bplast_to_end(i) = T_tend.length(i) - sum(T_tend{i,5:9})
        
    end
    
     T_tend_obliques = T_tend;
     
     % Save and export table
    filename = fullfile(outDirPdf,'pathlength_estimation', sprintf('pl_tend_%s.xls', dendSectionType));
    writetable(T_tend, filename);

     
    % Calculate and create table for means of bp distances
    T_means = table([0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'av_to_bp01', 'av_bp01_to_bp02', 'av_bp02_to_bp03', 'av_bp03_to_bp04', 'av_bp04_to_bp05', 'av_bplast_to_end','av_bps', 'av_length'});
    
        
    T_means.av_to_bp01 = mean(nonzeros(T_tend.to_bp01));
    T_means.av_bp01_to_bp02 = mean(nonzeros(T_tend.bp01_to_bp02));
    T_means.av_bp02_to_bp03 = mean(nonzeros(T_tend.bp02_to_bp03));
    T_means.av_bp03_to_bp04 = mean(nonzeros(T_tend.bp03_to_bp04));
    T_means.av_bp04_to_bp05 = mean(nonzeros(T_tend.bp04_to_bp05));
    T_means.av_bplast_to_end = mean(T_tend.bplast_to_end);
    T_means.av_length = mean(T_tend.length);
%     T_means.av_bps = int32(mean(T_tend.bp_order));
    T_means.av_bps = int32(1) %manually overrule...

    % Calculate Estimation for basal dendritic pathlength
    
    pl_seg(:,:) = 0; %create variable to store values from loop
    
    for i = 0:(T_means.av_bps-1); % we subtract -1 because we want the last segment to be the long segment from last bp to end, see below
        pl_seg(i+1) = T_means{1,i+1} * 2^i; % multiply each segment with 2 to the power of bp order
    end
    
    pl_obl = sum(pl_seg) + T_means.av_bplast_to_end * 2^T_means.av_bps; % add the bplast to end segment
    pl_total_obliques = pl_obl * av_num_obliques; % multiply by average number of primary basal denrites
    
    T_means_oblique = T_means;
    
        %% apical tuft

dendSectionType = 'apicals';
av_num_apical = 2; %two main branches after main bifurcation

    % import data from xls sheets
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType))
    T = readtable(filename);
    
    % Make table for true endings only
    T_tend = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', 'bp02_to_bp03', ...
            'bp03_to_bp04', 'bp04_to_bp05', 'bp05_to_bp06', 'bplast_to_end'});
    
        
    true_endings = find(strcmp('true ending', T.ending));

    for i = 1:numel(true_endings);
        id = true_endings(i);
        T_tend.name{i}= T.name{id};
        T_tend.ending{i} = T.ending{id};
        T_tend.bp_order(i) = T.bp_order(id);
        T_tend.length(i) = T.BIFURtoEND(id);
        
        % calculations for distances between branchpoints
        %to bp01
        if T_tend.bp_order(i) > 0
        T_tend.to_bp01(i) = T.BIFURtoBP1(id);
        else 
        T_tend.to_bp01(i) = 0;
        end
        
        %bp01 to bp02
        if T_tend.bp_order(i) > 1
        T_tend.bp01_to_bp02(i) = T.BIFURtoBP2(id) - T.BIFURtoBP1(id);
        else 
        T_tend.bp01_to_bp02(i) = 0;
        end
        
        %bp02 to bp03
        if T_tend.bp_order(i) > 2
        T_tend.bp02_to_bp03(i) = T.BIFURtoBP3(id) - T.BIFURtoBP2(id);
        else 
        T_tend.bp02_to_bp03(i) = 0;
        end
        
        %bp03 to bp04
        if T_tend.bp_order(i) > 3
        T_tend.bp03_to_bp04(i) = T.BIFURtoBP4(id) - T.BIFURtoBP3(id);
        else 
        T_tend.bp03_to_bp04(i) = 0;
        end
        
        %bp04 to bp05
        if T_tend.bp_order(i) > 4
        T_tend.bp04_to_bp05(i) = T.BIFURtoBP5(id) - T.BIFURtoBP4(id);
        else 
        T_tend.bp04_to_bp05(i) = 0;
        end
        
        %bp05 to bp06
        if T_tend.bp_order(i) > 5
        T_tend.bp05_to_bp06(i) = T.BIFURtoBP6(id) - T.BIFURtoBP5(id);
        else 
        T_tend.bp05_to_bp06(i) = 0;
        end
        
        %bplast to end
        T_tend.bplast_to_end(i) = T_tend.length(i) - sum(T_tend{i,5:10})
        
    end
    
     T_tend_apicals = T_tend;
     
    % Save and export table
    filename = fullfile(outDirPdf,'pathlength_estimation', sprintf('pl_tend_%s.xls', dendSectionType));
    writetable(T_tend, filename);

     
    % Calculate and create table for means of bp distances
    T_means = table([0],[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'av_to_bp01', 'av_bp01_to_bp02', 'av_bp02_to_bp03', 'av_bp03_to_bp04', 'av_bp04_to_bp05', ...
            'av_bp05_to_bp06', 'av_bplast_to_end','av_bps', 'av_length'});
    
        
    T_means.av_to_bp01 = mean(nonzeros(T_tend.to_bp01));
    T_means.av_bp01_to_bp02 = mean(nonzeros(T_tend.bp01_to_bp02));
    T_means.av_bp02_to_bp03 = mean(nonzeros(T_tend.bp02_to_bp03));
    T_means.av_bp03_to_bp04 = mean(nonzeros(T_tend.bp03_to_bp04));
    T_means.av_bp04_to_bp05 = mean(nonzeros(T_tend.bp04_to_bp05));
    T_means.av_bp05_to_bp06 = mean(nonzeros(T_tend.bp05_to_bp06));
    T_means.av_bplast_to_end = mean(T_tend.bplast_to_end);
    T_means.av_length = mean(T_tend.length);
%     T_means.av_bps = int32(mean(T_tend.bp_order));
    T_means.av_bps = int32(3) %manually overrule...

    % Calculate Estimation for basal dendritic pathlength
    
    pl_seg(:,:) = 0; %create variable to store values from loop
    
    for i = 0:(T_means.av_bps-1); % we subtract -1 because we want the last segment to be the long segment from last bp to end, see below
        pl_seg(i+1) = T_means{1,i+1} * 2^i; % multiply each segment with 2 to the power of bp order
    end
    
    pl_apical = sum(pl_seg) + T_means.av_bplast_to_end * 2^T_means.av_bps; % add the bplast to end segment
    pl_total_apical = pl_apical * av_num_apical; % multiply by average number of primary basal denrites
    
    T_means_apical = T_means;
    %% Main Apical dendrite (until main bifurcation)
    
   [G, ID] = findgroups(T.CBtoBIFUR); %one AD per cell...
   av_AD_pl = mean(ID);
   
   %% Total Dendritic Pathlength Calculation
   
       T_total = table([0],[0],[0],[0],[0],  ...
            'VariableNames', {'Basal', 'Oblique', 'Apical', 'AD', 'Total'});
        
        T_total.Basal = pl_total_basals;
        T_total.Oblique = pl_total_obliques;
        T_total.Apical = pl_total_apical;
        T_total.AD = av_AD_pl;
        T_total.Total = sum(T_total{1, 1:4})
        
  % Save and export estimation in xls
    filename = fullfile(outDirPdf,'pathlength_estimation', 'Dendritic_PL_Estimation_tend.xls');
    writetable(T_total, filename);
        
