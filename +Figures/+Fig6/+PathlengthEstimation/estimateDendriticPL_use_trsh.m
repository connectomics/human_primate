%% Written by NH

% on gaba: 
addpath(genpath('/gaba/u/natalieheike/code/auxiliaryMethods'));
addpath(genpath('/gaba/u/natalieheike/code/Export_figures'));
dataDir = ('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_comb/whole_cells/L2');
outDirPdf = fullfile('/gaba/u/natalieheike/code/H5_10_analysis/results/H5_10_comb/whole_cells/L2');
% % 
% %%% local
% addpath(genpath('/Users/natalieheike/U/code/auxiliaryMethods')); 
% addpath(genpath('/Users/natalieheike/U/code/Export_figures'));
% dataDir = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_ext/whole_cells/L3');
% outDirPdf = fullfile('/Users/natalieheike/U/code/H5_10_analysis/results/H5_10_ext/whole_cells/L3');

%% Set Tresholds in �m

trsh_basal = 200;
trsh_oblique = 200;
trsh_apical = 300;

%% basals

dendSectionType = 'basals';
av_prim_basal = 5;


    % import data from xls sheets
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType));
    T = readtable(filename);
    
    % Make table for true endings only
    T_all = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', 'bp02_to_bp03', 'bp03_to_bp04', 'bp04_to_bp05', 'bplast_to_end'});
   

    for i = 1:numel(T.name);
    
        
        T_all.name{i}= T.name{i};
        T_all.ending{i} = T.ending{i};
        T_all.bp_order(i) = T.bp_order(i);
        T_all.length(i) = T.CBtoEnd(i);
        
        % calculations for distances between branchpoints
        %to bp01
        if T_all.bp_order(i) > 0
        T_all.to_bp01(i) = T.CBtoBP1(i);
        else 
        T_all.to_bp01(i) = 0;
        end
        
        %bp01 to bp02
        if T_all.bp_order(i) > 1
        T_all.bp01_to_bp02(i) = T.CBtoBP2(i) - T.CBtoBP1(i);
        else 
        T_all.bp01_to_bp02(i) = 0;
        end
        
        %bp02 to bp03
        if T_all.bp_order(i) > 2
        T_all.bp02_to_bp03(i) = T.CBtoBP3(i) - T.CBtoBP2(i);
        else 
        T_all.bp02_to_bp03(i) = 0;
        end
        
        %bp03 to bp04
        if T_all.bp_order(i) > 3
        T_all.bp03_to_bp04(i) = T.CBtoBP4(i) - T.CBtoBP3(i);
        else 
        T_all.bp03_to_bp04(i) = 0;
        end
        
        %bp04 to bp05
        if T_all.bp_order(i) > 4
        T_all.bp04_to_bp05(i) = T.CBtoBP5(i) - T.CBtoBP4(i);
        else 
        T_all.bp04_to_bp05(i) = 0;
        end
        
        %bplast to end
        T_all.bplast_to_end(i) = T_all.length(i) - sum(T_all{i,5:9})
        
    end
    
    T_all_basal = T_all;
    
    % Use treshold to only include dendrites above certain length
    
    T_trsh = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02',...
            'bp02_to_bp03', 'bp03_to_bp04', 'bp04_to_bp05', 'bplast_to_end'});
    
    for x = 1:numel(T_all_basal.name);
        
        if T_all_basal.length(x) > trsh_basal;
        T_trsh(x,:) = T_all_basal(x,:);
        end 
    end
        
    %     Delete Empty Rows with no data
    toDelete = T_trsh.length == 0;
    T_trsh(toDelete,:) = [];

    T_all = T_trsh % To use T_trsh for the calculation of means and dendritic pathlength below
    
    trsh = num2str(trsh_basal);
    
     % Save and export table
    filename = fullfile(outDirPdf,'pathlength_estimation', sprintf('pl_trsh_%s_%s.xls', trsh, dendSectionType));
    writetable(T_all, filename);
    
    % Calculate and create table for means of bp distances
    T_means = table([0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'av_to_bp01', 'av_bp01_to_bp02', 'av_bp02_to_bp03', 'av_bp03_to_bp04', ...
            'av_bp04_to_bp05', 'av_bplast_to_end','av_bps', 'av_length'});
    
        
    T_means.av_to_bp01 = mean(nonzeros(T_all.to_bp01));
    T_means.av_bp01_to_bp02 = mean(nonzeros(T_all.bp01_to_bp02));
    T_means.av_bp02_to_bp03 = mean(nonzeros(T_all.bp02_to_bp03));
    T_means.av_bp03_to_bp04 = mean(nonzeros(T_all.bp03_to_bp04));
    T_means.av_bp04_to_bp05 = mean(nonzeros(T_all.bp04_to_bp05));
    T_means.av_bplast_to_end = mean(T_all.bplast_to_end);
    T_means.av_length = mean(T_all.length);
    T_means.av_bps = int32(mean(T_all.bp_order));

    % Calculate Estimation for basal dendritic pathlength
    
    pl_seg(:,:) = 0; %create variable to store values from loop
    
    for i = 0:(T_means.av_bps-1); % we subtract -1 because we want the last segment to be the long segment from last bp to end, see below
        pl_seg(i+1) = T_means{1,i+1} * 2^i; % multiply each segment with 2 to the power of bp order
    end
    
    pl_prim_basal = sum(pl_seg) + T_means.av_bplast_to_end * 2^T_means.av_bps; % add the bplast to end segment
    pl_total_basals = pl_prim_basal * av_prim_basal; % multiply by average number of primary basal denrites
    
    T_means_basals = T_means;
    
    %% obliques

dendSectionType = 'obliques';
av_num_obliques = 6;

    % import data from xls sheets
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType))
    T = readtable(filename);
    
    % Make table for true endings only
    T_all = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', 'bp02_to_bp03', 'bp03_to_bp04', 'bp04_to_bp05', 'bplast_to_end'});
    

    for i = 1:numel(T.name);
        T_all.name{i}= T.name{i};
        T_all.ending{i} = T.ending{i};
        T_all.bp_order(i) = T.bp_order(i);
        T_all.length(i) = T.rootToEnd(i);
        
        % calculations for distances between branchpoints
        %to bp01
        if T_all.bp_order(i) > 0
        T_all.to_bp01(i) = T.rootToBP1(i);
        else 
        T_all.to_bp01(i) = 0;
        end
        
        %bp01 to bp02
        if T_all.bp_order(i) > 1
        T_all.bp01_to_bp02(i) = T.rootToBP2(i) - T.rootToBP1(i);
        else 
        T_all.bp01_to_bp02(i) = 0;
        end
        
        %bp02 to bp03
        if T_all.bp_order(i) > 2
        T_all.bp02_to_bp03(i) = T.rootToBP3(i) - T.rootToBP2(i);
        else 
        T_all.bp02_to_bp03(i) = 0;
        end
        
        %bp03 to bp04
        if T_all.bp_order(i) > 3
        T_all.bp03_to_bp04(i) = T.rootToBP4(i) - T.rootToBP3(i);
        else 
        T_all.bp03_to_bp04(i) = 0;
        end
        
        %bp04 to bp05
        if T_all.bp_order(i) > 4
        T_all.bp04_to_bp05(i) = T.rootToBP5(i) - T.rootToBP4(i);
        else 
        T_all.bp04_to_bp05(i) = 0;
        end
        
        %bplast to end
        T_all.bplast_to_end(i) = T_all.length(i) - sum(T_all{i,5:9})
        
    end
    
     T_all_obliques = T_all;
     
    % Use treshold to only include dendrites above certain length
    
    T_trsh = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', 'bp02_to_bp03', 'bp03_to_bp04', 'bp04_to_bp05', 'bplast_to_end'});
    
    for x = 1:numel(T_all_obliques.name);
        
        if T_all_obliques.length(x) > trsh_oblique;
        T_trsh(x,:) = T_all_obliques(x,:);
        end 
    end
        
    %     Delete Empty Rows with no data
    toDelete = T_trsh.length == 0;
    T_trsh(toDelete,:) = [];

    T_all = T_trsh % To use T_trsh for the calculation of means and dendritic pathlength below
    
    trsh = num2str(trsh_oblique);
    
     % Save and export table
    filename = fullfile(outDirPdf,'pathlength_estimation', sprintf('pl_trsh_%s_%s.xls', trsh, dendSectionType));
    writetable(T_all, filename);
    
     
    % Calculate and create table for means of bp distances
    T_means = table([0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'av_to_bp01', 'av_bp01_to_bp02', 'av_bp02_to_bp03', 'av_bp03_to_bp04',...
            'av_bp04_to_bp05', 'av_bplast_to_end','av_bps', 'av_length'});
    
        
    T_means.av_to_bp01 = mean(nonzeros(T_all.to_bp01));
    T_means.av_bp01_to_bp02 = mean(nonzeros(T_all.bp01_to_bp02));
    T_means.av_bp02_to_bp03 = mean(nonzeros(T_all.bp02_to_bp03));
    T_means.av_bp03_to_bp04 = mean(nonzeros(T_all.bp03_to_bp04));
    T_means.av_bp04_to_bp05 = mean(nonzeros(T_all.bp04_to_bp05));
    T_means.av_bplast_to_end = mean(T_all.bplast_to_end);
    T_means.av_length = mean(T_all.length);
    T_means.av_bps = int32(mean(T_all.bp_order));
%     T_means.av_bps = int32(2); %manually overrule...

    % Calculate Estimation for basal dendritic pathlength
    
    pl_seg(:,:) = 0; %create variable to store values from loop
    
    for i = 0:(T_means.av_bps-1); % we subtract -1 because we want the last segment to be the long segment from last bp to end, see below
        pl_seg(i+1) = T_means{1,i+1} * 2^i; % multiply each segment with 2 to the power of bp order
    end
    
    pl_obl = sum(pl_seg) + T_means.av_bplast_to_end * 2^T_means.av_bps; % add the bplast to end segment
    pl_total_obliques = pl_obl * av_num_obliques; % multiply by average number of primary oblique denrites per cell
    
    T_means_oblique = T_means;
    
        %% apical tuft

dendSectionType = 'apicals';
av_num_apical = 2; %two main branches after main bifurcation

    % import data from xls sheets
    filename = fullfile(dataDir, 'distance_tables', sprintf('pl_all_%s.xls', dendSectionType))
    T = readtable(filename);
    
    % Make table for true endings only
    T_all = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', 'bp02_to_bp03', ...
            'bp03_to_bp04', 'bp04_to_bp05', 'bp05_to_bp06', 'bplast_to_end'});
    

    for i = 1:numel(T.name);
        T_all.name{i}= T.name{i};
        T_all.ending{i} = T.ending{i};
        T_all.bp_order(i) = T.bp_order(i);
        T_all.length(i) = T.BIFURtoEND(i);
        
        % calculations for distances between branchpoints
        %to bp01
        if T_all.bp_order(i) > 0
        T_all.to_bp01(i) = T.BIFURtoBP1(i);
        else 
        T_all.to_bp01(i) = 0;
        end
        
        %bp01 to bp02
        if T_all.bp_order(i) > 1
        T_all.bp01_to_bp02(i) = T.BIFURtoBP2(i) - T.BIFURtoBP1(i);
        else 
        T_all.bp01_to_bp02(i) = 0;
        end
        
        %bp02 to bp03
        if T_all.bp_order(i) > 2
        T_all.bp02_to_bp03(i) = T.BIFURtoBP3(i) - T.BIFURtoBP2(i);
        else 
        T_all.bp02_to_bp03(i) = 0;
        end
        
        %bp03 to bp04
        if T_all.bp_order(i) > 3
        T_all.bp03_to_bp04(i) = T.BIFURtoBP4(i) - T.BIFURtoBP3(i);
        else 
        T_all.bp03_to_bp04(i) = 0;
        end
        
        %bp04 to bp05
        if T_all.bp_order(i) > 4
        T_all.bp04_to_bp05(i) = T.BIFURtoBP5(i) - T.BIFURtoBP4(i);
        else 
        T_all.bp04_to_bp05(i) = 0;
        end
        
        %bp05 to bp06
        if T_all.bp_order(i) > 5
        T_all.bp05_to_bp06(i) = T.BIFURtoBP6(i) - T.BIFURtoBP5(i);
        else 
        T_all.bp05_to_bp06(i) = 0;
        end
        
        %bplast to end
        T_all.bplast_to_end(i) = T_all.length(i) - sum(T_all{i,5:10})
        
    end
    
     T_all_apicals = T_all;
     
    % Use treshold to only include dendrites above certain length
    
    T_trsh = table({''},{''},[0],[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'name', 'ending' 'bp_order', 'length', 'to_bp01', 'bp01_to_bp02', ...
            'bp02_to_bp03', 'bp03_to_bp04', 'bp04_to_bp05', 'bp05_to_bp06', 'bplast_to_end'});
    
    for x = 1:numel(T_all_apicals.name);
        
        if T_all_apicals.length(x) > trsh_apical;
        T_trsh(x,:) = T_all_apicals(x,:);
        end 
    end
        
    %     Delete Empty Rows with no data
    toDelete = T_trsh.length == 0;
    T_trsh(toDelete,:) = [];

    T_all = T_trsh % To use T_trsh for the calculation of means and dendritic pathlength below
    
    trsh = num2str(trsh_apical);
    
     % Save and export table
    filename = fullfile(outDirPdf,'pathlength_estimation', sprintf('pl_trsh_%s_%s.xls', trsh, dendSectionType));
    writetable(T_all, filename);
 
    % Calculate and create table for means of bp distances
    T_means = table([0],[0],[0],[0],[0],[0],[0],[0],[0],  ...
            'VariableNames', {'av_to_bp01', 'av_bp01_to_bp02', 'av_bp02_to_bp03', 'av_bp03_to_bp04', 'av_bp04_to_bp05', ...
            'av_bp05_to_bp06', 'av_bplast_to_end','av_bps', 'av_length'});
    
        
    T_means.av_to_bp01 = mean(nonzeros(T_all.to_bp01));
    T_means.av_bp01_to_bp02 = mean(nonzeros(T_all.bp01_to_bp02));
    T_means.av_bp02_to_bp03 = mean(nonzeros(T_all.bp02_to_bp03));
    T_means.av_bp03_to_bp04 = mean(nonzeros(T_all.bp03_to_bp04));
    T_means.av_bp04_to_bp05 = mean(nonzeros(T_all.bp04_to_bp05));
    T_means.av_bp05_to_bp06 = mean(nonzeros(T_all.bp05_to_bp06));
    T_means.av_bplast_to_end = mean(T_all.bplast_to_end);
    T_means.av_length = mean(T_all.length);
    T_means.av_bps = int32(mean(T_all.bp_order));
%     T_means.av_bps = int32(3) %manually overrule...

    % Calculate Estimation for basal dendritic pathlength
    
    pl_seg(:,:) = 0; %create variable to store values from loop
    
    for i = 0:(T_means.av_bps-1); % we subtract -1 because we want the last segment to be the long segment from last bp to end, see below
        pl_seg(i+1) = T_means{1,i+1} * 2^i; % multiply each segment with 2 to the power of bp order
    end
    
    pl_apical = sum(pl_seg) + T_means.av_bplast_to_end * 2^T_means.av_bps; % add the bplast to end segment
    pl_total_apical = pl_apical * av_num_apical; % multiply by 2 because of main bifurcation
    
    T_means_apical = T_means;
    %% Main Apical dendrite (until main bifurcation)
    
   [G, ID] = findgroups(T.CBtoBIFUR); %one AD per cell...
   av_AD_pl = mean(ID);
   
   %% Total Dendritic Pathlength Calculation
   
       T_total = table([0],[0],[0],[0],[0],  ...
            'VariableNames', {'Basal', 'Oblique', 'Apical', 'AD', 'Total'});
        
        T_total.Basal = pl_total_basals;
        T_total.Oblique = pl_total_obliques;
        T_total.Apical = pl_total_apical;
        T_total.AD = av_AD_pl;
        T_total.Total = sum(T_total{1, 1:4})
        
  % Save and export estimation in xls
    trsh_b = num2str(trsh_basal);
    trsh_o = num2str(trsh_oblique);
    trsh_a = num2str(trsh_apical);
    
    filename = fullfile(outDirPdf,'pathlength_estimation', sprintf('Dendritic_PL_Estimation_trsh_%s_%s_%s.xls', trsh_b, trsh_o, trsh_a ));
    writetable(T_total, filename);
        
