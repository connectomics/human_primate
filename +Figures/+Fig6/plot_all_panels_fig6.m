% Written by
%  Sahil Loomba <sahil.loomba@brain.mpg.de>
%% panel A
% EM overview image with pyramidal cells.
% See Materials and methods for links to webKnossos containing data

%% panel B
clear; clc;

cx_path = fullfile('./data/excel_files');
% note used
cx_scaldata = [6.5 22;...   % mm total dend PL (NH: Mouse;Human currently based on v03?, not used anyway...)
    2.7 8.2;... % basals
    2.7 11.3;... % apical tuft
    0.8 2.4;... % obliques
    1.9 0.7;... % spine dens
    12000 16000;... % syn on spines
    0.2 0.2;...% shaft syn dens
    1400 5200;... %  syn on shafts
    13600 22400]; %  total syn

% from Figures.Proofread.dendrites_input_synapse_densities
mqS1_spine_density = 0.56;
mqS1_shaft_density = 0.13;

mqSTG_spine_density = 1.09;
mqSTG_shaft_density = 0.20;

mq_spine_density = 0.86;
mq_shaft_density = 0.17;

% read from xls sheet:
cx_scalData_xls = xlsread(fullfile(cx_path,'InputDegrees_simpleCalcs_v05_NH_out.xlsx'));

cx_thisxlsoffset_Mouse_List = {[64 0],[10 10]};
cx_thisxlsoffset_Human_List = {[53 0],[0 10]};

cx_scalFig = figure;
cx_summaryFig = figure;

for jj=1:length(cx_thisxlsoffset_Mouse_List) %NH: first round NH numbers, second round Mohan et al. numbers for pathlengths
    cx_thisxlsoffset_Mouse = cx_thisxlsoffset_Mouse_List{jj};
    cx_thisxlsoffset_Human = cx_thisxlsoffset_Human_List{jj};
    
    cx_thisscaldata_Mouse = cx_scalData_xls(cx_thisxlsoffset_Mouse(1)+[1:8],cx_thisxlsoffset_Mouse(2)+[1:6]);
    cx_thisscaldata_Human = cx_scalData_xls(cx_thisxlsoffset_Human(1)+[1:8],cx_thisxlsoffset_Human(2)+[1:6]);

    cx_scaldata(1,1:2) = [cx_thisscaldata_Mouse(8,1) cx_thisscaldata_Human(8,1)]/1000; % mouse total pl, hum pl in mm
    cx_scaldata(2,1:2) = [cx_thisscaldata_Mouse(1,1) cx_thisscaldata_Human(1,1)]/1000; % basals
    cx_scaldata(3,1:2) = [cx_thisscaldata_Mouse(4,1) cx_thisscaldata_Human(4,1)]/1000; % apical TUFT
    cx_scaldata(4,1:2) = [cx_thisscaldata_Mouse(3,1) cx_thisscaldata_Human(3,1)]/1000; % apical obliques
    cx_scaldata(5,1:2) = [median(cx_thisscaldata_Mouse(1:4,2)) median(cx_thisscaldata_Human(1:4,2))]; % spine dens
    cx_scaldata(7,1:2) = [median(cx_thisscaldata_Mouse(1:4,3)) median(cx_thisscaldata_Human(1:4,3))]; % shaft dens
    cx_scaldata(6,1:2) = [cx_thisscaldata_Mouse(8,4) cx_thisscaldata_Human(8,4)]; % spine syn total
    cx_scaldata(8,1:2) = [cx_thisscaldata_Mouse(8,5) cx_thisscaldata_Human(8,5)]; % shaft syn total
    cx_scaldata(9,1:2) = [cx_thisscaldata_Mouse(8,6) cx_thisscaldata_Human(8,6)]; % TOTAL syn
    
    col = ['m'; 'k'; 'k'; 'k';'m'; 'k'; 'k'; 'k'; 'g']; %added by NH for colors
    figure(cx_scalFig)
    %NH: plot total, basal, oblique and apical tuft pathlength
    subplot(3,1,1)
    for kk=1:4
        plot(cx_scaldata(kk,:),'Color',col(kk));hold on %NH: changed mh_getcolor(kk) to col(kk)
    end
    legend('Complete L3 PC', 'basal', 'oblique', 'apical') %NH: added
    
    set(gca,'YScal','log');
    set(gca,'XLim',[0 3]);box off,set(gca,'TickDir','out');
    set(gca,'YLim',[5*10^-1 5*10^1]);
    
    %NH: plot spine and shaft density (median from above)
    subplot(3,1,2)
    for kk=[5 7]
        plot(cx_scaldata(kk,:),'Color',col(kk));hold on %NH: changed mh_getcolor(kk) to col(kk)
    end
    
    % SL add MQ spine and shaft densities as dotted lines
    plot([cx_scaldata(5,1)  mq_spine_density],'Color','m', 'LineStyle','--'); % spine density MQ s1+STG
    plot([cx_scaldata(7,1)  mq_shaft_density],'Color','k', 'LineStyle','--'); % shaft density MQ s1+STG
    
%     plot([cx_scaldata(5,1)  mqS1_spine_density],'Color','m', 'LineStyle','--'); % spine density MQ s1
%     plot([cx_scaldata(7,1)  mqS1_shaft_density],'Color','k', 'LineStyle','--'); % shaft density MQ s1
% 
%     plot([cx_scaldata(5,1)  mqSTG_spine_density],'Color','m', 'LineStyle','-.'); % spine density MQ stg
%     plot([cx_scaldata(7,1)  mqSTG_shaft_density],'Color','k', 'LineStyle','-.'); % shaft density MQ stg
    
    legend('spine dens', 'shaft dens', 'spine dens MQS1','shaft dens MQS1', 'spine dens MQSTG','shaft dens MQSTG' ) %NH/SL: added
    set(gca,'YScal','log');
    set(gca,'XLim',[0 3]);box off,set(gca,'TickDir','out');
    set(gca,'YLim',[10^-1 10^1]);
    
    %NH: plot absolute number of spine, shaft and total synapses
    subplot(3,1,3)
    for kk=[6 8 9]
        plot(cx_scaldata(kk,:),'Color',col(kk));hold on %NH: changed mh_getcolor(kk) to col(kk)
    end
    legend('total syn', 'spine', 'shaft')
    
    set(gca,'YScal','log');
    set(gca,'XLim',[0 3]);box off,set(gca,'TickDir','out');
    set(gca,'YLim',[10^3 10^5]);
    
    %NH: plot total pl and total syns
    figure(cx_summaryFig)
    for kk=[1 9]
        plot(cx_scaldata(kk,:),'Color',col(kk));hold on %NH: changed mh_getcolor(kk) to col(kk)
    end
    legend('total pl our data', 'total syns our data', 'total pl Mohan', 'total syns Mohan')
end

% save
% export_fig(cx_scalFig, fullfile(outDir, 'Fig_6B_scaldata.eps'),'-q101', '-nocrop','-transparent')

% for comparison: brain size neuron number cortex thickness
% from mh
% cx_scaldata2= [500 500000;...   %brain volume in mm^2
%     75 86000;...    % neuron number in million
%     1.1 3.5]    % cortex thickness in mm
% from SL
% cx_scaldata2= [500 1400000;...   %brain volume in mm^3 Hofman  2014 Frontiers
%     71 86000;...    % neuron number in million Herculano-Houzel 2009
%     1.210 2.622];    % cortex thickness in mm Felipe et all 2002
cx_scaldata2= [500 1400000;...   %brain volume in mm^3 Hofman  2014 Frontiers
    71 86000;...    % neuron number in million Herculano-Houzel 2009
    1.1 3];    % cortex thickness in mm NH: 2.9 to 3 mm

figure(cx_summaryFig)
plot([cx_scaldata2;cx_scaldata([1 9],:)]'); % total path length, total syn
set(gca,'YScal','log');
set(gca,'XLim',[0 3]);box off,set(gca,'TickDir','out');

legend('total pl our data', 'total syns our data', 'total pl our data', 'total syns our data', 'Brain Volume', 'Number of Neurons', 'Cortical Thickness','total pl Mohan', 'total syns Mohan')

%export_fig(fullfile(outDir, 'Fig_6B_summary.eps'),'-q101', '-nocrop','-transparent')

%% panel C
% see HNHP.Manual.networkSketch.m