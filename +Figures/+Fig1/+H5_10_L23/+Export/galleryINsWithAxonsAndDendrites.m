% export cell bodies from skeletons to plot in amira by Heiko
regionName = 'H5_10_L23';

Figures.setConfig; % set main region based folder

%bbox = Util.convertWebknossosToMatlabBbox(config.bbox);
bbox_wk = [18432, 166169, 0, 488448, 213793, 1024]; % manually chosen
bbox = Util.convertWebknossosToMatlabBbox(bbox_wk);

info = Util.runInfo();
Util.showRunInfo(info);

somaSize = 50; tubeSize = 2;
do3D = false;

outDirPdf = fullfile(mainFolder,'outData/figures/wholecells/IN/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

% load data from excel sheet
xlsfile = config.wholeCellsExcel;
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);
xlTable(1,:) = []; % remove headers

% remove empty rows
cellIds = xlTable.xl1;
cellTypes = xlTable.xl2;
multipolarType = xlTable.xl6; %Note
bipolarType = xlTable.xl7;

idxNan = cellfun(@(x) any(isnan(x)),cellTypes);
cellIds(idxNan) = [];
cellTypes(idxNan) = [];
bipolarType(idxNan) = [];
multipolarType(idxNan) = [];

% extract bipolar flag for INs
idxIN = cellfun(@(x) any(regexp(x,'IN')), cellTypes);
bipolarFlag = bipolarType(idxIN);
bipolarFlag = cell2mat(bipolarFlag);
multipolarFlag = multipolarType(idxIN);
multipolarFlag = cell2mat(multipolarFlag);

Util.log('Loading nml with IN axon and dend tracings...')
nmlDir = fullfile(mainFolder,'data/tracings/wholecells/IN/');
file = dir(fullfile(nmlDir,'*.nml'))
skel = skeleton(fullfile(nmlDir,file.name)); % two trees per cell

% sort trees
[~,idxSort] = sort(upper(skel.names));
skel = skel.reorderTrees(idxSort);

% extract unique IDs
skelDend = skel.keepTreeWithName('dend(?!_undefined)','regexp');
inCellIds = [];
inCellTypes = {};
for i=1:skelDend.numTrees
    curSkel = skelDend.deleteTrees(i,true);
    curId = regexpi(curSkel.names{1}, '(cell|soma)_(?<id>\d+)','names');
    curId = str2num(curId(1).id); % could be duplicate
    inCellIds(i) = curId;
    if bipolarFlag(i)
        inCellTypes{i} = 'BP';
    elseif multipolarFlag(i)
        inCellTypes{i} = 'MP';
    else
        inCellTypes{i} = 'UC';
    end

end

assert(numel(inCellIds) == skelDend.numTrees)
sprintf('Found %d IN cell Ids',numel(inCellIds))
% check that Ids match
disp(table(cellIds(idxIN), inCellIds'))

% scale all spheres
scale = skel.scale./1000;
bbox = bsxfun(@times, bbox, config.scale(:)./1000);

%% plot cell bodies per type with dendrite
alphaVal = 1;
somaColor = [0.5,0.5,0.5]; % grey IN
dendColor = [0.5,0.5,0.5]; % grey
axonColor = [0,158,115]/255; % green

% amira data
out = struct();

Util.log('Plotting INs for gallery:')
for idx = 1:numel(inCellIds)
    curCellId = inCellIds(idx);
    curCellType = inCellTypes{idx};
    curSkel =  skel.keepTreeWithName([sprintf('_%03d',curCellId),'(?!\d+)'],'regexp');

    idxTreeDend = curSkel.getTreeWithName('dend','regexp');
    idxTreeAxon = curSkel.getTreeWithName('axon','regexp');

    f = figure();
    f.Color = 'black';
    hold on;

    somaCoords = Util.getNodeCoordsWithComment(curSkel,{'soma','cellbody'},'regexpi', idxTreeDend);
    if isempty(somaCoords)
        error('Empty soma %s', curSkel.names{:})
    end
    out.somaLocs(idx,:) = somaCoords;

    somaLocs = skel.setScale(somaCoords,scale);

    myelinLocs = Util.getNodeCoordsWithComment(curSkel,'(?<!un)myelin','regexpi');% negative lookbehind
    if ~isempty(myelinLocs)
            myelinLocs = skel.setScale(myelinLocs,scale);
    end

    endLocs = Util.getNodeCoordsWithComment(curSkel,'(real([\s_])?end|true([\s_])?end)','regexpi');
    if ~isempty(endLocs)
            endLocs = skel.setScale(endLocs,scale);
    end


    for k = [1,2]
        a(k) = subplot(2, 1, k);
        hold on

        % plot soma
        objectSomas = ...
                    scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3)...
                    ,150,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                    'MarkerFaceColor', somaColor(1:3));

        % plot dend tree
        curSkel.plot(idxTreeDend, dendColor, true, tubeSize,'','','','','',alphaVal);
        out.skelsDend{idx} = curSkel.deleteTrees(idxTreeDend, true);

        % plot axon
        if ~isempty(idxTreeAxon)
            curSkel.plot(idxTreeAxon, axonColor, true, tubeSize,'','','','','',alphaVal);
            out.skelsAxon{idx} = curSkel.deleteTrees(idxTreeAxon, true);
        else
            out.skelsAxon{idx} = [];
        end

        % check myelination start in axon
        if ~isempty(myelinLocs)
            objectMyelin = ...
                    scatter3(myelinLocs(:,1),myelinLocs(:,2),myelinLocs(:,3)...
                    ,50,'x','MarkerEdgeColor', [1,1,1], ...
                    'MarkerFaceColor', [1,1,1]);
        end

        if ~isempty(endLocs)
            objectEnd = ...
                    scatter3(endLocs(:,1),endLocs(:,2),endLocs(:,3)...
                    ,50,'x','MarkerEdgeColor', [1,0,0], ...
                    'MarkerFaceColor', [1,0,0]);
        end

        % switch view
        switch k
            case 1 % xy
                Util.setView(1, true);
            case 2 % xz
                Util.setView(3, true);
            case 3 % yz
                Util.setView(2, true);
        end

        if do3D
            % do once for sanity check then remove
            % axis on
            % box on
            % ax.BoxStyle = 'full';
            camorbit(10,0,'camera');
            camorbit(0,15,'data',[0 1 0]);
        end
        box on
        axis on
        axis equal
        set(gca,'XLim',bbox(1,:));
        set(gca,'YLim',bbox(2,:));
        set(gca,'ZLim',bbox(3,:));
        set(gca,'color','k')

    end
%    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
     
% scalebar
     scaleBarLength = 50; % um
     line(a(k),[bbox(1,2)-scaleBarLength, bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % x axis
     line([bbox(1,2), bbox(1,2)], [bbox(2,2)-scaleBarLength, bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % y axis
     %line([bbox(1,2), bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2)-scaleBarLength, bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % z axis

    annotation( ...
            f,...
            'textbox', [0, 0.01,1,0.1], ...
            'String', sprintf('IN_%03d_%s.png',curCellId, curCellType), ...
            'EdgeColor', 'none',...
            'Color', [1,1,1], ...
            'Interpreter','none',...
            'HorizontalAlignment', 'center')

    outfile = fullfile(outDirPdf,sprintf('IN_%03d_%s.png',curCellId, curCellType));
    export_fig(outfile,'-q101', '-nocrop', '-m8');
    Util.log('Saving file %s.', outfile);
end

% type indices for Amira
out.idxBipolar = bipolarFlag;
out.idxMultipolar = multipolarFlag;
out.idxUC = ~(bipolarFlag|multipolarFlag);
out.info = info;
outfile = fullfile(mainFolder,'outData','amira',sprintf('%s-IN_gallery_data-%s.mat',regionName,datestr(clock,30)));
Util.saveStruct(outfile, out);
sprintf('Saved to: %s', outfile)
