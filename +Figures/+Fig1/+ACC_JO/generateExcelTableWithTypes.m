regionName = 'ACC_JO';

Figures.setConfig; % set main region based folder

config.bbox = [0, 0, 0, 9216, 14336, 3328];

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholeCellsWithSoma');
file = dir(fullfile(nmlDir,'*.nml'));
skel = skeleton(fullfile(nmlDir,file.name)); % one tree per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% extract cell types from tree names
cellTypes = {};
for curTree = 1:skel.numTrees
    curTreeName = skel.names{curTree};
    if any(regexpi(curTreeName,'(Pyr)'))
        cellTypes{curTree} = 'PC';
    elseif  any(regexpi(curTreeName,'(exc|exn)'))
        cellTypes{curTree} = 'ExN';
    elseif  any(regexp(curTreeName,'IN')) % capital for IN otherwise spiny includes
        cellTypes{curTree} = 'IN';
    elseif  any(regexpi(curTreeName,'astro'))
        cellTypes{curTree} = 'astro';
    elseif  any(regexpi(curTreeName,'micro'))
        cellTypes{curTree} = 'microglia';
    elseif  any(regexpi(curTreeName,'oligo'))
        cellTypes{curTree} = 'oligo';
    elseif  any(regexpi(curTreeName,'glia'))
        cellTypes{curTree} = 'glia';
    elseif  any(regexp(curTreeName,'(UN_Neuron|UC|unsure)'))
        cellTypes{curTree} = 'UC';
   elseif  any(regexp(curTreeName,'L1'))
        cellTypes{curTree} = 'L1';
   elseif  any(regexpi(curTreeName,'Pericyte'))
        cellTypes{curTree} = 'Pericyte';
    else
        error('No type found!: %s', curTreeName)
    end
end
tempTable = table(skel.names, reshape(cellTypes,'',1));
writetable(tempTable, fullfile(mainFolder,'data', sprintf('%s_cell_types.xlsx', regionName)))
ct = categorical(cellTypes);
countTable = table;
countTable.PC = sum(ct == 'PC');
countTable.ExN = sum(ct == 'ExN');
countTable.IN = sum(ct == 'IN');
countTable.Glia = sum(ct == 'glia' | ct == 'astro' | ct == 'microglia' | ct == 'oligo');
countTable.UC = sum(ct == 'UC');
countTable.L1 = sum(ct == 'L1');
countTable.Pericyte = sum(ct == 'Pericyte');
disp(countTable)
%{
colors = [1,0,1,1; ...% ExN
          1,1,0,1; ... % Glia
          0.5,0.5,0.5,1; ...%IN
          1,0,1,1; ...% PC
          1,0,0,1; ...%UC  
          0.75,0.75,0,1; ... % Astro
          0,0,0,1; ... %  Endothelial
          0,0,0,1]; %  Pericyte
colors = colors(categorical(cellTypes),:);
skel.colors = mat2cell(colors,ones(1,size(colors,1)) ,4);
skel.write(fullfile(mainFolder,'data','tracings', sprintf('%s_cell_types_v01.nml', regionName)))
%}
