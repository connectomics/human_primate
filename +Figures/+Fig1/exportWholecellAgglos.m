% Export wholecell dendrite segmentations as iso using cell-types annotations
Util.log('Doing for %s', regionName)

info = Util.runInfo();
Util.showRunInfo(info);

Figures.setConfig
neuronsFlag = false;

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholeCellsWithSoma');
file = dir(fullfile(nmlDir,'*.nml'));
skel = skeleton(fullfile(nmlDir,file.name)); % one tree per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% keep neurons only
if neuronsFlag
    idxKeep = cellfun(@(x) any(regexp(x,'(PC|pyr|Pyr|IN|ExN|Exc|exc)')), skel.names);
    skel = skel.deleteTrees(idxKeep, true);
    sprintf('Found %d neurons', sum(idxKeep))
end

% Dendrites isosurfaces
switch regionName
    case 'ex145'
        mappingName = '';
    case 'ex144'
        mappingName = '';
    case 'PPC_AK'
        mappingName = ''; % default
    case 'ACC_JO'
        mappingName = '';
    case 'V2_L23'
        mappingName = '';
    case 'A1_JO'
        mappingName = 'agglomerate_view_30';
    case 'mk_L4'
        mappingName = '';
    case 'mk_L23'
        mappingName = 'agglomerate_view_70';
    case 'Mk1_T2'
        mappingName = 'agglomerate_view_50';
    case 'H5_5_AMK'
        mappingName = 'agglomerate_view_75';
    case 'H6_4S_L23'
        mappingName = 'agglomerate_view_60';
    case 'Mouse_A2'
        mappingName = '';
    otherwise
        error(sprintf('Unknown regionName %s',regionName))
end

doForThisRegion(regionName, skel, info, mappingName);

function doForThisRegion(regionName, skel, info, mappingName)
    setConfig
    
    if exist('mappingName','var') & ~isempty(mappingName)
        mappingDir = fileparts(param.files.aggloFile);
        af = AggloFile(fullfile(mappingDir, strcat(mappingName, '.hdf5')));
    else
        af = AggloFile(param.files.aggloFile);
    end
 
    timeStamp = datestr(clock,30);
    
    plyDir = fullfile(param.saveFolder,'amira','wholeCellAgglos', sprintf('auto_%s',timeStamp));
    mkdir(plyDir)

    % extract agglomerate IDs from skel, one agglo per tree
    [outAggloIds, outCellTypes] = extractAggloIdsFromSkel(skel, param, af, true);

    % extract segIDs comprising aggloIds per tree
    outAgglos = cell(numel(outAggloIds),1);
    for i=1:numel(outAggloIds)
        curAggloIds = outAggloIds{i}; % agglo ids per tree
        curAgglo = af.agglomerates(curAggloIds); % segIds per agglo as cell array
        outAgglos{i} = unique(vertcat(curAgglo{:})); % segIds merged across underlying agglomerates
    end
    Util.log('Found %d agglomerates for wholecells for %s', numel(outAgglos), regionName)

    % run jobs
    Util.log('Creating Iso file for wholecells: %s', plyDir)
    Visualization.exportAggloToAmira(param, outAgglos, plyDir,'reduce',0.01,'smoothSizeHalf',4,'smoothWidth',8);

    % out struct
    out = struct;
    out.plyFiles = plyDir;
    out.scale = config.scale;
    out.bbox = Util.convertMatlabToWKBbox(param.bbox, false);
    out.datasetName = config.datasetName;
    out.cellTypes = outCellTypes;
    out.info = info;
    mkdir(fullfile(mainDir,'figures','fig1','amira','wholeCellAgglos'))
    outFile = fullfile(mainDir,'figures','fig1','amira','wholeCellAgglos', sprintf('%s_data_iso_files_%s.mat', regionName, timeStamp));
    Util.saveStruct(outFile, out)
    Util.protect(outFile);
    sprintf('Data file at :\n %s', outFile)
end

function [outAggloIds, outCellTypes] = extractAggloIdsFromSkel(skel, param, af, nucleiFlag)
    Util.log('Segment pickup and corresponding aggloID pickup')
    if nucleiFlag
        % all agglomerate nodes
        outAggloIds = cell(skel.numTrees,1);
        outCellTypes = cell(skel.numTrees,1);
        for i = 1:skel.numTrees
            nodeCoords = skel.getNodes(i);
            curSegIds = Seg.Global.getSegIds(param, nodeCoords);
            curAggloIds = af.segmentToAgglomerateIds(curSegIds);
            outAggloIds{i} = curAggloIds; % agglo ids for cur tree
            clear curSegIds curAggloIds

            if any(regexp(skel.names{i}, '(Pyr|PC|pyr|Exc|exc|exn|Exn|ExN)'))
                outCellTypes{i} = 'Pyr';
            elseif any(regexp(skel.names{i}, 'IN'))
                outCellTypes{i} = 'IN';
            elseif any(regexp(skel.names{i}, '(Astro|astro|glia|Glia|Oligo|oligo|)'))
                outCellTypes{i} = 'Glia';
            else
                outCellTypes{i} = 'UC';
            end

        end
    else
        outAggloIds = cell(skel.numTrees,1);
        outCellTypes = cell(skel.numTrees,1);
        for i = 1:skel.numTrees
            curSkel = skel.deleteTrees(i, true);
            nodeIdxSoma = curSkel.getNodesWithComment('(soma|cellbody)',1,'regexp');
            nodeIdxOuter = setdiff(1:size(curSkel.nodes{1},1), nodeIdxSoma); % node in wholecell agglomerate
            nodeCoordOuter = curSkel.nodes{1}(nodeIdxOuter,1:3);
            segIdxOuter = Seg.Global.getSegIds(param, nodeCoordOuter);
            outAggloIds{i} = af.segmentToAgglomerateIds(segIdxOuter);

            if any(regexp(skel.names{i}, '(Pyr|PC|pyr|Exc|exc|exn|Exn|ExN)'))
                outCellTypes{i} = 'Pyr';
            elseif any(regexp(skel.names{i}, 'IN'))
                outCellTypes{i} = 'IN';
            elseif any(regexp(skel.names{i}, '(Astro|astro|glia|Glia|Oligo|oligo|)'))
                outCellTypes{i} = 'Glia';
            else
                outCellTypes{i} = 'UC';
            end

        end
    end
end
