regionName = 'H5_5_AMK';

Figures.setConfig; % set main region based folder


Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholeCellsWithSoma');
file = dir(fullfile(nmlDir,'*.nml'));
skel = skeleton(fullfile(nmlDir,file.name)); % one tree per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% extract cell types from tree names
cellTypes = {};
for curTree = 1:skel.numTrees
    curTreeName = skel.names{curTree};
    if any(regexpi(curTreeName,'(Pyr)'))
        cellTypes{curTree} = 'PC';
    elseif  any(regexpi(curTreeName,'(exc|exn)'))
        cellTypes{curTree} = 'ExN';
    elseif  any(regexp(curTreeName,'IN')) % capital for IN otherwise spiny includes
        cellTypes{curTree} = 'IN';
    elseif  any(regexpi(curTreeName,'astro'))
        cellTypes{curTree} = 'astro';
    elseif  any(regexpi(curTreeName,'micro'))
        cellTypes{curTree} = 'microglia';
    elseif  any(regexpi(curTreeName,'oligo'))
        cellTypes{curTree} = 'oligo';
    elseif  any(regexpi(curTreeName,'glia'))
        cellTypes{curTree} = 'glia';
    elseif  any(regexp(curTreeName,'(UN_Neuron|UC)'))
        cellTypes{curTree} = 'UC';
    else
        error('No type found!: %s', curTreeName)
    end
end
tempTable = table(skel.names, reshape(cellTypes,'',1));
writetable(tempTable, fullfile(mainFolder,'data', sprintf('%s_cell_types.xlsx', regionName)))
ct = categorical(cellTypes);
countTable = table;
countTable.PC = sum(ct == 'PC');
countTable.ExN = sum(ct == 'ExN');
countTable.IN = sum(ct == 'IN');
countTable.Glia = sum(ct == 'glia' | ct == 'astro' | ct == 'microglia' | ct == 'oligo');
countTable.UC = sum(ct == 'UC');
disp(countTable)
%{
colors = [1,0,1,1; ...% ExN
          1,1,0,1; ... % Glia
          0.5,0.5,0.5,1; ...%IN
          1,0,1,1; ...% PC
          1,0,0,1; ...%UC  
          0.75,0.75,0,1; ... % Astro
          0,0,0,1; ... %  Endothelial
          0,0,0,1]; %  Pericyte
colors = colors(categorical(cellTypes),:);
skel.colors = mat2cell(colors,ones(1,size(colors,1)) ,4);
skel.write(fullfile(mainFolder,'data','tracings', sprintf('%s_cell_types_v01.nml', regionName)))
%}
