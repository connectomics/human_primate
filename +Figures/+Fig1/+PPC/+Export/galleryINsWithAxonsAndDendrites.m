% export cell bodies from skeletons to plot in amira by Heiko
regionName = 'PPC_AK';

Figures.setConfig; % set main region based folder

bbox = Util.convertWebknossosToMatlabBbox(config.bbox);

info = Util.runInfo();
Util.showRunInfo(info);

somaSize = 50; tubeSize = 2;
do3D = false;

outDirPdf = fullfile(mainFolder,'outData/figures/wholecells/IN/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

Util.log('Loading nml with IN axon and dend tracings...')
nmlDir = fullfile(mainFolder,'data/tracings/wholecells/IN/');
file = dir(fullfile(nmlDir,'*.nml'))
skel = skeleton(fullfile(nmlDir,file.name)); % two trees per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% extract unique IDs
skelDend = skel.keepTreeWithName('(Cell|Soma)','regexpi');
inCellIds = [];
inCellTypes = {};
for i=1:skelDend.numTrees
    curSkel = skelDend.deleteTrees(i,true);
    curId = regexpi(curSkel.names{1}, '(Cell|cell|soma)_(?<id>\d+)','names');
    curId = str2num(curId(1).id); % could be duplicate
    inCellIds(i) = curId;

    if contains(curSkel.names{1}, '_BP')
        inCellTypes{i} = 'BP';
    elseif contains(curSkel.names{1}, '_MP')
        inCellTypes{i} = 'MP';
    else
        inCellTypes{i} = 'UC';
    end

end

assert(numel(inCellIds) == skelDend.numTrees)
sprintf('Found %d IN cell Ids',numel(inCellIds))

% scale all spheres
scale = skel.scale./1000;
bbox = bsxfun(@times, bbox, config.scale(:)./1000);

%% plot cell bodies per type with dendrite
alphaVal = 1;
somaColor = [0.5,0.5,0.5]; % grey IN
dendColor = [0.5,0.5,0.5]; % grey
axonColor = [0,158,115]/255; % green

% amira data
out = struct();

Util.log('Plotting INs for gallery:')
for idx = 1:numel(inCellIds)
    curCellId = inCellIds(idx);
    curCellType = inCellTypes{idx};
    curSkel =  skel.keepTreeWithName([sprintf('_%03d',curCellId),'(?!\d+)'],'regexp');

    idxTreeDend = curSkel.getTreeWithName('dend','regexp');
    idxTreeAxon = curSkel.getTreeWithName('axon','regexp');

    f = figure();
    f.Color = 'black';
    hold on;

    somaCoords = Util.getNodeCoordsWithComment(curSkel,{'soma','cellbody'},'regexpi', idxTreeDend);
    if isempty(somaCoords)
        warning('Empty soma %s', curSkel.names{:})
        somaLocs = [];
    else
        out.somaLocs(idx,:) = somaCoords;
        somaLocs = skel.setScale(somaCoords,scale);
    end

    myelinLocs = Util.getNodeCoordsWithComment(curSkel,'(?<!un)myelin','regexpi');% negative lookbehind
    if ~isempty(myelinLocs)
            myelinLocs = skel.setScale(myelinLocs,scale);
    end

    endLocs = Util.getNodeCoordsWithComment(curSkel,'(real([\s_])?end|true([\s_])?end)','regexpi');
    if ~isempty(endLocs)
            endLocs = skel.setScale(endLocs,scale);
    end


    for k = [1,2]
        a(k) = subplot(2, 1, k);
        hold on
        
        % plot soma
        if ~isempty(somaLocs)
        objectSomas = ...
                    scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3)...
                    ,150,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                    'MarkerFaceColor', somaColor(1:3));
        end
        % plot dend tree
        curSkel.plot(idxTreeDend, dendColor, true, tubeSize,'','','','','',alphaVal);
        out.skelsDend{idx} = curSkel.deleteTrees(idxTreeDend, true);

        % plot axon
        if ~isempty(idxTreeAxon)
            curSkel.plot(idxTreeAxon, axonColor, true, tubeSize,'','','','','',alphaVal);
            out.skelsAxon{idx} = curSkel.deleteTrees(idxTreeAxon, true);
        else
            out.skelsAxon{idx} = [];
        end

        % check myelination start in axon
        if ~isempty(myelinLocs)
            objectMyelin = ...
                    scatter3(myelinLocs(:,1),myelinLocs(:,2),myelinLocs(:,3)...
                    ,50,'x','MarkerEdgeColor', [1,1,1], ...
                    'MarkerFaceColor', [1,1,1]);
        end

        if ~isempty(endLocs)
            objectEnd = ...
                    scatter3(endLocs(:,1),endLocs(:,2),endLocs(:,3)...
                    ,50,'x','MarkerEdgeColor', [1,0,0], ...
                    'MarkerFaceColor', [1,0,0]);
        end

        % switch view
        switch k
            case 1 % xy
                Util.setView(1, true);
            case 2 % xz
                Util.setView(2, true);
            case 3 % yz
                Util.setView(3, true);
        end

        if do3D
            % do once for sanity check then remove
            % axis on
            % box on
            % ax.BoxStyle = 'full';
            camorbit(10,0,'camera');
            camorbit(0,15,'data',[0 1 0]);
        end
        box on
        axis on
        axis equal
        set(gca,'XLim',bbox(1,:));
        set(gca,'YLim',bbox(2,:));
        set(gca,'ZLim',bbox(3,:));
        set(gca,'color','k')

    end
%    Visualization.Figure.removeWhiteSpaceInSubplotOfTwo(f,a, 'col');
     % scalebar
     scaleBarLength = 50; % um
     line(a(k),[bbox(1,2)-scaleBarLength, bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % x axis
     %line([bbox(1,2), bbox(1,2)], [bbox(2,2)-scaleBarLength, bbox(2,2)], [bbox(3,2), bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % y axis
     %line([bbox(1,2), bbox(1,2)], [bbox(2,2), bbox(2,2)], [bbox(3,2)-scaleBarLength, bbox(3,2)],'LineStyle','-','LineWidth',4,'Color',[1,1,1]) % z axis

    annotation( ...
            f,...
            'textbox', [0, 0.01,1,0.1], ...
            'String', sprintf('IN_%03d_%s.png',curCellId, curCellType), ...
            'EdgeColor', 'none',...
            'Color', [1,1,1], ...
            'Interpreter','none',...
            'HorizontalAlignment', 'center')

    outfile = fullfile(outDirPdf,sprintf('IN_%03d_%s.png',curCellId, curCellType));
    export_fig(outfile,'-q101', '-nocrop', '-m8');
    Util.log('Saving file %s.', outfile);
end
%{
% type indices for Amira
out.idxBipolar = bipolarFlag;
out.idxMultipolar = multipolarFlag;
out.idxUC = ~(bipolarFlag|multipolarFlag);
out.info = info;
outfile = fullfile(mainFolder,'outData','amira',sprintf('%s-IN_gallery_data-%s.mat',regionName,datestr(clock,30)));
Util.saveStruct(outfile, out);
sprintf('Saved to: %s', outfile)
%}
