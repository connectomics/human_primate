clear; clc;
% report segmentation boxes for all datasets
regionNames =  {'ex144','Mouse_A2','V2_L23','PPC_AK','ACC_JO','V2_L23', ...
                'mk_L23','Mk1_T2', 'H5_5_AMK','H6_4S_L23'};
            
for i=1:numel(regionNames)
   regionName = regionNames{i};
   setConfig % screening voxelytics pipeline
   param.bbox = Util.convertMatlabToWKBbox(param.bbox);
   
   dims = (param.bbox(4:end) .* config.scale)/1e3;
   Util.log('%s %d x %d x %d um3', regionName, floor(dims(1)), floor(dims(2)), floor(dims(3)))
end