% export seeds soma for dendrite tracings
regionName = 'H5_10_ext';

Figures.setConfig; % set main region based folder

info = Util.runInfo();
Util.showRunInfo(info);

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholeCellsWithSoma');
file = dir(fullfile(nmlDir,'*.nml'));
skel = skeleton(fullfile(nmlDir,file.name)); % one tree per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% extract cell types from tree names
idxIN  = contains(skel.names,'IN');
skel = skel.deleteTrees(idxIN, true);
Util.log('Found %d IN somata:', skel.numTrees)
seedNodes = [];
for i=1:skel.numTrees
    seedNodes(i,:) = Util.getNodeCoordsWithComment(skel,'cellbody','partial',i);
end

% remove previous seeds
m = load('/u/sahilloo/data/H5_10_ext/data/tracings/hiwi_projects_INs/H5_10_ext-IN_seeds-20210930T151734.mat');
idxKeep = ~ismember(seedNodes, m.seeds, 'rows');
seedNodes = seedNodes(idxKeep,:);

% export
out = struct;
out.seeds = seedNodes;
out.datasetName = config.datasetName;
out.info = info;
outfile = fullfile(mainFolder,'data','tracings','hiwi_projects_INs',sprintf('%s-IN_seeds_ARK-%s.mat',regionName,datestr(clock,30)));
Util.saveStruct(outfile, out);
sprintf('Saved to: %s', outfile)

