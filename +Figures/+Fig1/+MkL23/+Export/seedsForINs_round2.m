% send seeds to heiko for unfinished IN mk L23 dendrites

regionName = 'mk_L23';

Figures.setConfig; % set main region based folder

info = Util.runInfo();
Util.showRunInfo(info);

Util.log('Loading nml with IN axon and dend tracings...')
nmlDir = fullfile(mainFolder,'data/tracings/wholecells/IN/');
file = dir(fullfile(nmlDir,'*_unfinished.nml'))
skel = skeleton(fullfile(nmlDir,file.name)); % two trees per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% extract unique IDs
skelDend = skel.keepTreeWithName('unfinished','regexp');

seedCoords = [];
for i=1:skelDend.numTrees
    curSkel = skelDend.deleteTrees(i,true);
    somaCoords = Util.getNodeCoordsWithComment(curSkel,{'soma','cellbody'},'regexpi');
    if isempty(somaCoords)
        error('Empty soma')
    end
    seedCoords = [seedCoords; somaCoords];
    clear somaCoords
end

sprintf('Found %d unfinished IN cell Ids',size(seedCoords,1))
disp(skelDend.names)

outfile = fullfile(mainFolder,'seeds',sprintf('%s-seedsForINsDendrites-%s.mat',regionName,datestr(clock,30)));
Util.save(outfile,...
        info, skel, seedCoords);
sprintf('Written to :%s', outfile)

