% extract data for INsi + UC  and save
regionName = 'mk_L23';

Figures.setConfig; % set main region based folder

xlsfile = config.wholeCellsExcel;

outDirPdf = fullfile(mainFolder,'outData/figures/wholecells/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

% load data from excel sheet
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);
xlTable(1,:) = []; % remove headers

% remove empty rows
cellIds = xlTable.xl1;
cellTypes = xlTable.xl2;
comments = xlTable.xl3;

idxNan = cellfun(@(x) any(isnan(x)), cellIds);
cellIds(idxNan) = [];
cellTypes(idxNan) = [];
comments(idxNan) = [];

% count cells in each classification group
idxPC = cellfun(@(x) any(regexpi(x,'(Pyr|PC|ExN)')), cellTypes);
idxIN = cellfun(@(x) any(regexpi(x,'^IN')), cellTypes); % occurs in undefined
idxGlia = cellfun(@(x) any(regexpi(x,'(Glia|Astro)')), cellTypes);
idxUC = cellfun(@(x) any(regexpi(x,'(UC|undefined|unsure)')), cellTypes);
idxPeri = cellfun(@(x) any(regexpi(x,'Pericyte')), cellTypes);

nanComments = cellfun(@(x) any(isnan(x)), comments);
idxBorder = false(numel(comments),1);
idxBorder(~nanComments) = cellfun(@(x) any(regexpi(x,'border')), comments(~nanComments));
idxNA = cellfun(@(x) any(regexp(x,'/')), cellTypes);

% update cell types
cellTypes(idxPC) = {'PC'};
cellTypes(idxIN) = {'IN'};
cellTypes(idxGlia) = {'Glia'};
cellTypes(idxUC) = {'UC'};

countNeurons = sum(idxPC|idxIN);
countUC = sum(idxUC);
countExc = sum(idxPC);
countIN = sum(idxIN);
countGlia = sum(idxGlia);
countPericytes = sum(idxPeri);
totalCells = sum([countNeurons, countGlia, countUC]);

% output stats
sprintf('Total cell bodies: %d ', totalCells)
sprintf('Total neurons: %d (%.2f), total Glia: %d (%.2f), UC: %d (%.2f)', countNeurons, countNeurons/totalCells, ...
                                                     countGlia, countGlia/totalCells,...
                                                     countUC, countUC/totalCells)
d = sum([countExc, countIN]);
sprintf('Within neurons:- \n Exc: %d (%.2f), INs: %d (%.2f) ',...
                                                     countExc, countExc/d, ...
                                                     countIN, countIN/d)

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholeCellsWithSoma');
file = dir(fullfile(nmlDir,'*.nml'))
skel = skeleton(fullfile(nmlDir,file.name)); % one tree per cell

% extract soma positions and add to corresponding cellId
Util.log('Extracing soma comments:')
nodesSoma = zeros(skel.numTrees,3);
colors = zeros(skel.numTrees,4);
for i=1:skel.numTrees

    curSkel = skel.deleteTrees(i,true);
    somaCoords = Util.getNodeCoordsWithComment(curSkel,{'soma','cellbody'},'regexpi');

    curId = regexpi(curSkel.names{1}, '(cell|soma)_(?<id>\d+)','names');
    curId = str2num(curId(1).id); % could be duplicate
    
    if ~isempty(somaCoords)
        nodesSoma(curId,:) = somaCoords;
    else
        warning(sprintf('Empty soma %d',curId))
    end
end

% remove empty ID soma nodes
idxDel = any(nodesSoma,2);
nodesSoma = nodesSoma(idxDel,:);
assert(size(nodesSoma,1) == numel(cellIds))
assert(size(nodesSoma,1) == numel(cellTypes))


% type indices for Amira
types = struct();
types.idxExc = idxPC;
types.idxInh = idxIN;
types.idxGlia = idxGlia;

% seed INs + UC
seedCoords = nodesSoma(idxIN | idxUC,:);

Util.save(fullfile(mainFolder,'seeds',sprintf('%s-seedsForINsDendrites-%s.mat',regionName,datestr(clock,30))),...
        nodesSoma, types, seedCoords);
