% Use mk L23 seeded cellbodies of INs to link to traced dendrites
regionName = 'mk_L23';

%seedFile = fullfile(mainFolder,'seeds', 'mk_L23-seedsForINsDendrites-20210127T111924.mat');
%seedData = load(seedFile);
projectName = 'SL_Mk1_F6_L23_JS_SubI_v1_IN_dendrites_27_01_2021';

info = Util.runInfo();
Util.showRunInfo(info);

Figures.setConfig; % set main region based folder
xlsfile = config.wholeCellsExcel;

% load data from excel sheet
[~,~,xl] = xlsread(xlsfile);
xlTable = cell2table(xl);
xlTable(1,:) = []; % remove headers

% remove empty rows
cellIds = xlTable.xl1;
cellTypes = xlTable.xl2;
comments = xlTable.xl3;

idxNan = cellfun(@(x) any(isnan(x)), cellIds);
cellIds(idxNan) = [];
cellTypes(idxNan) = [];
comments(idxNan) = [];

% count cells in each classification group
idxPC = cellfun(@(x) any(regexpi(x,'(Pyr|PC|ExN)')), cellTypes);
idxIN = cellfun(@(x) any(regexpi(x,'^IN')), cellTypes); % occurs in undefined
idxGlia = cellfun(@(x) any(regexpi(x,'(Glia|Astro)')), cellTypes);
idxUC = cellfun(@(x) any(regexpi(x,'(UC|undefined|unsure)')), cellTypes);
idxPeri = cellfun(@(x) any(regexpi(x,'Pericyte')), cellTypes);

nanComments = cellfun(@(x) any(isnan(x)), comments);
idxBorder = false(numel(comments),1);
idxBorder(~nanComments) = cellfun(@(x) any(regexpi(x,'border')), comments(~nanComments));
idxNA = cellfun(@(x) any(regexp(x,'/')), cellTypes);

% update cell types
cellTypes(idxPC) = {'PC'};
cellTypes(idxIN) = {'IN'};
cellTypes(idxGlia) = {'Glia'};
cellTypes(idxUC) = {'UC'};

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholeCellsWithSoma');
file = dir(fullfile(nmlDir,'*.nml'))
skel = skeleton(fullfile(nmlDir,file.name)); % one tree per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% assertions
assert(numel(skel.names) == numel(cellTypes))
assert(issorted(skel.names))
assert(issorted(cellIds))

% extract soma positions and add to corresponding cellId
Util.log('Extracing soma comments:')
nodesSoma = zeros(skel.numTrees,3);
colors = zeros(skel.numTrees,4);
for i=1:skel.numTrees

    curSkel = skel.deleteTrees(i,true);
    somaCoords = Util.getNodeCoordsWithComment(curSkel,{'soma','cellbody'},'regexpi');

    curId = regexpi(curSkel.names{1}, '(cell|soma)_(?<id>\d+)','names');
    curId = str2num(curId(1).id); % could be duplicate

    if ~isempty(somaCoords)
        nodesSoma(curId,:) = somaCoords;
    else
        warning(sprintf('Empty soma %d',curId))
    end
end

% remove empty ID soma nodes
idxDel = any(nodesSoma,2);
nodesSoma = nodesSoma(idxDel,:);
assert(size(nodesSoma,1) == numel(cellIds))
assert(size(nodesSoma,1) == numel(cellTypes))

% load task data from xlsx
projectTaskFile = fullfile(mainFolder, 'data','tracings','hiwiProjects', [projectName, '.csv']);
xlTable = readtable(projectTaskFile);
taskIds = xlTable.taskId;
seedCoords = [xlTable.x, xlTable.y, xlTable.z];

% load traced dendrites
hiwiProjectNmls = fullfile(mainFolder, 'data', 'tracings', 'hiwiProjects', projectName);
files = dir(fullfile(hiwiProjectNmls,'*.nml'));

hiwiProjectNmlsProcessed = fullfile(mainFolder, 'data', 'tracings', 'hiwiProjects', strcat(projectName, '_processed'))
mkdir(hiwiProjectNmlsProcessed)

Util.log('Processing hiwi tracings %s:', projectName)
for curFile = 1:numel(files)
    curSkel = skeleton(fullfile(hiwiProjectNmls, files(curFile).name));

    d = regexp(curSkel.filename, [config.datasetName '__(?<taskId>\w*)__(?<username>\w*)(__|-)'],'names');
    curTaskId = d.taskId;

    idxFound = contains(taskIds, curTaskId);
    curSeedCoords =  seedCoords(idxFound,:);

    % corresponding soma coords
    idxSomaFound = ismember(nodesSoma, curSeedCoords, 'rows');
    if any(idxSomaFound)
        curSomaName = cellIds{idxSomaFound};
    else
        warning('Soma not found for: %s at ',curTaskId)
        disp(curSeedCoords)
        curSomaName = curTaskId;
    end

    % soma node Idx
    curNodes = curSkel.getNodes;
    somaNodeIdx = find(ismember(curNodes, curSeedCoords + [1,1,1], 'rows')); % offset

    % write new skel
    skelOut = curSkel;
    skelOut.names{1} = strcat(curSomaName, '_dend');
    if isempty(somaNodeIdx)
        warning('Soma node not found in %s', curSkel.names{1})
    else
        skelOut = skelOut.setComments(1, somaNodeIdx, sprintf('cellbody_%s', curSomaName));
    end
    skelOut.write(fullfile(hiwiProjectNmlsProcessed, [curSomaName,'.nml']));
    clear skelOut curSkel
end

