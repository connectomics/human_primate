% extract data and save
regionName = 'ex144';

Figures.setConfig; % set main region based folder

info = Util.runInfo();
Util.showRunInfo(info);

xlsfile = config.wholeCellsExcel;

outDirPdf = fullfile(mainFolder,'outData/figures/wholecells/');
if ~exist(outDirPdf,'dir')
   mkdir(outDirPdf);
end

% load data from excel sheet
xlTable = readtable(xlsfile);

% remove empty rows
cellIds = xlTable.Var1;
cellTypes = xlTable.Var2;
%comments = xlTable.xl3;

idxNan = cellfun(@(x) any(isnan(x)), cellIds);
cellIds(idxNan) = [];
cellTypes(idxNan) = [];
%comments(idxNan) = [];

% count cells in each classification group
idxPC = cellfun(@(x) any(regexpi(x,'(Pyr|PC|ExN)')), cellTypes);
idxIN = cellfun(@(x) any(regexpi(x,'^IN')), cellTypes); % occurs in undefined
idxGlia = cellfun(@(x) any(regexpi(x,'(glia|astro|microglia|oligo)')), cellTypes);
idxUC = cellfun(@(x) any(regexpi(x,'(UC|undefined|unsure)')), cellTypes);
idxPeri = cellfun(@(x) any(regexpi(x,'Pericyte')), cellTypes);

%nanComments = cellfun(@(x) any(isnan(x)), comments);

% update cell types
cellTypes(idxPC) = {'PC'};
cellTypes(idxIN) = {'IN'};
cellTypes(idxGlia) = {'Glia'};
cellTypes(idxUC) = {'UC'};

% save for later
countNeurons = sum(idxPC|idxIN);
countUC = sum(idxUC);
countExc = sum(idxPC);
countIN = sum(idxIN);
countGlia = sum(idxGlia);
countPericytes = sum(idxPeri);
totalCells = sum([countNeurons, countGlia, countUC]);

Util.save(fullfile(mainFolder,'outData','wholeCellsTypeData.mat'),countNeurons, countGlia, countUC, countExc, countIN, countPericytes)

% output stats
sprintf('Total cell bodies: %d ', totalCells)
sprintf('Total neurons: %d (%.2f), total Glia: %d (%.2f), UC: %d (%.2f)', countNeurons, countNeurons/totalCells, ...
                                                     countGlia, countGlia/totalCells,...
                                                     countUC, countUC/totalCells)
d = sum([countExc, countIN]);
sprintf('Within neurons:- \n Exc: %d (%.2f), INs: %d (%.2f) ',...
                                                     countExc, countExc/d, ...
                                                     countIN, countIN/d)

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholeCellsWithSoma');
file = dir(fullfile(nmlDir,'*.nml'))
skel = skeleton(fullfile(nmlDir,file.name)); % one tree per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% assertions
assert(numel(skel.names) == numel(cellTypes))
assert(issorted(skel.names))
assert(issorted(cellIds))

% extract soma positions and add to corresponding cellId
Util.log('Extracing soma comments:')
nodesSoma = zeros(skel.numTrees,3);
colors = zeros(skel.numTrees,4);
for i=1:skel.numTrees

    curSkel = skel.deleteTrees(i,true);
    somaCoords = Util.getNodeCoordsWithComment(curSkel,{'soma','cellbody'},'regexpi');

    curId = regexpi(curSkel.names{1}, '(cell|soma)_(?<id>\d+)','names');
    curId = str2num(curId(1).id); % could be duplicate
    
    if ~isempty(somaCoords)
        nodesSoma(curId,:) = somaCoords;
    else
        warning(sprintf('Empty soma %d',curId))
    end
end

% remove empty ID soma nodes
idxDel = any(nodesSoma,2);
nodesSoma = nodesSoma(idxDel,:);
assert(size(nodesSoma,1) == numel(cellIds))
assert(size(nodesSoma,1) == numel(cellTypes))

% type indices for Amira
types = struct();
types.idxExc = idxPC;
types.idxInh = idxIN;
types.idxGlia = idxGlia;

% sanity check
table(skel.names(types.idxExc), cellIds(types.idxExc))
table(skel.names(types.idxGlia), cellIds(types.idxGlia))
table(skel.names(types.idxInh), cellIds(types.idxInh))

keyboard
Util.save(fullfile(mainFolder,'outData','amira',sprintf('%s-wholeCellsSkelsSomaCoordsColors-%s.mat',regionName,datestr(clock,30))),...
        nodesSoma, colors, types, info);
