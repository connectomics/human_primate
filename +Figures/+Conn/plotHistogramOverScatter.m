function [axPost, axPre] = plotHistogramOverScatter(fig, ax, dataX, dataY, xBinWidth, yBinWidth, xLim, yLim)
    %% Histogram over incoming synapses
    Util.log('Plotting histogram post...')
    axPost = axes(fig,'Color','none');
    axPost.Position = ax.Position;
    axPost.Position(2:2:4) = [ax.Position(2)+ax.Position(4), 0.05]; % top
    axPost.Position(2:2:4) = [0.05, ax.Position(2)-0.05]; % bottom
    axPost.Color = 'none';
    
    histogram(axPost, dataX,...
        'BinWidth', xBinWidth, ...
        'EdgeColor', 'k', ...
        'DisplayStyle','stairs', ...
        'FaceAlpha', 0, 'LineWidth',2);

    axPost.YDir = 'reverse';
    axPost.TickDir = 'out';
    axPost.YMinorTick = 'off';

    xlim(axPost, xLim);
    %ylim(axPost, [0, max(max(postSynCount),1)]);
    ylabel(axPost, 'Freq.');
    set(gca, 'color','none')
    
    %% Histogram over outgoing synapses
    Util.log('Plotting histogram pre...')
    axPre = axes(fig,'Color','none');
    axPre.Position = ax.Position;
    axPre.Position(1) = sum(ax.Position(1:2:end));
    axPre.Position(3) = 0.95 - axPre.Position(1);
    axPre.Color = 'none';
    histogram(axPre, dataY, ...
        'BinWidth', yBinWidth, ...
        'Orientation', 'horizontal', ...
        'EdgeColor','k', ...
        'DisplayStyle','stairs', ...
        'FaceAlpha', 0, 'LineWidth',2);

    axPre.TickDir = 'out';
    axPre.XAxisLocation = 'top';
    axPre.XMinorTick = 'off';
    axPre.YDir = 'normal';
    axPre.YAxisLocation = 'right';

    %xlim(axPre, [0, 1]);
    ylim(axPre, yLim);
    xlabel('Freq.');
    set(gca, 'color','none')
end
