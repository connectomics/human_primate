function [synMap, somaMap, shaftMap, spineMap, aisMap] = getPreConnectome(skelAll, preClassIdx, postClassIds, outDir)
    
    skel = skelAll.deleteTrees(preClassIdx,true); % all except pre

    preIds = 1:skel.numTrees; % random ids for pre axons
    spineMap = doPre(skel, preIds, postClassIds, 'sph');
    shaftMap = doPre(skel, preIds, postClassIds, 'sh');
    aisMap = doPre(skel, preIds, postClassIds, 'ais');
    somaMap =  doPre(skel, preIds, postClassIds, 'soma');

    synMap = somaMap + shaftMap + spineMap + aisMap;
end

function synMap = doPre(skel, preIds, postIds, searchStr)
    assert(numel(postIds) == max(postIds))
   synMap = zeros(numel(preIds),numel(postIds));
   for i=1:numel(preIds)
        axonComments = skel.getAllComments(i);% per axon
        for j=1:numel(axonComments)
            thisComment = axonComments{j};
            id = regexp(thisComment,[searchStr ' [ipu](?<id>\w+)'],'names');% sh i05
            if isempty(id)
                clear id
                continue;
            end
            id = str2double(id.id);
            synMap(i,id) = synMap(i,id)+1;
        end
    end
end
