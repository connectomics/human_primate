function outTable = computePosteriorExc(outTable, regionName, varargin)
    % compute excitatory posterior probability assuming binomial distrbution and certain priors

    % default arguments
    priors = getPriorsForAxonModelling(regionName);

    % overwrite with user specified options
    if ~isempty(varargin)
        opts = varargin(1:2:end);
        optExists = ismember(opts, fieldnames(priors));
        if ~all(optExists)
            error('Unrecognized options ''%s''.\n', ...
                opts{find(~optExists,1)});
        end
        priors = Util.modifyStruct(priors, varargin{:});
    end

    Util.log(sprintf('Modelling: with priors: excFrac: %.2f, excSphFrac:%.2f, inhSphFrac: %.2f',...
                 priors.excAxonAggloFrac, priors.excSingleSynSpineHeadSynFrac, priors.inhSingleSynSpineHeadSynFrac))
    
    % Compute posterior
    priorExcAxonAggloFrac = priors.excAxonAggloFrac;
    priorInhAxonAggloFrac = 1 - priorExcAxonAggloFrac;
    
    priorExcSingleSynSpineHeadSynFrac = priors.excSingleSynSpineHeadSynFrac;
    priorInhSingleSynSpineHeadSynFrac = priors.inhSingleSynSpineHeadSynFrac;
    
    curSingleSynSpineHeadSynCounts = outTable.sph;
    curAxonSynCounts = outTable.total;
    
    curLikExc = priorExcAxonAggloFrac * binopdf( ...
        curSingleSynSpineHeadSynCounts, curAxonSynCounts, ...
        priorExcSingleSynSpineHeadSynFrac);
    curLikInh = priorInhAxonAggloFrac * binopdf( ...
        curSingleSynSpineHeadSynCounts, curAxonSynCounts, ...
        priorInhSingleSynSpineHeadSynFrac);
    
    curPosteriorExc = curLikExc ./ (curLikExc + curLikInh);
    if any(isnan(curPosteriorExc))
        curMask = isnan(curPosteriorExc);
        Util.log('Patching %d NaN entries', sum(curMask));
        curPosteriorExc(curMask) = mean(curPosteriorExc, 'omitnan');
    end
    
    outTable.curPosteriorExc = curPosteriorExc;
    outTable.curLikExc = curLikExc;
    outTable.curLikInh = curLikInh;
end

function priors = getPriorsForAxonModelling(regionName)
% priors for building a binomial model for classifying axons into exc and inh types
switch regionName
    case {'ex145', 'ex144', 'PPC_AK','ACC_JO','V2_L23', 'Mouse_A2'}
        priors.excAxonAggloFrac = 0.9;
        priors.excSingleSynSpineHeadSynFrac = 0.8;
        priors.inhSingleSynSpineHeadSynFrac = 0.2;
    case {'H5_5_AMK', 'mk_L23', 'mk_L4','H5_10_L23','H5_10_L4','H5_10_ext', 'H6_4S_L23', 'Mk1_T2', 'H2_3'}
        priors.excAxonAggloFrac = 0.8;
        priors.excSingleSynSpineHeadSynFrac = 0.30;
        priors.inhSingleSynSpineHeadSynFrac = 0.05;
end

end
