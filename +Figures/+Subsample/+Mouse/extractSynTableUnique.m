% Use soma-based dendrites tracing and to sub-sample regions to get exc fraction
% save unique synaspes dist table

clear 

config = struct;

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'mouse_L4_hua';
plotPerDend = false;
doAxons = false;

Figures.setConfig

Util.log('Doing for this region: %s', regionName)
switch regionName
     case {'mk_L4', 'mk_L23', 'H5_10_L4', 'H5_10_L23', 'rat_L23_MEC', 'mouse_L4_hua'}
        tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling'); % NOTE: These are all on single branches
     case {'ex144', 'PPC_AK', 'V2_L23', 'lpta','ACC_JO', 'H2_3'}
        tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling_unique'); % NOTE     
end

outDir = fullfile(mainFolder, 'results');

if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'dend_*.nml'));
func = @(x) str2double(x.id);

Util.log('doing for region: %s', regionName)
% do per dend group
for i = 1:numel(files)
    nml = fullfile(tracingFolder, files(i).name);
    skelAll = skeleton(nml);

    % check for duplicate nodes
    assert(size(skelAll.nodes{1},1) == size(unique(skelAll.nodes{1},'rows'),1))
    config.scaleEM = skelAll.scale;

    curGroupId = func(regexp(nml,'dend_(?<id>\d+)','names'));
    curGroupName = sprintf('dend_%02d',curGroupId);

    % dend statistics
    dendTreeId = find(contains(skelAll.names, curGroupName) & ~contains(skelAll.names, 'axon'));
    skelDend = skelAll.deleteTrees(dendTreeId, true); % remove all rest
    treeName = skelDend.names{1};
    
    % syn table for dendrite    
%    synTable = extractSynTable( skelDend, 1, regionName);
%    outData(i).synTable = synTable;

    dendData = extractSynComments(skelDend, 1, 'dendrite', false, outDir);
    outData(i).name = skelAll.names{1};
    outData(i).dend = dendData;
    clear synTable
end

T = vertcat(outData.dend);

writetable(T, fullfile(outDir, sprintf('dendData-%s.xlsx',regionName)));
Util.save(fullfile(outDir, sprintf('dendData-%s.mat', regionName)), T);

function synTable = extractSynTable(skel, curTree, regionName)

    % find cellbody type
    cellbodyIdx = skel.getNodesWithComment('(cellbody|soma)',curTree,'regexp');
    commentCellbody = skel.nodesAsStruct{curTree}(cellbodyIdx).comment;
    temp = regexp(commentCellbody,'(cellbody|soma)_(?<type>\w+)','names');
    cellbodyType = {temp.type};
    
    allPaths = skel.getShortestPaths(1);

    % for complete dend, each syn and its distance from soma
    commentTypes = {'sph','prim','sec','shaft','neck','stub','tertiary'};
    commentTypesSearchStr = {'(sph|spineSingle)', 'prim', 'sec', 'sh', 'neck', 'stub', 'ter'}; % regexpi
    
    % synTable each syn per row
    regionNames = {};
    dendNames = {};
    synDistToSoma = [];
    synTypes = {};

    for i = 1:numel(commentTypesSearchStr)
        curCommentType = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment(curCommentSearchStr, curTree, 'regexpi');
        curNodesDist = allPaths(cellbodyIdx, curNodesIdx) ./ 1e3; % um
        curNodesDist = sort(curNodesDist, 'ascend');
        
        synDistToSoma = [synDistToSoma; curNodesDist(:)];
        synTypes = [synTypes; repelem({curCommentType}, numel(curNodesDist),1)];
        clear curNodesDist
    end
    assert(numel(synTypes) == numel(synDistToSoma))
    regionNames = repelem({regionName}, numel(synTypes),1);   
    dendNames = repelem(skel.names(curTree), numel(synTypes),1);
    cellTypes = repelem(cellbodyType, numel(synTypes),1);
    synTable = table(regionNames, dendNames, cellTypes, synDistToSoma, synTypes);
end


function out = extractSynComments(skel, curTree, tracingType, plotDendrogram, outDir)
    c = {skel.nodesAsStruct{curTree}.comment};
    c(cellfun(@isempty, c)) = [];

    % remove seed synapses
%    idxSeed = cellfun(@(x) any(regexpi(x,'^seed')),c);
%    c(idxSeed) = [];

    out = table;
    out.name = skel.names(curTree);
    out.sph = sum(cellfun(@(x) any(regexpi(x,'(spine|sph|sp_root)')),c));
    out.prim  =  sum(cellfun(@(x) any(regexpi(x,'prim')),c));
    out.second =  sum(cellfun(@(x) any(regexpi(x,'sec')),c));
    out.shaft = sum(cellfun(@(x) any(regexpi(x,'sh')),c));
    out.neck = sum(cellfun(@(x) any(regexpi(x,'neck')),c));
    out.stub =  sum(cellfun(@(x) any(regexpi(x,'stub')),c));
    out.ter = sum(cellfun(@(x) any(regexpi(x,'ter')),c));
    out.filopodia = sum(cellfun(@(x) any(regexpi(x,'filo')),c));
    out.total = out.sph + out.prim + out.second + out.shaft + out.neck + ...
                out.stub + out.ter + out.filopodia;

    out.excFraction = out.sph ./ out.total; % NOTE:
    switch tracingType
         case 'axon'
            [~,idxDel] = Util.getNodeCoordsWithComment(skel, {'post'},'partial', curTree);
            skel = skel.deleteNodes(curTree,idxDel,true);
            out.pL = skel.pathLength(curTree)./1e3; %um

         case 'dendrite'
            startIdx = skel.getNodesWithComment('_root',1,'exact');
            endIdx  =  skel.getNodesWithComment('_end',1,'exact');

            startId = str2double(skel.nodesAsStruct{1}(startIdx).id);
            endId = str2double(skel.nodesAsStruct{1}(endIdx).id);

            [~, ~, out.pL] = skel.getShortestPath(startId, endId); %nm
            out.pL = out.pL./1e3;
     end
     out.synD = out.total./ out.pL;
end
