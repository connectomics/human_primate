% Use soma-based axons tracing and to sub-sample regions to get exc fraction
% do sliding window samples seeded at nodes going away from cutoff distance from soma

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'mk_L23'; % do separately for H5_10_ext and H5_10_L23
Figures.setConfig

restrictDistal = false;
subsampleLengths = [4];
gtAxonType = 'IN';
% gtAxonColors = [0.75,0,0.75];
gtAxonColors = [0,0,0];

% numSynThr = 5;
thrExc = 0.05;
cutoff = 0; % dist away from soma 50 human, 20 mk

Util.log('Doing for this region: %s', regionName)
tracingFolder = fullfile(mainFolder,'data','tracings','axons','cellbody_attached');
files = dir(fullfile(tracingFolder, '*.nml'));

outDir = fullfile(mainFolder, 'results_sub_sampling_sliding');
if ~exist(outDir,'dir')
   mkdir(outDir);
end

rng(0)
Util.log('Reading cellbody attached axons')
nml = fullfile(tracingFolder, files(1).name);
skelAll = skeleton(nml);

% check for duplicate nodes
% assert(size(skelAll.nodes{1},1) == size(unique(skelAll.nodes{1},'rows'),1))

% dend statistics
axonTreeId = find(contains(skelAll.names, 'axon') & ...
                   cellfun(@(x) any(regexpi(x, gtAxonType)), skelAll.names));
Util.log(sprintf('Found %d %s axons',numel(axonTreeId), gtAxonType))

allData = {};
outData = struct;
allMCR = [];
allSampleSizes = [];
for subsampleLength = subsampleLengths
    % do per dend group
    for i = 1:numel(axonTreeId)
        skelAxon = skelAll.deleteTrees(axonTreeId(i), true); % remove all rest
        treeName = skelAxon.names{1};

        Util.log('Delete duplicate nodes')
        skelAxon = skelAxon.deleteDuplicateNodes;

        Util.log('Extracting syn table')
        [synTable] = extractSynTable( skelAxon, 1); % keep only distal comments in the skeleton
        outData(i).synTable = synTable;

        if restrictDistal
            skelAxon = skelDistal;
        end
        if isempty(synTable)
           continue 
        end
        
        Util.log('do sub-sampling on this')
        startNode =  skelAxon.getNodesWithComment('cellbody',1,'partial');
        [sub, branchIds, branchOrder] = skelAxon.unBranch('',startNode);
        branchIds = branchIds{1};
        branchOrder = branchOrder{1};
        G = Util.extractGraphOnBranchIds(sub, branchIds, branchOrder);
        
        Util.log(sprintf('Found %d branches for %s',sub.numTrees, skelAxon.names{1}))
        
        allSynCounts = []; 
        curSphFrac = [];
        for idBranch=1:max(branchIds)
            curBranchIds = G.Nodes.branchIds(Graph.reachableNodes(G,idBranch, subsampleLength, 'up_to'));
            curSkel = sub.deleteTrees(curBranchIds,true); % delete the rest
            
            curComments = curSkel.getAllComments;
            curSphCount = sum(contains(curComments,{'sph'}));
            curTotalCount = sum(contains(curComments,{'sph','prim','sh','sec','stub','neck','soma','ais'}));
            
            if curTotalCount<1
                sprintf('Not enough syns in this subsample')
                curSphFrac(idBranch) = nan; % NaN when below 1
            else
                curSphFrac(idBranch) = curSphCount / curTotalCount;
            end
            allSynCounts(idBranch) = curTotalCount;
        end
        Util.log('Finished sampling for this axon')
        outData(i).sphFractions = curSphFrac;
        outData(i).allSynCounts = allSynCounts;
        outData(i).subsampleLength = subsampleLength;
    end
    % remove empty skeletons without syns
    outData(arrayfun(@(x) isempty(x.synTable),outData)) = [];
    allData{end+1} = outData;
end

%% plot misclassification rate wrt number of samples per subsample length
% Simulate sph fractions using following parameters
num_axons = 1000;
num_synapses = 50;
sprintf('Simulating %d axons each with %d synapses',num_axons,num_synapses)

offset = 0.5;
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on

ax2_pos = ax.Position;
ax2 = axes('Position',ax2_pos,...
    'YAxisLocation','right','color','none');
hold on

allLegends = {};
allLegendsAx2 = {};
for idxSubsample=1:numel(allData)
    % histogram of syn counts in the raw real data samples
    curData = allData{idxSubsample};
    synsPicked =  cat(2,curData.allSynCounts);
    h = histogram(ax, synsPicked,'BinWidth',1,'DisplayStyle','stairs','LineWidth',2, ...
        'EdgeColor',gtAxonColors(idxSubsample,:));
    allLegends{end+1} = sprintf('numBranches %d (n=%d, n=%d %s axons )',subsampleLengths(idxSubsample),numel(synsPicked), numel(curData), gtAxonType);
    
    % add misclassification rate per bin for this subsampleLength curve
    counterSyns = h.BinEdges;
    sphFractions = cat(2,curData.sphFractions); % real data, there can be NaNs
    
    % sampling real data to estimate mcr
    mcr = [];
    mcrSphF = {};
    for j=1:numel(counterSyns)       
        curSamples = synsPicked == counterSyns(j);
        curSphFractions = sphFractions(curSamples);
        curSphFractions(isnan(curSphFractions)) = [];
        mcr(j) = sum(curSphFractions < thrExc) / numel(curSphFractions);  % misclassification rate
        mcrSphF{j} = curSphFractions;
    end
    
    % simulate axons based on synapse counts distribution of these samples
    orphanDist = fitdist(synsPicked(:),'Poisson'); % num synapses of real samples fitted as Poisson
    sim_fraction_data = datasample(sphFractions(~isnan(sphFractions)), num_axons); % sph fractions of real data bootstrap
    axons = zeros(num_axons, num_synapses);
    for idxAxon=1:num_axons
        curFraction  = sim_fraction_data(idxAxon);
        idxUpdate = randperm(num_synapses,round(100*curFraction*num_synapses/100));
        axons(idxAxon,idxUpdate) = 1;
    end

    % sampling simulated data to estimate mcr
    mcrSim = [];
    mcrSimSphF = {};
    for i=1:num_axons
        pickSyns = orphanDist.random(1); % drawn from poisson distribution
        idxUpdate = randperm(num_synapses,pickSyns);
        curSyns = axons(i,idxUpdate);
        curFraction = 100*sum(curSyns)/numel(curSyns);
        allPickedSyns(i) = pickSyns;
        allPickedFraction(i) = curFraction;
    end

    numPickSyns = 1:num_synapses;
    for i=1:num_synapses
        idx = allPickedSyns==i;
        curFrac = allPickedFraction(idx);
        mcrSim(i) = sum(curFrac<thrExc) ./ numel(curFrac);  % misclassification rate simulated
        mcrSimSphF{i} = curFrac;
    end
    
    % plot Misclassification rates curves
    if strcmp(gtAxonType,'IN')
        mcr = 1-mcr;
        mcrSim = 1-mcrSim;
    end
    plot(ax2,counterSyns + offset, mcr,'--','LineWidth',1, 'Color',gtAxonColors(idxSubsample,:))
%     plot(ax2,(1:num_synapses) + offset, mcrSim,'-.','LineWidth',1, 'Color',colors(idxSubsample,:))
%     allLegendsAx2{end+1} = sprintf('mcr real (excThr=%.2f)',thrExc);
%     allLegendsAx2{end+1} = sprintf('mcr sim N=%d, ?=%.2f (excThr=%.2f)',size(axons,1), orphanDist.lambda, thrExc);

    % scatter plot per bin
    widthS = 0.5;
    for i=1:numel(mcrSphF)
        data = mcrSphF{i};
        pos = counterSyns(i) + offset;
        xpos = repelem(pos, numel(data));
        scatter(ax2,xpos(:), data(:),100, 'x', 'filled', 'MarkerEdgeColor',gtAxonColors(idxSubsample,:), 'MarkerFaceAlpha',1','jitter','on','jitterAmount',0.5*widthS);
%         h = boxplot(ax2, data(:), 'Colors',colors(idxSubsample,:),'Widths',widthS,'Positions',pos,'Symbol','','Jitter',widthS);
    end
end
legend(ax, allLegends)
xlabel(ax,'Number of synapses per sample');
ylabel(ax,'Number of samples per bin')
ax.XAxis.Limits = [0,50];
ax.XAxis.TickValues = 0:10:50;
ax.XAxis.MinorTick = 'on';
Util.setPlotDefault(ax,'','')

ylabel(ax2,sprintf('Spine targeting fraction \n Fraction below %.2f (--)', thrExc))
% legend(ax2, allLegendsAx2,'Location','southeast')
ax2.XAxis.Limits = [0,50];
ax2.XAxis.TickValues = 0:10:50;
ax2.XAxis.Color = 'none';
ax2.YAxis.Limits = [0,1];
Util.setPlotDefault(ax2,'','')
title(sprintf('%s (restrictDistal=%d)',regionName, restrictDistal),'Interpreter','none')

%% save
set(gcf, 'Position', get(0, 'Screensize'));
outfile = fullfile(mainDir, 'figures', 'sim', 'real',sprintf('%s_%s_axon_sampling_sliding_branches',regionName, gtAxonType));
export_fig(strcat(outfile, '.png'),'-q101', '-transparent','-nocrop','-m8')
close all

%% functions
function [synTable, skelDistal] = extractSynTable(skel, curTree)
    proximalDistThr = 200; % Human

    % delete post nodes
    idxDel = skel.getNodesWithCommentAndDegree('post',curTree,'partial','',1);
    skel = skel.deleteNodes(curTree,idxDel,true);

    % find cellbody type
    cellbodyIdx = skel.getNodesWithComment('cellbody',1,'partial');
    commentCellbody = skel.nodesAsStruct{1}(cellbodyIdx).comment;
    temp = regexp(commentCellbody,'cellbody_(?<type>\w+)','names');
    cellbodyType = 'pyr'; %{temp.type};

    allPaths = skel.getShortestPaths(1);

    % for complete dend, each syn and its distance from soma
    commentTypes = {'sph','prim','sec','sh','neck','stub','ter'};
    commentTypesSearchStr = {'(sph|spineSingle)', 'prim', 'sec', 'sh', 'neck', 'stub', 'ter'}; % regexpi

    % find max num of each comment
    count = [];
    for i = 1:numel(commentTypes)
        curComment = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment( curCommentSearchStr, curTree, 'regexpi');
        count = [count, numel(curNodesIdx)];
    end

    maxCount = max(count);

    if maxCount<1
       synTable = table; % no syns found
       return
    end
    synTable = cell2table(cell( maxCount, 1 + numel(commentTypes)));
    synTable.Properties.VariableNames = [ 'name', commentTypes];

    synTable.name(1) = skel.names;

    allNodesIdx = [];
    allNodesDist = [];
    for i = 1:numel(commentTypes)
        curComment = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment(curCommentSearchStr, curTree, 'regexpi');
        curNodesDist = allPaths(cellbodyIdx, curNodesIdx) ./ 1e3; % um
        curNodesDist = sort(curNodesDist, 'ascend');

        synTable.(curComment)(1:numel(curNodesDist)) = num2cell(curNodesDist);
        
        allNodesIdx = cat(1,allNodesIdx, curNodesIdx(:));
        allNodesDist = cat(1, allNodesDist, curNodesDist(:));
    end
    
    % delete distal comments
    skelDistal = skel;
    idxProximal = allNodesDist < proximalDistThr;
    allNodesIdxProx = allNodesIdx(idxProximal);
    
    for i = 1:numel(allNodesIdxProx)
        skelDistal.nodesAsStruct{curTree}(allNodesIdxProx(i)).comment = '';
    end
end