% Use soma-based dendrites tracing and to sub-sample regions to get exc fraction
clear 

config = struct;
version = datestr(now,'yyyy-mm-dd'); % hiwi tasks version

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'H5_10_L23';
doAxons = false;

Util.log('Doing for %s', regionName)
switch regionName
    case 'mk_L4'
        mainFolder = fullfile('/tmpscratch/sahilloo/mk_L4/data/tracings/dendritesWithEI/');
        datasetName = 'Mk1_F6_JS_SubI_v1';
        ownerName = 'SL';
    case 'H5_10_L4'
        mainFolder = fullfile('/tmpscratch/sahilloo/H5_10_L4/data/tracings/dendritesWithEI/');
        datasetName = 'H5_10_270um_final_v1';
        ownerName = 'NH';
    case 'mk_L23'
        mainFolder = fullfile('/tmpscratch/sahilloo/mk_L23/data/tracings/dendritesWithEI/');
        datasetName = 'Mk1_F6_L23_JS_SubI_v1';
        ownerName = 'SL';
    case 'H5_10_L23'
        mainFolder = fullfile('/tmpscratch/sahilloo/H5_10_L23/data/tracings/dendritesWithEI/');
        datasetName = 'H5_10_270um_final_v1';
        ownerName = 'NH';
end
tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling');
outDir = fullfile(mainFolder, 'results_sub_sampling');

if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'dend_*.nml'));
func = @(x) str2double(x.id);

% do per dend group
Util.log('Now extracting...')
for i = 1:numel(files)
    nml = fullfile(tracingFolder, files(i).name);
    skelAll = skeleton(nml);
    config.scaleEM = skelAll.scale;

    curGroupId = func(regexp(nml,'dend_(?<id>\d+)','names'));
    curGroupName = sprintf('dend_%02d',curGroupId);

    % dend statistics
    dendTreeId = find(contains(skelAll.names, curGroupName) & ~contains(skelAll.names, 'axon'));
    skelDend = skelAll.deleteTrees(dendTreeId, true); % remove all rest
    treeName = skelDend.names{1};

    % do sub-sampling on this dendrite
    T = subsampleDend(skelDend, outDir);
    writetable(T, fullfile(outDir, sprintf('dendData-%s.xlsx', treeName)));
        
    outData(i).dend = T;
    outData(i).name = curGroupName;
    clear T
end

T = vertcat(outData.dend)
writetable(T, fullfile(outDir, sprintf('dendData-subsampling-%s.xlsx', regionName)));
Util.save(fullfile(outDir, sprintf('dendData-subsampling-%s.mat', regionName)), T);
Util.log('Finished saving table.')

% make histogram
Util.log('Plotting histogram:')
fig = figure();
fig.Color = 'white';
ax = axes(fig);
hold on;
for i=1:numel(outData)
    x = outData(i).dend.excFraction(2:end);
    histogram(x, 'DisplayStyle','stairs', 'BinWidth',0.05);
end
legend({outData.name},'Interpreter', 'none') 
ax.YAxis.TickValues = 0:1:5; % manually
xlabel('Spine syn fraction')
ylabel('Frequency')
ax.LineWidth = 2;
Util.setPlotDefault(gca,true,'');
export_fig(fullfile(outDir, sprintf('dendData-subsampling-%s.png', regionName)),'-q101', '-nocrop','-transparent', '-m8');
close all

function T = subsampleDend(skel, outDir)
    % sort edges
    edges = skel.edges{1};
    skel.edges{1} = sort(edges,2,'ascend');

    allNodes = skel.getNodes;

    % find start and end point
    startIdx = skel.getNodesWithComment('_root',1,'regexp');
    endIdx  =  skel.getNodesWithComment('_end',1,'regexp');

    startId = str2double(skel.nodesAsStruct{1}(startIdx).id);
    endId = str2double(skel.nodesAsStruct{1}(endIdx).id);
    allPaths = skel.getShortestPaths(1);
    [path, ~, pL] = skel.getShortestPath(startId, endId);
    pL = pL./1e3;

    
    % find node >20um away from soma
    somaInd = path(1);
    somaCoord = skel.nodes{1}(somaInd,1:3);
    endInd = path(end);

    numSamples = floor(pL./20);
    sprintf('Found %d samples',numSamples)
    nodesTraversed = [0,0,0];
    i = 1;
    for curSample = 1:numSamples
        sprintf('Doing for sample: %d',curSample)
        somaInd = path(i); % start of this sample
        thisPathLength = 0;
        thisComment = '';
        while thisPathLength < 20 
            i = i+1;
            curNodeInd = path(i);
            thisPathLength = allPaths(somaInd, curNodeInd) ./1e3;
            thisComment = skel.nodesAsStruct{1}(curNodeInd).comment;
        end

        % split tree
        curSkel = skel.splitTreeAtNode(str2double(skel.nodesAsStruct{1}(curNodeInd).id), true);
        
        % find soma sided tree
        idxSomaSideTree = findSkelWithSoma(curSkel, somaCoord);
        curSkel = curSkel.deleteTrees(idxSomaSideTree, true);

        % find nodes of this sample alone        
        curNodes = curSkel.nodes{1}(:,1:3);
        idxKeep = ~ismember(curNodes, nodesTraversed,'rows');

        % update nodes traversed
        nodesTraversed = curNodes;
        curNodes = curNodes(idxKeep,:);

        % find indices of nodes of current sample
        idxKeepForSyn  = ismember(allNodes, curNodes, 'rows');
        % get new skel for syn analysis
        s = skel.deleteNodes(1, ~idxKeepForSyn);
        assert(s.numTrees == 1)
        s.names{1} = sprintf('%s-sample-%d',skel.names{1}, curSample);
        % calculate distribution for this sample
        curData = extractSynComments(s, 1, 'dendrite', false, outDir, thisPathLength);
        
        % catenate
        outData(curSample).data = curData;
    end

    T = table;
    T = vertcat(outData.data);
end

function found = findSkelWithSoma(skel, somaCoord)
    found = [];
    for i=1:skel.numTrees
        nodes = skel.nodes{i}(:,1:3);
        if any(ismember(nodes, somaCoord,'rows'))
            sprintf('Tree with soma node found')
            found = i;
            break;
        end
    end
end

function out = extractSynComments(skel, curTree, tracingType, plotDendrogram, outDir, pL)
    c = {skel.nodesAsStruct{curTree}.comment};
    c(cellfun(@isempty, c)) = []; 
    out = table;
    out.name = skel.names(curTree);
    out.sph = sum(contains(c, 'sph'));
    out.shaft = sum(contains(c, 'sh'));
    out.prim  =  sum(contains(c, 'prim'));
    out.second =  sum(contains(c, {'second','sec'}));
    out.neck =  sum(contains(c, 'neck'));
    out.stub =  sum(contains(c, 'stub'));
    out.soma =  sum(contains(c, 'soma'));
    out.total = sum(contains(c,{'sph','sh','prim','sec','second','neck','stub', 'soma'}));
 
    out.excFraction = (out.sph + out.prim) ./ out.total;
    switch tracingType
         case 'axon'
            [~,idxDel] = Util.getNodeCoordsWithComment(skel, {'post'},'partial', curTree);
            skel = skel.deleteNodes(curTree,idxDel,true);
            out.pL = skel.pathLength(curTree)./1e3; %um
 
         case 'dendrite'
            out.pL = pL;
     end
     out.synD = out.total./ out.pL; 

    if plotDendrogram

        synSize = 100; tubeSize = 1; dotSize= 3;
        synColors = {[1,0,0],... % red shaft
                     [0,1,0],... % green sph
                     [1,0,1],... % blue stub 
                     [230,159,0]./255,... % orange prim
                     [240,228,66]./255,... %Yellow second
                     [204,121,167]./255,... % reddish purple ter
                     [0,114,178]./255}; % bluish neck

        % make axonogram (PLASS-like)
        startpoint = skel.getNodesWithComment('_root',1,'regexp');
        endpoint  =  skel.getNodesWithComment('_end',1,'regexp');
    
        % attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
        comments = {skel.nodesAsStruct{1}.comment}';
        assert(startpoint == find(contains(comments,'_root')))
        
        tic;
        config = struct;
        config.scaleEM = skel.scale;
        Util.log('Now scatterting for %s',skel.names{1})
        [TREE, f] = Axonogram.axonogram(skel,'',startpoint,config.scaleEM, true);
        xlabel('Path length from root (um)')
        ylabel('')
        synComments = {'sh', 'sph', 'stub', 'prim', 'sec', 'ter', 'neck'};
        for i=1:numel(synComments)
            curSynLocs = find(cellfun(@(x) any(regexp(x,synComments{i})),comments));
            curSynColor = synColors{i};
            if ~isempty(curSynLocs)
                XY = Axonogram.findPointsInAxonogram(curSynLocs, TREE, skel, config.scaleEM);
                gcf, scatter(XY(:,1),XY(:,2),synSize, 'o','MarkerEdgeColor',curSynColor, 'MarkerFaceColor',curSynColor);
                clear XY
            end
        end

        % cosmetics
        box off
        camroll(90);
        set(gca,'linewidth',2, 'color', 'white');
        title( skel.names{curTree}, 'Interpreter', 'none')
        outfile = fullfile(outDir, ...
            sprintf('%s.png', skel.names{curTree}));
        export_fig(outfile,'-q101', '-nocrop', '-transparent');
        Util.log('Saving file %s', outfile);
        close(f);
        toc;
    end

end 
