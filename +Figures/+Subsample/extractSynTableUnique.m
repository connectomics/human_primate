% Use soma-based dendrites tracing and to sub-sample regions to get exc fraction
% save unique synaspes dist table

clear 

config = struct;

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'H2_3';
plotPerDend = false;
doAxons = false;

Figures.setConfig

Util.log('Doing for this region: %s', regionName)
switch regionName
     case {'mk_L4', 'mk_L23', 'H5_10_L4', 'H5_10_L23', 'PPC2', 'ex145'}
        tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling'); % NOTE: These are all on single branches
     case {'ex144', 'PPC_AK', 'V2_L23', 'lpta','ACC_JO', 'H2_3'}
        tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling_unique'); % NOTE     
end

outDir = fullfile(mainFolder, 'results');

if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'dend_*.nml'));
func = @(x) str2double(x.id);

Util.log('doing for region: %s', regionName)
% do per dend group
for i = 1:numel(files)
    nml = fullfile(tracingFolder, files(i).name);
    skelAll = skeleton(nml);

    % check for duplicate nodes
    assert(size(skelAll.nodes{1},1) == size(unique(skelAll.nodes{1},'rows'),1))
    config.scaleEM = skelAll.scale;

    curGroupId = func(regexp(nml,'dend_(?<id>\d+)','names'));
    curGroupName = sprintf('dend_%02d',curGroupId);

    % dend statistics
    dendTreeId = find(contains(skelAll.names, curGroupName) & ~contains(skelAll.names, 'axon'));
    skelDend = skelAll.deleteTrees(dendTreeId, true); % remove all rest
    treeName = skelDend.names{1};
    
    % syn table for dendrite    
    synTable = extractSynTable( skelDend, 1, regionName);
    outData(i).synTable = synTable;
    outData(i).name = skelAll.names{1};
    clear synTable
end

Util.save(fullfile(outDir, sprintf('dendData-synTableUnique-%s.mat', regionName)), outData, info);
Util.log('Finished saving table.')

function synTable = extractSynTable(skel, curTree, regionName)

    % find cellbody type
    cellbodyIdx = skel.getNodesWithComment('(cellbody|soma)',curTree,'regexp');
    commentCellbody = skel.nodesAsStruct{curTree}(cellbodyIdx).comment;
    temp = regexp(commentCellbody,'(cellbody|soma)_(?<type>\w+)','names');
    cellbodyType = {temp.type};
    
    allPaths = skel.getShortestPaths(1);

    % for complete dend, each syn and its distance from soma
    commentTypes = {'sph','prim','sec','shaft','neck','stub','tertiary'};
    commentTypesSearchStr = {'(sph|spineSingle)', 'prim', 'sec', 'sh', 'neck', 'stub', 'ter'}; % regexpi
    
    % synTable each syn per row
    regionNames = {};
    dendNames = {};
    synDistToSoma = [];
    synTypes = {};

    for i = 1:numel(commentTypesSearchStr)
        curCommentType = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment(curCommentSearchStr, curTree, 'regexpi');
        curNodesDist = allPaths(cellbodyIdx, curNodesIdx) ./ 1e3; % um
        curNodesDist = sort(curNodesDist, 'ascend');
        
        synDistToSoma = [synDistToSoma; curNodesDist(:)];
        synTypes = [synTypes; repelem({curCommentType}, numel(curNodesDist),1)];
        clear curNodesDist
    end
    assert(numel(synTypes) == numel(synDistToSoma))
    regionNames = repelem({regionName}, numel(synTypes),1);   
    dendNames = repelem(skel.names(curTree), numel(synTypes),1);
    cellTypes = repelem(cellbodyType, numel(synTypes),1);
    synTable = table(regionNames, dendNames, cellTypes, synDistToSoma, synTypes);
end
