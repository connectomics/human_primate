clear 

info = Util.runInfo();
Util.showRunInfo(info);

outputDir = '/tmpscratch/sahilloo/primate_and_human/outData/';

rng(0)
% make histogram
Util.log('Plotting histogram:')
fig = figure();
fig.Color = 'white';
ax = axes(fig);

regionNames = {'mk_L4','mk_L23', 'H5_10_L4', 'H5_10_L23'};
colors = Util.getSomaColors;
for i = 1:numel(regionNames)
    regionName = regionNames{i};
    curColor = colors{i}(1:3);
    Figures.setConfig
    tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling');
    outDir = fullfile(mainFolder, 'results_sub_sampling');

    d = dir(fullfile(outDir,'dendData-subsampling*.mat'));
    m = load(fullfile(outDir,d.name));
    T = m.T;
    idxDel = cellfun(@(x) any(regexp(x,'sample-1$')), T.name);
    T = T(~idxDel,:);
    x = T.excFraction;
    if i~=1
        curAxPos = ax.Position;
        curAxPos(2) = curAxPos(2)+0.01*rand(1);
        curAx = axes('Position', curAxPos, 'color','none');
    else
        curAx = ax;
    end
    hold on
    histogram(x, 'DisplayStyle','stairs', 'BinWidth',0.05, 'EdgeColor', curColor, ...
            'Normalization', 'probability', 'LineWidth', 2, 'Parent', curAx);
    curAx.YAxis.TickValues = 0:0.1:1; % manually
    curAx.YAxis.Limits = [0,0.5]; % manually

    if i~=1
        set(gca,'xcolor','none', 'ycolor','none')
    end
    Util.setPlotDefault(gca,true,'');
end
ax.XAxis.Label.String = 'Spine syn fraction';
ax.YAxis.Label.String = 'Frequency (Norm.)';
ax.LineWidth = 2;
export_fig(fullfile(outputDir, 'figures', 'dendData-subsampling-all-projects-hist-spineSynFraction.png'),'-q101', '-nocrop','-transparent', '-m8');
close all
