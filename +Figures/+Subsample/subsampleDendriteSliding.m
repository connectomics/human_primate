% Use soma-based dendrites tracing and to sub-sample regions to get exc fraction

% do sliding window samples seeded at nodes going away from cutoff distance from soma
clear 

config = struct;

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'V2_L23';
cutoff = 0; % dist away from soma 50 human, 20 mk
slidingWindowBuffer = 5; % num nodes to go forward for each window
plotPerDend = false;
doAxons = false;

Figures.setConfig

Util.log('Doing for this region: %s', regionName)
tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling');
outDir = fullfile(mainFolder, 'results_sub_sampling_sliding');

if ~exist(outDir,'dir')
   mkdir(outDir);
end

files = dir(fullfile(tracingFolder, 'dend_*.nml'));
func = @(x) str2double(x.id);

subsampleLengths = [30];

rng(0)
colors = Util.getRangedColors(0,1,0,1,0,1, numel(files));

Util.log('doing for region: %s', regionName)
for subsampleLength = subsampleLengths
    % do per dend group
    for i = 1:numel(files)
        nml = fullfile(tracingFolder, files(i).name);
        skelAll = skeleton(nml);

        % check for duplicate nodes
        assert(size(skelAll.nodes{1},1) == size(unique(skelAll.nodes{1},'rows'),1))
        config.scaleEM = skelAll.scale;
    
        curGroupId = func(regexp(nml,'dend_(?<id>\d+)','names'));
        curGroupName = sprintf('dend_%02d',curGroupId);
    
        % dend statistics
        dendTreeId = find(contains(skelAll.names, curGroupName) & ~contains(skelAll.names, 'axon'));
        skelDend = skelAll.deleteTrees(dendTreeId, true); % remove all rest
        treeName = skelDend.names{1};
        
        % syn table for dendrite    
        synTable = extractSynTable( skelDend, 1);
        outData(i).synTable = synTable;

        % do sub-sampling on this dendrite
        T = subsampleDend(skelDend, outDir, cutoff, subsampleLength, slidingWindowBuffer, regionName);
        if isempty(T)
            outData(i).dend = table;    
            outData(i).name = treeName;
            continue;
        else
            outData(i).dend = T;
            outData(i).name = treeName;
        if plotPerDend
            writetable(T, fullfile(outDir, sprintf('dendData-%s.xlsx', treeName)));
            % plot histogram for this region
            fig = figure();
            fig.Color = 'white';
            ax = axes(fig);
            x = T.sph ./ T.total;
            hold on
            histogram(x, 'DisplayStyle','stairs', 'BinWidth',0.05, 'EdgeColor', 'k', ...
                    'Normalization', 'probability', 'LineWidth', 2);
            ax.YAxis.TickValues = 0:0.1:1; % manuallyax
            ax.YAxis.Limits = [0,0.5]; % manually
            Util.setPlotDefault(gca,true,'');
            ax.XAxis.Label.String = 'Sph / total';
            ax.YAxis.Label.String = 'Frequency (Norm.)';
            ax.LineWidth = 2;
            curLeg = legend(sprintf('cutoff-%d-sampleLength-%d', cutoff, subsampleLength));
            set(curLeg, 'Box', 'Off', 'Location', 'best','Interpreter', 'none');
            title(ax, ...
                {info.filename; info.git_repos{1}.hash; sprintf('%s', treeName)}, ...
                'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
            export_fig(fullfile(outDir,sprintf('dendData-subsampling-%s-%s-hist-spineSynFraction.png',regionName, treeName)),'-q101', '-nocrop', '-m8');
            close all
    
            % plot scatter dist dependence
            fig = figure();
            fig.Color = 'white';
            ax = axes(fig);
            x = T.distToCellbody;
            y = T.sph ./ T.total;% NOTE 
            hold on
            s = scatter(x, y, 'x','filled', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'none');        
            ax.YAxis.TickValues = 0:0.1:1; % manuallyax
            ax.YAxis.Limits = [0,1]; % manually
            
            Util.setPlotDefault(gca,'','');
            ax.XAxis.Label.String = 'Dist to cellbody';
            ax.YAxis.Label.String = 'sph / total';
            ax.LineWidth = 2;
            curLeg = legend(sprintf('cutoff-%d-sampleLength-%d', cutoff, subsampleLength));
            set(curLeg, 'Box', 'Off', 'Location', 'best','Interpreter', 'none');
            title(ax, ...
                {info.filename; info.git_repos{1}.hash; sprintf('%s', treeName)}, ...
                'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
            export_fig(fullfile(outDir,sprintf('dendData-subsampling-%s-%s-scatter-spineSynFraction.png',regionName, treeName)),'-q101', '-nocrop', '-m8');
            close all
        end
        end
        clear T synTable
    end
    
    T = vertcat(outData.dend)
    if ~isempty(T)
        writetable(T, fullfile(outDir, sprintf('dendData-subsampling-%s-subsampleLength-%d.xlsx', regionName, subsampleLength)));
    end

    Util.save(fullfile(outDir, sprintf('dendData-subsampling-%s-subsampleLength-%d.mat', regionName, subsampleLength)), T, outData);
    Util.log('Finished saving table.')
    
    % plot histogram for this region
    fig = figure();
    fig.Color = 'white';
    ax = axes(fig);
    hold on
    legendNames = {};
    for i=1:numel(outData)
        curName = outData(i).name;
        curColor = colors(i,1:3);
        T = outData(i).dend;
        if isempty(T)
            continue;
        end
        x = T.sph ./ T.total;
        histogram(x, 'DisplayStyle','stairs', 'BinWidth',0.05, 'EdgeColor', curColor, ...
                'Normalization', 'probability', 'LineWidth', 2);
        legendNames = [legendNames, {curName}];
    end
    ax.YAxis.TickValues = 0:0.1:1; % manuallyax
    ax.YAxis.Limits = [0,0.5]; % manually
    Util.setPlotDefault(gca,true,'');
    ax.XAxis.Label.String = 'Sph / total';
    ax.YAxis.Label.String = 'Frequency (Norm.)';
    ax.LineWidth = 2;
    curLeg = legend(legendNames);
    set(curLeg, 'Box', 'Off', 'Location', 'best','Interpreter', 'none');
    title(ax, ...
        {info.filename; info.git_repos{1}.hash; sprintf('cutoff-%d-sampleLength-%d-slidingWindowNodes-%d', cutoff, subsampleLength, slidingWindowBuffer)}, ...
        'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
    export_fig(fullfile(outDir,sprintf('dendData-subsampling-%s-hist-subsampleLength-%d-spineSynFraction.png',regionName, subsampleLength)),'-q101', '-nocrop', '-m8');
    close all
    
    % plot scatter dist dependence
    fig = figure();
    fig.Color = 'white';
    ax = axes(fig);
    hold on
    legendNames = {};
    for i=1:numel(outData)
        curName = outData(i).name;
        curColor = colors(i,1:3);
        T = outData(i).dend;
        if isempty(T)
            continue;
        end
        x = T.distToCellbody;
        y = T.sph ./ T.total;% NOTE
        s = scatter(x, y, 'x','filled', 'MarkerEdgeColor', curColor, 'MarkerFaceColor', 'none');
        legendNames = [legendNames, {curName}];
    end
    ax.YAxis.TickValues = 0:0.1:1; % manuallyax
    ax.YAxis.Limits = [0,1]; % manually
    ax.XAxis.TickValues = 0:50:350;
    ax.XAxis.Limits = [0,350]; %
    Util.setPlotDefault(gca,'','');
    ax.XAxis.Label.String = 'Dist to cellbody';
    ax.YAxis.Label.String = 'sph / total';
    ax.LineWidth = 2;
    curLeg = legend(legendNames);
    set(curLeg, 'Box', 'Off', 'Location', 'best','Interpreter', 'none');
    title(ax, ...
        {info.filename; info.git_repos{1}.hash; sprintf('cutoff-%d-sampleLength-%d-slidingWindowNodes-%d', cutoff, subsampleLength, slidingWindowBuffer)}, ...
        'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
    export_fig(fullfile(outDir,sprintf('dendData-subsampling-%s-scatter--subsampleLength-%d-spineSynFraction.png',regionName, subsampleLength)),'-q101', '-nocrop', '-m8');
    close all

end % end subsample length

function T = subsampleDend(skel, outDir, cutoff, subsampleLength, slidingWindowBuffer, regionName)

    % sort edges
    edges = skel.edges{1};
    skel.edges{1} = sort(edges,2,'ascend');

    allNodes = skel.getNodes;

    % find cellbody type    
    cellbodyIdx = skel.getNodesWithComment('cellbody',1,'partial');
    commentCellbody = skel.nodesAsStruct{1}(cellbodyIdx).comment;
    temp = regexp(commentCellbody,'cellbody_(?<type>\w+)','names');
    cellbodyType = {temp.type};
    clear cellbodyIdx

    % find start and end point
    % if sampling dendrites from KArimi et al, start comaprtments from 'start'
    switch regionName
        case {'mk_L4', 'mk_L23', 'H5_10_L4', 'H5_10_L23', 'ex144'}
            startIdx = skel.getNodesWithComment('cellbody',1,'regexp');
        case {'PPC_AK', 'V2_L23', 'lpta','ACC_JO'}
            startIdx = skel.getNodesWithComment('(^start$|^newEndings$)',1,'regexp');
    end
    endIdx  =  skel.getNodesWithComment('_end',1,'regexp');

    startId = str2double(skel.nodesAsStruct{1}(startIdx).id);
    endId = str2double(skel.nodesAsStruct{1}(endIdx).id);
    allPaths = skel.getShortestPaths(1);
    [path, ~, pL] = skel.getShortestPath(startId, endId);
    pL = pL./1e3;
    
    if pL<cutoff
        warning(sprintf('dend %s was not greater than cutoff to sample!',skel.names{1}))
        T = table;
        return
    end

    somaInd = skel.getNodesWithComment('cellbody',1,'regexp'); % cellbody
    somaCoord = skel.nodes{1}(somaInd,1:3);
    endInd = path(end);

    % find node cutoff eg. 20/50um away from _root
    i=1;
    thisPathLength = 0;
    while thisPathLength < cutoff 
        i = i+1;
        curNodeInd = path(i);
        thisPathLength = allPaths(startIdx, curNodeInd) ./1e3; %um
    end

    % update path to remove first proximal stretch
    path = path(i+1:end);
    clear i 

    numNodes = numel(path);
    numSamples = floor(numNodes / slidingWindowBuffer); % approx

    % pick first node and select eg. 20um stretch endNode away from soma (check that it exits), the slide forward x nodes
    outData = struct;
    slidingWindowStartNode = 1; % _root node

    for curSample = 1:numSamples
        sprintf('Doing for sample: %d',curSample)
        
        i = slidingWindowStartNode;
        startInd = path(i); % start of this sample

        % update sliding window start node for next sample
        slidingWindowStartNode = slidingWindowStartNode + slidingWindowBuffer; 

        % find how far was this sample
        thisSampleDistToSoma = allPaths(somaInd, startInd) ./1e3; %um    

        % track all nodes behind this sample start
        tempSkel =  skel.splitTreeAtNode(str2double(skel.nodesAsStruct{1}(startInd).id), false); % remove splitnode
        idxSomaSideTree = findSkelWithSoma(tempSkel, somaCoord);
        tempSkel = tempSkel.deleteTrees(idxSomaSideTree, true);
        nodesTraversed = tempSkel.nodes{1}(:,1:3); % exclude this from cur sample

        thisPathLength = 0;
        thisComment = '';
        while thisPathLength < subsampleLength 
            i = i+1;
            if i>numel(path)
                break; % too far at the end
            end
            curNodeInd = path(i);
            thisPathLength = allPaths(startInd, curNodeInd) ./1e3;
            thisComment = skel.nodesAsStruct{1}(curNodeInd).comment;
        end

        if i>numel(path)
            sprintf('Sampled node too far!')
            continue; % go to next sample
        end
        % split tree
        curSkel = skel.splitTreeAtNode(str2double(skel.nodesAsStruct{1}(curNodeInd).id), true);
        
        % find soma sided tree and keep only that
        idxSomaSideTree = findSkelWithSoma(curSkel, somaCoord);
        curSkel = curSkel.deleteTrees(idxSomaSideTree, true);

        % find nodes of this sample alone but deleting behind nodes
        curNodes = curSkel.nodes{1}(:,1:3);
        idxKeep = ~ismember(curNodes, nodesTraversed,'rows');
        curNodes = curNodes(idxKeep,:);

        % find indices of nodes of current sample
        idxKeepForSyn  = ismember(allNodes, curNodes, 'rows');
    
        % get new skel for syn analysis
        s = skel.deleteNodes(1, ~idxKeepForSyn);
        assert(s.numTrees == 1)
        s.names{1} = sprintf('%s-sample-%d',skel.names{1}, curSample);
   
         % calculate distribution for this sample
        curData = extractSynComments(s, 1, 'dendrite', false, outDir, thisPathLength, cellbodyType, thisSampleDistToSoma);
        
        % catenate
        outData(curSample).data = curData;
    end

    T = table;
    if isfield(outData, 'data') % some sample was available
        T = vertcat(outData.data);
    end
end

function found = findSkelWithSoma(skel, somaCoord)
    found = [];
    for i=1:skel.numTrees
        nodes = skel.nodes{i}(:,1:3);
        if any(ismember(nodes, somaCoord,'rows'))
            %sprintf('Tree with soma node found')
            found = i;
            break;
        end
    end
end

function out = extractSynComments(skel, curTree, tracingType, plotDendrogram, outDir, pL, cellbodyType, thisSampleDistToSoma)
    c = {skel.nodesAsStruct{curTree}.comment};
    c(cellfun(@isempty, c)) = []; 
    out = table;
    out.name = skel.names(curTree);
    out.sph = sum(contains(c, {'sph','spineSingleInnervated'}));
    out.prim  =  sum(contains(c, {'prim'}));
    out.second =  sum(contains(c, {'second','sec'}));
    out.shaft = sum(contains(c, 'sh'));
    out.neck =  sum(contains(c, 'neck'));
    out.stub =  sum(contains(c, 'stub'));
    out.ter =  sum(contains(c, 'ter'));
    out.total = sum([out.sph,out.shaft, out.prim, out.second, out.neck, out.stub, out.ter]);
 
    out.excFraction = out.sph ./ out.total;
    switch tracingType
         case 'axon'
            [~,idxDel] = Util.getNodeCoordsWithComment(skel, {'post'},'partial', curTree);
            skel = skel.deleteNodes(curTree,idxDel,true);
            out.pL = skel.pathLength(curTree)./1e3; %um
 
         case 'dendrite'
            out.pL = pL;
     end
     out.synD = out.total./ out.pL;

     out.cellbodyAttached = 1;
     out.cellbodyType = cellbodyType;
     out.distToCellbody =  thisSampleDistToSoma;
     out.distToEods = NaN;

    if plotDendrogram

        synSize = 100; tubeSize = 1; dotSize= 3;
        synColors = {[1,0,0],... % red shaft
                     [0,1,0],... % green sph
                     [1,0,1],... % blue stub 
                     [230,159,0]./255,... % orange prim
                     [240,228,66]./255,... %Yellow second
                     [204,121,167]./255,... % reddish purple ter
                     [0,114,178]./255}; % bluish neck

        % make axonogram (PLASS-like)
        startpoint = skel.getNodesWithComment('_root',1,'regexp');
        endpoint  =  skel.getNodesWithComment('_end',1,'regexp');
    
        % attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
        comments = {skel.nodesAsStruct{1}.comment}';
        assert(startpoint == find(contains(comments,'_root')))
        
        tic;
        config = struct;
        config.scaleEM = skel.scale;
        Util.log('Now scatterting for %s',skel.names{1})
        [TREE, f] = Axonogram.axonogram(skel,'',startpoint,config.scaleEM, true);
        xlabel('Path length from root (um)')
        ylabel('')
        synComments = {'sh', 'sph', 'stub', 'prim', 'sec', 'ter', 'neck'};
        for i=1:numel(synComments)
            curSynLocs = find(cellfun(@(x) any(regexp(x,synComments{i})),comments));
            curSynColor = synColors{i};
            if ~isempty(curSynLocs)
                XY = Axonogram.findPointsInAxonogram(curSynLocs, TREE, skel, config.scaleEM);
                gcf, scatter(XY(:,1),XY(:,2),synSize, 'o','MarkerEdgeColor',curSynColor, 'MarkerFaceColor',curSynColor);
                clear XY
            end
        end

        % cosmetics
        box off
        camroll(90);
        set(gca,'linewidth',2, 'color', 'white');
        title( skel.names{curTree}, 'Interpreter', 'none')
        outfile = fullfile(outDir, ...
            sprintf('%s.png', skel.names{curTree}));
        export_fig(outfile,'-q101', '-nocrop', '-transparent');
        Util.log('Saving file %s', outfile);
        close(f);
        toc;
    end

end 

function synTable = extractSynTable(skel, curTree)

    % find cellbody type
    cellbodyIdx = skel.getNodesWithComment('cellbody',1,'partial');
    commentCellbody = skel.nodesAsStruct{1}(cellbodyIdx).comment;
    temp = regexp(commentCellbody,'cellbody_(?<type>\w+)','names');
    cellbodyType = {temp.type};
    
    allPaths = skel.getShortestPaths(1);

    % for complete dend, each syn and its distance from soma
    commentTypes = {'sph','prim','sec','sh','neck','stub','ter'};
    commentTypesSearchStr = {'(sph|spineSingle)', 'prim', 'sec', 'sh', 'neck', 'stub', 'ter'}; % regexpi
    
    % find max num of each comment
    count = [];
    for i = 1:numel(commentTypes)
        curComment = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment( curCommentSearchStr, curTree, 'regexpi');
        count = [count, numel(curNodesIdx)];
    end

    maxCount = max(count);

    synTable = cell2table(cell( maxCount, 1 + numel(commentTypes)));
    synTable.Properties.VariableNames = [ 'name', commentTypes];    

    synTable.name(1) = skel.names;
   
    for i = 1:numel(commentTypes)
        curComment = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment(curCommentSearchStr, curTree, 'regexpi');
        curNodesDist = allPaths(cellbodyIdx, curNodesIdx) ./ 1e3; % um
        curNodesDist = sort(curNodesDist, 'ascend');

        synTable.(curComment)(1:numel(curNodesDist)) = num2cell(curNodesDist);
    end
end
