clear 

info = Util.runInfo();
Util.showRunInfo(info);

outputDir = '/tmpscratch/sahilloo/primate_and_human/outData/';

rng(0)
fig = figure();
fig.Color = 'white';
ax = axes(fig);
hold on

regionNames = {'mk_L4','mk_L23', 'H5_10_L4', 'H5_10_L23'};
colors = Util.getSomaColors;
for i = 1:numel(regionNames)
    regionName = regionNames{i};
    curColor = colors{i}(1:3);
    Figures.setConfig
    tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling');
    outDir = fullfile(mainFolder, 'results_sub_sampling');

    d = dir(fullfile(outDir,'dendData-*.mat'));
    m = load(fullfile(outDir,d.name));
    T = m.T;
    x = T.excFraction;
    y = (T.sph + T.prim)./T.pL ; % spine density
    s = scatter(x, y, 'o','filled', 'MarkerEdgeColor', curColor, 'MarkerFaceColor', 'none');
    s.SizeData = 150;
end
Util.setPlotDefault(gca,true,'');
ax.YAxis.Limits = [0, 2]; % manual
ax.YAxis.TickValues = 0:0.2:2;
ax.XAxis.TickValues = 0:0.2:1;
ax.XAxis.Label.String = 'Spine syn fraction';
ax.YAxis.Label.String = '# spines per um';
ax.LineWidth = 2;
legend(regionNames, 'Interpreter','none', 'Location', 'bestoutside')
export_fig(fullfile(outputDir, 'figures', 'dendData-subsampling-all-projects-scatter-spineSynDensity-spineSynFrac.png'),'-q101', '-nocrop','-transparent', '-m8');
close all

%% shaft syn fraction vs shaft syn density
fig = figure();
fig.Color = 'white';
ax = axes(fig);
hold on
for i = 1:numel(regionNames)
    regionName = regionNames{i};
    curColor = colors{i}(1:3);

    Figures.setConfig

    tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling');
    outDir = fullfile(mainFolder, 'results_sub_sampling');

    d = dir(fullfile(outDir,'dendData-*.mat'));
    m = load(fullfile(outDir,d.name));
    T = m.T;
    x = (T.shaft+ T.neck + T.stub)./ T.total;
    y = (T.shaft+ T.neck + T.stub)./T.pL ; % shaft syn density
    s = scatter(x, y, 'o', 'MarkerEdgeColor', curColor, 'MarkerFaceColor', 'none');
    s.SizeData = 150;
end

Util.setPlotDefault(gca,true,'');
%ax.YAxis.Limits = [0, 1.2]; % manual
%ax.YAxis.TickValues = 0:0.2:1.2;
%ax.XAxis.TickValues = 0:0.2:1;
ax.XAxis.Label.String = 'Shaft syn fraction';
ax.YAxis.Label.String = '# shaft syns per um';
ax.LineWidth = 2;
legend(regionNames, 'Interpreter','none', 'Location', 'bestoutside')
export_fig(fullfile(outputDir, 'figures', 'dendData-subsampling-all-projects-scatter-shaftSynDensity-shaftSynFrac.png'),'-q101', '-nocrop','-transparent', '-m8');
close all

%% spine density vs path length
rng(0)
fig = figure();
fig.Color = 'white';
ax = axes(fig);
hold on
regionNames = {'mk_L4','mk_L23', 'H5_10_L4', 'H5_10_L23'};
colors = Util.getSomaColors;
for i = 1:numel(regionNames)
    regionName = regionNames{i};
    curColor = colors{i}(1:3);

    Figures.setConfig

    tracingFolder = fullfile(mainFolder,'dend_for_sub_sampling');
    outDir = fullfile(mainFolder, 'results_sub_sampling');

    d = dir(fullfile(outDir,'dendData-*.mat'));
    m = load(fullfile(outDir,d.name));
    T = m.T;
    x = T.pL;
    y = (T.sph + T.prim)./T.pL ; % spine density
    s = scatter(x, y, 'o','filled', 'MarkerEdgeColor', curColor, 'MarkerFaceColor', 'none');
    s.SizeData = 150;
end

Util.setPlotDefault(gca,'','');
ax.YAxis.Limits = [0, 2]; % manual
ax.YAxis.TickValues = 0:0.2:2;
%ax.XAxis.TickValues = 0:0.2:1;
ax.XAxis.Label.String = 'Path Length (um)';
ax.YAxis.Label.String = '# spines per um';
ax.LineWidth = 2;
legend(regionNames, 'Interpreter','none', 'Location', 'bestoutside')
export_fig(fullfile(outputDir, 'figures', 'dendData-subsampling-all-projects-scatter-spineSynDensity-PL.png'),'-q101', '-nocrop','-transparent', '-m8');
close all

