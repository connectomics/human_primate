% plot distance dependence spine density for all regions

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'ex144'; % for getting outputDir
Figures.setConfig
clear regionName

ex145 = doForThisRegion('ex145');
ex144 = doForThisRegion('ex144');
ACC_JO = doForThisRegion('ACC_JO');
PPC_AK = doForThisRegion('PPC_AK');
PPC2 = doForThisRegion('PPC2');
V2_L23 = doForThisRegion('V2_L23');
%lpta = doForThisRegion('lpta'); % too long distances

dataRegion = {ex145, ex144, ACC_JO, PPC_AK, PPC2, V2_L23};
dataRegionNames = {'ex145_S1_L4', 'ex144_S1_L23', 'ACC_JO_L23', 'PPC_AK_L23', 'PPC2_L23', 'V2_L23'};

%% do per region
fig = figure;
fig.Color = 'white';
for idxRegion = 1: numel(dataRegion)
    outData = dataRegion{idxRegion};
    ax = subplot(2,3,idxRegion)
    hold on
    binWidth = 10; % 10 um
    synTypesSearch = {'(sph|prim)'};
    dendTypeColors = {'k','m','b',[0.5,0.5,0.5],[0.25,0.25,0.25],[0,0.5,0]};
    lineStyles = {'-','-','--','-','--','--'};

    for i = 1:numel(outData)
        hold on
        curT = outData(i).synTable;
    
        % do for 'pyr' dendrites only
        if ~contains(curT.cellTypes{1}, 'pyr')
            continue;
        end
        % choose color for apical, basal, oblique
        if any(regexpi(curT.dendNames{1},'apical'))
            curColor = dendTypeColors{1};
        elseif any(regexpi(curT.dendNames{1},'basal'))
            curColor = dendTypeColors{2};
        elseif any(regexpi(curT.dendNames{1},'oblique'))
            curColor = dendTypeColors{3};
        elseif any(regexpi(curT.dendNames{1},'tuft'))
            curColor = dendTypeColors{4};
        else
            error('Dend type not defined!')
        end
        synDistToSoma = curT.synDistToSoma;
        synTypes = curT.synTypes;
        curAx = ax;
        for j = 1:numel(synTypesSearch)
            curStr = synTypesSearch{j};
            curLineStyle = lineStyles{j};
            curIdx = cellfun(@(x) any(regexp(x,curStr)),synTypes);
            x = synDistToSoma(curIdx);
            histogram(x,'DisplayStyle','stairs', ...
            'BinWidth',binWidth, 'Parent', curAx,...
            'EdgeColor', curColor, ...
            'LineWidth', 2, ...
            'LineStyle',curLineStyle)
            curAx.XAxis.MinorTick = 'on';
            curAx.XAxis.Limits = [0,200];
            curAx.XAxis.TickValues = 0:20:200;
            curAx.XAxis.MinorTickValues = 0:10:200;
            curAx.YAxis.Limits = [0,60];
            curAx.YAxis.TickValues = 0:10:60;
            curAx.YAxis.MinorTick = 'on';
            curAx.YAxis.MinorTickValues = 0:5:60;
            curAx.LineWidth = 1;
            Util.setPlotDefault(gca,'','');
        end
        %curLeg = legend( sprintf('%s',curT.dendNames{1}));
        %set(curLeg,'box','off','Location','best')
        ax.XAxis.Label.String = 'Distance from cellbody (\mum)';
        ax.YAxis.Label.String = 'Frequency';
        set(gca,'FontSize',6)
        title(ax, ...
                    {info.filename; info.git_repos{1}.hash; sprintf('%s',curT.dendNames{1})}, ...
            'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
    end
    title(ax, ...
                {info.filename; info.git_repos{1}.hash; sprintf('%s',dataRegionNames{idxRegion})}, ...
        'FontWeight', 'normal', 'FontSize', 6, 'Interpreter', 'none');
end

outfile = fullfile(outputDir,'figures','control_regions',sprintf('panel_spine_density_comparison_mouse_with_distance'));
export_fig(strcat(outfile,'.eps'), '-q101', '-nocrop', '-transparent', '-m8');
export_fig(strcat(outfile,'.png'), '-q101', '-nocrop', '-transparent', '-m8');
close all

function outData = doForThisRegion(regionName)
    Figures.setConfig
    outDir = fullfile(mainFolder, 'results');
    load(fullfile(outDir, sprintf('dendData-synTableUnique-%s.mat', regionName)))
end

