% plot per um syn densities with distance from soma
% clear

info = Util.runInfo();
Util.showRunInfo(info);

regionName = 'H2_3';

Figures.setConfig

outDir = fullfile(mainFolder, 'results');
load(fullfile(outDir, sprintf('dendData-synTableUnique-%s.mat', regionName)))

fig = figure;
fig.Color = 'white';
hold on
binWidth = 10; % 10 um
synTypesSearch = {'(sph|prim|sec|sh|stub|neck|ter)','(sph|prim)','(sec|ter)','(sh|stub)','neck','(sph|prim|sec|ter)'};
synTypesColors = {'k','m','b',[0.5,0.5,0.5],[0.25,0.25,0.25],[0,0.5,0]};
lineStyles = {'-','-','--','-','--','--'};
for i = 1:numel(outData)
    ax = subplot(3,2,i);
    hold on
    curT = outData(i).synTable; 
    synDistToSoma = curT.synDistToSoma;
    synTypes = curT.synTypes;
    curAx = ax;
    for j = 1:numel(synTypesSearch)
        curStr = synTypesSearch{j};
        curColor = synTypesColors{j};
        curLineStyle = lineStyles{j};
        curIdx = cellfun(@(x) any(regexp(x,curStr)),synTypes);
        x = synDistToSoma(curIdx);
        histogram(x,'DisplayStyle','stairs', ...
        'BinWidth',binWidth, 'Parent', curAx,...
        'EdgeColor', curColor, ...
        'LineWidth', 2, ...
        'LineStyle',curLineStyle)
        curAx.XAxis.MinorTick = 'on';
        curAx.XAxis.Limits = [0,140];
        curAx.XAxis.TickValues = 0:20:140;
        curAx.XAxis.MinorTickValues = 0:10:140;
        curAx.YAxis.Limits = [0,30];
        curAx.YAxis.MinorTick = 'on';
        curAx.YAxis.MinorTickValues = 0:1:30;
        curAx.LineWidth = 2;
        Util.setPlotDefault(gca,'','');
    end
    curLeg = legend(synTypesSearch);
    set(curLeg,'box','off','Location','best')
    ax.XAxis.Label.String = 'Distance from cellbody/eods (\mum)';
    ax.YAxis.Label.String = 'Frequency';
    title(ax, ...
        {info.filename; info.git_repos{1}.hash; sprintf('%s',curT.dendNames{1})}, ...
        'FontWeight', 'normal', 'FontSize', 4, 'Interpreter', 'none');
    set(gca,'FontSize',4)
end

export_fig(fullfile(mainFolder, 'results',sprintf('%s-dendDistanceSynPlot.eps',regionName)),'-q101', '-nocrop','-transparent', '-m8');
export_fig(fullfile(mainFolder, 'results',sprintf('%s-dendDistanceSynPlot.png',regionName)),'-q101', '-nocrop','-transparent', '-m8');
close all
