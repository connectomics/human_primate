% Export IN wholecell reconstructions nmls for amira
% 
Util.log('Doing for %s', regionName)
info = Util.runInfo();
Util.showRunInfo(info);

Figures.setConfig

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholecells','IN');
file = dir(fullfile(nmlDir,'*.nml'));
nmlFile = fullfile(nmlDir,file.name); % one tree per cell

% Dendrites isosurfaces
switch regionName
    case 'ex145'
        mappingName = '';
    case 'ex144'
        mappingName = '';
    case 'PPC_AK'
        mappingName = ''; % default
    case 'ACC_JO'
        mappingName = '';
    case 'V2_L23'
        mappingName = '';
    case 'mk_L4'
        mappingName = '';
    case 'mk_L23'
        mappingName = '';
    case 'H5_5_AMK'
        mappingName = 'agglomerate_view_75';
    otherwise
        error(sprintf('Unknown regionName %s',regionName))
end

doForThisRegion(regionName, info, mappingName, nmlFile);

function doForThisRegion(regionName, info, mappingName, nmlFile)
    setConfig

    if exist('mappingName','var') & ~isempty(mappingName)
        mappingDir = fileparts(param.files.aggloFile);
        af = AggloFile(fullfile(mappingDir, strcat(mappingName, '.hdf5')));
    else
        af = AggloFile(param.files.aggloFile);
    end

    timeStamp = datestr(clock,30);
    
    plyDir = fullfile(param.saveFolder,'amira','IN_gallery', sprintf('manual_%s',timeStamp));
    mkdir(plyDir)

    Util.log('Creating Iso file for IN gallery: %s', plyDir)
    Visualization.exportNmlToAmira(param, nmlFile, plyDir,'reduce',0.01,'smoothSizeHalf',4,'smoothWidth',8,'treeNames',true);

    % out struct
    out = struct;
    out.plyFiles = plyDir;
    out.skelFile = nmlFile;
    out.scale = config.scale;
    out.bbox = Util.convertMatlabToWKBbox(param.bbox);
    out.datasetName = config.datasetName;
    out.info = info;
    mkdir(fullfile(mainDir,'figures','fig5','amira','IN_gallery_isos'))
    outFile = fullfile(mainDir,'figures','fig5','amira','IN_gallery_isos', sprintf('%s_data_iso_files_manual_%s.mat', regionName, timeStamp));
    Util.saveStruct(outFile, out)
    Util.protect(outFile);
    sprintf('Data file at :\n %s', outFile)
end
