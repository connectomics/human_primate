% Written by
%  Sahil Loomba <sahil.loomba@brain.mpg.de>
%% panel A, B, C
% Bipolar vs Multipolar interneuron reconstructions.
% See Materials and methods for links to webKnossos containing data

%% panel D
clear; clc;
% BP fraction for INs with bootstrap
Figures.setConfig;

cx_path = fullfile('./data/excel_files');
cx_data = readtable(fullfile(cx_path,'INsubtypeClassification_v05_out.xlsx'));
cx_data.Properties.VariableNames = {'regionName','IN','Multipolar','Bipolar','Unclass','Unipolar'};

% combine H5 SBEM and mSEM data to one
newT = table;
newT.regionName = {'H5_pooled'};
idxH5 = contains(cx_data.regionName, {'H5_5_AMK','H5_10_L23'});
varNames = cx_data.Properties.VariableNames;
for i=2:numel(varNames)
   curVar = varNames{i};
   newT.(curVar) =  sum(cx_data(idxH5,:).(curVar));
end
cx_data = cat(1, cx_data, newT);

cx_data.BipolarFrac = cx_data.Bipolar ./ (cx_data.Bipolar + cx_data.Multipolar);

mouseColor = [0,0,0]; 
humanColor = [0,1,1];% cyan
mkColor = 'b';
homeColorEdge = 'k';

cx_speciesTick = [0.8, 7, 10]; % for species center
cx_speciesTickWithLayers = [0.70,2, 6.90, 7.10, 9.90, 10.10, 8.5]; % L4 and L23 around centers
cx_species = {'Mouse',...
              'Macaque','Human'};

allRegionNames = {'ex145', 'ex144', 'A1_JO', 'Mouse_A2', 'V2_L23','PPC_AK', 'ACC_JO',...
                    'mouse_L4_Hua',...
                    'rat_L4',...
                    'mk_L4','mk_L23','Mk1_T2_STG',...
                    'H5_10_L4', 'H5_10_L23','H5_5_AMK', 'H2_3','H6_4S_L23', 'H5_pooled'};
allRegionNamesText = {'S1 L4', 'S1 L23','A1 L34', 'A2', 'V2', 'PPC L23', 'ACC L23', ...
                    'Ms L4 Hua', ...
                    'Rat S1 L4',...
                    'MQ S1 L4', 'MQ S1 L23', 'MQ STG L23',...
                    'Hum ST L4', 'Hum ST L23','Hum ST L23', 'Hum MT L4', 'Hum FC','H5_pooled'};
allRegionNamesPos = [0.70, 0.80, 0.90, 1, 1.10, 1.20, 1.30,...
                    0.70, ...
                    1.9, ...
                    6.9, 7.1,7.3,...
                    9.9, 10.1, 10.3, 9.9, 10.5, 10.2];
allRegionNamesMarker = {'o','x','x','x','x','x','x',...
                    'o',...
                    'o', ...
                    'o','x','x',...
                    'o','x','x','o','x','x'};
allRegionNamesMarkerSize = [20,20,20,20,20,20,20,...
                    20, ...
                    20,...
                    20,20,20,...
                    20,20,20,20,20,20];

Util.log('Now plotting:')
fig = figure;
fig.Color = 'white';
ax = axes(fig);
hold on;

% ex144
doForThisRegion(cx_data, 'ex144', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% PPC
doForThisRegion(cx_data, 'PPC_AK', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% ACC
doForThisRegion(cx_data, 'ACC_JO', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% V2 
doForThisRegion(cx_data, 'V2_L23', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% Mouse_A2
doForThisRegion(cx_data, 'Mouse_A2', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% doForThisRegion(cx_data, 'mouse_L4_Hua', mouseColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% MQ
% doForThisRegion(cx_data, 'mk_L4', mkColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
doForThisRegion(cx_data, 'mk_L23', mkColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
doForThisRegion(cx_data, 'Mk1_T2_STG', mkColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

% Human
% doForThisRegion(cx_data, 'H5_10_L23', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
% doForThisRegion(cx_data, 'H5_5_AMK', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
% doForThisRegion(cx_data, 'H5_5_AMK', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
doForThisRegion(cx_data, 'H5_pooled', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

doForThisRegion(cx_data, 'H6_4S_L23', humanColor, allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)

idx = contains(cx_data.regionName, {'ex144','PPC_AK','ACC_JO','V2_L23','Mouse_A2'});
[mouseData, mouseB] = plotForSpecies('mouse', cx_data, idx, cx_speciesTickWithLayers(2), 'c', 'x', 20);

% idx = contains(cx_data.regionName,  {'mk_L23','Mk1_T2_STG', 'H5_5_AMK','H5_10_L23','H6_4S_L23'});
idx = contains(cx_data.regionName,  {'mk_L23','Mk1_T2_STG', 'H5_pooled','H6_4S_L23'});
[nhpHumanData, nhpHumanB] = plotForSpecies('nhpHuman', cx_data, idx, cx_speciesTickWithLayers(7), 'c', 'x', 20);

% test on bootstrap data
nhpHumanMean = mean(nhpHumanB);
mouseMean = mean(mouseB);
factor = nhpHumanMean/mouseMean;
pVal1 = sum(mouseB>=min(nhpHumanB))/numel(mouseB);
Util.log('<strong>Bootstrap on pooled data </strong> Mouse vs nhpHuman:\n  pVal = %e (test increase), factor: %.2f',pVal1,nhpHumanMean/mouseMean)

% cosmetics
ax.YAxis.Label.String = 'Fraction of Bipolar INs';
ax.YAxis.Limits = [0,80];
ax.YAxis.TickValues = 0:20:80;
ax.YAxis.MinorTick = 'on';
ax.YAxis.MinorTickValues = 0:10:80;
ax.XAxis.Limits = [0,12];
ax.XAxis.TickValues = cx_speciesTick;
ax.XAxis.TickLabels = cx_species;
ax.LineWidth = 2;
Util.setPlotDefault(gca,'','');
set(gca,'FontSize',18)
    
% %% save
% Util.log('Saving:')
% outfile = fullfile(mainDir, 'figures', 'fig5', sprintf('panel_IN_subtypes'));
% export_fig(strcat(outfile, '.eps'),'-q101', '-nocrop','-transparent')
% export_fig(strcat(outfile, '.fig'),'-q101', '-nocrop','-transparent')
% close all

%% panel E
% BP and MP IN axon examples with output synapses
% representative IN examples
clear; clc;
allRegionNames = {'H5_5_AMK', 'mk_L23'};
for idxRegion = 1:2
    regionName = allRegionNames{idxRegion};
    Figures.setConfig;

    % nml = fullfile('C:/Users/loombas/Downloads/mk-L23_INs_BP_MP_examples.nml');
    % outfile = 'C:/Users/loombas/Downloads/mk_L23_IN_output_synapses'

    %nml = fullfile('C:/Users/loombas/Downloads/H5_5_AMK_INs_BP_MP_examples.nml');
    %outfile = 'C:/Users/loombas/Downloads/H5_5_AMK_IN_output_synapses';

    nml = fullfile('./data/nml_files', sprintf('%s_INs_BP_MP_examples.nml',regionName));
    skel = skeleton(nml);

    outData = struct('name','','synLocs',[]);
    for i=1:skel.numTrees
        curSkel = skel.deleteTrees(i,true);
        outData(i).name = skel.names{i};
        outData(i).synLocsExc = Util.getNodeCoordsWithComment(curSkel,'(Exc)','regexpi');
        outData(i).scale = curSkel.scale;

        allNodes = curSkel.getNodes;
        % Exc
        idxKeep = false(size(allNodes,1),1);
        for j=1:numel(curSkel.nodesAsStruct{1})     
            curComment = curSkel.nodesAsStruct{1}(j).comment;
            idxKeep(j) = checkExc(curComment);
        end
        outData(i).synLocsExc = allNodes(idxKeep,:);

        % Inh
        idxKeep = false(size(allNodes,1),1);
        for j=1:numel(curSkel.nodesAsStruct{1})     
            curComment = curSkel.nodesAsStruct{1}(j).comment;
            idxKeep(j) = checkInh(curComment);
        end
        outData(i).synLocsInh = allNodes(idxKeep,:);

    end

    % plot
    for i=1:numel(outData)
        fontSize = 15;
        curName = outData(i).name;
        scale = outData(i).scale;
        somaCoordsExc = outData(i).synLocsExc;
        somaCoordsInh = outData(i).synLocsInh;

        f = figure();
        f.Color = 'white';
        hold on;

        somaLocs = skel.setScale(somaCoordsExc,scale);
        somaColor = [1,0,1]; % magenta
        % plot syn
        objectSomasE = ...
                    scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3)...
                    ,150,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                    'MarkerFaceColor', 'none');  
        text(somaLocs(:,1), somaLocs(:,2), repelem('e',size(somaLocs,1),1), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',somaColor,'FontSize',fontSize);

        somaLocs = skel.setScale(somaCoordsInh,scale);
        somaColor = [0,0,0]; % black
        % plot syn
        objectSomasI = ...
                    scatter3(somaLocs(:,1),somaLocs(:,2),somaLocs(:,3)...
                    ,150,'filled', 'MarkerEdgeColor', somaColor(1:3), ...
                    'MarkerFaceColor', 'none');  
        text(somaLocs(:,1), somaLocs(:,2), repelem('in',size(somaLocs,1),1), ...
        'horizontalalignment','center','verticalalignment','middle', 'Color',somaColor,'FontSize',fontSize);

        Util.setView(1, true); % xy
        daspect([1,1,1])
        Util.setPlotDefault();
        title(curName, 'Interpreter','none')

    %     set(gcf,'Position',get(0, 'Screensize')) 
    %     saveas(gcf, strcat(outfile,'_',curName,'.fig'))
    %     export_fig(strcat(outfile,'_',curName,'.eps'),'-q101', '-nocrop','-transparent')
    %     close all
    end
end

%% panel F
% Mouse vs Macaque/Human, BP vs MP IN-IN targeting
clear; clc;
Figures.setConfig
cx_path_axons = fullfile('data','excel_files','data_spine_targets_fraction_axons_per_axon-cellbody-attached-combined_v33_out.xlsx');
cx_data_axons = readtable(cx_path_axons);

nSynThr = 10;
Util.log('IN-IN target fraction: nSynThr>=%d:', nSynThr)
fig = figure;
fig.Color = 'white';
ax1 = subplot(1,5,1); hold on;
ax2 = subplot(1,5,2); hold on;
ax3 = subplot(1,5,3); hold on;
ax4 = subplot(1,5,4); hold on;
ax5 = subplot(1,5,5); hold on;

fig6 = figure;
ax6 = axes(fig6); hold on;

% mouse
[targetFractionMouseMP, numSynsINMouseMP, numSynsMouseMP, curColor, targetFractionMouseMPBulk, mouseBMP] = getTargetFraction(cx_data_axons, nSynThr, 'mouse', 'MP');
plot(ax1, 2 -0.2 + 0.4*rand(numel(targetFractionMouseMP),1), targetFractionMouseMP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'x', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');
plot(ax1, [3, 3], 100*prctile(mouseBMP, [10, 90]), 'Color', curColor, 'LineStyle','-', 'LineWidth',4);      % bootstrap
histogram(ax2, targetFractionMouseMP,'BinWidth',10, 'DisplayStyle','stairs','Normalization','probability' ,...
        'EdgeColor',curColor,'LineWidth',2,'Orientation','horizontal', 'LineStyle','-','BinLimits',[0,100]);

% scatter N-syns vs N-IN-syns
plot(ax6, numSynsMouseMP, numSynsINMouseMP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'x', 'MarkerSize',10,'LineWidth',1, 'LineStyle', 'none');

[targetFractionMouseBP, numSynsINMouseBP, numSynsMouseBP, curColor, targetFractionMouseBPBulk, mouseBBP] = getTargetFraction(cx_data_axons, nSynThr, 'mouse', 'BP');
plot(ax1, 2 -0.2 + 0.4*rand(numel(targetFractionMouseBP),1), targetFractionMouseBP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'x', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');
plot(ax1, [3, 3], 100*prctile(mouseBBP, [10, 90]), 'Color', curColor, 'LineStyle','-', 'LineWidth',4);      % bootstrap
histogram(ax2, targetFractionMouseBP,'BinWidth',10, 'DisplayStyle','stairs', 'Normalization','probability',...
        'EdgeColor',curColor,'LineWidth',2,'Orientation','horizontal', 'LineStyle','-','BinLimits',[0,100]);        
title(ax2, 'Mouse')

% scatter N-syns vs N-IN-syns
plot(ax6, numSynsMouseBP, numSynsINMouseBP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'x', 'MarkerSize',10,'LineWidth',1, 'LineStyle', 'none');
        
% nhp
[targetFractionNhpMP, numSynsINNhpMP, numSynsNhpMP, curColor, targetFractionNhpMPBulk] = getTargetFraction(cx_data_axons, nSynThr, 'nhp', 'MP');
plot(ax3, 1.5 -0.2 + 0.4*rand(numel(targetFractionNhpMP),1), targetFractionNhpMP,...
            'MarkerFaceColor', 'none', 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10,'LineWidth',1, 'LineStyle', 'none');
% scatter N-syns vs N-IN-syns
plot(ax6, numSynsNhpMP, numSynsINNhpMP,...
            'MarkerFaceColor', 'none', 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');    
        
[targetFractionNhpBP, numSynsINNhpBP, numSynsNhpBP, curColor, targetFractionNhpBPBulk] = getTargetFraction(cx_data_axons, nSynThr, 'nhp', 'BP');
plot(ax3, 1.5 -0.2 + 0.4*rand(numel(targetFractionNhpBP),1), targetFractionNhpBP,...
            'MarkerFaceColor', 'none', 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');
% scatter N-syns vs N-IN-syns
plot(ax6, numSynsNhpBP, numSynsINNhpBP,...
            'MarkerFaceColor', 'none', 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');        

% human        
[targetFractionHumanMP, numSynsINHumanMP, numSynsHumanMP, curColor, targetFractionHumanMPBulk] = getTargetFraction(cx_data_axons, nSynThr, 'human', 'MP');
plot(ax3, 2 -0.2 + 0.4*rand(numel(targetFractionHumanMP),1), targetFractionHumanMP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');
% scatter N-syns vs N-IN-syns
plot(ax6, numSynsHumanMP, numSynsINHumanMP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');    
        
[targetFractionHumanBP, numSynsINHumanBP, numSynsHumanBP, curColor, targetFractionHumanBulk] = getTargetFraction(cx_data_axons, nSynThr, 'human', 'BP');
plot(ax3, 2 -0.2 + 0.4*rand(numel(targetFractionHumanBP),1), targetFractionHumanBP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');
% scatter N-syns vs N-IN-syns
plot(ax6, numSynsHumanBP, numSynsINHumanBP,...
            'MarkerFaceColor', curColor, 'MarkerEdgeColor', curColor, ...
            'Marker', 'o', 'MarkerSize',10, 'LineWidth',1, 'LineStyle', 'none');  
        
% nhp human
[targetFractionNhpHumanMP, numSynsINNhpHumanMP, numSynsNhpHumanMP, curColor, targetFractionNhpHumanMPBulk, nhpHumanBMP] = getTargetFraction(cx_data_axons, nSynThr, 'nhpHuman', 'MP');
plot(ax3, [1, 1], 100*prctile(nhpHumanBMP, [10, 90]), 'Color', curColor, 'LineStyle','-', 'LineWidth',4);      % bootstrap
histogram(ax4, targetFractionNhpHumanMP,'BinWidth',10, 'DisplayStyle','stairs', 'Normalization','probability',...
        'EdgeColor',curColor,'LineWidth',2,'Orientation','horizontal', 'LineStyle','-','BinLimits',[0,100]);  

[targetFractionNhpHumanBP, numSynsINNhpHumanBP, numSynsNhpHumanBP, curColor, targetFractionNhpHumanBPBulk, nhpHumanBBP] = getTargetFraction(cx_data_axons, nSynThr, 'nhpHuman', 'BP');
plot(ax3, [1, 1], 100*prctile(nhpHumanBBP, [10, 90]), 'Color', curColor, 'LineStyle','-', 'LineWidth',4);      % bootstrap
histogram(ax4, targetFractionNhpHumanBP,'BinWidth',10, 'DisplayStyle','stairs', 'Normalization','probability',...
        'EdgeColor',curColor,'LineWidth',2,'Orientation','horizontal', 'LineStyle','-','BinLimits',[0,100]);      
title(ax4, 'Nhp human')

% cosmetics
ticklen = [0.025, 0.025];
ax1.XAxis.Limits = [1,3];
ax1.XAxis.TickValues = 2;
ax1.XAxis.TickLabels = {'Mouse'};
ax1.YAxis.Label.String = {'IN-to-IN synapses','% per interneuron'};
ax1.YAxis.Limits = [0,100];
ax1.YAxis.TickValues = 0:10:100;
ax1.YAxis.MinorTick = 'off';
ax1.Title.String = sprintf('numSynThr>=%d', nSynThr);
ax1.LineWidth = 2;
ax1.TickLength = ticklen;
Util.setPlotDefault(ax1);

ax2.XAxis.Limits = [0,0.6];
ax2.XAxis.TickValues = 0:0.2:1;
ax2.YAxis.Limits = [0,100];
ax2.YAxis.TickValues = 0:10:100;
ax2.YAxis.MinorTick = 'off';
ax2.LineWidth = 2;
ax2.TickLength = ticklen;
Util.setPlotDefault(ax2);
ax2.YAxis.Visible = 'off';

ax3.XAxis.Limits = [1,3];
ax3.XAxis.TickValues = 2;
ax3.XAxis.TickLabels = {'Macaque/Human'};
ax3.YAxis.Label.String = {'IN-to-IN synapses','% per interneuron'};
ax3.YAxis.Limits = [0,100];
ax3.YAxis.TickValues = 0:10:100;
ax3.YAxis.MinorTick = 'off';
ax3.Title.String = sprintf('numSynThr>=%d', nSynThr);
ax3.LineWidth = 2;
ax3.TickLength = ticklen;
Util.setPlotDefault(ax3);
ax3.YAxis.Visible = 'off';

ax4.XAxis.Limits = [0,0.6];
ax4.XAxis.TickValues = 0:0.2:1;
ax4.YAxis.Limits = [0,100];
ax4.YAxis.TickValues = 0:10:100;
ax4.YAxis.MinorTick = 'off';
ax4.LineWidth = 2;
ax4.TickLength = ticklen;
Util.setPlotDefault(ax4);
ax4.YAxis.Visible = 'off';

% ax5.XAxis.Limits = [0,300];
% ax5.XAxis.TickValues = 0:100:300;
% ax5.XAxis.MinorTick = 'on';
% ax5.XAxis.MinorTickValues = 0:50:300;
% ax5.XAxis.Limits = [0,300];
ax6.XAxis.Scale = 'log';
ax6.XAxis.Limits = [1,300];
ax6.XAxis.TickValues = [1,10,100,1000];
ax6.XAxis.TickLabels = [1,10,100,1000];
ax6.XAxis.Label.String = 'Number of output synapses (log)';

% ax5.YAxis.Limits = [0,100];
% ax5.YAxis.TickValues = 0:10:100;
% ax5.YAxis.MinorTick = 'off';
ax6.YAxis.Scale = 'log';
ax6.YAxis.Limits = [1,100];
ax6.YAxis.TickValues = [1,10,100,1000];
ax6.YAxis.TickLabels = [1,10,100,1000];
ax6.YAxis.Label.String = 'Number of IN synapses (log)';
curLeg = legend(ax6,{'Mouse MP', 'Mouse BP','Nhp MP', 'NHP BP', 'Human MP', 'Human BP'},'Location','best');
set(curLeg, 'Box','off')
ax6.LineWidth = 2;
ax6.TickLength = ticklen;
Util.setPlotDefault(ax6);

% statistical tests
% kstest for mouse vs nhpHuman BP
[~,pVal] = kstest2(targetFractionMouseBP, targetFractionNhpHumanBP);
Util.log('<strong>Mean +/- s.d. </strong> Mouse (%.2f +- %.2f, N=%d) vs  NhpHuman (%.2f +- %.2f, N=%d), pVal (ks) = %e',...
        mean(targetFractionMouseBP), std(targetFractionMouseBP), numel(targetFractionMouseBP),...
        mean(targetFractionNhpHumanBP), std(targetFractionNhpHumanBP), numel(targetFractionNhpHumanBP),...
        pVal)

% test on bootstrap data for MP
nhpHumanMean = mean(nhpHumanBMP);
mouseMean = mean(mouseBMP);
factor = nhpHumanMean/mouseMean;
pVal1 = sum(mouseBMP>=min(nhpHumanBMP))/numel(mouseBMP);
Util.log('<strong>Bootstrap for MP on pooled synapses </strong> Mouse vs nhpHuman:\n  pVal = %e (test increase), factor: %.2f (%.2f%% %d/%d, N=%d to %.2f%% %d/%d, N=%d axons)',... 
    pVal1, factor,...
    100*mouseMean, sum(numSynsINMouseMP), sum(numSynsMouseMP), numel(numSynsMouseMP),...
    100*nhpHumanMean, sum(numSynsINNhpHumanMP), sum(numSynsNhpHumanMP), numel(numSynsNhpHumanMP))
    
% test on bootstrap data for BP
nhpHumanMean = mean(nhpHumanBBP);
mouseMean = mean(mouseBBP);
factor = nhpHumanMean/mouseMean;
pVal1 = sum(mouseBBP>=min(nhpHumanBBP))/numel(mouseBBP);
Util.log('<strong>Bootstrap for BP on pooled synapses </strong> BP Mouse vs nhpHuman:\n  pVal = %e (test increase), factor: %.2f (%.2f%% %d/%d, N=%d to %.2f%% %d/%d, N=%d axons)',... 
    pVal1, factor,...
    100*mouseMean, sum(numSynsINMouseBP), sum(numSynsMouseBP), numel(numSynsMouseBP),...
    100*nhpHumanMean, sum(numSynsINNhpHumanBP), sum(numSynsNhpHumanBP), numel(numSynsNhpHumanBP))

histogram(ax5, 100*mouseBBP,'BinWidth',5, 'DisplayStyle','stairs', 'Normalization','probability',...
        'EdgeColor','c','LineWidth',2,'Orientation','horizontal', 'LineStyle','--','BinLimits',[0,100]);
histogram(ax5, 100*nhpHumanBBP,'BinWidth',5, 'DisplayStyle','stairs', 'Normalization','probability',...
        'EdgeColor','c','LineWidth',2,'Orientation','horizontal', 'LineStyle','-','BinLimits',[0,100]);    
legend(ax5,{'Mouse', 'nhpHuman'})

ax5.XAxis.Limits = [0,1];
ax5.XAxis.TickValues = 0:0.2:1;
ax5.YAxis.Limits = [0,100];
ax5.YAxis.Label.String = {'IN-IN synapses' ,'(% in bootstrapped sample)'};
ax5.YAxis.TickValues = 0:10:100;
ax5.YAxis.MinorTick = 'off';
ax5.XAxis.Label.String = {'Frequency','(norm.)'};
ax5.LineWidth = 2;
ax5.TickLength = ticklen;
Util.setPlotDefault(ax5);

%% panel H
% see HNHP.Auto.inToINConnectivity.m

%% panel I
% see Materials and methods for links to webKnossos containing data

%% panel J, K:
% see HNHP.Manual.compareDatasets.m

%% Utility functions panel D
function [curData, bootstrap] = plotForSpecies(curRegion, cx_data, idx, curPos, curColor, curMarker, curMarkerSize)
    curData.Multipolar = sum(cx_data(idx,:).Multipolar,1);
    curData.Bipolar = sum(cx_data(idx,:).Bipolar,1);
    [curMean, confidenceInterval, curN, curB, bootstrap] = boostrapForThis(curData);
    confidenceInterval = 100*confidenceInterval;
    curMean = 100*curMean;

    Util.log('%s: Mean (CI: 10th - 90th):  %.2f (%.2f - %.2f) (N=%d of %d)', curRegion, curMean, confidenceInterval(1), confidenceInterval(2), curB, curN)
    
    plot([curPos, curPos], confidenceInterval, 'Color', curColor, 'LineStyle','-', 'LineWidth',1);
    d = curMean;
    plot(curPos,d, curMarker,'MarkerSize',curMarkerSize,...
        'MarkerFaceColor','none', 'MarkerEdgeColor',curColor);
end

function doForThisRegion(cx_data, curRegion, curColor, ...
            allRegionNames,  allRegionNamesPos, allRegionNamesMarker, allRegionNamesMarkerSize)
    curIdx = contains(allRegionNames, curRegion);
    curPos = allRegionNamesPos(curIdx);
    curMarker = allRegionNamesMarker{curIdx};
    curMarkerSize = allRegionNamesMarkerSize(curIdx);
        
    idx = contains(cx_data.regionName, curRegion);

    [curMean, confidenceInterval, curN, curB] = boostrapForThis(cx_data(idx,:));
    confidenceInterval = 100*confidenceInterval;
    curMean = 100*curMean;

    Util.log('%s: Mean (CI: 10th - 90th):  %.2f (%.2f - %.2f) (N=%d of %d)', curRegion, curMean, confidenceInterval(1), confidenceInterval(2), curB,curN)

    plot([curPos, curPos], confidenceInterval, 'Color', curColor, 'LineStyle','-', 'LineWidth',1);
%   errorbar(curPos, curMean, curSEM, 'Color', curColor, 'LineStyle','-', 'LineWidth',1)
%   Util.log('%s: %.2f +- %.2f', curRegion, curMean, curSEM)
    
    %d = 100 * cx_data(idx,:).BipolarFrac;
    d = curMean;
    plot(curPos,d, curMarker,'MarkerSize',curMarkerSize,...
        'MarkerFaceColor','none', 'MarkerEdgeColor',curColor);
end

function [curMean, confidenceInterval, N, B, bootstrapped] = boostrapForThis(curData)
    rng(0)
    % MP:0, BP =1
    M = curData.Multipolar;
    B = curData.Bipolar;

    N= B+M;

    isBP = false(1, B + M);
    isBP(1:B) = true;

    bootstrapped = nan(1000, 1);
    for curIdx = 1:numel(bootstrapped)
      bootstrapped(curIdx) = mean(datasample(isBP, numel(isBP), 'Replace', true));
    end

    confidenceInterval = prctile(bootstrapped, [10, 90]);
    curMean = mean(bootstrapped);
%    curMean = mean(bp_fraction);
%    curSEM = Util.Stat.sem(bp_fraction); % std(bp_fraction) ./ sum(~isnan(bp_fraction));
end

%% Utility functions panel E
function keep = checkInh(x)
% onto IN
out.sphIN = any(regexpi(x,'sph')) & any(regexp(x,'(IN|Inh)')); % sph of IN
out.primIN  =  any(regexpi(x,'prim')) & any(regexp(x,'(IN|Inh)'));
out.secondIN =  any(regexpi(x,'sec'))  & any(regexp(x,'(IN|Inh)'));
out.shaftIN = any(regexpi(x,'sh'))  & any(regexp(x,'(IN|Inh)'));
out.neckIN = any(regexpi(x,'neck'))  & any(regexp(x,'(IN|Inh)'));
out.stubIN = any(regexpi(x,'(stub|butt)'))  & any(regexp(x,'(IN|Inh)'));
out.somaIN =  any(regexpi(x,'soma'))  & any(regexp(x,'(IN|Inh)'));
out.aisIN = any(regexpi(x,'ais'))  & any(regexp(x,'(IN|Inh)'));
out.terIN =any(regexpi(x,'ter'))  & any(regexp(x,'(IN|Inh)'));
out.flioSynIN = any(regexpi(x,'filoSyn'))  & any(regexpi(x,'(IN|Inh)'));
out.spine_stbbyIN = any(regexpi(x,'spine_stbby'))  & any(regexpi(x,'(IN|Inh)'));

keep = out.sphIN | out.primIN | out.secondIN | out.shaftIN | out.neckIN | ...
                out.stubIN | out.somaIN | out.aisIN | out.terIN | out.flioSynIN | out.spine_stbbyIN;

end

function keep = checkExc(x)
% onto IN
out.sphIN = any(regexpi(x,'sph')) & any(regexpi(x,'exc')); % sph of IN
out.primIN  =  any(regexpi(x,'prim')) & any(regexpi(x,'exc'));
out.secondIN =  any(regexpi(x,'sec'))  & any(regexpi(x,'exc'));
out.shaftIN = any(regexpi(x,'sh'))  & any(regexpi(x,'exc'));
out.neckIN = any(regexpi(x,'neck'))  & any(regexpi(x,'exc'));
out.stubIN = any(regexpi(x,'(stub|butt)'))  & any(regexpi(x,'exc'));
out.somaIN =  any(regexpi(x,'soma'))  & any(regexpi(x,'exc'));
out.aisIN = any(regexpi(x,'ais'))  & any(regexpi(x,'exc'));
out.terIN =any(regexpi(x,'ter'))  & any(regexpi(x,'exc'));
out.flioSynIN = any(regexpi(x,'filoSyn'))  & any(regexpi(x,'exc'));
out.spine_stbbyIN = any(regexpi(x,'spine_stbby'))  & any(regexpi(x,'exc'));

keep = out.sphIN | out.primIN | out.secondIN | out.shaftIN | out.neckIN | ...
                out.stubIN | out.somaIN | out.aisIN | out.terIN | out.flioSynIN | out.spine_stbbyIN;

end

%% Utility functions panel F
function [targetFraction, targetsIN, numSyns, curColor, bulkTargetFraction, curBootstrap] = getTargetFraction(cx_data_axons, nSynThr, curSpecies, curCellType)
    switch curSpecies
        case 'mouse'
            curRegions = {'Mouse_A2','PPC2'};
        case 'nhp'
            curRegions = 'mk_L23';
        case 'human'
            curRegions = {'H5_5_AMK','H5_10_ext'};
        case 'nhpHuman'
            curRegions = {'mk_L23','H5_5_AMK','H5_10_ext'};
    end
    switch curCellType
        case 'MP'
            curColor = [107 76 154]/255; % purple
        case 'BP'
            curColor = [0,1,1]; % cyan
    end
    curIdx = contains(cx_data_axons.Var1, curRegions);
    curAxonData = cx_data_axons(curIdx,:);
    curAxonData = curAxonData(curAxonData.total>=nSynThr,:);
    idxKeep = contains(curAxonData.name, curCellType);
    curAxonData = curAxonData(idxKeep,:);
    
    % IN-target fraction per cell
    targetsIN = sum([curAxonData.sphIN, curAxonData.primIN, curAxonData.secondIN, curAxonData.terIN, ...
                        curAxonData.stubIN, curAxonData.neckIN, curAxonData.shaftIN, curAxonData.somaIN, curAxonData.aisIN],2);
    targetsExc = sum([curAxonData.sphExc, curAxonData.primExc, curAxonData.secondExc, curAxonData.terExc,...
                        curAxonData.stubExc, curAxonData.neckExc, curAxonData.shaftExc, curAxonData.somaExc, curAxonData.aisExc],2);                    
    numSyns = sum([targetsIN, targetsExc],2);
    targetFraction = targetsIN ./ numSyns;
    targetFraction = 100*targetFraction;
    
    % bulk
    bulkTargetFraction = sum(targetsIN,1) ./ sum(numSyns,1);
    
    % bootstrap
    curData.Exc = sum(targetsExc,1); % all sum
    curData.IN = sum(targetsIN,1); % all sum
    [curMean, confidenceInterval, curN, curBootstrap] = boostrapForThisF(curData);
    confidenceInterval = 100*confidenceInterval;
    curMean = 100*curMean;
    Util.log('%s %s: %.2f (%.2f - %.2f, N=%d synapses)',curSpecies, curCellType, curMean, confidenceInterval(1),confidenceInterval(2), curN)
end

function [curMean, confidenceInterval, N, bootstrapped] = boostrapForThisF(curData)
    rng(0)
    % MP:0, BP =1
    M = curData.Exc;
    B = curData.IN;

    N= B+M;

    isBP = false(1, B + M);
    isBP(1:B) = true;

    bootstrapped = nan(1000, 1);
    for curIdx = 1:numel(bootstrapped)
      bootstrapped(curIdx) = mean(datasample(isBP, numel(isBP), 'Replace', true));
    end

    confidenceInterval = prctile(bootstrapped, [10, 90]);
    curMean = mean(bootstrapped);
end