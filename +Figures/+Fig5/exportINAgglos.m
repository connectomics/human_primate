% Export IN dendrite segmentations as iso
Util.log('Doing for %s', regionName)

info = Util.runInfo();
Util.showRunInfo(info);

Figures.setConfig

Util.log('Loading nml with dend tracings...')
nmlDir = fullfile(mainFolder, 'data','tracings', 'wholecells','IN');
file = dir(fullfile(nmlDir,'*.nml'));
nmlFile = fullfile(nmlDir,file.name);
skel = skeleton(nmlFile); % one tree per cell

% sort trees
[~,idxSort] = sort(skel.names);
skel = skel.reorderTrees(idxSort);

% Dendrites isosurfaces
switch regionName
    case 'ex145'
        mappingName = 'agglomerate_view_5';
    case 'ex144'
        mappingName = nan;
    case 'PPC_AK'
        mappingName = 'agglomerate_view_5'; % default
    case 'ACC_JO'
        mappingName = 'agglomerate_view_5';
    case 'V2_L23'
        mappingName = 'agglomerate_view_5';
    case 'A1_JO'
        mappingName = 'agglomerate_view_40';
    case 'mk_L4'
        mappingName = 'agglomerate_view_5';
    case 'mk_L23'
        mappingName = 'agglomerate_view_5';
    case 'Mk1_T2'
        mappingName = 'agglomerate_view_5';
    case 'H5_5_AMK'
        mappingName = 'agglomerate_view_5';
    case 'H6_4S_L23'
        mappingName = 'agglomerate_view_5';
    case 'Mouse_A2'
        mappingName = nan;
    otherwise
        error(sprintf('Unknown regionName %s',regionName))
end

doForThisRegion(regionName, skel, info, mappingName, nmlFile);

function doForThisRegion(regionName, skel, info, mappingName, nmlFile)
    setConfig
    
    if exist('mappingName','var') & ~isempty(mappingName) & not(isnan(mappingName)) 
        mappingDir = fileparts(param.files.aggloFile);
        af = AggloFile(fullfile(mappingDir, strcat(mappingName, '.hdf5')));
    else
        af = AggloFile(param.files.aggloFile);
    end
 
    timeStamp = datestr(clock,30);
    
    plyDir = fullfile(param.saveFolder,'amira','INAgglos', sprintf('auto_%s',timeStamp));
    mkdir(plyDir)

    % extract agglomerate IDs from skel, one agglo per tree
    [outAggloIds, outCellTypes] = extractAggloIdsFromSkel(skel, param, af, true);

    % extract segIDs comprising aggloIds per tree
    outAgglos = cell(numel(outAggloIds),1);
    for i=1:numel(outAggloIds)
        curAggloIds = outAggloIds{i}; % agglo ids per tree
        curAgglo = af.agglomerates(curAggloIds); % segIds per agglo as cell array
        outAgglos{i} = unique(vertcat(curAgglo{:})); % segIds merged across underlying agglomerates
    end
    Util.log('Found %d agglomerates for INs for %s', numel(outAgglos), regionName)

    % run jobs
    Util.log('Creating Iso file for IN cells: %s', plyDir)
    if isnan(mappingName)
        Visualization.exportNmlToAmira(param, nmlFile, plyDir, 'treeNames', true,'reduce',0.01,'smoothSizeHalf',4,'smoothWidth',8)
    else
        outFileNames = skel.names;
        outFileNames = cellfun(@(x) strrep(x,' ','_'),outFileNames, 'uni',0);
        outFileNames = cellfun(@(x) strrep(x,'(','_'),outFileNames, 'uni',0);
        outFileNames = cellfun(@(x) strrep(x,'_','_'),outFileNames, 'uni',0);

        Visualization.exportAggloToAmiraSL(param, outAgglos, plyDir,outFileNames,'reduce',0.01,'smoothSizeHalf',4,'smoothWidth',8);
    end

    % out struct
    out = struct;
    out.plyFiles = plyDir;
    out.scale = config.scale;
    out.bbox = Util.convertMatlabToWKBbox(param.bbox, false);
    out.datasetName = config.datasetName;
    out.cellTypes = outCellTypes;
    out.info = info;
    mkdir(fullfile(mainDir,'figures','fig1','amira','INAgglos'))
    outFile = fullfile(mainDir,'figures','fig1','amira','INAgglos', sprintf('%s_data_iso_files_%s.mat', regionName, timeStamp));
    Util.saveStruct(outFile, out)
    Util.protect(outFile);
    sprintf('Data file at :\n %s', outFile)
end

function [outAggloIds, outCellTypes] = extractAggloIdsFromSkel(skel, param, af, nucleiFlag)
    Util.log('Segment pickup and corresponding aggloID pickup')
    if nucleiFlag
        % all agglomerate nodes
        outAggloIds = cell(skel.numTrees,1);
        outCellTypes = cell(skel.numTrees,1);
        for i = 1:skel.numTrees
            nodeCoords = skel.getNodes(i);
            curSegIds = Seg.Global.getSegIds(param, nodeCoords);
            curAggloIds = af.segmentToAgglomerateIds(curSegIds);
            outAggloIds{i} = curAggloIds; % agglo ids for cur tree
            clear curSegIds curAggloIds

            if contains(skel.names{i}, '_BP')
                outCellTypes{i} = 'BP';
            elseif contains(skel.names{i}, '_MP')
                outCellTypes{i} = 'MP';
            else
                outCellTypes{i} = 'UC';
            end

        end
    else
        outAggloIds = cell(skel.numTrees,1);
        outCellTypes = cell(skel.numTrees,1);
        for i = 1:skel.numTrees
            curSkel = skel.deleteTrees(i, true);
            nodeIdxSoma = curSkel.getNodesWithComment('(soma|cellbody)',1,'regexp');
            nodeIdxOuter = setdiff(1:size(curSkel.nodes{1},1), nodeIdxSoma); % node in wholecell agglomerate
            nodeCoordOuter = curSkel.nodes{1}(nodeIdxOuter,1:3);
            segIdxOuter = Seg.Global.getSegIds(param, nodeCoordOuter);
            outAggloIds{i} = af.segmentToAgglomerateIds(segIdxOuter);

            if any(regexp(skel.names{i}, '(Pyr|PC|pyr|Exc|exc|exn|Exn|ExN)'))
                outCellTypes{i} = 'Pyr';
            elseif any(regexp(skel.names{i}, 'IN'))
                outCellTypes{i} = 'IN';
            elseif any(regexp(skel.names{i}, '(Astro|astro|glia|Glia|Oligo|oligo|)'))
                outCellTypes{i} = 'Glia';
            else
                outCellTypes{i} = 'UC';
            end

        end
    end
end
