% Written by
%  Sahil Loomba <sahil.loomba@brain.mpg.de>
%% panel B
clear; clc;

% soma based Pyr and IN data
rng(0)
Figures.setConfig;
cx_path_axons = fullfile('./data','excel_files','data_spine_targets_fraction_axons_per_axon-cellbody-attached-combined_v33_out.xlsx');
cx_data_axons = readtable(cx_path_axons);

mouseMarker = 'x';
nhpHumanMarker = 'x';
nSynThr = 10;

% plot
fig = figure; hold on;
ax = gca;
% mouse pyr
dorForThisSpecies('mouse', 'pyr', cx_data_axons, nSynThr)
% mouse IN
dorForThisSpecies('mouse', 'IN', cx_data_axons, nSynThr)
% human pyr
dorForThisSpecies('human', 'pyr', cx_data_axons, nSynThr)
% human IN
dorForThisSpecies('human', 'IN', cx_data_axons, nSynThr)

% % mq pyr
% dorForThisSpecies('nhp', 'pyr', cx_data_axons, nSynThr)
% % mq IN
% dorForThisSpecies('nhp', 'IN', cx_data_axons, nSynThr)

% cosmetics
ticklen = [0.025, 0.025];
ax.YAxis.Limits = [0,100];
ax.YAxis.TickValues = 0:10:100;
ax.YAxis.MinorTick = 'off';
ax.YAxis.Label.String = {'Fraction of output synapses onto','spines (per axon, %)'};

ax.XAxis.Limits = [0,6];
ax.Title.Interpreter = 'none';
Util.setPlotDefault(ax);
ax.TickLength = ticklen;
ax.LineWidth = 2;

% figure legends
nSynThr = 10;
cx_path_axons = fullfile('./data','excel_files','data_spine_targets_fraction_axons_per_axon-cellbody-attached-combined_v33_out.xlsx');
cx_data_axons = readtable(cx_path_axons);

% pyr and IN for figure legend
curIdx = contains(cx_data_axons.Var1, {'Mouse_A2','PPC_AK','PPC2','H5_10_ext','H5_5_AMK'}); % ,'mk_L23'
curAxonData = cx_data_axons(curIdx,:);
curAxonData = curAxonData(curAxonData.total>=nSynThr,:);
idxKeep = contains(curAxonData.name, {'pyr','IN'});
curAxonData = curAxonData(idxKeep,:);
Util.log('Pyr and IN axons: %d with %d synapses', height(curAxonData), sum(curAxonData.total))

%% panel E
% see HNHP.Auto

%% panel G
% see Materials and methods for links to reconstructions

%% panel H
% axonograms of human pyr axons
clear; clc;

allRegionNames = {'H5_10_ext', 'H5_5_AMK',}; % {'H5_10_ext', 'mk_L23', 'H5_5_AMK','PPC2', 'Mouse_A2'}
axonClasses = {'pyr'}; % {'pyr','IN','MP','BP'}
rootType = ''; % '' for cellbody / 'FirstSynapse' for first synapse

for idxRegion = 1:numel(allRegionNames)
    %clearvars -except i j allRegionNames info axonClasses
    regionName = allRegionNames{idxRegion};
    Util.log('Region: %s', regionName)
    
    for idxAxonClass=1:numel(axonClasses)
        axonClass = axonClasses(idxAxonClass);

        Figures.setConfig;
        plotAxonogram = true;
        
        clear outData synTable    
        Util.log('Doing for this region: %s', regionName)
        tracingFolder = fullfile('data','nml_files','axons',regionName);
        files = dir(fullfile(tracingFolder, '*.nml'));

        outDir = fullfile(mainFolder, 'results','axon','somaBased');
        if ~exist(outDir,'dir')
           mkdir(outDir);
        end

        Util.log('Reading cellbody attached axons')
        nml = fullfile(tracingFolder, files(1).name);
        skelAll = skeleton(nml);

        if strcmp(regionName, 'H5_5_AMK')
            plotAxonogram = false; % override for automated data
            idxDel = contains(skelAll.names, 'axon_x');
            skelAll = skelAll.deleteTrees(idxDel);
            clear idxDel
        end

        % dend statistics
        axonTreeId = find(contains(skelAll.names, 'axon') & contains(skelAll.names, axonClass));
        Util.log(sprintf('Found %d %s axons',numel(axonTreeId), axonClass{1}))

        % cosmetics for plots
        [tubeSize, commentTypes, synColors,synComments,synMarkers, synSizes ] = getCosmetics(axonClass);
        
        % get axon data
        rootComment = 'cellbody'; %''ais_start'; issue with ais_start
        outData.synTable = table;
        widthS = 250;
        if contains(axonClass, {'pyr'})
            binLimits = [0,1500];
        elseif contains(axonClass, {'IN', 'BP','MP'})
            binLimits = [0,500];
        end

        for i = 1:numel(axonTreeId)
            skelAxon = skelAll.deleteTrees(axonTreeId(i), true); % delete rest

            % delete post nodes comment
            idxDel = skelAxon.getNodesWithCommentAndDegree('^post',1,'regexp','',1);
            skelAxon = skelAxon.deleteNodes(1, idxDel);

            % Turn on syn table extraction to plot histograms
%             Util.log('Extracting syn table')
%             synTable = extractSynTable( skelAxon, 1, commentTypes, synComments, rootType);
%             outData(i).synTable = synTable;
            
            % axonogram
            if plotAxonogram
                % make axonogram (PLASS-like)
                startpoint = skelAxon.getNodesWithComment(rootComment,1,'regexp');

                % attach comments to nodes and presrve empty comments: one to one mapping of nodes to comments
                comments = {skelAxon.nodesAsStruct{1}.comment}';
                assert(startpoint == find(contains(comments,rootComment)))

                tic;
                config = struct;
                config.scaleEM = skelAxon.scale;
                Util.log('Now scatterting for %s',skelAxon.names{1})
                [TREE, f] = Axonogram.axonogram(skelAxon,'',startpoint,config.scaleEM, true);
                % cosmetics
                ax = gca;
                xlabel('Path length from root (um)')
                ylabel('')
                ax.XAxis.Limits = binLimits;
                ax.XAxis.TickValues = binLimits(1):100:binLimits(2);
                ax.XAxis.MinorTick = 'on';
                ax.XAxis.MinorTickValues = binLimits(1):50:binLimits(2);
                for curCommentIdx=1:numel(synComments)
                    curSynLocs = find(cellfun(@(x) any(regexpi(x,synComments{curCommentIdx})),comments)); % & ~cellfun(@(x) any(regexpi(x,'?')),comments)); % not proceeded by ?
                    curSynColor = synColors{curCommentIdx};
                    if ~isempty(curSynLocs)
                        XY = Axonogram.findPointsInAxonogram(curSynLocs, TREE, skelAxon, config.scaleEM);
                        gcf, scatter(XY(:,1),XY(:,2),synSizes(curCommentIdx), synMarkers{curCommentIdx},...
                            'MarkerEdgeColor',curSynColor, 'MarkerFaceColor',curSynColor,...
                            'LineWidth', 2);
                        clear XY
                    end
                end

                % cosmetics
                box off
                %camroll(90); % vertically up
                if contains(axonClass, {'pyr'})
                    daspect([1,4,1])
                else
                    daspect([1,1,1])
                end
                set(gca,'linewidth',1, 'color', 'white');
        %        set(gcf,'Position',get(0, 'Screensize')) % set fullscreen size
                title( skelAxon.names{1}, 'Interpreter', 'none');
                ax.Title.Visible = 'off';
                set(gca,'linewidth',1, 'color', 'none'); if not(i==numel(axonTreeId)); axis('off'); end
%                 outfile = fullfile(outDir, ...
%                     sprintf('%s.eps', skelAxon.names{1}));
%                 export_fig(outfile,'-q101', '-nocrop', '-transparent');
%                 Util.log('Saving file %s', outfile);
%                 close(f);
                toc;
            end

%             %% normalized histogram per target class: total exc vs total inh targets
%             binWidth = 50;
%             xbins = binLimits(1):binWidth:binLimits(2);
%             xbins = xbins(1:end-1);
%             normType =  'perTargetClass'; %'perTargetClass' ; %'total';
% 
%             fig = figure;
%             fig.Color = 'white';
%             hold on;
%             ax = gca;
%             x1 = vertcat(synTable.sph_Exc, synTable.sh_Exc, synTable.prim_Exc, synTable.sec_Exc, synTable.stub_Exc, synTable.neck_Exc);
%             x1 = cell2mat(x1);
%             x2 = vertcat(synTable.sph_Inh, synTable.sh_Inh, synTable.prim_Inh, synTable.sec_Inh, synTable.stub_Inh, synTable.neck_Inh);
%             x2 = cell2mat(x2);
%             % normalized per target class
%             switch normType
%                 case 'perTargetClass'
%                     histogram(x1, 'DisplayStyle','stairs', 'BinWidth', binWidth, 'EdgeColor', 'm', ...
%                         'Normalization', 'probability', 'LineWidth', 2);
%                     histogram(x2, 'DisplayStyle','stairs', 'BinWidth', binWidth, 'EdgeColor', 'k', ...
%                         'Normalization', 'probability', 'LineWidth', 2);
%                     ax.YAxis.Limits = [0,1];
%                     ax.YAxis.TickValues = 0:0.20:1;
%                 case 'total'
%                     % normalized by total synapses
%                     x1Bins = histcounts(x1, 'BinWidth',binWidth,'BinLimits',binLimits);
%                     x2Bins = histcounts(x2, 'BinWidth',binWidth,'BinLimits',binLimits);
%                     total = sum([x1Bins(:); x2Bins(:)]);
% 
%                     stairs(xbins, 100*x1Bins./total, 'LineWidth',2, 'Color','m','LineStyle','-');
%                     stairs(xbins, 100*x2Bins./total, 'LineWidth',2, 'Color','k','LineStyle','-');
%                     ax.YAxis.Limits = [0,100];
%                     ax.YAxis.TickValues = 0:20:100;
%             end
%             ax.XAxis.Limits = binLimits;
%             ax.XAxis.TickValues = xbins;
%             ax.XAxis.MinorTick = 'on';
%             ax.FontSize = 14;
%             ax.YAxis.Label.String = 'Fraction (%)';
%             ax.XAxis.Label.String = sprintf('Distance from soma (um) (%s %s) %s', skelAxon.names{1}, axonClass{1}, rootType);
%             ax.XAxis.Label.Interpreter = 'none';
%             Util.setPlotDefault(ax,'','');
%             set(gcf,'Position', get(0, 'Screensize'))
%             daspect([50,1,1]) % manual
%             % save
%             outfile = fullfile(outDir, ...
%                 sprintf('%s_target_fraction_with_distance_normalized_%s%s.png', skelAxon.names{1}, normType, rootType));
%             export_fig(outfile,'-q101', '-nocrop', '-transparent','-m8');
%             Util.log('Saving file %s', outfile)
%             close(fig);
        end

%         %% save synTables
%         outfile = fullfile(outDir, sprintf('%s_synTables_%s%s.mat',regionName, axonClass{1}, rootType));
%         Util.save(outfile, outData)
    end
end

%% panel I
% Path length dependent spine target Human pyr cells
clear; clc;
allRegionNames = {'H5_10_ext'}; % % {'H5_10_ext', 'mk_L23', 'PPC2', 'Mouse_A2'}
axonClasses = {'pyr'}; % {'BP','MP'}; % {'pyr','IN','MP','BP'}
rootType = ''; % FirstSynapse / empty

for idxRegion = 1:numel(allRegionNames)
    %clearvars -except i j allRegionNames info axonClasses
    
    regionName = allRegionNames{idxRegion};
    Util.log('Region: %s', regionName)
    
    for idxAxonClass=1:numel(axonClasses)
        axonClass = axonClasses(idxAxonClass);
        Util.log('Axon type: %s', axonClass{1})
        Figures.setConfig;
        Util.log('Doing for this region: %s', regionName)
        m = load(fullfile('data','mat_files',sprintf('%s_synTables_%s%s.mat',regionName,axonClass{1}, rootType)));
        outData = m.outData;
        
        if strcmp(regionName, 'H5_10_ext')
            regionName = 'H5_5_AMK';
            Figures.setConfig;
            Util.log('Doing for this region: %s', regionName)
            m = load(fullfile('data','mat_files',sprintf('%s_synTables_%s%s.mat',regionName,axonClass{1}, rootType)));
            outData = cat(2, outData, m.outData);
        
            % switch to H5_10_ext
            regionName = 'H5_10_ext'; % H5_10_ext, mk_L23
            Figures.setConfig;
        end
        
        % plot fraction of synapses onto target per distance bin
        synTable = cat(1,outData.synTable);
        if contains(axonClass, 'pyr')
            binLimits = [0,1500];
            binWidth = 250;
        elseif contains(axonClass, {'IN', 'BP','MP'})
            binLimits = [0,500];
            binWidth = 50;
        end
        
        fig = figure;
        fig.Color = 'white';
        hold on;
        ax = gca;
        [sphFraction, inhShaftFraction, excShaftFraction,  excOtherFraction, inhOtherFraction, xbins] = measureTargetFractionPerBin(synTable, binWidth, binLimits);
        sphFraction = 100*sphFraction; % fraction to percentage
        inhShaftFraction = 100*inhShaftFraction;
        excShaftFraction = 100*excShaftFraction;
        if contains(axonClass, {'IN', 'BP','MP'})
            excOtherFraction = 100*excOtherFraction;
            inhOtherFraction = 100*inhOtherFraction;
        end
        stairs(xbins, sphFraction, 'LineWidth',2, 'Color','m');
        stairs(xbins, inhShaftFraction, 'LineWidth',2, 'Color','k');
        stairs(xbins, excShaftFraction, 'LineWidth',2, 'Color','b');
        if contains(axonClass, {'IN', 'BP','MP'})
            stairs(xbins, inhOtherFraction, 'LineWidth',2, 'Color','k','LineStyle','--');
            stairs(xbins, excOtherFraction, 'LineWidth',2, 'Color','b','LineStyle','--');
        end
        
        ax.XAxis.Limits = binLimits;
        ax.XAxis.TickValues = xbins;
        ax.XAxis.MinorTick = 'on';
        ax.YAxis.Limits = [0,100];
        ax.YAxis.TickValues = 0:10:100;
        ax.FontSize = 14;
        ax.YAxis.Label.String = 'Fraction (%)';
        ax.XAxis.Label.String = sprintf('Distance from soma (um) (%s %s) %s', regionName, axonClass{1}, rootType);
        Util.setPlotDefault(ax,'','');
                
        % normalized histogram per target class: total exc vs total inh targets
        binWidth = 50;
        xbins = binLimits(1):binWidth:binLimits(2);
        xbins = xbins(1:end-1);
        normType =  'perTargetClass'; %'perTargetClass' ; %'total';

        fig = figure;
        fig.Color = 'white';
        hold on;
        ax = gca;
        x1 = vertcat(synTable.sph_Exc, synTable.sh_Exc, synTable.prim_Exc, synTable.sec_Exc, synTable.stub_Exc, synTable.neck_Exc);
        x1 = cell2mat(x1);
        x2 = vertcat(synTable.sph_Inh, synTable.sh_Inh, synTable.prim_Inh, synTable.sec_Inh, synTable.stub_Inh, synTable.neck_Inh);
        x2 = cell2mat(x2);
        % normalized per target class
        switch normType
            case 'perTargetClass'
                histogram(x1, 'DisplayStyle','stairs', 'BinWidth', binWidth, 'EdgeColor', 'm', ...
                    'Normalization', 'probability', 'LineWidth', 2);
                histogram(x2, 'DisplayStyle','stairs', 'BinWidth', binWidth, 'EdgeColor', 'k', ...
                    'Normalization', 'probability', 'LineWidth', 2);
                ax.YAxis.Limits = [0,1];
                ax.YAxis.TickValues = 0:0.20:1;
            case 'total'
                % normalized by total synapses
                x1Bins = histcounts(x1, 'BinWidth',binWidth,'BinLimits',binLimits);
                x2Bins = histcounts(x2, 'BinWidth',binWidth,'BinLimits',binLimits);
                total = sum([x1Bins(:); x2Bins(:)]);

                stairs(xbins, 100*x1Bins./total, 'LineWidth',2, 'Color','m','LineStyle','-');
                stairs(xbins, 100*x2Bins./total, 'LineWidth',2, 'Color','k','LineStyle','-');
                ax.YAxis.Limits = [0,100];
                ax.YAxis.TickValues = 0:20:100;
        end
        ax.XAxis.Limits = binLimits;
        ax.XAxis.TickValues = xbins;
        ax.XAxis.MinorTick = 'on';
        ax.FontSize = 14;
        ax.YAxis.Label.String = 'Fraction (%)';
        ax.XAxis.Label.String = sprintf('Distance from soma (um) (%s %s)', regionName, axonClass{1});
        ax.XAxis.Label.Interpreter = 'none';
        Util.setPlotDefault(ax,'','');
    end
end

%% Utility functions panel H
function synTable = extractSynTable(skel, curTree, commentTypes, commentTypesSearchStr, rootType)
    
    % replace ais_start and ais_end
    skel = skel.replaceComments('ais_start','axon_initial_seg_start','partial','complete');
    skel = skel.replaceComments('ais_end','axon_initial_seg_end','partial','complete');

    % find cellbody type and start node
    cellbodyIdx = skel.getNodesWithComment('cellbody',1,'partial');
    commentCellbody = skel.nodesAsStruct{1}(cellbodyIdx).comment;
    temp = regexp(commentCellbody,'cellbody_(?<type>\w+)','names');
    cellbodyType = {temp.type};
    
    allPaths = skel.getShortestPaths(1);

    % check if start node is different from cellbody
    if strcmp(rootType, 'FirstSynapse')
        sprintf('Replace root node with first synapse!')
        tempSynNodes = skel.getNodesWithComment( 'sph|sh|sec|prim|ter|stub|neck|soma|ais', curTree, 'regexpi');
        [~, minIdx] = min(allPaths(cellbodyIdx, tempSynNodes)); % update start to first synapse away from cellbody
        nbrsList = skel.getNeighborList(curTree); % first nearest neighbour towards cellbody
        curNbrsIdx = nbrsList{tempSynNodes(minIdx)};
        curNbrsDistToSoma = allPaths(cellbodyIdx,curNbrsIdx);
        [~, minIdx] = min(curNbrsDistToSoma);
        cellbodyIdx = curNbrsIdx(minIdx);
        %disp(skel.nodes{1}(cellbodyIdx,1:3))
    end

    % find max num of each comment
    count = [];
    for i = 1:numel(commentTypes)
        curComment = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment( curCommentSearchStr, curTree, 'regexpi');
        count = [count, numel(curNodesIdx)];
    end

    maxCount = max(count);

    if maxCount<1
       synTable = table; % no syns found
       return
    end
    synTable = cell2table(cell( maxCount, 1 + numel(commentTypes)));
    synTable.Properties.VariableNames = [ 'name', commentTypes];

    synTable.name(1) = skel.names;

    for i = 1:numel(commentTypes)
        curComment = commentTypes{i};
        curCommentSearchStr = commentTypesSearchStr{i};
        curNodesIdx = skel.getNodesWithComment(curCommentSearchStr, curTree, 'regexpi');
        curNodesDist = allPaths(cellbodyIdx, curNodesIdx) ./ 1e3; % um
        curNodesDist = sort(curNodesDist, 'ascend');

        % remove infs in distances measured
        curNodesDist(isinf(curNodesDist)) = nan;
        synTable.(curComment)(1:numel(curNodesDist)) = num2cell(curNodesDist);
    end
end

function [tubeSize, commentTypes, synColors,synComments,synMarkers, synSizes ] = getCosmetics(axonClass)
        tubeSize = 1; 
        commentTypes = {'sh_Inh',...
                       'sh_Exc',...
                       'sph_Inh',...
                       'sph_Exc',...
                       'prim_Inh',...
                       'prim_Exc',...
                       'sec_Inh',...
                       'sec_Exc',...
                       'stub_Inh',...
                       'stub_Exc',...
                       'ter_Inh',...
                       'ter_Exc',...
                       'neck_Inh',...
                       'neck_Exc',...
                        };
        synColors = {[0,0,0],... % black
                     [0,0,1],... % blue
                     [0,0,0],... % black
                     [1,0,1],... % magenta
                     [0,0,0],...
                     [1,0,1],...
                     [0,0,0],...
                     [1,0,1],...
                     [0,0,0],... % black
                     [0,0,1],... % blue
                     [0,0,0],...
                     [1,0,1],...
                     [0,0,0],...
                     [0,0,1],... % blue
                     };
        synComments = {'(sh(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|sh(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'sh(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(sph(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|sph(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'sph(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(prim(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|prim(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'prim(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(sec(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|sec(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'sec(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(stub(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|stub(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'stub(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(ter(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|ter(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'ter(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                       '(neck(\s)?(.)?(\s)?(\?)?(\s)?\(Inh\)|neck(\s)?(.)?(\s)?(\?)?(\s)?\(IN\))',...
                       'neck(\s)?(.)?(\s)?(\?)?(\s)?\(Exc\)',...
                        };
        synMarkers = {'o','o',...
                      'o','o',...
                      'x','x',...
                      'x','x',...
                      'x','x',...
                      'x','x',...
                      'x','x',...
                       };
        if contains(axonClass, {'pyr'})
        synSizes = [2, ...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                    2,...
                       ];
        else
        synSizes = [20, ...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                    20,...
                       ];
        end
end

%% Utility functions panel I
function [sphFraction, inhShaftFraction, excShaftFraction, excOtherFraction, inhOtherFraction, xbins] = measureTargetFractionPerBin(synTable, binWidth, binLimits)
    % onto exc spine
    x1 = vertcat(synTable.sph_Exc);
    x1 = cell2mat(x1);
    x1Bins = histcounts(x1, 'BinWidth',binWidth,'BinLimits',binLimits);    

    x2 = vertcat(synTable.sph_Exc, synTable.sph_Inh, ... 
                synTable.sh_Exc, synTable.sh_Inh,...
                synTable.prim_Exc, synTable.prim_Inh, ...
                synTable.neck_Exc, synTable.neck_Inh, ...
                synTable.sec_Exc, synTable.sec_Inh, ...
                synTable.stub_Exc, synTable.stub_Inh);
    x2 = cell2mat(x2);
    x2Bins = histcounts(x2, 'BinWidth',binWidth,'BinLimits',binLimits);    
    sphFraction = x1Bins ./ x2Bins;

    % onto inh shaft
    x1 = vertcat(synTable.sh_Inh);
    x1 = cell2mat(x1);
    x1Bins = histcounts(x1, 'BinWidth',binWidth,'BinLimits',binLimits);
    
    x2 = vertcat(synTable.sph_Exc, synTable.sph_Inh, ... 
                synTable.sh_Exc, synTable.sh_Inh,...
                synTable.prim_Exc, synTable.prim_Inh, ...
                synTable.neck_Exc, synTable.neck_Inh, ...
                synTable.sec_Exc, synTable.sec_Inh, ...
                synTable.stub_Exc, synTable.stub_Inh);
    x2 = cell2mat(x2);
    x2Bins = histcounts(x2, 'BinWidth',binWidth,'BinLimits',binLimits);
    inhShaftFraction =  x1Bins ./ x2Bins;

    % onto exc shaft
    x1 = vertcat(synTable.sh_Exc);
    x1 = cell2mat(x1);
    x1Bins = histcounts(x1, 'BinWidth',binWidth,'BinLimits',binLimits);

    x2 = vertcat(synTable.sph_Exc, synTable.sph_Inh, ... 
                synTable.sh_Exc, synTable.sh_Inh,...
                synTable.prim_Exc, synTable.prim_Inh, ...
                synTable.neck_Exc, synTable.neck_Inh, ...
                synTable.sec_Exc, synTable.sec_Inh, ...
                synTable.stub_Exc, synTable.stub_Inh);
    x2 = cell2mat(x2);
    x2Bins = histcounts(x2, 'BinWidth',binWidth,'BinLimits',binLimits);
    excShaftFraction = x1Bins ./ x2Bins;

    % onto exc other
    x1 = vertcat(synTable.prim_Exc, synTable.sec_Exc, synTable.stub_Exc, synTable.neck_Exc);
    x1 = cell2mat(x1);
    x1Bins = histcounts(x1, 'BinWidth',binWidth,'BinLimits',binLimits);

    x2 = vertcat(synTable.sph_Exc, synTable.sph_Inh, ... 
                synTable.sh_Exc, synTable.sh_Inh,...
                synTable.prim_Exc, synTable.prim_Inh, ...
                synTable.neck_Exc, synTable.neck_Inh, ...
                synTable.sec_Exc, synTable.sec_Inh, ...
                synTable.stub_Exc, synTable.stub_Inh);
    x2 = cell2mat(x2);
    x2Bins = histcounts(x2, 'BinWidth',binWidth,'BinLimits',binLimits);
    excOtherFraction = x1Bins ./ x2Bins;
    
    % onto inh other
    x1 = vertcat(synTable.prim_Inh, synTable.sec_Inh, synTable.stub_Inh, synTable.neck_Inh);
    x1 = cell2mat(x1);
    x1Bins = histcounts(x1, 'BinWidth',binWidth,'BinLimits',binLimits);

    x2 = vertcat(synTable.sph_Exc, synTable.sph_Inh, ... 
                synTable.sh_Exc, synTable.sh_Inh,...
                synTable.prim_Exc, synTable.prim_Inh, ...
                synTable.neck_Exc, synTable.neck_Inh, ...
                synTable.sec_Exc, synTable.sec_Inh, ...
                synTable.stub_Exc, synTable.stub_Inh);
    x2 = cell2mat(x2);
    x2Bins = histcounts(x2, 'BinWidth',binWidth,'BinLimits',binLimits);
    inhOtherFraction = x1Bins ./ x2Bins;
    
    xbins = binLimits(1):binWidth:binLimits(2);
    xbins = xbins(1:end-1);
end

function [sphFraction, shaft_SP_fraction, sphFractionExcOnly, excFraction] = measureSphFraction(synTable, distThr)
    % remove name col
    synTable(:,1) = [];
        
    % remove synapses away from distThr
    varNames = synTable.Properties.VariableNames;
    for i=1:numel(varNames)
        curData = synTable.(varNames{i});
        curCol = nan(height(synTable),1);
        idxKeep = ~cellfun(@isempty,curData);
        curCol(idxKeep) = cell2mat(curData);
        idxDel = curCol<distThr;
        synTable.(varNames{i})(idxDel) = {[]};
    end
    
    % measure sph/sph+shaft fraction
    %sph = vertcat(synTable.sph_Exc, synTable.sph_Inh, synTable.prim_Exc, synTable.prim_Inh, synTable.sec_Exc, synTable.sec_Inh, synTable.ter_Inh, synTable.ter_Exc);
    sph = vertcat(synTable.sph_Exc, synTable.sph_Inh);
    sph = cell2mat(sph);
    sphCount = sum(not(isnan(sph)));
    shaft = vertcat(synTable.sh_Exc, synTable.sh_Inh);
    shaft = cell2mat(shaft);
    shaftCount = sum(not(isnan(shaft)));
    sphFraction = sphCount/(sphCount+shaftCount);
    
    % measure shaft_SP / shaft_SP + shaft_SM
    shaft_SP = synTable.sh_Exc;
    shaft_SP = cell2mat(shaft_SP);
    shaftSPCount = sum(not(isnan(shaft_SP)));
    shaft_SM = synTable.sh_Inh;
    shaft_SM = cell2mat(shaft_SM);
    shaftSMCount = sum(not(isnan(shaft_SM)));
    shaft_SP_fraction = shaftSPCount/(shaftSPCount+shaftSMCount);

    % measure sph/sph+shaft (Exc only)
    sph = vertcat(synTable.sph_Exc);
    sph = cell2mat(sph);
    sphCount = sum(not(isnan(sph)));
    shaft = vertcat(synTable.sh_Exc);
    shaft = cell2mat(shaft);
    shaftCount = sum(not(isnan(shaft)));
    sphFractionExcOnly = sphCount/(sphCount+shaftCount);

    % measure Exc vs IN
    exc = vertcat(synTable.sph_Exc, synTable.prim_Exc, synTable.sec_Exc, synTable.ter_Exc, synTable.sh_Exc, synTable.neck_Exc);
    exc = cell2mat(exc);
    excCount = sum(not(isnan(exc)));
    inh = vertcat(synTable.sph_Inh, synTable.prim_Inh, synTable.sec_Inh, synTable.ter_Inh, synTable.sh_Inh, synTable.neck_Inh);
    inh = cell2mat(inh);
    inhCount = sum(not(isnan(inh)));
    excFraction = excCount/(excCount+inhCount);

end


%% Utility functions panel A
function dorForThisSpecies(speciesName, cellType, cx_data_axons,nSynThr)

    switch speciesName
        case 'mouse'
            curRegions = {'Mouse_A2','PPC_AK','PPC2'};
            marker = 'x';
            idxPos = 2;
        case 'human'
            curRegions = {'H5_10_ext','H5_5_AMK'};
            marker = 'x';
            idxPos = 4;
        case 'nhp'
            curRegions = {'mk_L23'};
            marker = 'x';
            idxPos = 6;
    end

    switch cellType
        case 'IN'
            color = 'k';
        case 'pyr'
            color = 'm';
    end
     
    curIdx = contains(cx_data_axons.Var1, curRegions);
    curAxonData = cx_data_axons(curIdx,:);
    curAxonData = curAxonData(curAxonData.total>=nSynThr,:);
    idxKeep = contains(curAxonData.name, cellType);
    curAxonData = curAxonData(idxKeep,:);
    d = curAxonData.sph ./ curAxonData.total;
    dBulk =  sum(curAxonData.sph)/sum(curAxonData.total);
    ci = 100*prctile(d, [25, 75]);
    Util.log('%s %s axons onto single spines: N=%d axons, %.2f +/- %.2f %% (bulk = %.2f%%,  N=%d/%d) (%.2f - %.2f, 25th to 75th perc)', ...
    speciesName, cellType, height(curAxonData), 100*mean(d), 100*std(d), 100*dBulk, sum(curAxonData.sph),sum(curAxonData.total), ci(1), ci(2))
    data1 = idxPos - 1 + 1*rand(numel(d),1);
    data2 = 100*d;
    plotInputDataScatter(data1, data2, marker, color)
end
function plotInputDataScatter(data1, data2, marker, color)
    plot(data1, data2,...
        'Color', color, ...
        'Marker', marker, ...
        'MarkerSize', 10, ...
        'LineWidth', 2, ...
        'LineStyle', 'none')
end