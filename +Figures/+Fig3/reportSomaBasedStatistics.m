% Figures.setConfig
% nSynThr = 10;
% cx_path_axons = fullfile(outputDir,  'figures','fig2','data','data_spine_targets_fraction_axons_per_axon-cellbody-attached-combined_v32.xlsx');
% cx_data_axons = readtable(cx_path_axons);


%% updated GT based data single spine synapses
Figures.setConfig;
nSynThr = 10;
cx_path_axons = fullfile(outputDir,  'figures','fig2','data','data_spine_targets_fraction_axons_per_axon-cellbody-attached-combined_v33.xlsx');
cx_data_axons = readtable(cx_path_axons);

%% pyr and IN for figure legend
curIdx = contains(cx_data_axons.Var1, {'Mouse_A2','PPC_AK','PPC2','H5_10_ext','H5_5_AMK'}); % ,'mk_L23'
curAxonData = cx_data_axons(curIdx,:);
curAxonData = curAxonData(curAxonData.total>=nSynThr,:);
idxKeep = contains(curAxonData.name, {'pyr','IN'});
curAxonData = curAxonData(idxKeep,:);
Util.log('Pyr and IN axons: %d with %d synapses', height(curAxonData), sum(curAxonData.total))
