% choose which project to process
outputDir = '/u/sahilloo/data/primate_and_human/outData/';
mainDir = '/u/sahilloo/data/screening/';
dataDir = '.\+HNHP\+Manual\+Data\';

if exist('regionName','var') && ~isempty(regionName)
Util.log('Doing for %s', regionName)

config = struct;
switch regionName
    case 'mk_L23'
        mainFolder = fullfile(dataDir);
        datasetName = 'Mk1_F6_L23_JS_SubI_v1';
        ownerName = 'SL';
        config.wholeCellsExcel = fullfile(mainFolder,'Mk_L23_celltype_classification_VG_ARK_v5.xlsx');
        config.datasetName = datasetName;
        config.scale = [11.24, 11.24, 30];
        config.bbox = [2978, 2991, 19, 15522, 20135, 3376];
        config.bbox_center = [3419, 3320, 166, 14569, 19329, 3066];
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-mk_L23_v16.xlsx');
        config.cx_path_dend = fullfile(mainFolder, 'dendData-mk_L23_v04.xlsx');
        config.distThr = 45; %um
    case 'H5_10_L23'
        mainFolder = fullfile(dataDir);
        datasetName = 'H5_10_270um_final_v1';
        ownerName = 'NH';
        config.wholeCellsExcel = fullfile(mainFolder,'H5_10_L23_celltype_classification_NH_v5.xlsx');
        config.datasetName = datasetName;
        config.scale = [4, 4, 37];
        config.bbox = [159251, 187530, 0, 50000, 50000, 766];
        config.bbox_center = [18432, 166169, 101, 488448, 213793, 529];
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-H5_10_L23_v17.xlsx');
        config.cx_path_dend = fullfile(mainFolder, 'dendData-H5_10_L23_v08.xlsx');
        config.distThr = 80; %um
    case 'ex144'
        mainFolder = fullfile(dataDir);
        datasetName = '2012-11-23_ex144_st08x2'; % from KMb/AG tracings
        ownerName = 'KMB';
        config.wholeCellsExcel =  fullfile(mainFolder,'ex144_cell_types_v04.xlsx');
        config.datasetName = datasetName;
        config.scale = [11.24, 11.24, 28];
        config.bbox = [310, 199, 391, 8315, 5500, 7771]; % from 2018-10-10_ex144_st08x2_mrnet
        config.bbox_center = [633, 2333, 238, 7240, 3241, 7392]; % from 2012-11-23_ex144_st08x2
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-ex144_v14.xlsx');
        config.cx_path_dend = fullfile(mainFolder, 'dendData-ex144_v06.xlsx');
        config.distThr = 30; %um
    case 'V2_L23'
        mainFolder = fullfile(dataDir);
        datasetName = 'V2_102_l23_ak_straightenedPosthoc';
        ownerName = 'AK';
        config.datasetName = datasetName;
        config.scale = [12, 12, 30];
        config.wholeCellsExcel =  fullfile(mainFolder,'V2_L23_cell_types.xlsx');
        config.bbox = [713, 1513, 20, 5684, 7284, 5070];
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-V2_L23_v03.xlsx');
        config.cx_path_dend = fullfile(mainFolder, 'dendData-V2_L23_v03.xlsx');
        config.distThr = 30; %um
    case 'ACC_JO'
        mainFolder = fullfile(dataDir);
        datasetName = 'JO_ACC_Z_straightenedPosthoc';
        ownerName = 'JO';
        config.datasetName = datasetName;
        config.scale = [12, 12, 30];
        config.wholeCellsExcel =  fullfile(mainFolder,'ACC_JO_cell_types.xlsx');
        %config.bbox = [2093, 1363, 21, 5654, 11194, 3211];
        config.bbox = [0, 0, 0, 9216, 14336, 3328];
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-ACC_JO_v05.xlsx');
        config.cx_path_dend = fullfile(mainFolder, 'dendData-ACC_JO_v04.xlsx');
        config.distThr = 30; %um
   case 'PPC_AK'
        mainFolder = fullfile(dataDir);
        datasetName = '2017-04-03_ppcAK99_76x79';
        ownerName = 'AK';
        config.datasetName = datasetName;
        config.scale = [12, 12, 30];
        config.wholeCellsExcel =  fullfile(mainFolder,'PPC_AK_cell_types.xlsx');
        config.bbox = [1125, 1300, 30, 5900, 7600, 4743];
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-PPC_AK_v05.xlsx');
        config.cx_path_dend = fullfile(mainFolder,'dendData-PPC_AK_v04.xlsx');
        config.distThr = 30; %um
   case 'PPC2'
        mainFolder = fullfile(dataDir);
        datasetName = 'AK_CorrelativeExperiment06469_P57_st003_st004';
        ownerName = 'AK';
        config.datasetName = datasetName;
        config.scale = [11.24, 11.24, 30];
        config.bbox = [385, 2, 31, 25000, 34379, 8000];
  case 'H5_5_AMK'
        mainFolder = fullfile(dataDir);
        datasetName = '2020-06-28-H5_5_AMK_2021-st700-align-v2';
        ownerName = 'ARK';
        config.datasetName = datasetName;
        config.scale = [11.24, 11.24, 30];
        config.wholeCellsExcel =  fullfile(mainFolder,'H5_5_AMK_cell_types_v04.xlsx');
        config.bbox = [713,793,21,14845,19260,3759]; % wk
    case 'H6_4S_L23'
        mainFolder = fullfile(dataDir);
        datasetName = 'Human_H6_4S_L23_VG_SL_v1';
        ownerName = 'VG';
        config.datasetName = datasetName;
        config.scale = [11.24, 11.24, 30];
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-H6_4S_L23_v11.xlsx'); % 'dendSeededAxonDataAll-H6_4S_L23_vDistalOnly.xlsx'
        config.cx_path_dend = fullfile(mainFolder, 'dendData-H6_4S_L23_v10Distal.xlsx');
        config.distThr = 80; %um
        config.bbox = [445, 624, 21, 15110, 19197, 2626];
    case 'Mk1_T2'
        mainFolder = fullfile(dataDir);
        datasetName = 'Mk1_T2_STG_VG_segmentation_v1_typ_soma_exclusion_v1';
        ownerName = 'VG';
        config.datasetName = datasetName;
        config.scale = [11.24, 11.24, 30];   
        config.wholeCellsExcel =  fullfile(mainFolder,'Mk1_T2_cell_types.xlsx');
        config.bbox = [533,433,21,14904,19404,3559];
        config.cx_path_axons = fullfile(mainFolder, 'dendSeededAxonDataAll-Mk1_T2_v05.xlsx');
        config.cx_path_dend = fullfile(mainFolder,'dendData-Mk1_T2_v04.xlsx');
        config.distThr = 45; %um
    case 'H5_10_ext'
        mainFolder = fullfile(dataDir);
        datasetName = ' H5_10_270um_L23_NH_NJ_extended_Aug22_2021_v2';
        ownerName = 'NJ';
        config.wholeCellsExcel = fullfile(mainFolder,'H5_10_ext_celltype_classification.xlsx');
        config.datasetName = datasetName;
        config.scale = [4, 4, 38];
        %config.bbox = [100000, 160169, 290, 175000, 125000, 1550];
        config.bbox = [32270, 106359, 0, 300615, 256968, 2120];
        config.bbox_center = [100000, 160169, 290, 175000, 125000, 1550];
    case 'Mouse_A2'
        mainFolder = fullfile(dataDir);
        datasetName = 'Mouse_A2_L23_AMK_rotated_segmentation_v1_typ_soma_excl_v1';
        ownerName = 'ARK';
        config.wholeCellsExcel = fullfile(mainFolder,'Mouse_A2_cell_types.xlsx');
        config.datasetName = datasetName;
        config.scale = [11.24, 11.24, 30];
        %config.bbox = [483,764,30, 9402,14669,3602];
        config.bbox = [120, 0, 30, 10276, 15999, 3602];
        config.cx_path_axons = fullfile(mainFolder,'dendSeededAxonDataAll-Mouse_A2_v07.xlsx');
        config.cx_path_dend = fullfile(mainFolder, 'dendData-Mouse_A2_v06.xlsx');
        config.distThr = 30; %um
end
end
clear dataDir
